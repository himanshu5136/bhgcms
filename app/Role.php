<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function permissions() {

        return $this->belongsToMany(Permission::class,'roles_permissions');
            
     }
     
     public function users() {
     
        return $this->belongsToMany(User::class,'users_roles');
            
     }

     // Helper Methods
     public static function getAllRoles(){
        $allRoles = Role::all('id','name')->toArray();
        $roles = [];
        foreach($allRoles as $k=>$v){
            $roles[$v['id']] = $v['name'];
        }
        return $roles;
     }

     // Helper Methods
     public static function getAllAdminRoles(){
      $allRoles = Role::all('id','name')->toArray();
      $roles = [];
      foreach($allRoles as $k=>$v){
         if($v['id'] != '3') { // Doctor Role
          $roles[$v['id']] = $v['name'];
         }
      }
      return $roles;
   }
}
