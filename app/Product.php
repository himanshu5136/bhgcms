<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'sku','picture','video','qr_text','qr_image','description'
    ];

    public function langs() {
        return $this->belongsToMany(Lang::class,'products_langs')->withPivot('prdct_name', 'prdct_qrcode', 'prdct_picture', 'prdct_video', 'prdct_qrimg', 'prdct_description');
    }

}
