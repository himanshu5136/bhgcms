<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Csection extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'title','cids'
    ];

    public function contents() {
        return $this->belongsToMany(Content::class,'contents_csections');
    }

    public function cpages() {
        return $this->belongsToMany(Cpage::class,'cpages_csections');
    }

    public function langs() {
        return $this->belongsToMany(Lang::class,'csections_langs')->withPivot('csec_title');
    }
}
