<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description',
    ];
    
    public function clients() {
        return $this->belongsToMany(Client::class,'tags_clients');
    }

    public function langs() {
        return $this->belongsToMany(Lang::class,'tags_langs')->withPivot('tag_name', 'tag_description');;
    }

    public function contents() {
        return $this->belongsToMany(Content::class,'contents_tags');
    }
}
