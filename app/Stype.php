<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stype extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'is_required',
    ];

    public function users() {
        return $this->belongsToMany(User::class,'users_stypes');
    }
}
