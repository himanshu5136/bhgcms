<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Superusers extends Model
{
    public static function getuserData($full_name ='', $status=''){
        $cond = [];
        if(!empty($full_name)){ $cond[] = ['full_name', 'like', '%'.$full_name.'%']; }
        if(!empty($status)){ $cond[] = ['status', '=', $status]; }
        $value=DB::table('users')->where($cond)->orderBy('id', 'asc')->get();
        return $value;
    }

    public static function getuserDetail($id) {
        $value=DB::table('users')->where('id',$id)->first();
        return $value;
    }
      
}
