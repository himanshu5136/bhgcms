<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description',
    ];

     public function users() {
             return $this->belongsToMany(User::class,'users_clients');
            
     }
     public function doctors() {
        return $this->belongsToMany(Doctor::class,'clients_doctors');
     }

     public function cpages() {
      return $this->belongsToMany(Cpage::class,'clients_cpages');
    }

    public function cpagesbyptype($ptyp) {
      return $this->belongsToMany(Cpage::class,'clients_cpages')->where('ptype',$ptyp);
    }

     public function doctorUsers() {
        $result = User::allDoctors()->toArray();
        $allDoctors = [];
        foreach($result as $k=>$v){
            $allDoctors[] = $v['id'];
        }       
        return $this->belongsToMany(User::class,'users_clients')->whereIn('user_id',$allDoctors);
     }
     
     public function tags() {
        return $this->belongsToMany(Tag::class,'tags_clients');
     }

     public function langs() {
      return $this->belongsToMany(Lang::class,'clients_langs');
      }

     // Helper Methods
     public static function getAllClients(){
        $allClients = Client::all('id','name')->toArray();
        $clients = [];
        foreach($allClients as $k=>$v){
            $clients[$v['id']] = $v['name'];
        }
        return $clients;
     }
}
