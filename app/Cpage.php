<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cpage extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','title','ptype','description'
    ];

    public function clients() {
        return $this->belongsToMany(Client::class,'clients_cpages');
    }

    public function csections() {
        return $this->belongsToMany(Csection::class,'cpages_csections');
    }

    public function langs() {
        return $this->belongsToMany(Lang::class,'cpages_langs')->withPivot('cpage_title', 'cpage_description');
    }
}
