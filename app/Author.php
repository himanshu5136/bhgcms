<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_name', 'title','picture','bio'
    ];

    public function langs() {
        return $this->belongsToMany(Lang::class,'authors_langs')->withPivot('athr_full_name', 'athr_title', 'athr_picture','athr_bio');
    }
}
