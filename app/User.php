<?php

namespace App;
use App\Client;
use App\Tag;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Permissions\HasPermissionsTrait;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','full_name','last_login_at','last_login_ip','status','user_brief','contact_number',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    use HasPermissionsTrait; //Import The Trait

    public function clients() {
        return $this->belongsToMany(Client::class,'users_clients');
    }

    public function doctors() {
        return $this->belongsToMany(Doctor::class,'users_doctors');
    }

    public function profile(){
        return $this->hasOne(Profile::class);
    }

    public function stypes() {
        return $this->belongsToMany(Stype::class,'users_stypes');
    }

    public function langs() {
        return $this->belongsToMany(Lang::class,'users_langs')->withPivot('doc_full_name', 'doc_title', 'doc_bg_image', 'doc_profile_video', 'doc_picture', 'doc_bio');
    }

    public static function allDoctors() {
        $condition = [];
        $condition[] = ['is_deleted','=','0'];
        $query = User::where($condition);
        $query = $query->whereHas('roles', function ($q) {
            return $q->where('id', '3'); // 3 is for Doctor User Role
        });
        $query = $query->join('profiles', 'users.id', '=', 'profiles.user_id')->select('users.*', 'profiles.*');
        return $query->get();
    }
    // Fetch all user's Full Name
    // Return Array
    public static function allProfilesByUid() {
        $condition = [];
        $condition[] = ['is_deleted','=','0'];
        $query = User::where($condition);
        $query = $query->join('profiles', 'users.id', '=', 'profiles.user_id')->select('users.id', 'profiles.full_name');
        $result = $query->get()->toArray();
        $profilesAry = [];
        foreach($result as $k=>$v) {
            $profilesAry[$v['id']] = $v['full_name'];
        }
        return $profilesAry;
    }

    // Method to get clients of Current User
    public static function getClients() {
        $role = auth()->user()->roles()->first()->toArray();
        if($role['slug'] == 'superadmin') {
            return Client::all()->toArray();
        } else if($role['slug'] == 'admin') {
            return auth()->user()->clients()->get()->toArray();
        }  else if($role['slug'] == 'doctor') {
            return auth()->user()->clients()->get()->toArray();
        } else {
            return [];
        }
    }
    // Method to get role of Current User
    public static function getProfile() {
        $profile = auth()->user()->profile()->first()->toArray();
        return $profile;
    }
    // Method to get role of Current User
    public static function getRole() {
        $role = auth()->user()->roles()->first()->toArray();
        return $role['slug'];
    }
    // Method to get tags of Current User
    public static function getTags() {
        $role = auth()->user()->roles()->first()->toArray();
        if($role['slug'] == 'superadmin') {
            return Tag::all()->toArray();
        } else if($role['slug'] == 'admin') {
            $userClientIds = [];
            $userClients = auth()->user()->clients()->get()->toArray();
            foreach($userClients as $k=>$v) {
                $userClientIds[] = $v['id'];
            }
            $cond = [];
            $tagQuery = Tag::where($cond)->whereHas('clients', function ($q) use ($userClientIds) {
                return $q->whereIn('id', $userClientIds ); // Check in assigned Clients of current user
            });
            return $tagQuery->get()->toArray();
            //return [];
        } else {
            return [];
        }
    }
}
