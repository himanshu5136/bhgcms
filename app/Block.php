<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Block extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'type', 'disabled'
    ];

    public function blockdata(){
        return $this->hasOne(Blockdata::class);
    }
    
    public function contents() {
        return $this->belongsToMany(Content::class,'blocks_contents')->withPivot('content_pages', 'display_location');
    }
}
