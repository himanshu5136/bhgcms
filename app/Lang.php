<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lang extends Model
{

    public function clients() {
        return $this->belongsToMany(Client::class,'clients_langs');
    }

    public function tags() {
        return $this->belongsToMany(Tag::class,'tags_langs');
    }

    public function csections() {
        return $this->belongsToMany(Csection::class,'csections_langs');
    }

    public function cpages() {
        return $this->belongsToMany(Cpage::class,'cpages_langs');
    }

    public function authors() {
        return $this->belongsToMany(Author::class,'authors_langs');
    }

    public function products() {
        return $this->belongsToMany(Product::class,'products_langs');
    }

    public function contents() {
        return $this->belongsToMany(Content::class,'contents_langs');
    }

    public function users() {
        return $this->belongsToMany(User::class,'users_langs');
    }

    //All Enabled Language with Sort Languages by English First
    public static function getEnabledLanguages() {
        $langs = Lang::where([['is_enable', '=', '1']])->orderByRaw('name ASC')->get()->toArray();
        $langsRes = [];
        foreach($langs as $key =>$val) {
            $langsRes[$val['id']] = $val;
        }
        $langData = []; 
        $langData[] = $langsRes['1']; // English Language on TOP
        unset($langsRes['1']);
        foreach($langsRes as $k =>$v) {
            $langData[] = $v;
        }

        return $langData;
    }
}
 