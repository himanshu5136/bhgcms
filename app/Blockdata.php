<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blockdata extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'block_id','ad_image', 'uploaded_video', 'embed_video','text_body','qrcode_text','qrcode_link','qrcode_content','qrcode_image',
    ];
}
