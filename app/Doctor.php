<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    public function users() {
        return $this->belongsToMany(User::class,'users_doctors');
     }

    public function clients() {
        return $this->belongsToMany(Client::class,'clients_doctors');
    }

     // Helper Methods
     public static function getAllDoctors(){
        $allDoctors = Doctor::all('id','name')->toArray();
        $doctors = [];
        foreach($allDoctors as $k=>$v){
            $doctors[$v['id']] = $v['name'];
        }
        return $doctors;
     }
}
