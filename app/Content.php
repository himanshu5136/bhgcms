<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','title', 'body','status','attachment','guid','is_this_zip','zip_files','bg_images','tn_images','author_id','product_id','ctype'
    ];


    public function tags() {
        return $this->belongsToMany(Tag::class,'contents_tags');
    }

    public function blocks() {
        return $this->belongsToMany(Block::class,'blocks_contents')->withPivot('content_pages', 'display_location');;
    }

    public function csections() {
        return $this->belongsToMany(Csection::class,'contents_csections');
    }

    public function author(){
        return $this->hasOne(Author::class);
    }

    public function product(){
        return $this->hasOne(Product::class);
    }

    public function langs() {
        return $this->belongsToMany(Lang::class,'contents_langs')->withPivot('cntnt_title', 'cntnt_bg_images', 'cntnt_tn_images', 'cntnt_body', 'cntnt_author_id','cntnt_attachment','cntnt_product_id');
    }

}
