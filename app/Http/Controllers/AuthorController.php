<?php

namespace App\Http\Controllers;

use App\Author;
use App\Variable;
use App\Lang;
use App\Content;  
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\FilesystemManager;

class AuthorController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $anm = request()->query('anm');
        $atl = request()->query('atl');
        $condition = [];
        if(!empty($anm)){ $condition[] = ['full_name', 'like', '%'.$anm.'%']; }
        if(!empty($atl)){ $condition[] = ['title', 'like', '%'.$atl.'%']; }
        $query = Author::where($condition);
        $authors = $query->orderBy('created_at', 'desc')->paginate(10);

        $authorData = [];
        foreach($authors as $author) {
            $tempData = [];
            $author_langs  = $author->langs()->get()->toArray();
            $tempData = $author->toArray();
            $tempData['langs'] = [];
            if(count($author_langs) > 0){
                foreach($author_langs as $k=>$v) {
                    $tempData['langs'][$v['id']] = $v['pivot'];
                    $tempData['langs'][$v['id']]['lang_name'] = $v['name'];
                }
            }
            $authorData[] = $tempData;
        }
        //echo "<pre>"; print_r($authorData); die;
        $qry_data = ['anm'=>$anm,'atl'=>$atl];
        return view('authors.index')
        ->with("paginate",$authors->appends($qry_data))
        ->with("authors",$authorData)
        ->with('page_title','List of Authors')
        ->with('qry',$qry_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $langs = Lang::getEnabledLanguages();
        return view('authors.create')->with("langs",$langs);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        // $dt = $request->all();
        //echo "<pre>"; print_r($dt); die;
        
        //- - - - - - - - NEED TO DELETE LATER - - - - - - - 
        $authrlangdata = []; 
        $ald = $request->input('langdata');
        foreach($ald as $key=>$val) {
            $authrlangdata[$key]['athr_full_name']  = (isset($val['authr_name']))?$val['authr_name']:'';
            $authrlangdata[$key]['athr_title']      = (isset($val['authr_title']))?$val['authr_title']:'';
            $authrlangdata[$key]['athr_bio']        = (isset($val['authr_bio']))?$val['authr_bio']:'';
            $pictureFile                            = $request->file('langdata_'.$key.'_authr_picture'); //(isset($val['authr_picture']))?$val['authr_picture']:''; //;
            $picture_url = '';
            if(!empty($pictureFile)) {
                $pictureFileName = $pictureFile->getClientOriginalName();
                $file_path = "author/".date('Ymdhis').'_'.$pictureFileName;
                $s3 = Storage::disk('s3');
                $s3->put($file_path, file_get_contents($pictureFile), array('ContentDisposition' => 'inline; filename=' . $pictureFileName . '', 'ACL' => 'public-read'));
                $picture_url = Storage::disk('s3')->url($file_path);
            }
            $authrlangdata[$key]['athr_picture']     = $picture_url;
        }
        //- - - - - - - - ENGLISH FIELDS - - - - - - - 
        $authr_name     = $authrlangdata['1']['athr_full_name'];
        $authr_title    = $authrlangdata['1']['athr_title'];
        $authr_bio      = $authrlangdata['1']['athr_bio'];
        $authr_picture  = $authrlangdata['1']['athr_picture'];
        //- - - - - - - - - - - - - - - - - - - - - - - - - 
        //- - - - - - - - - - - - - - - - - - - - - - - - - 

        // Validate Fields 
        // Commented due to multi lingual
        $validateAry = [
            // 'name' => ['required','max:255'],
            // 'title' => ['required'],
            // 'bio' => ['required']
        ];
        $request->validate($validateAry);
        // Check Picture Image uploaded or not
        $pictureFile = $request->file('picture');
        // Create Author
        $author =  Author::create([
            'full_name'     => $authr_name,
            'title'         => $authr_title,
            'picture'       => $authr_picture,
            'bio'           => $authr_bio
        ]);

        // Attach client with this languages
        $author->langs()->sync($authrlangdata); // Sync with name & description in different languages. 

        if($author) {
            return redirect()->to('/contents/authors')->with('success','Congrats! New Author has been added successfully!');
        } else {
            return redirect()->to('/contents/authors')->with('error','Sorry! Invalid Request.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $author                 = Author::where('id',$id)->first();
        $author_langs           = $author->langs()->get()->toArray();
        $authorData             = $author->toArray();
        $authorData['langs']    = $author_langs;
        return view('authors.show')->with("author",$authorData);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $author  = Author::where('id',$id)->first();
        $author_langs = $author->langs()->get()->toArray();
        $authorData = $author->toArray();
        $authorData['langs'] = [];
        if(count($author_langs) > 0){
            foreach($author_langs as $k=>$v) {
                $authorData['langs'][$v['id']] = $v['pivot'];
            }
        }
        $langs = Lang::getEnabledLanguages();
        return view('authors.edit')->with("author",$authorData)->with("langs",$langs);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        $author1                 = Author::where('id',$id)->first();
        $author_langs           = $author1->langs()->get()->toArray();
        $authorData             = $author1->toArray();
        $authorData['langs']    = $author_langs;

        //- - - - - - - - NEED TO DELETE LATER - - - - - - - 
        $authrlangdata = []; 
        $ald = $request->input('langdata');
        foreach($ald as $key=>$val) {
            $authrlangdata[$key]['athr_full_name']  = (isset($val['authr_name']))?$val['authr_name']:'';
            $authrlangdata[$key]['athr_title']      = (isset($val['authr_title']))?$val['authr_title']:'';
            $authrlangdata[$key]['athr_bio']        = (isset($val['authr_bio']))?$val['authr_bio']:'';
            $pictureFile                            = $request->file('langdata_'.$key.'_authr_picture');
            $removePicture  = $request->input('langdata_'.$key.'_remove_picture');
            $picture_url = (isset($authorData['langs'][$key]['athr_picture']))?$authorData['langs'][$key]['athr_picture']:'';
            if($removePicture == '1') {
                $picture_url = '';
            } else {
                if(!empty($pictureFile)) {
                    $pictureFileName = $pictureFile->getClientOriginalName();
                    $file_path = "author/".date('Ymdhis').'_'.$pictureFileName;
                    $s3 = Storage::disk('s3');
                    $s3->put($file_path, file_get_contents($pictureFile), array('ContentDisposition' => 'inline; filename=' . $pictureFileName . '', 'ACL' => 'public-read'));
                    $picture_url = Storage::disk('s3')->url($file_path);
                }
            }
            $authrlangdata[$key]['athr_picture']     = $picture_url;
        }
        //echo "<pre>"; print_r($authrlangdata); die;
        //- - - - - - - - ENGLISH FIELDS - - - - - - - 
        $authr_name     = $authrlangdata['1']['athr_full_name'];
        $authr_title    = $authrlangdata['1']['athr_title'];
        $authr_bio      = $authrlangdata['1']['athr_bio'];
        $authr_picture  = $authrlangdata['1']['athr_picture'];
        //- - - - - - - - - - - - - - - - - - - - - - - - - 

        // Validate Fields
        $request->validate([
            // 'name' => ['required','max:255'],
            // 'title' => ['required'],
            // 'bio' => ['required']
        ]);
           
        $author = Author::find($id);
        $author->full_name  = $authr_name;
        $author->title      = $authr_title;
        $author->picture    = $authr_picture;
        $author->bio        = $authr_bio;
        $author->save();
    
        // Attach client with this languages
        $author->langs()->sync($authrlangdata); // Sync with name & description in different languages. 

        return redirect('/contents/authors')->with('success', 'Author has been updated successfully!');
    }
 
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $author  = Author::find($id);
        $contentData = Content::where('author_id',$id)->get()->toArray();
        if(count($contentData) == 0 ) {
            $author->delete();
            return redirect('/contents/authors')->with('success', 'Author has been deleted successfully.');
        } else {
            return redirect('/contents/authors')->with('error', 'Sorry, You can\'t delete this author. This author is associated with content.' );
        }
    }
}