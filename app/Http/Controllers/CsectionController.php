<?php

namespace App\Http\Controllers;

use App\Content;
use App\Csection;
use App\Lang;
use App\Blockdata;  
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\FilesystemManager;

class CsectionController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $snm = request()->query('snm');
        $ttl = request()->query('ttl');
        $condition = [];
        if(!empty($snm)){ $condition[] = ['name', 'like', '%'.$snm.'%']; }
        if(!empty($ttl)){ $condition[] = ['title', 'like', '%'.$ttl.'%']; }
        $query = Csection::where($condition);
        $csections = $query->orderBy('created_at', 'desc')->paginate(10);
        $csectionData = [];
        foreach($csections as $csection) {
            $tempData = [];
            $tempData = $csection->toArray();
            // Fetch languages of sections
            $csec_langs  = $csection->langs()->get()->toArray();
            $tempData['langs'] = [];
            if(count($csec_langs) > 0){
                foreach($csec_langs as $k=>$v) {
                    $tempData['langs'][$v['id']] = $v['pivot'];
                    $tempData['langs'][$v['id']]['lang_name'] = $v['name'];
                }
            }
            $csectionData[] = $tempData;
        }
        //echo "<pre>"; print_r($csectionData); die;
        // Fetch All Contents
        $contents = Content::all()->toArray();
        foreach($contents as $k=>$v) {
            $contentsAry[$v['id']] = $v['title'];
        }
        $qry_data = ['snm'=>$snm];
        return view('csections.index')
        ->with("paginate",$csections->appends($qry_data))
        ->with("csections",$csectionData)
        ->with('contents',$contentsAry)
        ->with('page_title','List of Sections')
        ->with('qry',$qry_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $contents = Content::all()->toArray();
        $langs = Lang::getEnabledLanguages();
        return view('csections.create')->with('contents',$contents)->with("langs",$langs);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        //- - - - - - - - NEED TO DELETE LATER - - - - - - - 
        $csec_lang_data = $request->input('langdata');
        $csec_title    = $csec_lang_data['1']['csec_title'];
        //- - - - - - - - - - - - - - - - - - - - - - - - - 

        // // Validate Fields
        $validateAry = [
            'name' => ['required','max:255'],
            'content' => ['required'],
        ];
        $request->validate($validateAry);

        // Create Section
        $csection =  Csection::create([
            'name'  => $request->input('name'),
            'title'  => $csec_title, //$request->input('title'),
            'cids'  => json_encode($request->input('content'))
        ]);

        // Attach client with this languages
        $csec_lang = $request->input('langdata');
        $csection->langs()->sync($csec_lang); // Sync with name & description in different languages. 


        if($csection) {
            return redirect()->to('/cpages/csections')->with('success','Congrats! New Section has been added successfully!');
        } else {
            return redirect()->to('/cpages/csections')->with('error','Sorry! Invalid Request.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        // Fetch All Contents
        $contents = Content::all()->toArray();
        foreach($contents as $k=>$v) {
            $contentsAry[$v['id']] = $v['title'];
        }
        $csection  = Csection::where('id',$id)->first();
        $csectionData = $csection->toArray();
        // Fetch languages of sections
        $csec_langs  = $csection->langs()->get()->toArray();
        $csectionData['langs'] = [];
        if(count($csec_langs) > 0){
            foreach($csec_langs as $k=>$v) {
                $csectionData['langs'][$v['id']] = $v;
            }
        }

        return view('csections.show')->with("csection",$csectionData)->with('contents',$contentsAry);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $csection  = Csection::where('id',$id)->first();
        $csection_langs = $csection->langs()->get()->toArray();
        $csectionData = $csection->toArray();
        $csectionData['langs'] = [];
        if(count($csection_langs) > 0){
            foreach($csection_langs as $k=>$v) {
                $csectionData['langs'][$v['id']] = $v['pivot'];
                $csectionData['langs'][$v['id']]['id'] = $v['id'];
            }
        }
        $contents = Content::all()->toArray();
        $langs = Lang::getEnabledLanguages();
        return view('csections.edit')->with("csection",$csectionData)->with('contents',$contents)->with('langs',$langs);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

         //- - - - - - - - NEED TO DELETE LATER - - - - - - - 
         $csec_lang_data = $request->input('langdata');
         $csec_title    = $csec_lang_data['1']['csec_title'];
         //- - - - - - - - - - - - - - - - - - - - - - - - - 
 
         // // Validate Fields
         $validateAry = [
             'name' => ['required','max:255'],
             'content' => ['required'],
         ];

        $csection = Csection::find($id);
        $csection->name   = $request->get('name');
        $csection->title  = $csec_title; //$request->get('title')
        $csection->cids   = json_encode($request->get('content'));
        $csection->save();

        // Attach section with this languages
        $csec_lang = $request->input('langdata');
        $csection->langs()->sync($csec_lang); // Sync with name & description in different languages. 
    
        return redirect('/cpages/csections')->with('success', 'Section has been updated successfully!');
    }
 
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $csection  = Csection::find($id);
        $contents = $csection->contents()->get()->toArray();
        if(count($contents) == 0 ) {
            $csection->delete();
            return redirect('/cpages/csections')->with('success', 'Section has been deleted successfully.');
        } else {
            return redirect('/cpages/csections')->with('error', 'Sorry, You can\'t delete this section This section is associated with content.' );
        }
    }
}
