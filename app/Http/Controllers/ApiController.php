<?php

namespace App\Http\Controllers;

use App\Client;
use App\User;
use App\Tag;
use App\Content;
use App\Cpage;
use App\Block;
use App\Author;
use App\Langs;
use App\Csection;
use App\Product;
use App\Stype;
use App\Pcusers;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\DB;

class ApiController extends Controller
{
    /*  
    *  00 - GenerateOauthClient 
    */
    public function generateOauthClient() {
        $bodyContent = \Request::getContent();
        if(!empty($bodyContent)) {
            $content = json_decode($bodyContent);
            $content = (array)$content;
            if(!empty($content['name'])) {
                $oauthClientName = $content['name'];
                $oauthClientsecret = Str::random(40);
                $tableName       = 'oauth_clients';

                $oauthClientId = DB::table($tableName)->insertGetId([
                    'name'                  => $oauthClientName, 
                    'secret'                => $oauthClientsecret,
                    'redirect'              => '',
                    'personal_access_client'=> '0',
                    'password_client'       => '0',
                    'revoked'               => false,
                    'created_at'            => date('Y-m-d H:i:s'),
                    'updated_at'            => date('Y-m-d H:i:s')
                ]);
                $oauthClientDetail = DB::table($tableName)->where('id', $oauthClientId)->first();
                $res['status'] = 'success';
                $res['data']['client_id']       = $oauthClientDetail->id;
                $res['data']['client_secret']   = $oauthClientDetail->secret;
                return response()->json($res,200);
            } else {
                return response()->json(['status'=>'error','message'=>'Name cannot be empty.'],400);
            }            
        } else {
            return response()->json(['status'=>'error','message'=>'Invalid Parameters passed to request'],400);
        }
    }

    /*
    *  01 - GetAllClientList
    */
    public function getAllClientList() {
        $clients = Client::get();
        $clientData = [];
        foreach($clients as $client) {
            $tempData = [];
            $client_langs = $client->langs()->get(['name','code'])->toArray();
            $tempClient = $client->toArray();
            $tempData['id'] = $tempClient['id'];
            $tempData['name'] = $tempClient['name'];
            $tempData['description'] = $tempClient['description'];
            foreach($client_langs as $kl=>$vl){
                $tempData['langs'][] = ['name'=>$vl['name'], 'code'=>$vl['code']];
            }
            //$tempData['langs'] = $client_langs;
            $clientData[] = $tempData;
        }
        $res['status'] = 'success';
        $res['data']   = $clientData;
        return response()->json($res,200);      
    }
    
    /*
    *  02 - GetAllDoctorList
    */
    public function getAllDoctorList() {
        $docData = User::allDoctors();
        $docRes = [];
        foreach($docData as $k=>$v) {
            $docRes[$k]['id']                   = $v['id'];
            $docRes[$k]['full_name']            = $v['full_name'];
            $docRes[$k]['title']                = $v['title'];
            $docRes[$k]['bio']                  = $v['bio'];
            $docRes[$k]['picture']              = $v['picture'];
            $docRes[$k]['status']               = $v['status'];
            $docRes[$k]['contact_number']       = $v['contact_number'];
            $docRes[$k]['background_image']     = $v['bg_image'];
            $docRes[$k]['profile_video']        = $v['profile_video'];                 
            //Doctor Service Type
            $usr = User::where(['id' => $v['id']])->first();
            $userStype = $usr->stypes()->get()->toArray();
            if(!empty($userStype)){
                foreach($userStype as $key=>$val) {
                    $docRes[$k]['services'][]      = $val['name'];
                }
            }
            //Doctor Langs
            $userLangs = $usr->langs()->get()->toArray();
            if(!empty($userLangs)){
                foreach($userLangs as $key=>$val) {
                    $docRes[$k]['langs'][$val['code']]      = $val['pivot'];
                }
            }
        }
        $res['status'] = 'success';
        $res['data']   = $docRes;
        return response()->json($res,200);
    }

    /*
    *  03 - GetDoctorListByID
    */
    public function getDoctorListByClientId() {
        $bodyContent = \Request::getContent();
        if(!empty($bodyContent)) {
            $content = json_decode($bodyContent);
            $content = (array)$content;
            if(!empty($content['client_id'])) {
                $cid = $content['client_id'];
                // Check 01 : Is Valid Client Id
                $isValidClientId = ApiController::helper_isValidClientId($content['client_id']);
                if($isValidClientId == true) {
                    $conditions = [['status', '=', '1'], ['is_deleted', '=', '0'] ];
                    $query = User::where($conditions);
                    $query = $query->whereHas('roles', function ($q) {
                        return $q->where('id', '3'); // Role Id '3' for doctors
                    });
                    $query = $query->whereHas('clients', function ($q) use ($cid){
                        return $q->where('id', $cid); // Role Id '3' for doctors
                    });
                    $query = $query->join('profiles', 'users.id', '=', 'profiles.user_id')->select('users.*', 'profiles.*');
                    $docData = $query->get()->toArray();
                    $docRes = [];
                    if(count($docData) > 0) {
                        foreach($docData as $k=>$v) {
                            $docRes[$k]['id']               = $v['id'];
                            $docRes[$k]['full_name']        = $v['full_name'];
                            $docRes[$k]['title']            = $v['title'];
                            $docRes[$k]['bio']              = $v['bio'];
                            $docRes[$k]['picture']          = $v['picture'];
                            $docRes[$k]['status']           = $v['status'];
                            $docRes[$k]['contact_number']   = $v['contact_number'];
                            $docRes[$k]['background_image']     = $v['bg_image'];
                            $docRes[$k]['profile_video']        = $v['profile_video'];   
                            $usr = User::where(['id' => $v['id']])->first();
                            //Doctor Service Type
                            $userStype = $usr->stypes()->get()->toArray();
                            if(!empty($userStype)){
                                foreach($userStype as $key=>$val) {
                                    $docRes[$k]['services'][]      = $val['name'];
                                }
                            }
                            //Doctor Langs
                            $userLangs = $usr->langs()->get()->toArray();
                            if(!empty($userLangs)){
                                foreach($userLangs as $key=>$val) {
                                    $docRes[$k]['langs'][$val['code']]      = $val['pivot'];
                                }
                            }
                        }
                    }
                    $res['status'] = 'success';
                    $res['data']   = $docRes;
                    return response()->json($res,200);
                } else {
                    return response()->json(['status'=>'error','message'=>'Invalid Client Id'],400);
                }  
            } else {
                return response()->json(['status'=>'error','message'=>'Client Id cannot be empty.'],400);
            }            
        } else {
            return response()->json(['status'=>'error','message'=>'Invalid Parameters passed to request'],400);
        }
    }
    
    /*
    *  04 - ChangeDoctorStatus
    */
    public function changeDoctorStatus() {
        $bodyContent = \Request::getContent();
        if(!empty($bodyContent)) {
            $content = json_decode($bodyContent);
            $content = (array)$content;
            // Check 01 : Doctor Id & Status - Not Empty
            if((!empty($content['doc_id'])) && (!empty($content['doc_status']))) {
                // Check 02 : Doctor Status should be 1 or 2
                if(in_array($content['doc_status'], ['1','2'])) {
                    $doc_id = $content['doc_id'];
                    $doc_status = $content['doc_status'];
                    // Check 03 : Is Valid Doctor Id
                    $isValidDocId = ApiController::helper_isValidDoctorId($doc_id);
                    if($isValidDocId == true) {
                        User::whereId($doc_id)->update(['status' => $doc_status]);
                        $res['status']   = 'success';
                        $res['message']  = 'Doctor status has been changed sucessfully.';
                        return response()->json($res,200);
                    } else {
                        return response()->json(['status'=>'error','message'=>'Invalid Doctor Id'],400);
                    }  
                } else {
                    return response()->json(['status'=>'error','message'=>'Invalid doctor status parameter passed'],400);
                }  
            } else {
                return response()->json(['status'=>'error','message'=>'Doctor Id & Status cannot be empty.'],400);
            }            
        } else {
            return response()->json(['status'=>'error','message'=>'Invalid Parameters passed to request'],400);
        }
    }

    /*
    *  05 - GetDoctorDetailedProfiles
    */
    public function getDoctorDetailedProfile() {
        $bodyContent = \Request::getContent();
        if(!empty($bodyContent)) {
            $content = json_decode($bodyContent);
            $content = (array)$content;
            // Check 01 : Doctor Id - Not Empty
            if(!empty($content['doc_id'])) {
                $doc_id = $content['doc_id'];
                // Check 02 : Is Valid Doctor Id
                $isValidDocId = ApiController::helper_isValidDoctorId($doc_id);
                if($isValidDocId == true) {
                    $conditions = [['id', '=', $doc_id],['is_deleted', '=', '0'] ];
                    $query = User::where($conditions);
                    $query = $query->join('profiles', 'users.id', '=', 'profiles.user_id')->select('users.*', 'profiles.*');
                    $docData = $query->first()->toArray();
                    $docRes = [];
                    $docRes['id']               = $docData['id'];
                    $docRes['full_name']        = $docData['full_name'];
                    $docRes['title']            = $docData['title'];
                    $docRes['bio']              = $docData['bio'];
                    $docRes['picture']          = $docData['picture'];
                    $docRes['status']           = $docData['status'];
                    $docRes['contact_number']   = $docData['contact_number'];
                    $docRes['created_at']       = date('Y-m-d h:i:s', strtotime($docData['created_at']));
                    $docRes['background_image']     = $docData['bg_image'];
                    $docRes['profile_video']        = $docData['profile_video'];
                    $usr = User::where(['id' => $docData['id']])->first();
                    //Doctor Service Type
                    $userStype = $usr->stypes()->get()->toArray();
                    if(!empty($userStype)){
                        foreach($userStype as $key=>$val) {
                            $docRes['services'][]      = $val['name'];
                        }
                    }
                    //Doctor Langs
                    $userLangs = $usr->langs()->get()->toArray();
                    if(!empty($userLangs)){
                        foreach($userLangs as $key=>$val) {
                            $docRes['langs'][$val['code']]      = $val['pivot'];
                        }
                    }
                
                    $rateResponse = ApiController::helper_fetchDoctorRate($docData['doc_rate_id']);
                    $res = json_decode($rateResponse, true);
                    $res = (array)$res;          
                    if($res['success'] == true) {
                        $docRes['rate_data'] = $res['data'];
                    } else {
                        $docRes['rate_data']        = [];
                    }     
                    $res['status'] = 'success';
                    $res['data']   = $docRes;
                    return response()->json($res,200);
                } else {
                    return response()->json(['status'=>'error','message'=>'Invalid Doctor Id'],400);
                }  
            } else {
                return response()->json(['status'=>'error','message'=>'Doctor Id cannot be empty.'],400);
            }            
        } else {
            return response()->json(['status'=>'error','message'=>'Invalid Parameters passed to request'],400);
        }
    }
    
    /*
    *  06 - GetAllCategoryListByClientId
    */
    public function getAllCategoryListByClientId() {
        $bodyContent = \Request::getContent();
        if(!empty($bodyContent)) {
            $content = json_decode($bodyContent);
            $content = (array)$content;
            // Check 01 : Client Id - Not Empty
            if(!empty($content['client_id'])) {
                $cid = $content['client_id'];
                // Check 02 : Is Valid Client Id
                $isValidClientId = ApiController::helper_isValidClientId($content['client_id']);
                if($isValidClientId == true) {
                    $conditions = [];
                    $query = Tag::where($conditions);
                    $query = $query->whereHas('clients', function ($q) use ($cid) {
                        return $q->where('id', $cid);
                    });
                    $tagData = $query->get();
                    $tagRes = [];
                    //if(count($tagData) > 0) {
                        foreach($tagData as $tag) {
                            $tempTag = [];
                            $tag_langs = $tag->langs()->get()->toArray();
                            $tempTag         = $tag->toArray();
                            foreach($tag_langs as $k=>$v){
                                $tempTag['langs'][$v['code']] = $v['pivot'];
                            }
                            $tagRes[]        = $tempTag;
                        }
                    //}
                    $res['status'] = 'success';
                    $res['data']   = $tagRes;
                    return response()->json($res,200);
                } else {
                    return response()->json(['status'=>'error','message'=>'Invalid Client Id'],400);
                }  
            } else {
                return response()->json(['status'=>'error','message'=>'Client Id cannot be empty.'],400);
            }            
        } else {
            return response()->json(['status'=>'error','message'=>'Invalid Parameters passed to request'],400);
        }
    }
    
    /*
    *  07 - RetreiveContentByCategoryId
    */
    public function getContentByCategoryId() {
        $bodyContent = \Request::getContent();
        if(!empty($bodyContent)) {
            $content = json_decode($bodyContent);
            $content = (array)$content;
            // Check 01 : Category Id - Not Empty
            if(!empty($content['category_id'])) {
                $tagIdAry = $content['category_id'];
                // Check 02 : Is Valid Category Id
                $isValidTagId = ApiController::helper_isValidTagIdAry($tagIdAry);
                if($isValidTagId == true) {
                    $res = [];
                    $conditions = [];
                    $query = Content::where($conditions);
                    $query = $query->whereHas('tags', function ($q) use($tagIdAry) {
                        return $q->whereIn('id', $tagIdAry);
                    }); 
                    $contentData = $query->get();//->toArray();
                    //echo "<pre>"; print_r($contentData); die;
                    $resData = [];
                    $ctypeAry       = ['zip'=>'Zipped','image'=>'Image','video'=>'Video','product'=>'Product'];
                    $statusAry      = ['1'=>'Published','2'=>'Unpublished'];
                    
                    if(count($contentData) > 0) {
                        foreach($contentData as $content) {
                            $contentLangs   = $content->langs()->get()->toArray();
                            $contentAry     = $content->toArray();

                            $tempResData                = [];
                            $tempResData['id']          = $contentAry['id'];
                            $tempResData['title']       = $contentAry['title'];
                            $tempResData['body']        = $contentAry['body'];
                            $tempResData['status']      = $contentAry['status'];
                            $tempResData['attachment']  = $contentAry['attachment'];
                            $tempResData['created_at']  = $contentAry['created_at'];
                            $tempResData['type']        = (!empty($contentAry['ctype']))?$ctypeAry[$contentAry['ctype']]:'';
                            // Background Image
                                $bgImageData = [];
                                if(!empty($v['bg_images'])) {
                                    $bgImageData = unserialize($contentAry['bg_images']);
                                }
                                $tempResData['bg_images'] = $bgImageData;
                            // Thumbnail Image
                                $tnImageData = [];
                                if(!empty($v['tn_images'])) {
                                    $tnImageData = unserialize($contentAry['tn_images']);
                                }
                                $tempResData['tn_images'] = $tnImageData;
                            // Author - - - - - - -- 
                                $authorData = [];
                                if(!empty($contentAry['author_id'])) {
                                    $author  = Author::where('id',$contentAry['author_id'])->first();
                                    //$athrLangs = $author->langs()->get()->toArray();
                                    $authrAry = $author->toArray();
                                    $authorData['full_name']    = $authrAry['full_name'];
                                    $authorData['title']        = $authrAry['title'];
                                    $authorData['picture']      = $authrAry['picture'];
                                    $authorData['bio']          = $authrAry['bio'];
                                    // foreach($athrLangs as $alkey=>$alval){
                                    //     $authorData['langs'][$alval['code']]        = $alval['pivot'];
                                    // }
                                }
                                $tempResData['author']   = $authorData;
                            // Product - - - - - - -- 
                                $productData = [];
                                if(!empty($contentAry['product_id'])) {
                                    $product  = Product::where('id',$contentAry['product_id'])->first()->toArray();
                                    $productData['name']         = $product['name'];
                                    $productData['sku']          = $product['sku'];
                                    $productData['picture']      = $product['picture'];
                                    $productData['video']        = $product['video'];
                                    $productData['qr_text']      = $product['qr_text'];
                                    $productData['qr_image']     = $product['qr_image'];
                                    $productData['description']  = $product['description'];
                                }
                                $tempResData['product']   = $productData;
                                foreach($contentLangs as $klang=>$vlang) {
                                    $vlang['pivot']['cntnt_bg_images'] = unserialize($vlang['pivot']['cntnt_bg_images']);
                                    $vlang['pivot']['cntnt_tn_images'] = unserialize($vlang['pivot']['cntnt_tn_images']);
                                    $tempResData['langs'][$vlang['code']]   = $vlang['pivot'];
                                }
                                //$tempResData['langs']   = $contentLangs;
                                $resData[] = $tempResData;
                        }
                    }
                    $res['status'] = 'success';
                    $res['data']   = $resData;
                    return response()->json($res,200);
                } else {
                    return response()->json(['status'=>'error','message'=>'Invalid Category Id'],400);
                }  
            } else {
                return response()->json(['status'=>'error','message'=>'Category Id cannot be empty.'],400);
            }            
        } else {
            return response()->json(['status'=>'error','message'=>'Invalid Parameters passed to request'],400);
        }
    }

    /*
    *  08 - RetreiveContentByTypeAndClientId
    */
    public function getContentByTypeAndClientId() {
        $bodyContent = \Request::getContent();
        if(!empty($bodyContent)) {
            $content = json_decode($bodyContent);
            $content = (array)$content;
            // Check 01 : Client Id & Call Type - Not Empty
            if((!empty($content['client_id'])) && (!empty($content['call_type']))) {
                // Check 02 : Call Type is having valid value. 1-IDE,2-Group Discussion
                if(in_array($content['call_type'], ['1','2'])) {
                    $client_id = $content['client_id'];
                    $call_type = $content['call_type'];
                    // Check 03 : Is Valid Client Id
                    $isValidClientId = ApiController::helper_isValidClientId($client_id);
                    if($isValidClientId == true) {
                        // Fetch Tags on the basis of client_id & call_type
                        $clientTags = ApiController::helper_getTagsbyClientId($client_id, $call_type);

                        $res = [];
                        $conditions = [];
                        $query = Content::where($conditions);
                        $query = $query->whereHas('tags', function ($q) use($clientTags) {
                            return $q->whereIn('id', $clientTags);
                        });
                        //$contentData = $query->get()->toArray();
                        $contentData = $query->get();
                        $resData = [];
                        $ctypeAry       = ['zip'=>'Zipped','image'=>'Image','video'=>'Video','product'=>'Product'];
                        $statusAry      = ['1'=>'Published','2'=>'Unpublished'];
                        
                        if(count($contentData) > 0) {
                            foreach($contentData as $content) {
                                $contentLangs   = $content->langs()->get()->toArray();
                                $contentAry     = $content->toArray();

                                $tempResData                = [];
                                $tempResData['id']          = $contentAry['id'];
                                $tempResData['title']       = $contentAry['title'];
                                $tempResData['body']        = $contentAry['body'];
                                $tempResData['status']      = $contentAry['status'];
                                $tempResData['attachment']  = $contentAry['attachment'];
                                $tempResData['created_at']  = $contentAry['created_at'];
                                $tempResData['type']        = (!empty($contentAry['ctype']))?$ctypeAry[$contentAry['ctype']]:'';
                                // Background Image
                                    $bgImageData = [];
                                    if(!empty($v['bg_images'])) {
                                        $bgImageData = unserialize($contentAry['bg_images']);
                                    }
                                    $tempResData['bg_images'] = $bgImageData;
                                // Thumbnail Image
                                    $tnImageData = [];
                                    if(!empty($v['tn_images'])) {
                                        $tnImageData = unserialize($contentAry['tn_images']);
                                    }
                                    $tempResData['tn_images'] = $tnImageData;
                                // Author - - - - - - -- 
                                    $authorData = [];
                                    if(!empty($contentAry['author_id'])) {
                                        $author  = Author::where('id',$contentAry['author_id'])->first();
                                        //$athrLangs = $author->langs()->get()->toArray();
                                        $authrAry = $author->toArray();
                                        $authorData['full_name']    = $authrAry['full_name'];
                                        $authorData['title']        = $authrAry['title'];
                                        $authorData['picture']      = $authrAry['picture'];
                                        $authorData['bio']          = $authrAry['bio'];
                                        // foreach($athrLangs as $alkey=>$alval){
                                        //     $authorData['langs'][$alval['code']]        = $alval['pivot'];
                                        // }
                                    }
                                    $tempResData['author']   = $authorData;
                                // Product - - - - - - -- 
                                    $productData = [];
                                    if(!empty($contentAry['product_id'])) {
                                        $product  = Product::where('id',$contentAry['product_id'])->first()->toArray();
                                        $productData['name']         = $product['name'];
                                        $productData['sku']          = $product['sku'];
                                        $productData['picture']      = $product['picture'];
                                        $productData['video']        = $product['video'];
                                        $productData['qr_text']      = $product['qr_text'];
                                        $productData['qr_image']     = $product['qr_image'];
                                        $productData['description']  = $product['description'];
                                    }
                                    $tempResData['product']   = $productData;
                                    foreach($contentLangs as $klang=>$vlang) {
                                        $vlang['pivot']['cntnt_bg_images'] = unserialize($vlang['pivot']['cntnt_bg_images']);
                                        $vlang['pivot']['cntnt_tn_images'] = unserialize($vlang['pivot']['cntnt_tn_images']);
                                        $tempResData['langs'][$vlang['code']]   = $vlang['pivot'];
                                    }
                                    
                                    $resData[] = $tempResData;
                            }
                        }
                        $res['status'] = 'success';
                        $res['data']   = $resData;
                        return response()->json($res,200);
                    } else {
                        return response()->json(['status'=>'error','message'=>'Invalid Client Id'],400);
                    }  
                } else {
                    return response()->json(['status'=>'error','message'=>'Call type is having invalid value.'],400);
                } 
            } else {
                return response()->json(['status'=>'error','message'=>'Client Id & Call Type cannot be empty.'],400);
            }            
        } else {
            return response()->json(['status'=>'error','message'=>'Invalid Parameters passed to request'],400);
        }
    }

    /*
    *  09 - getContentListByCategoryId
    */
    public function getContentListByCategoryId() {
        $bodyContent = \Request::getContent();
        if(!empty($bodyContent)) {
            $content = json_decode($bodyContent);
            $content = (array)$content;
            // Check 01 : Category Id - Not Empty
            if(!empty($content['category_id'])) {
                $tag_id = $content['category_id'];
                // Check 02 : Is Valid Category Id
                $isValidTagId = ApiController::helper_isValidTagId($tag_id);
                if($isValidTagId == true) {
                    $res = [];
                    $conditions = [];
                    $query = Content::where($conditions);
                    $query = $query->whereHas('tags', function ($q) use($tag_id) {
                        return $q->where('id', $tag_id);
                    });
                    $contentData = $query->get();
                    //$contentData = $query->get()->toArray();
                    $resData = [];
                    $ctypeAry       = ['zip'=>'Zipped','image'=>'Image','video'=>'Video','product'=>'Product'];
                    $statusAry      = ['1'=>'Published','2'=>'Unpublished'];
                    
                    if(count($contentData) > 0) {
                        foreach($contentData as $content) {
                            $contentLangs   = $content->langs()->get()->toArray();
                            $contentAry     = $content->toArray();
                            $tempResData                = [];
                            $tempResData['id']          = $contentAry['id'];
                            $tempResData['title']       = $contentAry['title'];
                            $tempResData['body']        = $contentAry['body'];
                            $tempResData['status']      = $contentAry['status'];
                            $tempResData['attachment']  = $contentAry['attachment'];
                            $tempResData['created_at']  = $contentAry['created_at'];
                            $tempResData['type']        = (!empty($contentAry['ctype']))?$ctypeAry[$contentAry['ctype']]:'';
                            // Background Image
                                $bgImageData = [];
                                if(!empty($v['bg_images'])) {
                                    $bgImageData = unserialize($contentAry['bg_images']);
                                }
                                $tempResData['bg_images'] = $bgImageData;
                            // Thumbnail Image
                                $tnImageData = [];
                                if(!empty($v['tn_images'])) {
                                    $tnImageData = unserialize($contentAry['tn_images']);
                                }
                                $tempResData['tn_images'] = $tnImageData;
                            // Author - - - - - - -- 
                                $authorData = [];
                                if(!empty($contentAry['author_id'])) {
                                    $author  = Author::where('id',$contentAry['author_id'])->first();
                                    //$athrLangs = $author->langs()->get()->toArray();
                                    $authrAry = $author->toArray();
                                    $authorData['full_name']    = $authrAry['full_name'];
                                    $authorData['title']        = $authrAry['title'];
                                    $authorData['picture']      = $authrAry['picture'];
                                    $authorData['bio']          = $authrAry['bio'];
                                    // foreach($athrLangs as $alkey=>$alval){
                                    //     $authorData['langs'][$alval['code']]        = $alval['pivot'];
                                    // }
                                }
                                $tempResData['author']   = $authorData;
                            // Product - - - - - - -- 
                                $productData = [];
                                if(!empty($contentAry['product_id'])) {
                                    $product  = Product::where('id',$contentAry['product_id'])->first()->toArray();
                                    $productData['name']         = $product['name'];
                                    $productData['sku']          = $product['sku'];
                                    $productData['picture']      = $product['picture'];
                                    $productData['video']        = $product['video'];
                                    $productData['qr_text']      = $product['qr_text'];
                                    $productData['qr_image']     = $product['qr_image'];
                                    $productData['description']  = $product['description'];
                                }
                                $tempResData['product']   = $productData;
                                foreach($contentLangs as $klang=>$vlang) {
                                    $vlang['pivot']['cntnt_bg_images'] = unserialize($vlang['pivot']['cntnt_bg_images']);
                                    $vlang['pivot']['cntnt_tn_images'] = unserialize($vlang['pivot']['cntnt_tn_images']);
                                    $tempResData['langs'][$vlang['code']]   = $vlang['pivot'];
                                }
                                //$tempResData['langs']   = $contentLangs;
                                $resData[] = $tempResData;
                        }
                    }
                    $res['status'] = 'success';
                    $res['data']   = $resData;
                    return response()->json($res,200);
                } else {
                    return response()->json(['status'=>'error','message'=>'Invalid Category Id'],400);
                }  
            } else {
                return response()->json(['status'=>'error','message'=>'Category Id cannot be empty.'],400);
            }            
        } else {
            return response()->json(['status'=>'error','message'=>'Invalid Parameters passed to request'],400);
        }
    }

    /*
    *  10 - getContentById
    */
    public function getContentById() {
        $bodyContent = \Request::getContent();
        if(!empty($bodyContent)) {
            $content = json_decode($bodyContent);
            $content = (array)$content;
            // Check 01 : Content Id - Not Empty
            if(!empty($content['content_id'])) {
                $content_id = $content['content_id'];
                // Check 02 : Is Valid Content Id
                $isValidContentId = ApiController::helper_isValidContentId($content_id);
                if($isValidContentId == true) {
                    $res = [];
                    $conditions = []; 
                    $ctypeAry       = ['zip'=>'Zipped','image'=>'Image','video'=>'Video','product'=>'Product'];
                    $statusAry      = ['1'=>'Published','2'=>'Unpublished'];
                    //$contentData = Content::whereId($content_id)->first()->toArray();
                    $cntnt          = Content::whereId($content_id)->first();
                    $contentData    = $cntnt->toArray();
                    $contentAry     = [];
                    $contentAry['id']          = $contentData['id'];
                    $contentAry['title']       = $contentData['title'];
                    $contentAry['body']        = $contentData['body'];
                    $contentAry['status']      = $contentData['status'];
                    $contentAry['attachment']  = $contentData['attachment'];
                    $contentAry['created_at']  = $contentData['created_at'];
                    $contentAry['type']        = (!empty($contentData['ctype']))?$ctypeAry[$contentData['ctype']]:'';
                            // Background Image
                            $bgImageData = [];
                            if(!empty($contentData['bg_images'])) {
                                $bgImageData = unserialize($contentData['bg_images']);
                            }
                            $contentAry['bg_images'] = $bgImageData;
                        // Thumbnail Image
                            $tnImageData = [];
                            if(!empty($contentData['tn_images'])) {
                                $tnImageData = unserialize($contentData['tn_images']);
                            }
                            $contentAry['tn_images'] = $tnImageData;
                        // Author - - - - - - -- 
                            $authorData = [];
                            if(!empty($contentData['author_id'])) {
                                $author  = Author::where('id',$contentData['author_id'])->first()->toArray();
                                $authorData['full_name']    = $author['full_name'];
                                $authorData['title']        = $author['title'];
                                $authorData['picture']      = $author['picture'];
                                $authorData['bio']          = $author['bio'];
                            }
                            $contentAry['author']   = $authorData;
                        // Product - - - - - - -- 
                            $productData = [];
                            if(!empty($v['product_id'])) {
                                $product  = Product::where('id',$v['product_id'])->first()->toArray();
                                $productData['name']         = $product['name'];
                                $productData['sku']          = $product['sku'];
                                $productData['picture']      = $product['picture'];
                                $productData['video']        = $product['video'];
                                $productData['qr_text']      = $product['qr_text'];
                                $productData['qr_image']     = $product['qr_image'];
                                $productData['description']  = $product['description'];
                            }
                            $contentAry['product']   = $productData;
                            $content_langs = $cntnt->langs()->get()->toArray();
                            foreach($content_langs as $klang=>$vlang) {
                                $vlang['pivot']['cntnt_bg_images'] = unserialize($vlang['pivot']['cntnt_bg_images']);
                                $vlang['pivot']['cntnt_tn_images'] = unserialize($vlang['pivot']['cntnt_tn_images']);
                                $contentAry['langs'][$vlang['code']]   = $vlang['pivot'];
                            }
                    $resData = [];
                    $res['status'] = 'success';
                    $res['data']   = $contentAry;
                    return response()->json($res,200);
                } else {
                    return response()->json(['status'=>'error','message'=>'Invalid Content Id'],400);
                }  
            } else {
                return response()->json(['status'=>'error','message'=>'Content Id cannot be empty.'],400);
            }            
        } else {
            return response()->json(['status'=>'error','message'=>'Invalid Parameters passed to request'],400);
        }
    }
    
    /*
    *  11 - getContentByGuid
    */
    public function getContentByGuid() {
        $bodyContent = \Request::getContent();
        if(!empty($bodyContent)) {
            $content = json_decode($bodyContent);
            $content = (array)$content;
            // Check 01 : Content GUid - Not Empty
            if(!empty($content['guid'])) {
                $content_guid = $content['guid'];
                // Check 02 : Is Valid Content Id
                $isValidContentGuid = ApiController::helper_isValidContentGuid($content_guid);
                if($isValidContentGuid == true) {
                    $res = [];
                    $conditions = [];
                    $ctypeAry       = ['zip'=>'Zipped','image'=>'Image','video'=>'Video','product'=>'Product'];
                    $statusAry      = ['1'=>'Published','2'=>'Unpublished'];
                    $cntnt = Content::where(['guid'=>$content_guid])->first();
                    $contentData = $cntnt->toArray();
                        $contentAry = [];
                        $contentAry['id']          = $contentData['id'];
                        $contentAry['title']       = $contentData['title'];
                        $contentAry['body']        = $contentData['body'];
                        $contentAry['status']      = $contentData['status'];
                        $contentAry['attachment']  = $contentData['attachment'];
                        $contentAry['created_at']  = $contentData['created_at'];
                        $contentAry['type']        = (!empty($contentData['ctype']))?$ctypeAry[$contentData['ctype']]:'';
                            // Background Image
                            $bgImageData = [];
                            if(!empty($contentData['bg_images'])) {
                                $bgImageData = unserialize($contentData['bg_images']);
                            }
                            $contentAry['bg_images'] = $bgImageData;
                        // Thumbnail Image
                            $tnImageData = [];
                            if(!empty($contentData['tn_images'])) {
                                $tnImageData = unserialize($contentData['tn_images']);
                            }
                            $contentAry['tn_images'] = $tnImageData;
                        // Author - - - - - - -- 
                            $authorData = [];
                            if(!empty($contentData['author_id'])) {
                                $author  = Author::where('id',$contentData['author_id'])->first()->toArray();
                                $authorData['full_name']    = $author['full_name'];
                                $authorData['title']        = $author['title'];
                                $authorData['picture']      = $author['picture'];
                                $authorData['bio']          = $author['bio'];
                            }
                            $contentAry['author']   = $authorData;
                        // Product - - - - - - -- 
                            $productData = [];
                            if(!empty($v['product_id'])) {
                                $product  = Product::where('id',$v['product_id'])->first()->toArray();
                                $productData['name']         = $product['name'];
                                $productData['sku']          = $product['sku'];
                                $productData['picture']      = $product['picture'];
                                $productData['video']        = $product['video'];
                                $productData['qr_text']      = $product['qr_text'];
                                $productData['qr_image']     = $product['qr_image'];
                                $productData['description']  = $product['description'];
                            }
                            $contentAry['product']   = $productData;
                            $content_langs = $cntnt->langs()->get()->toArray();
                            foreach($content_langs as $klang=>$vlang) {
                                $vlang['pivot']['cntnt_bg_images'] = unserialize($vlang['pivot']['cntnt_bg_images']);
                                $vlang['pivot']['cntnt_tn_images'] = unserialize($vlang['pivot']['cntnt_tn_images']);
                                $contentAry['langs'][$vlang['code']]   = $vlang['pivot'];
                            }

                    $resData = [];
                    $res['status'] = 'success';
                    $res['data']   = $contentAry;
                    return response()->json($res,200);
                } else {
                    return response()->json(['status'=>'error','message'=>'Invalid Content Id'],400);
                }  
            } else {
                return response()->json(['status'=>'error','message'=>'Content Guid cannot be empty.'],400);
            }            
        } else {
            return response()->json(['status'=>'error','message'=>'Invalid Parameters passed to request'],400);
        }
    }
    
    /*
    *  12 - getblocksByGuid
    */
    public function getblocksByGuid() {
        $bodyContent = \Request::getContent();
        if(!empty($bodyContent)) {
            $content = json_decode($bodyContent);
            $content = (array)$content;
            // Check 01 : Content GUid - Not Empty
            if(!empty($content['guid'])) {
                $content_guid = $content['guid'];
                // Check 02 : Is Valid Content Id
                $isValidContentGuid = ApiController::helper_isValidContentGuid($content_guid);
                if($isValidContentGuid == true) {
                    $res = [];
                    $conditions = [];
                    $contentData = Content::where(['guid'=>$content_guid])->first();
                    $blocks = $contentData->blocks()->get()->toArray();
                    $resBlocks = [];
                    foreach($blocks as $key=>$val) {
                        $blockData = ApiController::helper_getBlockData($val['id']);
                        $resBlocks[$key]['title']       = $blockData['title'];
                        $resBlocks[$key]['description'] = $blockData['description'];
                        $resBlocks[$key]['type']        = $blockData['type'];
                        if($blockData['type'] == 'advertisement') {
                            $resBlocks[$key]['advertisement_image']    = $blockData['data']['ad_image'];
                        } else if($blockData['type'] == 'video') {
                            $resBlocks[$key]['uploaded_video']         = $blockData['data']['uploaded_video'];
                            $resBlocks[$key]['embed_video']            = $blockData['data']['embed_video'];
                        } else if($blockData['type'] == 'text') {
                            $resBlocks[$key]['text']                   = $blockData['data']['text_body'];
                        } else if($blockData['type'] == 'qrcode') {
                            $resBlocks[$key]['qrcode_type']            = $blockData['data']['qrcode_type'];
                            $resBlocks[$key]['qrcode_image']           = $blockData['data']['qrcode_image'];
                        }
                        $resBlocks[$key]['content_pages'] = explode(',',$val['pivot']['content_pages']);
                        $resBlocks[$key]['display_location'] = $val['pivot']['display_location'];
                    }
                    $resData = [];
                    $res['status'] = 'success';
                    $res['data']   = $resBlocks;
                    return response()->json($res,200);
                } else {
                    return response()->json(['status'=>'error','message'=>'Invalid Content Id'],400);
                }  
            } else {
                return response()->json(['status'=>'error','message'=>'Content Guid cannot be empty.'],400);
            }            
        } else {
            return response()->json(['status'=>'error','message'=>'Invalid Parameters passed to request'],400);
        }
    }

    /*
    *  13 - GetDoctorsByServiceType
    */
    public function getDoctorListByServiceType() {
        $bodyContent = \Request::getContent();
        if(!empty($bodyContent)) {
            $content = json_decode($bodyContent);
            $content = (array)$content;
            if(!empty($content['client_id'])) {
                $cid    = $content['client_id'];
                $stypid = $content['stype'];
                // Check 01 : Is Valid Client Id
                $isValidClientId = ApiController::helper_isValidClientId($cid);
                if($isValidClientId == true) {
                    $isValidServiceType = ApiController::helper_isValidServiceType($stypid);
                    if($isValidServiceType == true) {
                        $conditions = [['status', '=', '1'], ['is_deleted', '=', '0'] ];
                        $query = User::where($conditions);
                        $query = $query->whereHas('roles', function ($q) {
                            return $q->where('id', '3'); // Role Id '3' for doctors
                        });
                        $query = $query->whereHas('clients', function ($q) use ($cid) {
                            return $q->where('id', $cid); // cid is Client Id
                        });
                        $query = $query->whereHas('stypes', function ($q) use ($stypid) {
                            return $q->where('id', $stypid); // stypid is Service Type Id
                        });
                        $query = $query->join('profiles', 'users.id', '=', 'profiles.user_id')->select('users.*', 'profiles.*');
                        $docData = $query->get()->toArray();
                        $docRes = [];
                        if(count($docData) > 0) {
                            foreach($docData as $k=>$v) {
                                
                                $docRes[$k]['id']               = $v['id'];
                                $docRes[$k]['full_name']        = $v['full_name'];
                                $docRes[$k]['title']            = $v['title'];
                                $docRes[$k]['bio']              = $v['bio'];
                                $docRes[$k]['picture']          = $v['picture'];
                                $docRes[$k]['status']           = $v['status'];
                                $docRes[$k]['contact_number']   = $v['contact_number'];
                                $docRes[$k]['background_image']     = $v['bg_image'];
                                $docRes[$k]['profile_video']        = $v['profile_video'];
                                $usr = User::where(['id' => $v['id']])->first();
                                //Doctor Service Type
                                $userStype = $usr->stypes()->get()->toArray();
                                if(!empty($userStype)){
                                    foreach($userStype as $key=>$val) {
                                        $docRes[$k]['services'][]      = $val['name'];
                                    }
                                }

                                //Doctor Langs
                                $userLangs = $usr->langs()->get()->toArray();
                                if(!empty($userLangs)){
                                    foreach($userLangs as $key=>$val) {
                                        $docRes[$k]['langs'][$val['code']]      = $val['pivot'];
                                    }
                                }


                                $rateResponse = ApiController::helper_fetchDoctorRate($v['doc_rate_id']);
                                $res1 = json_decode($rateResponse, true);
                                $res1 = (array)$res1;          
                                if($res1['success'] == true) {
                                    $docRes[$k]['rate_data'] = $res1['data'];
                                } else {
                                    $docRes[$k]['rate_data']        = [];
                                }              
                            }
                        }
                        $res['status'] = 'success';
                        $res['data']   = $docRes;
                        return response()->json($res,200);
                    } else {
                        return response()->json(['status'=>'error','message'=>'Invalid Service Type'],400);
                    }  
                } else {
                    return response()->json(['status'=>'error','message'=>'Invalid Client Id'],400);
                }  
            } else {
                return response()->json(['status'=>'error','message'=>'Client Id cannot be empty.'],400);
            }            
        } else {
            return response()->json(['status'=>'error','message'=>'Invalid Parameters passed to request'],400);
        }
    }

    /*
    *  14 - GetDetailByPageTypeAndClientId
    */
    public function getDetailByPageTypeAndClientId() {
        $bodyContent = \Request::getContent();
        if(!empty($bodyContent)) {
            $content = json_decode($bodyContent);
            $content = (array)$content;
            // Check 01 : Client Id & Page Type- Not Empty
            if((!empty($content['client_id'])) && (!empty($content['page_type']))) {
                $client_id = $content['client_id'];
                $page_type = $content['page_type'];
                // Check 02 : Is Valid Client Id
                $isValidContentId = ApiController::helper_isValidClientId($client_id);
                if($isValidContentId == true) {
                    // Check 02 : Is Valid Page Type
                    $isValidPageType = ApiController::helper_isValidPageType($page_type);
                    if($isValidPageType == true) {
                        $ctypeAry       = ['zip'=>'Zipped','image'=>'Image','video'=>'Video','product'=>'Product'];
                        $statusAry      = ['1'=>'Published','2'=>'Unpublished'];
                        $responseData = [];
                        $client  = Client::where('id',$client_id)->first();
                        $clientPages = $client->cpagesbyptype($page_type)->get()->toArray();
                        if(!empty($clientPages)){
                            foreach($clientPages as $key=>$val) {
                                $pageRes = [];
                                $pageRes['id']          = $val['id'];
                                $pageRes['name']        = $val['name'];
                                $pageRes['title']       = $val['title'];
                                $pageRes['description'] = $val['description'];
                                // Fetch assigned sections to this client page
                                $cpage  = Cpage::where('id',$val['id'])->first();
                                $cpageLangs = $cpage->langs()->get()->toArray();
                                if(!empty($cpageLangs)){
                                    foreach($cpageLangs as $key=>$val) {
                                        $pageRes['langs'][$val['code']]      = $val['pivot'];
                                    }
                                }
                                $cpageSection = $cpage->csections()->get()->toArray();
                                foreach($cpageSection as $sk=>$sv) {
                                    $tempSection = [];
                                    $tempSection['id']      = $sv['id'];
                                    $tempSection['name']    = $sv['name'];
                                    $tempSection['title']   = $sv['title'];

                                    $cpsec = Csection::where(['id' => $sv['id']])->first();
                                    $cpsecLangs = $cpsec->langs()->get()->toArray();
                                    if(!empty($cpsecLangs)){
                                        foreach($cpsecLangs as $cpskey=>$cpsval) {
                                            $tempSection['langs'][$cpsval['code']]      = $cpsval['pivot'];
                                        }
                                    } 
                                    // Fetch contents under particular sections
                                    $cidsAry = json_decode($sv['cids']);
                                    foreach($cidsAry as $ck=>$cv) {
                                        $tempContent = [];
                                        $content  = Content::where('id',$cv)->first();
                                        $contentLangs = $content->langs()->get()->toArray();
                                        $contentData = $content->toArray();
                                        $tempContent['id']          = $contentData['id'];
                                        $tempContent['title']       = $contentData['title'];
                                        $tempContent['type']        = (!empty($contentData['ctype']))?$ctypeAry[$contentData['ctype']]:'';
                                        $tempContent['body']        = $contentData['body'];
                                        $tempContent['status']      = $statusAry[$contentData['status']];
                                        $tempContent['attachment']  = $contentData['attachment'];
                                        $tempContent['guid']        = $contentData['guid'];
                                        // Background Image
                                            $bgImageData = [];
                                            if(!empty($contentData['bg_images'])) {
                                                $bgImageData = unserialize($contentData['bg_images']);
                                            }
                                            $tempContent['bg_images'] = $bgImageData;
                                        // Thumbnail Image
                                            $tnImageData = [];
                                            if(!empty($contentData['tn_images'])) {
                                                $tnImageData = unserialize($contentData['tn_images']);
                                            }
                                            $tempContent['tn_images'] = $tnImageData;
                                        // Author - - - - - - -- 
                                            $authorData = [];
                                            if(!empty($contentData['author_id'])) {
                                                $author  = Author::where('id',$contentData['author_id'])->first()->toArray();
                                                $authorData['full_name']    = $author['full_name'];
                                                $authorData['title']        = $author['title'];
                                                $authorData['picture']      = $author['picture'];
                                                $authorData['bio']          = $author['bio'];
                                            }
                                            $tempContent['author']   = $authorData;
                                        // Product - - - - - - -- 
                                            $productData = [];
                                            if(!empty($contentData['product_id'])) {
                                                $product  = Product::where('id',$contentData['product_id'])->first()->toArray();
                                                $productData['name']         = $product['name'];
                                                $productData['sku']          = $product['sku'];
                                                $productData['picture']      = $product['picture'];
                                                $productData['video']        = $product['video'];
                                                $productData['qr_text']      = $product['qr_text'];
                                                $productData['qr_image']     = $product['qr_image'];
                                                $productData['description']  = $product['description'];
                                            }
                                            $tempContent['product']  = $productData;
                                            $contentLangData = [];
                                            if(!empty($contentLangs)) {
                                                foreach($contentLangs as $clky=>$clvl) {
                                                    $contentLangData[$clvl['code']] = $clvl['pivot']; 

                                                    // Background Image
                                                        $bgImageCData = [];
                                                        if(!empty($clvl['pivot']['cntnt_bg_images'])) {
                                                            $bgImageCData = unserialize($clvl['pivot']['cntnt_bg_images']);
                                                        }
                                                        $contentLangData[$clvl['code']]['cntnt_bg_images'] = $bgImageCData;
                                                    // Thumbnail Image
                                                        $tnImageCData = [];
                                                        if(!empty($clvl['pivot']['cntnt_tn_images'])) {
                                                            $tnImageCData = unserialize($clvl['pivot']['cntnt_tn_images']);
                                                        }
                                                        $contentLangData[$clvl['code']]['cntnt_tn_images'] = $tnImageCData;
                                                }
                                            }
                                            $tempContent['langs'] = $contentLangData;
                                        $tempSection['content'][] = $tempContent;
                                    }
                                    $pageRes['sections'][] = $tempSection;
                                }
                                $responseData[] = $pageRes;
                            }
                        } else {
                            $responseData = [];
                        }
                        $resData = [];
                        $res['status'] = 'success';
                        $res['data']   = $responseData;
                        return response()->json($res,200);
                    } else {
                        return response()->json(['status'=>'error','message'=>'Invalid page Type'],400);
                    }  
                } else {
                    return response()->json(['status'=>'error','message'=>'Invalid Content Id'],400);
                }  
            } else {
                return response()->json(['status'=>'error','message'=>'Content Id cannot be empty.'],400);
            }            
        } else {
            return response()->json(['status'=>'error','message'=>'Invalid Parameters passed to request'],400);
        }
    }

    /*
    *  15 - Get Premium Content
    */
    public function getPremiumContent() {
        $bodyContent = \Request::getContent();
        if(!empty($bodyContent)) {
            $content = json_decode($bodyContent);
            $content = (array)$content;
                $res = [];
                $conditions = [];
                $conditions[] = ['is_premium', '=', '1'];
                if((isset($content['paid'])) && (in_array($content['paid'], ['0','1']))) {
                    $conditions[] = ['is_paid', '=', ''.$content['paid'].''];
                }
                $query = Content::where($conditions);
                $contentData = $query->get();//->toArray();
                //echo "<pre>"; print_r($conditions); print_r($contentData); die;
                $resData = [];
                $ctypeAry       = ['zip'=>'Zipped','image'=>'Image','video'=>'Video','product'=>'Product'];
                $statusAry      = ['1'=>'Published','2'=>'Unpublished'];
                
                if(count($contentData) > 0) {
                    foreach($contentData as $content) {
                        $contentLangs   = $content->langs()->get()->toArray();
                        $contentAry     = $content->toArray();
                        $tempResData                = [];
                        $tempResData['id']          = $contentAry['id'];
                        $tempResData['title']       = $contentAry['title'];
                        $tempResData['body']        = $contentAry['body'];
                        $tempResData['status']      = $contentAry['status'];
                        $tempResData['attachment']  = $contentAry['attachment'];
                        $tempResData['points']     = $contentAry['points'];
                        $tempResData['created_at']  = $contentAry['created_at'];
                        $tempResData['type']        = (!empty($contentAry['ctype']))?$ctypeAry[$contentAry['ctype']]:'';
                        // Background Image
                            $bgImageData = [];
                            if(!empty($v['bg_images'])) {
                                $bgImageData = unserialize($contentAry['bg_images']);
                            }
                            $tempResData['bg_images'] = $bgImageData;
                        // Thumbnail Image
                            $tnImageData = [];
                            if(!empty($v['tn_images'])) {
                                $tnImageData = unserialize($contentAry['tn_images']);
                            }
                            $tempResData['tn_images'] = $tnImageData;
                        // Author - - - - - - -- 
                            $authorData = [];
                            if(!empty($contentAry['author_id'])) {
                                $author  = Author::where('id',$contentAry['author_id'])->first();
                                //$athrLangs = $author->langs()->get()->toArray();
                                $authrAry = $author->toArray();
                                $authorData['full_name']    = $authrAry['full_name'];
                                $authorData['title']        = $authrAry['title'];
                                $authorData['picture']      = $authrAry['picture'];
                                $authorData['bio']          = $authrAry['bio'];
                                // foreach($athrLangs as $alkey=>$alval){
                                //     $authorData['langs'][$alval['code']]        = $alval['pivot'];
                                // }
                            }
                            $tempResData['author']   = $authorData;
                        // Product - - - - - - -- 
                            $productData = [];
                            if(!empty($contentAry['product_id'])) {
                                $product  = Product::where('id',$contentAry['product_id'])->first()->toArray();
                                $productData['name']         = $product['name'];
                                $productData['sku']          = $product['sku'];
                                $productData['picture']      = $product['picture'];
                                $productData['video']        = $product['video'];
                                $productData['qr_text']      = $product['qr_text'];
                                $productData['qr_image']     = $product['qr_image'];
                                $productData['description']  = $product['description'];
                            }
                            $tempResData['product']   = $productData;
                            foreach($contentLangs as $klang=>$vlang) {
                                $vlang['pivot']['cntnt_bg_images'] = unserialize($vlang['pivot']['cntnt_bg_images']);
                                $vlang['pivot']['cntnt_tn_images'] = unserialize($vlang['pivot']['cntnt_tn_images']);
                                $tempResData['langs'][$vlang['code']]   = $vlang['pivot'];
                            }
                            //$tempResData['langs']   = $contentLangs;
                            $resData[] = $tempResData;
                    }
                }
                $res['status'] = 'success';
                $res['data']   = $resData;
                return response()->json($res,200);     
        } else {
            return response()->json(['status'=>'error','message'=>'Invalid Parameters passed to request'],400);
        }
    }
    /*  
    *  16 - Add Premium Content User
    */
    public function addPremiumContentUser() {
        $bodyContent = \Request::getContent();
        if(!empty($bodyContent)) {
            $postData = json_decode($bodyContent);
            $postData = (array)$postData;
            $res = [];
            //Check 01 : Doctor Id & Status - Not Empty
            if((!empty($postData['uuid'])) && (!empty($postData['content_id'])) && ($postData['points'] != '' )) {
                // Check 02 : Check is it valid Content Id or NOT
                $isValidPremiumContentId = ApiController::helper_isValidPremiumContentId($postData['content_id']);
                if($isValidPremiumContentId == true) {
                    $loadContent = Content::whereId($postData['content_id'])->first()->toArray();
                    // Check 03 : Passed points are equal to content points
                    if($postData['points'] >= $loadContent['points']) {
                        $uuid   = $postData['uuid'];
                        $cid    = $postData['content_id'];
                        $points = $postData['points'];
                        if($loadContent['expire_in_days'] == '0') {
                            $expiry = '';
                        } else {
                            $cDay = Date('d', strtotime('+'.$loadContent['expire_in_days'].' days'));
                            $cMonth = date("m", strtotime('+'.$loadContent['expire_in_days'].' days'));
                            $cYear = date("Y", strtotime('+'.$loadContent['expire_in_days'].' days'));
                            $expiry = mktime(0, 0, 0, $cMonth, $cDay, $cYear);
                        }
                        // Create Premium Content User
                        $pcuser =  Pcusers::create([
                            'uuid' => $uuid,
                            'content_id' => $cid,
                            'expiry_on' => $expiry,
                            'points_deducted' => $points,
                        ]);
                        if($pcuser) {
                            $res['status']   = 'success';
                            $res['message']  = 'Premium Content User has been added sucessfully.';
                            return response()->json($res,200);
                        } else {
                            return response()->json(['status'=>'error','message'=>'Invalid Request'],400);
                        }
                    } else {
                        return response()->json(['status'=>'error','message'=>'Points mismatch.'],400);
                    }
                } else {
                    return response()->json(['status'=>'error','message'=>'Invalid Premium Content Id'],400);
                }    
            } else {
                return response()->json(['status'=>'error','message'=>'UUID, Content_Id and Points cannot be empty.'],400);
            }            
        } else {
            return response()->json(['status'=>'error','message'=>'Invalid Parameters passed to request'],400);
        }
    }

    /*
    *  17 - Get Premium Content By Uuid
    */
    public function getPremiumContentByUuid() {
        $bodyContent = \Request::getContent();
        if(!empty($bodyContent)) {
            $content = json_decode($bodyContent);
            $content = (array)$content;
            // Check 01 : Content GUid - Not Empty
            if(!empty($content['uuid'])) {
                $content_uuid = $content['uuid'];
                $pcusers = Pcusers::where(['uuid'=>$content_uuid])->get()->toArray();
                
                $contentIds = []; 
                $currentTimestamp = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
                foreach($pcusers as $k=>$v) {
                    if(($v['expiry_on'] == '') || ($v['expiry_on'] >= $currentTimestamp )){
                        $contentIds[] = $v['content_id'];
                    }
                }
		//echo "<pre> OLD CONTENT-IDs"; print_r($contentIds);
		// Check 02 : Avail for this user or not
                if((!empty($content['available'])) && ($content['available'] == '1' )) {
			$acpAry = [];
			$allPremiumContent = Content::where([['is_premium', '=', '1']])->get()->toArray();
                	if(!empty( $allPremiumContent )){
				foreach($allPremiumContent as $key=>$val) {
					if(!in_array($val['id'],$contentIds)){
                    				$acpAry[] = $val['id'];
					}
                		}
			}
			$contentIds = $acpAry;

		}
                //echo "<pre>"; print_r($pcusers); print_r($contentIds); die;
                // $res = [];
                $fullContentDetail = []; 
                foreach($contentIds as $ck=>$cid) {
                    $ctypeAry       = ['zip'=>'Zipped','image'=>'Image','video'=>'Video','product'=>'Product'];
                    $statusAry      = ['1'=>'Published','2'=>'Unpublished'];
                    $cntnt = Content::whereId($cid)->first();
                    $contentData = $cntnt->toArray();
                        $contentAry = [];
                        $contentAry['id']          = $contentData['id'];
                        $contentAry['title']       = $contentData['title'];
                        $contentAry['body']        = $contentData['body'];
                        $contentAry['status']      = $contentData['status'];
                        $contentAry['points']      = $contentData['points'];
                        $contentAry['attachment']  = $contentData['attachment'];
                        $contentAry['created_at']  = $contentData['created_at'];
                        $contentAry['type']        = (!empty($contentData['ctype']))?$ctypeAry[$contentData['ctype']]:'';
                            // Background Image
                            $bgImageData = [];
                            if(!empty($contentData['bg_images'])) {
                                $bgImageData = unserialize($contentData['bg_images']);
                            }
                            $contentAry['bg_images'] = $bgImageData;
                        // Thumbnail Image
                            $tnImageData = [];
                            if(!empty($contentData['tn_images'])) {
                                $tnImageData = unserialize($contentData['tn_images']);
                            }
                            $contentAry['tn_images'] = $tnImageData;
                        // Author - - - - - - -- 
                            $authorData = [];
                            if(!empty($contentData['author_id'])) {
                                $author  = Author::where('id',$contentData['author_id'])->first()->toArray();
                                $authorData['full_name']    = $author['full_name'];
                                $authorData['title']        = $author['title'];
                                $authorData['picture']      = $author['picture'];
                                $authorData['bio']          = $author['bio'];
                            }
                            $contentAry['author']   = $authorData;
                        // Product - - - - - - -- 
                            $productData = [];
                            if(!empty($v['product_id'])) {
                                $product  = Product::where('id',$v['product_id'])->first()->toArray();
                                $productData['name']         = $product['name'];
                                $productData['sku']          = $product['sku'];
                                $productData['picture']      = $product['picture'];
                                $productData['video']        = $product['video'];
                                $productData['qr_text']      = $product['qr_text'];
                                $productData['qr_image']     = $product['qr_image'];
                                $productData['description']  = $product['description'];
                            }
                            $contentAry['product']   = $productData;
                            $content_langs = $cntnt->langs()->get()->toArray();
                            foreach($content_langs as $klang=>$vlang) {
                                $vlang['pivot']['cntnt_bg_images'] = unserialize($vlang['pivot']['cntnt_bg_images']);
                                $vlang['pivot']['cntnt_tn_images'] = unserialize($vlang['pivot']['cntnt_tn_images']);
                                $contentAry['langs'][$vlang['code']]   = $vlang['pivot'];
                            }
                    $fullContentDetail[] = $contentAry;
                }
                $resData = [];
                $res['status'] = 'success';
                $res['data']   = $fullContentDetail;
                return response()->json($res,200);
            } else {
                return response()->json(['status'=>'error','message'=>'Content Uuid cannot be empty.'],400);
            }            
        } else {
            return response()->json(['status'=>'error','message'=>'Invalid Parameters passed to request'],400);
        }
    }


    // SMOKE APIs
    public function getDashboardDetail(Request $request) {
        $bodyContent = \Request::getContent();
        if(!empty($bodyContent)) {
            $content = json_decode($bodyContent);
            $content = (array)$content;
            if(!empty($content['client_id'])) {
                $res = []; 
                $res['Status'] = 'Success';
                $res['Data'][]   = [
                    'ImageName' => 'Image2.jpg',
                    'SeqId' => '01',
                    'ImageURL' => 'https://borderlesshealthbkt.s3.ap-southeast-1.amazonaws.com/images/20200818050825_img_01.png',
                    'ActionId' => 'HN 201'
                ];
                $res['Data'][]   = [
                    'ImageName' => 'Image2.jpg',
                    'SeqId' => '02',
                    'ImageURL' => 'https://borderlesshealthbkt.s3.ap-southeast-1.amazonaws.com/images/20200818050909_img_02.png',
                    'ActionId' => 'HN 202'
                ];
                $res['Data'][]   = [
                    'ImageName' => 'Image3.jpg',
                    'SeqId' => '03',
                    'ImageURL' => 'https://borderlesshealthbkt.s3.ap-southeast-1.amazonaws.com/images/20200818050937_img_03.png',
                    'ActionId' => 'HN 203'
                ];
                return response()->json($res,200);
            } else {
                return response()->json(['Status'=>$content,'Message'=>'Invalid Client Id'],500);
            }            
        } else {
            return response()->json(['Status'=>'error','Message'=>'Invalid Parameters'],500);
        }
    }

    public function getTypeCall() {
        $bodyContent = \Request::getContent();
        if(!empty($bodyContent)) {
            $content = json_decode($bodyContent);
            $content = (array)$content;
            if((!empty($content['client_id'])) && (!empty($content['call_type']))) {
                $res = []; 
                $res['Status'] = 'Success';
                if($content['call_type'] == '1'){
                    $res['Data'][]   = [
                        'VideoPlayUrl' => 'https://file-examples-com.github.io/uploads/2017/04/file_example_MP4_480_1_5MG.mp4',
                        'VideoPlayImage' => 'https://borderlesshealthbkt.s3.ap-southeast-1.amazonaws.com/images/20200818050825_img_01.png',
                        'LaunchSessionImageURL' => 'https://borderlesshealthbkt.s3.ap-southeast-1.amazonaws.com/images/20200818050825_img_01.png'
                    ];
                } else if($content['call_type'] == '2') {
                    $res['Data'][]   = [
                        'VideoPlayUrl' => 'https://file-examples-com.github.io/uploads/2017/04/file_example_MP4_480_1_5MG.mp4',
                        'VideoPlayImage' => 'https://borderlesshealthbkt.s3.ap-southeast-1.amazonaws.com/images/20200818050825_img_02.png',
                        'LaunchSessionImageURL' => 'https://borderlesshealthbkt.s3.ap-southeast-1.amazonaws.com/images/20200818050825_img_02.png'
                    ];
                }
                return response()->json($res,200);
            } else {
                return response()->json(['Status'=>'error','Message'=>'Invalid Client Id & Call Type'],500);
            }
        } else {
            return response()->json(['Status'=>'error','Message'=>'Invalid Parameters'],500);
        }
    }


    // ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    //     Helper Methods
    // ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

    // Check : Is this Valid Client Id
    public function helper_isValidClientId($client_id) {
        $conditions = [['id', '=', $client_id]];
        $clientData = Client::where($conditions)->get()->toArray();
        if(!empty($clientData)){
            return true;
        }
        return false;
    }

    // Check : Is this Valid Page Type
    public function helper_isValidPageType($page_type) {
        $ptypes=['p1','p2','p3','p4'];
        if(in_array($page_type,$ptypes)){
            return true;
        }
        return false;
    
    }
    
    // Check : Is this Valid Service Type Id
    public function helper_isValidServiceType($stype) {
        $conditions = [['id', '=', $stype]];
        $stypeData = Stype::where($conditions)->get()->toArray();
        if(!empty($stypeData)){
            return true;
        }
        return false;
    }
    // Check : Is this Valid Client Id
    public function helper_isValidDoctorId($doc_id) {
        $conditions = [['id', '=', $doc_id],['is_deleted', '=', '0']];
        $query = User::where($conditions);
        $query = $query->whereHas('roles', function ($q) {
            return $q->where('id', '3'); // Role Id '3' for doctors
        });
        $docData = $query->get()->toArray();

        if(!empty($docData)) {
            return true;
        }
        return false;
    }

    // Check : Is this Valid Category Id
    public function helper_isValidTagId($tag_id) {
        $tagData = Tag::whereId($tag_id)->get()->toArray();
        if(!empty($tagData)) {
            return true;
        }
        return false;
    }

    // Check : Is this Valid Category Id Ary
    public function helper_isValidTagIdAry($tagIdAry) {
        foreach($tagIdAry as $tag_id ) {
            $tagData = Tag::whereId($tag_id)->get()->toArray();
            if(empty($tagData)) {
                return false;
            }
        }
        return true;
    }

    // Check : Is this Valid Content Id
    public function helper_isValidContentId($content_id) {
        $contentData = Content::whereId($content_id)->get()->toArray();
        if(!empty($contentData)) {
            return true;
        }
        return false;
    }
    

    // Check : Is this Valid Content Id
    public function helper_isValidPremiumContentId($content_id) {
        $conditions = [];
        $conditions[] = ['id', '=', $content_id];
        $conditions[] = ['is_premium', '=', '1'];
        $contentData = Content::where($conditions)->get()->toArray();
        if(!empty($contentData)) {
            return true;
        }
        return false;
    }


    // Check : Is this Valid Content Guid
    public function helper_isValidContentGuid($guid) {
        $contentData = Content::where(['guid'=>$guid])->first()->toArray();
        if(!empty($contentData)) {
            return true;
        }
        return false;
    }

    // Fetch : Categories by Client Id
    public function helper_getTagsbyClientId($client_id, $call_type) {
        $conditions = [];
        $query = Tag::where($conditions);
        $query = $query->whereHas('clients', function ($q) use($client_id) {
            return $q->where('id', $client_id);
        });
        $tagData = $query->get('id')->toArray();
        $resData = [];
        if( count($tagData) > 0 ) {
            foreach($tagData as $k=>$v) {
                $resData[] = $v['id'];
            }
        }
        return $resData; 
    }

    // Fetch : Blocks data
    public function helper_getBlockData($id) {
        $block          = Block::where('id',$id)->first();
        $block_data     = $block->blockdata()->first()->toArray();
        $blockData      = $block->toArray();
        $blockData['data'] = $block_data;
        return $blockData;
    }

    // Fetch : Doctor Rates by ID
    public function helper_fetchDoctorRate($docRateId){

        $endpoint = "http://borderlesslife.healthvalley.asia/xmlservices/getDoctorDetailsById.php";
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $endpoint, ['query' => [
            'api_key'       => '64GNyBJ5U7Lswp2tyEdZ2NEjmf3DyF', 
            'unique_key'    => 'L2AK5JStDsY2ttj',
            'doctor_id'     => $docRateId
        ]]);
        $statusCode = $response->getStatusCode();
        if($statusCode == '200') {
            $content = $response->getBody()->getContents();
            return $content;
        } else {
            return '{"success":"false","data":[]}';
        }
    }
}
