<?php

namespace App\Http\Controllers;

use App\Author;
use App\Product;
use App\Content;
use App\Lang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\FilesystemManager;

class ProductController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $pnm = request()->query('pnm');
        $sku = request()->query('sku');
        $condition = [];
        if(!empty($pnm)){ $condition[] = ['name', 'like', '%'.$pnm.'%']; }
        if(!empty($sku)){ $condition[] = ['sku', 'like', '%'.$sku.'%']; }
        $query = Product::where($condition);
        $products = $query->orderBy('created_at', 'desc')->paginate(10);
        $productData = [];
        // foreach($products as $product) {
        //     $productData[] = $product->toArray();
        // }
        foreach($products as $product) {
            $tempData = [];
            $product_langs  = $product->langs()->get()->toArray();
            $tempData = $product->toArray();
            $tempData['langs'] = [];
            if(count($product_langs) > 0){
                foreach($product_langs as $k=>$v) {
                    $tempData['langs'][$v['id']] = $v['pivot'];
                    $tempData['langs'][$v['id']]['lang_name'] = $v['name'];
                }
            }
            $productData[] = $tempData;
        }
        $qry_data = ['pnm'=>$pnm,'sku'=>$sku];
        return view('products.index')
        ->with("paginate",$products->appends($qry_data))
        ->with("products",$productData)
        ->with('page_title','List of Products')
        ->with('qry',$qry_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $langs = Lang::getEnabledLanguages();
        return view('products.create')->with('langs',$langs);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        // Validate Fields
        $validateAry = [
            'sku'           => ['required','string','max:255','unique:products'],
        ];
        $langdata = $request->input('langdata');
        $qrcode = $langdata['1']['prdct_qrcode'];
        $qrimage = $request->file('langdata_1_prdct_qrimage');  // For english only 
        if((empty($qrcode)) && (empty($qrimage))) {
            $validateAry['langdata_1_prdct_qrimage'] = ['required'];
        }
        $request->validate($validateAry);

        //- - - - - - - - NEED TO DELETE LATER - - - - - - - 
        $prdctlangdata = []; 
        $pld = $request->input('langdata');
        foreach($pld as $key=>$val) {
            $prdctlangdata[$key]['prdct_name']          = $val['prdct_name'];
            $prdctlangdata[$key]['prdct_qrcode']        = (isset($val['prdct_qrcode']))?$val['prdct_qrcode']:'';
            $prdctlangdata[$key]['prdct_description']   = (isset($val['prdct_description']))?$val['prdct_description']:'';
            // Product Picture
                $pictureFile    = $request->file('langdata_'.$key.'_prdct_picture'); 
                $picture_url = '';
                if(!empty($pictureFile)) {
                    $pictureFileName = $pictureFile->getClientOriginalName();
                    $file_path = "products/images/".date('Ymdhis').'_'.$pictureFileName;
                    $s3 = Storage::disk('s3');
                    $s3->put($file_path, file_get_contents($pictureFile), array('ContentDisposition' => 'inline; filename=' . $pictureFileName . '', 'ACL' => 'public-read'));
                    $picture_url = Storage::disk('s3')->url($file_path);
                }
                $prdctlangdata[$key]['prdct_picture']     = $picture_url;

            // Product Video
                $videoFile    = $request->file('langdata_'.$key.'_prdct_video'); 
                $video_url = '';
                if(!empty($videoFile)) {
                    $videoFileName = $videoFile->getClientOriginalName();
                    $file_path = "products/videos/".date('Ymdhis').'_'.$videoFileName;
                    $s3 = Storage::disk('s3');
                    $s3->put($file_path, file_get_contents($videoFile), array('ContentDisposition' => 'inline; filename=' . $videoFileName . '', 'ACL' => 'public-read'));
                    $video_url = Storage::disk('s3')->url($file_path);
                }
                $prdctlangdata[$key]['prdct_video']     = $video_url;
            
            // QR Code / Image
                $qrimagefile = $request->file('langdata_'.$key.'_prdct_qrimage');
                if(!empty($qrimagefile)) {
                    $rndnum = rand(999,9999);
                    $file_name = 'QR_'.date('Y_m_d_h_i_s').'_'.$rndnum.'.png';
                    $file_path = "blocks/qrcode/".$file_name;
                    $s3 = Storage::disk('s3');
                    $s3->put($file_path, $qrimagefile, array('ContentDisposition' => 'inline; filename=' . $file_name . '', 'ACL' => 'public-read'));
                    $qrcode_url = Storage::disk('s3')->url($file_path);
                } else {
                    if(isset($val['prdct_qrcode'])) {
                        // Generate QR Code Image
                        $barcode = new \Com\Tecnick\Barcode\Barcode();
                        $qrCodeData = $val['prdct_qrcode'];
                        $QRCodeImg = $barcode->getBarcodeObj('QRCODE,H', $qrCodeData, - 16, - 16, 'black', array(- 2,- 2,- 2,- 2))->setBackgroundColor('#fff')->getPngData();
                        $rndnum = rand(999,9999);
                        $file_name = 'QR_'.date('Y_m_d_h_i_s').'_'.$rndnum.'.png';
                        $file_path = "blocks/qrcode/".$file_name;
                        $s3 = Storage::disk('s3');
                        $s3->put($file_path, $QRCodeImg, array('ContentDisposition' => 'inline; filename=' . $file_name . '', 'ACL' => 'public-read'));
                        $qrcode_url = Storage::disk('s3')->url($file_path);
                    } else {
                        $qrcode_url = '';
                    }
                }
            $prdctlangdata[$key]['prdct_qrimg']     = $qrcode_url;
        }
        //echo "<pre>"; print_r($prdctlangdata); die;
        //- - - - - - - - ENGLISH FIELDS - - - - - - - 
        $prdct_name             = $prdctlangdata['1']['prdct_name'];
        $prdct_qrcode           = $prdctlangdata['1']['prdct_qrcode'];
        $prdct_description      = $prdctlangdata['1']['prdct_description'];
        $prdct_picture          = $prdctlangdata['1']['prdct_picture'];
        $prdct_video            = $prdctlangdata['1']['prdct_video'];
        $prdct_qrimg            = $prdctlangdata['1']['prdct_qrimg'];
        //- - - - - - - - - - - - - - - - - - - - - - - - - 
        //- - - - - - - - - - - - - - - - - - - - - - - - - - - 

        // Create product
        $product =  Product::create([
            'name'          => $prdct_name,
            'sku'           => $request->input('sku'),
            'picture'       => $prdct_picture,
            'video'         => $prdct_video,
            'qr_text'       => $prdct_qrcode,
            'qr_image'      => $qrcode_url,
            'description'   => $prdct_description
        ]);

        // Attach client with this languages
        $product->langs()->sync($prdctlangdata); // Sync with name & description in different languages. 

        if($product) {
            return redirect()->to('/contents/products')->with('success','Congrats! New Product has been added successfully!');
        } else {
            return redirect()->to('/contents/products')->with('error','Sorry! Invalid Request.');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $product  = Product::where('id',$id)->first()->toArray();

        $products       = Product::where('id',$id)->first();
        $product_langs  = $products->langs()->get()->toArray();
        $productData            = $products->toArray();
        $productData['langs']   = $product_langs;
        return view('products.show')->with("product",$productData);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $product  = Product::where('id',$id)->first();
        $product_langs = $product->langs()->get()->toArray();
        $productData = $product->toArray();
        $productData['langs'] = [];
        if(count($product_langs) > 0){
            foreach($product_langs as $k=>$v) {
                $productData['langs'][$v['id']] = $v['pivot'];
            }
        }
        $langs = Lang::getEnabledLanguages();
        return view('products.edit')->with("product",$productData)->with("langs",$langs);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        // $dt = $request->all();
        // echo "<pre>"; print_r($dt); die;

        $product  = Product::where('id',$id)->first();
        $product_langs = $product->langs()->get()->toArray();
        $productData = $product->toArray();
        $productData['langs'] = [];
        if(count($product_langs) > 0){
            foreach($product_langs as $k=>$v) {
                $productData['langs'][$v['id']] = $v['pivot'];
            }
        }

        // Validate Fields
        $validateAry = [];
        $langdata = $request->input('langdata');
        $qrcode = $langdata['1']['prdct_qrcode'];
        $qrimage = $request->file('langdata_1_prdct_qrimage');  // For english only 
        if((empty($qrcode)) && (empty($qrimage))) {
            $validateAry['langdata_1_prdct_qrimage'] = ['required'];
        }
        $request->validate($validateAry);
     
        //- - - - - - - - NEED TO DELETE LATER - - - - - - - 
        $prdctlangdata = []; 
        $pld = $request->input('langdata');
        foreach($pld as $key=>$val) {
            $prdctlangdata[$key]['prdct_name']          = $val['prdct_name'];
            $prdctlangdata[$key]['prdct_qrcode']        = (isset($val['prdct_qrcode']))?$val['prdct_qrcode']:'';
            $prdctlangdata[$key]['prdct_description']   = (isset($val['prdct_description']))?$val['prdct_description']:'';
            $removePicture  = $request->input('langdata_'.$key.'_remove_picture');
            $removeVideo    = $request->input('langdata_'.$key.'_remove_video');

            // Product Picture
                $pictureFile    = $request->file('langdata_'.$key.'_prdct_picture'); 
                $picture_url = (isset($productData['langs'][$key]['prdct_picture']))?$productData['langs'][$key]['prdct_picture']:'';
                if($removePicture == '1') {
                    $picture_url = '';
                } else {
                    if(!empty($pictureFile)) {
                        $pictureFileName = $pictureFile->getClientOriginalName();
                        $file_path = "products/images/".date('Ymdhis').'_'.$pictureFileName;
                        $s3 = Storage::disk('s3');
                        $s3->put($file_path, file_get_contents($pictureFile), array('ContentDisposition' => 'inline; filename=' . $pictureFileName . '', 'ACL' => 'public-read'));
                        $picture_url = Storage::disk('s3')->url($file_path);
                    }
                }
                $prdctlangdata[$key]['prdct_picture']     = $picture_url;

            // Product Video
                $videoFile    = $request->file('langdata_'.$key.'_prdct_video'); 
                $video_url = (isset($productData['langs'][$key]['prdct_video']))?$productData['langs'][$key]['prdct_video']:'';
                if($removeVideo == '1') {
                    $video_url = '';
                } else {
                    if(!empty($videoFile)) {
                        $videoFileName = $videoFile->getClientOriginalName();
                        $file_path = "products/videos/".date('Ymdhis').'_'.$videoFileName;
                        $s3 = Storage::disk('s3');
                        $s3->put($file_path, file_get_contents($videoFile), array('ContentDisposition' => 'inline; filename=' . $videoFileName . '', 'ACL' => 'public-read'));
                        $video_url = Storage::disk('s3')->url($file_path);
                    }
                }
                $prdctlangdata[$key]['prdct_video']     = $video_url;
            
            // QR Code / Image
                $qrimagefile = $request->file('langdata_'.$key.'_prdct_qrimage');
                if(!empty($qrimagefile)) {
                    $rndnum = rand(999,9999);
                    $file_name = 'QR_'.date('Y_m_d_h_i_s').'_'.$rndnum.'.png';
                    $file_path = "blocks/qrcode/".$file_name;
                    $s3 = Storage::disk('s3');
                    $s3->put($file_path, $qrimagefile, array('ContentDisposition' => 'inline; filename=' . $file_name . '', 'ACL' => 'public-read'));
                    $qrcode_url = Storage::disk('s3')->url($file_path);
                } else {
                    if(isset($val['prdct_qrcode'])) {
                        // Generate QR Code Image
                        $barcode = new \Com\Tecnick\Barcode\Barcode();
                        $qrCodeData = $val['prdct_qrcode'];
                        $QRCodeImg = $barcode->getBarcodeObj('QRCODE,H', $qrCodeData, - 16, - 16, 'black', array(- 2,- 2,- 2,- 2))->setBackgroundColor('#fff')->getPngData();
                        $rndnum = rand(999,9999);
                        $file_name = 'QR_'.date('Y_m_d_h_i_s').'_'.$rndnum.'.png';
                        $file_path = "blocks/qrcode/".$file_name;
                        $s3 = Storage::disk('s3');
                        $s3->put($file_path, $QRCodeImg, array('ContentDisposition' => 'inline; filename=' . $file_name . '', 'ACL' => 'public-read'));
                        $qrcode_url = Storage::disk('s3')->url($file_path);
                    } else {
                        $qrcode_url = '';
                    }
                }
            $prdctlangdata[$key]['prdct_qrimg']     = $qrcode_url;
        }
        //echo "<pre>"; print_r($prdctlangdata); die;
        //- - - - - - - - ENGLISH FIELDS - - - - - - - 
        $prdct_name             = $prdctlangdata['1']['prdct_name'];
        $prdct_qrcode           = $prdctlangdata['1']['prdct_qrcode'];
        $prdct_description      = $prdctlangdata['1']['prdct_description'];
        $prdct_picture          = $prdctlangdata['1']['prdct_picture'];
        $prdct_video            = $prdctlangdata['1']['prdct_video'];
        $prdct_qrimg            = $prdctlangdata['1']['prdct_qrimg'];
        //- - - - - - - - - - - - - - - - - - - - - - - - - 
        //- - - - - - - - - - - - - - - - - - - - - - - - - - - 

        $product = Product::find($id);
        $product->name          = $prdct_name;
        $product->picture       = $prdct_picture;
        $product->video         = $prdct_video;
        $product->qr_text       = $prdct_qrcode;
        $product->qr_image      = $prdct_qrimg;
        $product->description   = $prdct_description;
        $product->save();
        
        // Attach client with this languages
        $product->langs()->sync($prdctlangdata); // Sync with name & description in different languages. 

        return redirect('/contents/products')->with('success', 'Product has been updated successfully!');

    }
 
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $product  = Product::find($id);
        $contentData = Content::where('product_id',$id)->get()->toArray();
        if(count($contentData) == 0 ) {
            $product->delete();
            return redirect('/contents/products')->with('success', 'Product has been deleted successfully.');
        } else {
            return redirect('/contents/products')->with('error', 'Sorry, You can\'t delete this product. This product is associated with content.' );
        }
    }
}