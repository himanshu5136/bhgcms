<?php

namespace App\Http\Controllers;

use App\User;
use App\Tag;
use App\Content;
use App\Client;
use App\Csection;
use App\Author;
use App\Product;
use App\Lang;
use App\Variable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\FilesystemManager;
use ZipArchive;
use BaconQrCode\Encoder\QrCode;

class ContentController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()  {
        $cnm = request()->query('cnm'); // Content Title
        $sts = request()->query('sts'); // Content Status
        $tid = request()->query('tid'); // Content Category
        
        $condition = [];
        if(!empty($cnm)){ $condition[] = ['title', 'like', '%'.$cnm.'%']; }
        if(!empty($sts)){ $condition[] = ['status', '=', $sts]; }

        $query = Content::where($condition);
        if(!empty($tid)) {
            $query = $query->whereHas('tags', function ($q) use ($tid) {
                return $q->where('id', $tid);
            });
        }       
        // ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        // Condition for ADMIN User Role
        $userRole = auth()->user()->getRole();
        if ($userRole == 'admin') {  
            $userTags 		 = auth()->user()->getTags();            
            $userTagsId = [];
            foreach($userTags as $key=>$val) {
                $userTagsId[] = $val['id'];
            }
            $query = $query->whereHas('tags', function ($q) use ($userTagsId) {
                return $q->whereIn('id', $userTagsId ); // Check in assigned Clients of current user
            });
        }
        // ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        
        $contents = $query->orderBy('created_at', 'desc')->paginate(10);
        $contentData = [];
        foreach($contents as $content) {
            $tempData = [];
            $content_tag = $content->tags()->get()->toArray();
            $tempData = $content->toArray();
            $tempData['tags'] = $content_tag;
            $contentData[] = $tempData;
        }
        
        $tags = Tag::all()->toArray();
        $status  = ['1'=>'Published','2'=>'Unpublished'];
        $ctype      = [''=>'-','zip'=>'Zipped','image'=>'Image','video'=>'Video','product'=>'Product'];
        //echo "<pre>"; print_r($contentData); die;
        $qry_data = ['cnm'=>$cnm,'sts'=>$sts,'tid'=>$tid];
        return view('contents.index')
        ->with("paginate",$contents->appends($qry_data))
        ->with("contents",$contentData)
        ->with("status",$status)
        ->with("ctype",$ctype)
        ->with("tags",$tags)
        ->with('qry',$qry_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $tags       = Tag::all()->toArray();
        $authors    = Author::all()->toArray();
        $products   = Product::all()->toArray();
        $status     = ['1'=>'Published','2'=>'Unpublished'];
        $ctype      = ['zip'=>'Zipped','image'=>'Image','video'=>'Video','product'=>'Product'];
        $langs      = Lang::getEnabledLanguages();
        return view('contents.create')
        ->with("tags",$tags)
        ->with("ctype",$ctype)
        ->with("langs",$langs)
        ->with('authors',$authors)
        ->with('products',$products)
        ->with("status",$status);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)  {
        // $dt = $request->all();
        // echo "<pre>"; print_r($dt); die; 

        // Validate Fields
        $validateAry = [
            'status' => ['required'],
            'ctype' => ['required'],
            'tag' => ['required'],
            //'title' => ['required','string','max:512'],
            // 'author' => ['required'],
            //'body' => ['required'],
        ];  
        $content_type = $request->input('ctype');
        // if(in_array($content_type,['zip','image','video'])) {
        //     //$validateAry['attachment'] = ['required'];
        // } else if($content_type == 'product') {
        //     $validateAry['product'] = ['required'];
        // }
        $request->validate($validateAry);


        //- - - - - - - - NEED TO DELETE LATER - - - - - - - 
        $ctype = $request->input('ctype'); // Content Type
        $cntntzipdata = [];
        $cntntlangdata = []; 
        $ald = $request->input('langdata');
        foreach($ald as $key=>$val) {
            $cntntlangdata[$key]['cntnt_title']     = (isset($val['cntnt_title']))?$val['cntnt_title']:'';
            $cntntlangdata[$key]['cntnt_author_id'] = (isset($val['cntnt_author_id']))?$val['cntnt_author_id']:'0';
            $cntntlangdata[$key]['cntnt_body']      = (isset($val['cntnt_body']))?$val['cntnt_body']:'';
            $cntntlangdata[$key]['cntnt_product_id'] = ((isset($val['cntnt_product_id'])) && ($ctype == 'product'))?$val['cntnt_product_id']:'0';

            // Upload Background Images & Thumbnail Images
            $bg_images_ary = (!empty($request->file('langdata_'.$key.'_bg_images')))?$request->file('langdata_'.$key.'_bg_images'):[];
            $bg_images_result = [];
            if(count($bg_images_ary) > 0){
                foreach($bg_images_ary as $k=>$v) {
                    $bg_image_attachment = '';
                    $file_name = $v->getClientOriginalName();
                    $file_path = "background/".date('Ymdhis').'_'.$file_name;
                    $s3 = Storage::disk('s3');
                    $s3->put($file_path, file_get_contents($v), array('ContentDisposition' => 'inline; filename=' . $file_name . '', 'ACL' => 'public-read'));
                    $bg_image_attachment = Storage::disk('s3')->url($file_path);
                    $bg_images_result[] = $bg_image_attachment;
                }
            }
            $cntntlangdata[$key]['cntnt_bg_images']     = serialize($bg_images_result);

            // Thumbnail Images
            $tn_images_ary = (!empty($request->file('langdata_'.$key.'_tn_images')))?$request->file('langdata_'.$key.'_tn_images'):[];
            $tn_images_result = [];
            if(count($tn_images_ary) > 0) {
                foreach($tn_images_ary as $k=>$v) {
                    $tn_image_attachment = '';
                    $file_name = $v->getClientOriginalName();
                    $file_path = "thumbnail/".date('Ymdhis').'_'.$file_name;
                    $s3 = Storage::disk('s3');
                    $s3->put($file_path, file_get_contents($v), array('ContentDisposition' => 'inline; filename=' . $file_name . '', 'ACL' => 'public-read'));
                    $tn_image_attachment = Storage::disk('s3')->url($file_path);
                    $tn_images_result[] = $tn_image_attachment;
                }
            }
            $cntntlangdata[$key]['cntnt_tn_images']     = serialize($tn_images_result);

            
            if(in_array($ctype,['zip','image','video'])) {  // Only if Content type is of Zip/ Image/ Video
                $attachment_file = $request->file('langdata_'.$key.'_attachment');
                $attachment_url = '';
                $is_this_zip    = '0';
                $zip_files      = '';
                if(!empty($attachment_file)) {
                    $file_mime_type = $attachment_file->getMimeType();
                    $file_name = $attachment_file->getClientOriginalName();
                    if($file_mime_type == 'application/zip') {  // Check for ZIp Content
                        $base_url  = env('APP_URL').'/storage/app/public';
                        $storagePublic = Storage::disk('public');
                        $filePath = $storagePublic->putFile('file', $attachment_file);
                        $fpath = storage_path('app/public/').$filePath;
                        //echo "Path : ".$fpath;  die; 
                        $zip = new ZipArchive();
                        if ($zip->open($fpath)) {
                            $prefix_text = date('Y_m_d_h_i_s_');
                            $dirName = storage_path('app/public/content/'.$prefix_text.str_replace('.zip','',$file_name));
                            if (!file_exists($dirName)) {
                                mkdir($dirName, 0775);
                            } else {
                                ContentController::deleteAll($dirName);
                            }
                            $zip->extractTo($dirName);
                            $zip->close();
                            $all_files = ContentController::scanFullDir($dirName); 
                            $root_files = ContentController::scanRootFiles($dirName);
                            $res = [];
                            $paths = [];
                            foreach($all_files as $k=>$fl) {
                                $s3_file_path_ary = [];
                                //$res[$k]['old_path']   = $fl;
                                $local_file_path    = storage_path('app/public/content'.'/'.$prefix_text.str_replace('.zip','',$file_name)).'/';
                                //$res[$k]['lcl_dir']   = $local_file_path;
                                $s3_file_path_ary   = str_replace($local_file_path,'',$fl);
                                //$res[$k]['new_path']   = 'content/'.$prefix_text.str_replace('\\','/',$s3_file_path_ary);
                                $local_path         = $fl;
                                $s3_path            = 'content/'.$prefix_text.$s3_file_path_ary;
                                $explode_s3_path    = explode('/',$s3_path);
                                $s3_file_name       = $explode_s3_path[count($explode_s3_path)-1];
                                //$res[$k]['s3_name'] = $s3_file_name;
                                //$res[$k]['s3_path'] = 'content/'.$prefix_text.str_replace('.zip','',$file_name);
                                $s3                = Storage::disk('s3');
                                $s3->put($s3_path, file_get_contents($local_path), array('ContentDisposition' => 'inline; filename=' . $s3_file_name . '', 'ACL' => 'public-read'));
                                //$res[$k]['path'] = Storage::disk('s3')->url($s3_path);
                                $paths[$k][] = Storage::disk('s3')->url($s3_path);
                            }
                            if(!empty($paths)) {
                                ContentController::deleteAll($dirName); // Remove extracted files
                                $attachment_url  =  'https://borderlesshealthbkt.s3.ap-southeast-1.amazonaws.com/content/'.$prefix_text.str_replace('.zip','',$file_name);
                                $is_this_zip = '1';
                                $zip_files = base64_encode(json_encode($root_files));
                            } else {
                                return redirect()->back()->with('error','Invalid S3 Upload request');    
                            }
                        // Delete locally uploaded zip file
                        unlink($fpath);
                        } else {
                            return redirect()->back()->with('error','Corrupted Zip File Attached.');
                        }            
                    } else {
                        $file_name = $attachment_file->getClientOriginalName();
                        $file_path = "content/".date('Ymdhis').'_'.$file_name;
                        $s3 = Storage::disk('s3');
                        $s3->put($file_path, file_get_contents($attachment_file), array('ContentDisposition' => 'attachment; filename=' . $file_name . '', 'ACL' => 'public-read'));
                        $attachment_url = Storage::disk('s3')->url($file_path);
                    }
                }
            } else {
                $attachment_url = '';
            }
            $cntntzipdata[$key]['is_this_zip']  = $is_this_zip;
            $cntntzipdata[$key]['zip_files']    = $zip_files;
            $cntntlangdata[$key]['cntnt_attachment'] = $attachment_url;
        }
        //- - - - - - - - ENGLISH FIELDS - - - - - - - 
        $cntnt_title        = $cntntlangdata['1']['cntnt_title'];
        $cntnt_bg_images    = $cntntlangdata['1']['cntnt_bg_images'];
        $cntnt_tn_images    = $cntntlangdata['1']['cntnt_tn_images'];
        $cntnt_body         = $cntntlangdata['1']['cntnt_body'];
        $cntnt_author_id    = $cntntlangdata['1']['cntnt_author_id'];
        $cntnt_product_id   = $cntntlangdata['1']['cntnt_product_id'];
        $cntnt_attachment   = $cntntlangdata['1']['cntnt_attachment'];
        //- - - - - - - - - - - - - - - - - - - - - - - - - 
        //- - - - - - - - - - - - - - - - - - - - - - - - - 

        // Attachment Field
        // $file = $request->file('attachment');
        // $attachment_url = '';
        // $is_this_zip    = '0';
        // $zip_files      = '';
            
        // Generate GUID
        $contentGuid = ContentController::generateContentGuid();
        
        // Create Content
        $content =  Content::create([
            'user_id'           => auth()->user()->id,
            'title'             => $cntnt_title, //$request->input('title'),
            'body'              => $cntnt_body, //$request->input('body'),
            'attachment'        => $cntnt_attachment,
            'status'            => $request->input('status'),
            'guid'              => $contentGuid,
            'is_this_zip'       => $cntntzipdata['1']['is_this_zip'], //$is_this_zip,
            'zip_files'         => $cntntzipdata['1']['zip_files'], //$zip_files,
            'bg_images'         => $cntnt_bg_images, //serialize($bg_images_result),
            'tn_images'         => $cntnt_tn_images, //serialize($tn_images_result),
            'ctype'             => $request->input('ctype'),
            'author_id'         => $cntnt_author_id, //$request->input('author'),
            'product_id'        => $cntnt_product_id, //$request->input('product'),
        ]);
        // Attach category with this content
        $content_tag = $request->input('tag');
        $content->tags()->sync($content_tag); // Sync with categories

        // Attach content with this languages
        $content->langs()->sync($cntntlangdata); // Sync with fields values in different languages. 

        $role = auth()->user()->getRole();
        $role_segment = ($role == 'admin')?'/admin':'';
        if($content) {
            return redirect()->to($role_segment.'/contents')->with('success','Congrats! New content has been added successfully!');
        } else {
            return redirect()->to($role_segment.'/contents')->with('error','Sorry! Invalid Request.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $contents  = Content::where('id',$id)->first();
        $content_tags = $contents->tags()->get()->toArray();
        $content_langs = $contents->langs()->get()->toArray();
        $contentData = $contents->toArray();
        $contentData['tags'] = $content_tags;
        $contentData['langs'] = $content_langs;
        // All Contents
        $allContents    = Content::all()->toArray();
        $allContentsData = [];
        foreach($allContents as $k=>$v){
            $allContentsData[$v['id']] = $v;
        }
        // All Authors
        $authors    = Author::all()->toArray();
        $authorsData = [];
        foreach($authors as $k=>$v){
            $authorsData[$v['id']] = $v;
        }
        // All Products
        $products   = Product::all()->toArray();
        $productssData = [];
        foreach($products as $k=>$v){
            $productssData[$v['id']] = $v;
        }
        $statusAry  = ['1'=>'Published','2'=>'Unpublished'];
        $ctype      = ['zip'=>'Zipped','image'=>'Image','video'=>'Video','product'=>'Product'];
        $contentData['status'] = $statusAry[$contentData['status']];
        $userProfiles = User::allProfilesByUid();
        $contentData['user_full_name'] = $userProfiles[$contentData['user_id']];
        //echo "<pre>"; print_r($contentData); die;
        return view('contents.show')->with("content",$contentData)->with("ctype",$ctype)->with("authors",$authorsData)->with("products",$productssData)->with("allcontents",$allContentsData);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $content  = Content::where('id',$id)->first();
        $content_tags = $content->tags()->get()->toArray();
        $content_langs = $content->langs()->get()->toArray();
        $contentData = $content->toArray();
        $contentData['tags'] = [];
        if(count($content_tags) > 0){
            foreach($content_tags as $k=>$v) {
                $contentData['tags'][] = $v['id'];
            }
        }
        $contentData['langs'] = [];
        if(count($content_langs) > 0){
            foreach($content_langs as $k=>$v) {
                $contentData['langs'][$v['id']] = $v['pivot'];
            }
        }
         // All Contents
         $allContents    = Content::all()->toArray();
         $allContentsData = [];
         foreach($allContents as $k=>$v){
             $allContentsData[$v['id']] = $v;
         }
         // All Authors
         $authors    = Author::all()->toArray();
         $authorsData = [];
         foreach($authors as $k=>$v){
             $authorsData[$v['id']] = $v;
         }
         // All Products
         $products   = Product::all()->toArray();
         $productssData = [];
         foreach($products as $k=>$v){
             $productssData[$v['id']] = $v;
         }
        $tags = Tag::all()->toArray();
        $status  = ['1'=>'Published','2'=>'Unpublished'];
        $ctype      = ['zip'=>'Zipped','image'=>'Image','video'=>'Video','product'=>'Product'];
        $langs      = Lang::getEnabledLanguages();
        return view('contents.edit')
        ->with("content",$contentData)
        ->with("status",$status)
        ->with('tags',$tags)
        ->with('langs',$langs)
        ->with("ctype",$ctype)
        ->with("authors",$authorsData)
        ->with("products",$productssData)
        ->with("allcontents",$allContentsData);;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)  {
        // $dt = $request->all();
        // echo "<pre>"; print_r($dt); die;

        $validateAry = [
            'status' => ['required'],
            'ctype' => ['required'],
            'tag' => ['required'],
            //'title' => ['required','string','max:512'],
            // 'author' => ['required'],
            //'body' => ['required'],
        ];  
        //$content_type = $request->input('ctype');
        // if(in_array($content_type,['zip','image','video'])) {
        //     //$validateAry['attachment'] = ['required'];
        // } else if($content_type == 'product') {
        //     $validateAry['product'] = ['required'];
        // }
        $request->validate($validateAry);


        //- - - - - - - - NEED TO DELETE LATER - - - - - - - 
        $ctype = $request->input('ctype'); // Content Type
        $cntntzipdata = [];
        $cntntlangdata = []; 
        $ald = $request->input('langdata');
        foreach($ald as $key=>$val) {
            $is_this_zip    = '0';
            $zip_files      = '';
            $cntntlangdata[$key]['cntnt_title']     = (isset($val['cntnt_title']))?$val['cntnt_title']:'';
            $cntntlangdata[$key]['cntnt_author_id'] = (isset($val['cntnt_author_id']))?$val['cntnt_author_id']:'';
            $cntntlangdata[$key]['cntnt_body']      = (isset($val['cntnt_body']))?$val['cntnt_body']:'';
            $cntntlangdata[$key]['cntnt_product_id'] = ((isset($val['cntnt_product_id'])) && ($ctype == 'product'))?$val['cntnt_product_id']:'0';

            // Upload Background Images & Thumbnail Images
            $bg_images_result = [];
            for($i=0; $i<1; $i++) { 
                $background_image_url = '';
                $remove_picture = $request->get('remove_'.$key.'_bg_image_'.$i);
                if($remove_picture == '1') {
                    $background_image_url = '';
                } else {
                    $background_image_url = $request->get('langdata_'.$key.'_bg_images_hdn_url_'.$i);
                }

                $bg_image_file = $request->file('langdata_'.$key.'_bg_image_'.$i);
                if(!empty($bg_image_file)) {
                    $file_name = $request->file('langdata_'.$key.'_bg_image_'.$i)->getClientOriginalName();
                    $file_path = "background/".date('Ymdhis').'_'.$file_name; 
                    $s3 = Storage::disk('s3');
                    $s3->put($file_path, file_get_contents($bg_image_file), array('ContentDisposition' => 'inline; filename=' . $file_name . '', 'ACL' => 'public-read'));
                    $background_image_url = Storage::disk('s3')->url($file_path);
                }
                if($background_image_url != '') {
                    $bg_images_result[] = $background_image_url;
                }
            }
            $cntntlangdata[$key]['cntnt_bg_images']     = serialize($bg_images_result);

            // Upload THUMBNAIL Images & Thumbnail Images
            $tn_images_result = [];
            for($i=0; $i<1; $i++) { 
                $thumbnail_image_url = '';
                $remove_picture = $request->get('remove_'.$key.'_tn_image_'.$i);
                if($remove_picture == '1') {
                    $thumbnail_image_url = '';
                } else {
                    $thumbnail_image_url = $request->get('langdata_'.$key.'_tn_images_hdn_url_'.$i);
                }
                $tn_image_file = $request->file('langdata_'.$key.'_tn_image_'.$i);
                if(!empty($tn_image_file)) {
                    $file_name = $request->file('langdata_'.$key.'_tn_image_'.$i)->getClientOriginalName();
                    $file_path = "thumbnail/".date('Ymdhis').'_'.$file_name; 
                    $s3 = Storage::disk('s3');
                    $s3->put($file_path, file_get_contents($tn_image_file), array('ContentDisposition' => 'inline; filename=' . $file_name . '', 'ACL' => 'public-read'));
                    $thumbnail_image_url = Storage::disk('s3')->url($file_path);
                }
                if($thumbnail_image_url != '') {
                    $tn_images_result[] = $thumbnail_image_url;
                }
            }
            $cntntlangdata[$key]['cntnt_tn_images']     = serialize($tn_images_result);

            // ATTACHMENT
            if(in_array($ctype,['zip','image','video'])) {  // Only if Content type is of Zip/ Image/ Video
                $attachment_url = '';
                $remove_attachment = $request->get('remove_'.$key.'_attachment');
                if($remove_attachment == '1') {
                    $attachment_url = '';
                    $is_this_zip    = '0';
                    $zip_files      = '';
                } else {
                    $attachment_url = $request->get('langdata_'.$key.'_attachment_hdn_url');
                    $is_this_zip    = '0';
                    $zip_files      = '';
                }
                
                $attachment_file = $request->file('langdata_'.$key.'_attachment');
                if(!empty($attachment_file)) {
                    $file_mime_type = $attachment_file->getMimeType();
                    $file_name = $attachment_file->getClientOriginalName();
                    if($file_mime_type == 'application/zip') {  // Check for ZIp Content
                        $base_url  = env('APP_URL').'/storage/app/public';
                        $storagePublic = Storage::disk('public');
                        $filePath = $storagePublic->putFile('file', $attachment_file);
                        $fpath = storage_path('app/public/').$filePath;
                        //echo "Path : ".$fpath;  die; 
                        $zip = new ZipArchive();
                        if ($zip->open($fpath)) {
                            $prefix_text = date('Y_m_d_h_i_s_');
                            $dirName = storage_path('app/public/content/'.$prefix_text.str_replace('.zip','',$file_name));
                            if (!file_exists($dirName)) {
                                mkdir($dirName, 0775);
                            } else {
                                ContentController::deleteAll($dirName);
                            }
                            $zip->extractTo($dirName);
                            $zip->close();
                            $all_files = ContentController::scanFullDir($dirName); 
                            $root_files = ContentController::scanRootFiles($dirName);
                            $res = [];
                            $paths = [];
                            foreach($all_files as $k=>$fl) {
                                $s3_file_path_ary = [];
                                //$res[$k]['old_path']   = $fl;
                                $local_file_path    = storage_path('app/public/content'.'/'.$prefix_text.str_replace('.zip','',$file_name)).'/';
                                //$res[$k]['lcl_dir']   = $local_file_path;
                                $s3_file_path_ary   = str_replace($local_file_path,'',$fl);
                                //$res[$k]['new_path']   = 'content/'.$prefix_text.str_replace('\\','/',$s3_file_path_ary);
                                $local_path         = $fl;
                                $s3_path            = 'content/'.$prefix_text.$s3_file_path_ary;
                                $explode_s3_path    = explode('/',$s3_path);
                                $s3_file_name       = $explode_s3_path[count($explode_s3_path)-1];
                                //$res[$k]['s3_name'] = $s3_file_name;
                                //$res[$k]['s3_path'] = 'content/'.$prefix_text.str_replace('.zip','',$file_name);
                                $s3                = Storage::disk('s3');
                                $s3->put($s3_path, file_get_contents($local_path), array('ContentDisposition' => 'inline; filename=' . $s3_file_name . '', 'ACL' => 'public-read'));
                                //$res[$k]['path'] = Storage::disk('s3')->url($s3_path);
                                $paths[$k][] = Storage::disk('s3')->url($s3_path);
                            }
                            if(!empty($paths)) {
                                ContentController::deleteAll($dirName); // Remove extracted files
                                $attachment_url  =  'https://borderlesshealthbkt.s3.ap-southeast-1.amazonaws.com/content/'.$prefix_text.str_replace('.zip','',$file_name);
                                $is_this_zip = '1';
                                $zip_files = base64_encode(json_encode($root_files));
                            } else {
                                return redirect()->back()->with('error','Invalid S3 Upload request');    
                            }
                        // Delete locally uploaded zip file
                        unlink($fpath);
                        } else {
                            return redirect()->back()->with('error','Corrupted Zip File Attached.');
                        }            
                    } else {
                        $file_name = $attachment_file->getClientOriginalName();
                        $file_path = "content/".date('Ymdhis').'_'.$file_name;
                        $s3 = Storage::disk('s3');
                        $s3->put($file_path, file_get_contents($attachment_file), array('ContentDisposition' => 'attachment; filename=' . $file_name . '', 'ACL' => 'public-read'));
                        $attachment_url = Storage::disk('s3')->url($file_path);
                    }
                }
            } else {
                $attachment_url = '';
            }
            $cntntzipdata[$key]['is_this_zip']  = $is_this_zip;
            $cntntzipdata[$key]['zip_files']    = $zip_files;
            $cntntlangdata[$key]['cntnt_attachment'] = $attachment_url;
        }
        //- - - - - - - - ENGLISH FIELDS - - - - - - - 
        $cntnt_title        = $cntntlangdata['1']['cntnt_title'];
        $cntnt_bg_images    = $cntntlangdata['1']['cntnt_bg_images'];
        $cntnt_tn_images    = $cntntlangdata['1']['cntnt_tn_images'];
        $cntnt_body         = $cntntlangdata['1']['cntnt_body'];
        $cntnt_author_id    = $cntntlangdata['1']['cntnt_author_id'];
        $cntnt_product_id   = $cntntlangdata['1']['cntnt_product_id'];
        $cntnt_attachment   = $cntntlangdata['1']['cntnt_attachment'];
        //- - - - - - - - - - - - - - - - - - - - - - - - - 
        //- - - - - - - - - - - - - - - - - - - - - - - - -

            // Create Content
            $content = Content::find($id);
            $content->title         = $cntnt_title; //$request->get('title');
            $content->body          = $cntnt_body; //$request->get('body');
            $content->attachment    = $cntnt_attachment;
            $content->status        = $request->get('status');
            $content->bg_images     = $cntnt_bg_images; //serialize($bg_images_result);
            $content->tn_images     = $cntnt_tn_images; //serialize($tn_images_result);
            $content->ctype         = $request->get('ctype');
            $content->author_id     = $cntnt_author_id; //$request->get('author');
            $content->product_id    = $cntnt_product_id;
            $contentAry = $content->toArray();
            if(empty($contentAry['guid'])) {  // Generate GUID
                $content->guid = ContentController::generateContentGuid();
            }
            $content->is_this_zip   = $cntntzipdata['1']['is_this_zip']; //$is_this_zip,
            $content->zip_files     = $cntntzipdata['1']['zip_files']; //$zip_files, json_encode($zip_files);
            $content->save();
            
            // Attach category with this content
            $content_tag = $request->input('tag');
            $content->tags()->sync($content_tag); // Sync with categories

            // Attach content with this languages
            $content->langs()->sync($cntntlangdata); // Sync with fields values in different languages. 

            $role = auth()->user()->getRole();
            $role_segment = ($role == 'admin')?'/admin':'';
            if($content) {
                return redirect()->to($role_segment.'/contents')->with('success','Congrats! Content has been added updated!');
            } else {
                return redirect()->to($role_segment.'/contents')->with('error','Sorry! Invalid Request.');
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $role = auth()->user()->getRole();
        $role_segment = ($role == 'admin')?'/admin':'';
        
        $contentData = Content::where('id',$id)->first()->toArray();
        $content     = Content::find($id);
        $content->delete();
        return redirect($role_segment.'/contents')->with('success', 'Content "'.$contentData['title'].'" has been deleted successfully.');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ide() {
        $clients = Client::all()->toArray();
        $ide_client = Variable::where('key','ide_client')->first('value');
        $ide['client']  = (!empty($ide_client))?$ide_client->toArray() :[];

        $ide_images = Variable::where('key','ide_images')->first('value');
        $ide['images']  = (!empty($ide_images))?$ide_images->toArray() :[];
        
        return view('contents.ide')->with("ide", $ide)->with("clients",$clients);
    }

    public function ide_store(Request $request)  {
        // Validate Fields
        $validateAry = [
            'ide_client' => ['required']
        ];  
        $request->validate($validateAry);
        $uploaded_files = [];
        for($i=1;$i<=4; $i++) {
            $file = $request->file('ide_image_'.$i);
            $attachment_url = '';
            
            $rmv_img = $request->input('ide_rmv_image_'.$i);
            if(empty($rmv_img)) {
                if(!empty($file)) {
                    $file_name = $request->file('ide_image_'.$i)->getClientOriginalName();
                    $file_path = "images/".date('Ymdhis').'_'.$file_name;
                    $s3 = Storage::disk('s3');
                    $s3->put($file_path, file_get_contents($file), array('filename=' . $file_name . '', 'ACL' => 'public-read'));
                    $uploaded_files[] = Storage::disk('s3')->url($file_path);
                } else {
                    $hdn_path = $request->input('hidden_ide_image_'.$i);
                    if(!empty($hdn_path)){
                        $uploaded_files[] = $hdn_path;
                    }
                    
                }
            }
        }
        // Save Update IDE_Client
        $ide_client  = Variable::where('key','ide_client')->first();
        if(empty($ide_client)){
            $variable =  Variable::create([
                'key' => 'ide_client',
                'value' => $request->input('ide_client')
            ]);
        } else {
            $variable = Variable::where('key','ide_client')->first();
            $variable->value = $request->input('ide_client');
            $variable->save();
        }
        // Save Update IDE_Images
        $ide_images  = Variable::where('key','ide_images')->first();
        if(empty($ide_images)){
            $variable =  Variable::create([
                'key' => 'ide_images',
                'value' => (!empty($uploaded_files))?json_encode($uploaded_files):''
            ]);
        } else {
            $variable = Variable::where('key','ide_images')->first();
            $variable->value = (!empty($uploaded_files))?json_encode($uploaded_files):'';
            $variable->save();
        }
        if($variable) {
            return redirect()->to('/contents/ide')->with('success','Congrats! IDE has been updated successfully!');
        } else {
            return redirect()->to('/contents/ide')->with('error','Sorry! Invalid Request.');
        }
    }
     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function gde() {
        $clients = Client::all()->toArray();
        $gde_client = Variable::where('key','gde_client')->first('value');
        $gde['client']  = (!empty($gde_client))?$gde_client->toArray() :[];

        $gde_images = Variable::where('key','gde_images')->first('value');
        $gde['images']  = (!empty($gde_images))?$gde_images->toArray() :[];
        
        return view('contents.gde')->with("gde", $gde)->with("clients",$clients);
    }


    public function gde_store(Request $request)  {
        // Validate Fields
        $validateAry = [
            'gde_client' => ['required']
        ];  
        $request->validate($validateAry);
        $uploaded_files = [];
        for($i=1;$i<=4; $i++) {
            $file = $request->file('gde_image_'.$i);
            $attachment_url = '';
            
            $rmv_img = $request->input('gde_rmv_image_'.$i);
            if(empty($rmv_img)) {
                if(!empty($file)) {
                    $file_name = $request->file('gde_image_'.$i)->getClientOriginalName();
                    $file_path = "images/".date('Ymdhis').'_'.$file_name;
                    $s3 = Storage::disk('s3');
                    $s3->put($file_path, file_get_contents($file), array('filename=' . $file_name . '', 'ACL' => 'public-read'));
                    $uploaded_files[] = Storage::disk('s3')->url($file_path);
                } else {
                    $hdn_path = $request->input('hidden_gde_image_'.$i);
                    if(!empty($hdn_path)){
                        $uploaded_files[] = $hdn_path;
                    }
                    
                }
            }
        }
        // Save Update GDE_Client
        $gde_client  = Variable::where('key','gde_client')->first();
        if(empty($gde_client)){
            $variable =  Variable::create([
                'key' => 'gde_client',
                'value' => $request->input('gde_client')
            ]);
        } else {
            $variable = Variable::where('key','gde_client')->first();
            $variable->value = $request->input('gde_client');
            $variable->save();
        }
        // Save Update GDE_Images
        $gde_images  = Variable::where('key','gde_images')->first();
        if(empty($gde_images)){
            $variable =  Variable::create([
                'key' => 'gde_images',
                'value' => (!empty($uploaded_files))?json_encode($uploaded_files):''
            ]);
        } else {
            $variable = Variable::where('key','gde_images')->first();
            $variable->value = (!empty($uploaded_files))?json_encode($uploaded_files):'';
            $variable->save();
        }
        if($variable) {
            return redirect()->to('/contents/gde')->with('success','Congrats! GDE has been updated successfully!');
        } else {
            return redirect()->to('/contents/gde')->with('error','Sorry! Invalid Request.');
        }
    }



    // Delete all files & folders of a Directory
    function deleteAll($str) { 
        if (is_file($str)) {               
           return unlink($str); 
       } 
       elseif (is_dir($str)) {     
           $scan = glob(rtrim($str, '/').'/*'); 
            foreach($scan as $index=>$path) { 
                ContentController::deleteAll($path); 
           } 
            return @rmdir($str); 
       }
   }

   // Fetch list of all files
   function scanFullDir($dir, &$results = array()) {
       $files = scandir($dir);
       foreach ($files as $key => $value) {
           $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
           if (!is_dir($path)) {
               $results[] = $path;
           } else if ($value != "." && $value != "..") {
            ContentController::scanFullDir($path, $results);
               //$results[] = $path;
           }
       }
   
       return $results;
    }

    // Scan Root Files Only
    function scanRootFiles($dir) {
        $results = [];
        $files = scandir($dir);
        foreach ($files as $key => $value) {
            $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
            if (!is_dir($path)) {
                if(!in_array($value,['.DS_Store'])){ 
                    $results[] = $value;
                }
            }
        }
        return $results;
     }
     

    function generateContentGuid() { // GUID (Globally Unique Identifier)
        $guid = '';
        $namespace = rand(11111, 99999);
        $uid = uniqid('', true);
        $data = $namespace;
        $data .= $_SERVER['REQUEST_TIME'];
        $data .= $_SERVER['HTTP_USER_AGENT'];
        $data .= $_SERVER['REMOTE_ADDR'];
        $data .= $_SERVER['REMOTE_PORT'];
        $data .= date('ymdhis');
        $hash = strtoupper(hash('ripemd128', $uid . $guid . md5($data)));
        $guid = substr($hash,  0,  8) . '-' .
                substr($hash,  8,  4) . '-' .
                substr($hash, 12,  4) . '-' .
                substr($hash, 16,  4) . '-' .
                substr($hash, 20, 12);
        $checkGuid  = Content::where('guid',$guid)->first();//->toArray();
        if(!empty($checkGuid)) {
            return ContentController::generateContentGuid();
        } else {
            return $guid;
        }
    }
}
