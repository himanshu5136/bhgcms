<?php

namespace App\Http\Controllers;

use App\Tag;
use App\Lang;
use Illuminate\Http\Request;

class LangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $condition = [];
        $langs = Lang::where($condition)->orderByRaw('name ASC')->get()->toArray();
        foreach($langs as $key=>$val) {
            if($val['is_enable'] == '1'){
                $langsData['on'][] = $val;
            } else {
                $langsData['off'][] = $val;
            }
            
        }
        //echo "<pre>"; print_r($langsData); die;
        return view('langs.index')->with("langs",$langsData);
    }

   /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        // All Languages
        $cond = [];
        $cond[] = ['is_freeze', '=', '0']; // Skipping freez languages like english.
        $allLangs = Lang::where($cond)->get()->toArray();
        // get checked Languages
        $postLangs = [];
        $postLangs = $request->input('lang');
        foreach($allLangs as $key=>$val) {
            if(!empty($postLangs)){
                if(in_array($val['id'],$postLangs)) {
                    $is_enbl = '1' ;
                    $lang = Lang::find($val['id']);
                    $lang->is_enable = $is_enbl;
                    $lang->save();
                    unset($lang);
                } else {
                    if($val['is_enable'] == '1'){
                        $is_enbl = '0' ;
                        $lang1 = Lang::find($val['id']);
                        $lang1->is_enable = $is_enbl;
                        $lang1->save();
                        unset($lang1);
                    }
                }
            } else {
                if($val['is_enable'] == '1'){
                    $is_enbl = '0' ;
                    $lang1 = Lang::find($val['id']);
                    $lang1->is_enable = $is_enbl;
                    $lang1->save();
                    unset($lang1);
                }
            }
            
        }
        return redirect('/configs/langs')->with('success', 'Languages has been enabled successfully!');
    }

}
