<?php

namespace App\Http\Controllers;

use App\Content;
use App\Client;
use App\Cpage;
use App\Lang;
use App\Csection;
use App\Blockdata;  
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\FilesystemManager;

class CpageController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $pnm = request()->query('pnm');
        $ttl = request()->query('ttl');
        $pty = request()->query('pty');
        $clt = request()->query('clt');
        $condition = [];
        if(!empty($pnm)){ $condition[] = ['name', 'like', '%'.$pnm.'%']; }
        if(!empty($ttl)){ $condition[] = ['title', 'like', '%'.$ttl.'%']; }
        if(!empty($pty)){ $condition[] = ['ptype', '=', $pty]; }
        $query = CPage::where($condition);
        if(!empty($clt)) {
            $query = $query->whereHas('clients', function ($q) use ($clt) {
                return $q->where('id', $clt);
            });
        }    
        $cpages = $query->orderBy('created_at', 'desc')->paginate(10);
        $cpageData = [];
        foreach($cpages as $cpage) {
            $tempAry = [];
            $tempAry = $cpage->toArray();
            $tempAry['clients'] = $cpage->clients()->get()->toArray();
            $tempAry['csections'] = $cpage->csections()->get()->toArray();
            $cpages_langs = $cpage->langs()->get()->toArray();
            $tempAry['langs'] = [];
            if(count($cpages_langs) > 0){
                foreach($cpages_langs as $k=>$v) {
                    $tempAry['langs'][$v['id']] = $v['pivot'];
                    $tempAry['langs'][$v['id']]['lang_name'] = $v['name'];
                }
            }
            $cpageData[] = $tempAry;
        }
        //echo "<pre>"; print_r($cpageData); die;
        // Fetch All Clients for Filter
        $clients = Client::all()->toArray();
        foreach($clients as $k=>$v) {
            $clientsAry[$v['id']] = $v['name'];
        }
        $ptypes=['p1'=>'P1','p2'=>'P2','p3'=>'P3','p4'=>'P4'];
        $qry_data = ['pty'=>$pty,'pnm'=>$pnm,'ttl'=>$ttl,'clt'=>$clt];
        return view('cpages.index')
        ->with("paginate",$cpages->appends($qry_data))
        ->with("cpages",$cpageData)
        ->with("ptypes",$ptypes)
        ->with('clients',$clientsAry)
        ->with('page_title','List of Client Pages')
        ->with('qry',$qry_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $csections = Csection::all()->toArray();
        $clients = Client::all()->toArray();
        $langs = Lang::getEnabledLanguages();
        $ptypes=['p1'=>'P1','p2'=>'P2','p3'=>'P3','p4'=>'P4'];
        return view('cpages.create')->with('csections',$csections)->with('ptypes',$ptypes)->with('clients',$clients)->with('langs',$langs);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)  {
        // $dt = $request->all();
        // echo "<pre>"; print_r($dt); die;

        //- - - - - - - - NEED TO DELETE LATER - - - - - - - 
        $cpage_lang_data    = $request->input('langdata');
        $cpage_title        = $cpage_lang_data['1']['cpage_title'];
        $cpage_description  = $cpage_lang_data['1']['cpage_description'];
        //- - - - - - - - - - - - - - - - - - - - - - - - - 

        // Validate Fields
        $validateAry = [
            'name' => ['required','max:255'],
            'ptype' => ['required'],
            'client' => ['required'],
            'section' => ['required'],
        ];
        $request->validate($validateAry);

        // Create Page
        $cpage =  Cpage::create([
            'name'          => $request->input('name'),
            'title'         => $cpage_title, //$request->input('title'),
            'ptype'         => $request->input('ptype'),
            'description'   => $cpage_description //$request->input('description'),
        ]);

        // Attach clients with this page
        $cpage_client = $request->input('client');
        $cpage->clients()->sync($cpage_client); // Sync with clients

        // Attach clients with this page
        $cpage_section = $request->input('section');
        $cpage->csections()->sync($cpage_section); // Sync with clients

        // Attach Languages with this client page
        $cpage_lang = $request->input('langdata');
        $cpage->langs()->sync($cpage_lang); // Sync with name & description in different languages. 

        if($cpage) {
            return redirect()->to('/cpages')->with('success','Congrats! New client page has been added successfully!');
        } else {
            return redirect()->to('/cpages')->with('error','Sorry! Invalid Request.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $cpageAry = [];
        $cpage  = Cpage::where('id',$id)->first();
        $cpageAry = $cpage->toArray();
        $cpageAry['clients'] = $cpage->clients()->get()->toArray();
        $cpageAry['sections'] = $cpage->csections()->get()->toArray();
        $cpageAry['langs'] = $cpage->langs()->get()->toArray();
        $ptypes=['p1'=>'P1','p2'=>'P2','p3'=>'P3','p4'=>'P4'];
        return view('cpages.show')->with("cpage",$cpageAry)->with('ptypes',$ptypes);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)  {
        $cpage  = Cpage::where('id',$id)->first();
        $cpageAry = $cpage->toArray();
        // Page Clients
        $cpageAry['clients'] = [];
        $cpages_clients = $cpage->clients()->get()->toArray();
        foreach($cpages_clients as $k=>$v) {
            $cpageAry['clients'][] = $v['id'];
        }
        // Page Sections
        $cpageAry['sections'] = [];
        $cpages_csections = $cpage->csections()->get()->toArray();
        foreach($cpages_csections as $k=>$v) {
            $cpageAry['sections'][] = $v['id'];
        }
        // Page Language
        $cpageAry['langs'] = [];
        $cpages_langs = $cpage->langs()->get()->toArray();
        foreach($cpages_langs as $k=>$v) {
            $cpageAry['langs'][$v['id']] = $v['pivot'];
        }

        $csections = Csection::all()->toArray();
        $clients = Client::all()->toArray();
        $langs = Lang::getEnabledLanguages();
        $ptypes=['p1'=>'P1','p2'=>'P2','p3'=>'P3','p4'=>'P4'];
        return view('cpages.edit')
        ->with('cpage',$cpageAry)
        ->with('sections',$csections)
        ->with('ptypes',$ptypes)
        ->with('langs',$langs)
        ->with('clients',$clients);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        //- - - - - - - - NEED TO DELETE LATER - - - - - - - 
        $cpage_lang_data    = $request->input('langdata');
        $cpage_title        = $cpage_lang_data['1']['cpage_title'];
        $cpage_description  = $cpage_lang_data['1']['cpage_description'];
        //- - - - - - - - - - - - - - - - - - - - - - - - - 


        // Validate Fields
        $validateAry = [
            'name' => ['required','max:255'],
            'ptype' => ['required'],
            'client' => ['required'],
            'section' => ['required'],
        ];
        $request->validate($validateAry);

        // Update Page
        $cpage = Cpage::find($id);
        $cpage->name        = $request->get('name');
        $cpage->title       = $cpage_title; //$request->get('title');
        $cpage->ptype       = $request->get('ptype');
        $cpage->description = $cpage_description; //$request->get('description');
        $cpage->save();

        // Attach clients with this page
        $cpage_client = $request->input('client');
        $cpage->clients()->sync($cpage_client); // Sync with clients

        // Attach clients with this page
        $cpage_section = $request->input('section');
        $cpage->csections()->sync($cpage_section); // Sync with clients

        // Attach languages with this page
        $cpage_langs = $request->input('langdata');
        $cpage->langs()->sync($cpage_langs); // Sync with Languages Data

        if($cpage) {
            return redirect()->to('/cpages')->with('success','Congrats! Client page has been updated successfully!');
        } else {
            return redirect()->to('/cpages')->with('error','Sorry! Invalid Request.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $cpage  = Cpage::find($id);
        $cpage->delete();
        return redirect('/cpages')->with('success', 'Client page has been deleted successfully.');
    }
}
