<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Superusers;
use Illuminate\Support\Facades\Hash;

class SuperusersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $status = request()->query('status');
        $fname = request()->query('fname');
        $userData = Superusers::getuserData($fname,$status);
        return view('superusers.index')->with("users",$userData)->with('qry',['status'=>$status,'fname'=>$fname]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('superusers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)  {
        // Validate Fields
        $request->validate([
            'name' => ['required','string','max:255','unique:users'],
            'email' => ['required','string','email','max:255','unique:users'],
            'password' => ['required','string','min:8','confirmed'],
            'full_name' => ['required','string','max:255'],
            'status' => ['required']
        ]);
        // Create User
        $user =  User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
            'full_name' => $request->input('full_name'),
            'status' => $request->input('status'),
            'user_brief' => $request->input('user_brief'),
            'contact_number' => $request->input('contact_number'),
        ]);
        if($user) {
            return redirect()->to('/users')->with('success','Congrats! New user added successfully!');
        } else {
            return redirect()->to('/users')->with('error','Sorry! Invalid Request.');
        }
        // Response : {"name":"teamlead","email":"himanshu5136@gmail.com","full_name":"Team Lead","updated_at":"2020-07-26T06:07:03.000000Z","created_at":"2020-07-26T06:07:03.000000Z","id":2}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $userDetail = Superusers::getuserDetail($id);
        return view('superusers.show')->with("user",$userDetail);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)  {
        $user = Superusers::getuserDetail($id);
        return view('superusers.edit')->with("user",$user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)  {
        $validatedData = $request->validate([
            'full_name' => ['required','string','max:255'],
            'status' => ['required']
        ]);
        $usr = User::whereId($id)->update($validatedData);

        if($usr) {
            return redirect()->to('/users')->with('success','Congrats! User has been updated successfully!');
        } else {
            return redirect()->to('/users')->with('error','Sorry! Invalid Request.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)  {
        $user = User::find($id);
        $usr  = $user->delete();
        if($usr) {
            return redirect()->to('/users')->with('success','User has been removed successfully!');
        } else {
            return redirect()->to('/users')->with('error','Sorry! Invalid Request.');
        }
    }

}
