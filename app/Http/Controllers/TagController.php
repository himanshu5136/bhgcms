<?php

namespace App\Http\Controllers;

use App\User;
use App\Tag;
use App\Client;
use App\Lang;
use Illuminate\Http\Request;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $tnm = request()->query('tnm');
        $client_id = request()->query('cid');
        $condition = [];
        if(!empty($cnm)){ $condition[] = ['name', 'like', '%'.$tnm.'%']; }
        $query = Tag::where($condition);
        if(!empty($client_id)) {
            $query = $query->whereHas('clients', function ($q) use ($client_id) {
                return $q->where('id', $client_id);
            });
        }
        // ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        // Condition for ADMIN User Role
        $userRole = auth()->user()->getRole();
        if ($userRole == 'admin') {  
            $userClients = auth()->user()->getClients();
            $userClientsId = [];
            foreach($userClients as $key=>$val) {
                $userClientsId[] = $val['id'];
            }
            $query = $query->whereHas('clients', function ($q) use ($userClientsId) {
                return $q->whereIn('id', $userClientsId ); // Check in assigned Clients of current user
            });
        }
        // ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

        $tags = $query->orderBy('created_at', 'desc')->paginate(10); //get();
        $tagData = [];
        foreach($tags as $tag) {
            $tempData = [];
            $tag_client = $tag->clients()->get()->toArray();
            $tag_langs  = $tag->langs()->get()->toArray();
            $tempData = $tag->toArray();
            $tempData['clients'] = $tag_client;
            //$tempData['langs'] = $tag_langs;
            $tempData['langs'] = [];
            if(count($tag_langs) > 0){
                foreach($tag_langs as $k=>$v) {
                    $tempData['langs'][$v['id']] = $v['pivot'];
                    $tempData['langs'][$v['id']]['lang_name'] = $v['name'];
                }
            }
            $tagData[] = $tempData;
        }
        //echo "<pre>"; print_r($tagData); die;
        //$clients = Client::all()->toArray();
        $clients = auth()->user()->getClients();
        $qry_data = ['tnm'=>$tnm,'cid'=>$client_id];
        return view('tags.index')
        ->with("paginate",$tags->appends($qry_data))
        ->with("tags",$tagData)
        ->with("clients",$clients)
        ->with('qry',$qry_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $clients = Client::all()->toArray();
        $langs = Lang::getEnabledLanguages();
        return view('tags.create')->with("clients",$clients)->with("langs",$langs);;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        // $dt = $request->all();
        // echo "<pre>"; print_r($dt); die;
        
        //- - - - - - - - NEED TO DELETE LATER - - - - - - - 
        $tag_lang_data = $request->input('langdata');
        $tag_name           = $tag_lang_data['1']['tag_name'];
        $tag_description    = $tag_lang_data['1']['tag_description'];
        //- - - - - - - - - - - - - - - - - - - - - - - - - 


        $role = auth()->user()->getRole();
        $role_segment = ($role == 'admin')?'/admin':'';
        // Validate Fields
        $validateAry = [
            //'name' => ['required','string','max:255','unique:tags'],
            'client' => ['required']
        ];
        $request->validate($validateAry);
        // Create Category
        $tag =  Tag::create([
            'name' => $tag_name, //$request->input('name'),
            'description' => $tag_description //$request->input('description')
        ]);
        
        // Attach client with this category
        $tag_client = $request->input('client');
        $tag->clients()->sync($tag_client); // Sync with clients

        // Attach client with this languages
        $tag_lang = $request->input('langdata');
        $tag->langs()->sync($tag_lang); // Sync with name & description in different languages. 

        if($tag) {
            return redirect()->to($role_segment.'/tags')->with('success','Congrats! New category has been added successfully!');
        } else {
            return redirect()->to($role_segment.'/tags')->with('error','Sorry! Invalid Request.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $tags  = Tag::where('id',$id)->first();
        $tag_clients = $tags->clients()->get()->toArray();
        $tag_langs = $tags->langs()->get()->toArray();
        $tagData = $tags->toArray();
        $tagData['clients'] = $tag_clients;
        $tagData['langs'] = $tag_langs;
        return view('tags.show')->with("tag",$tagData);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $tag  = Tag::where('id',$id)->first();
        $tag_clients = $tag->clients()->get()->toArray();
        $tag_langs = $tag->langs()->get()->toArray();
        $tagData = $tag->toArray();
        $tagData['clients'] = [];
        if(count($tag_clients) > 0){
            foreach($tag_clients as $k=>$v) {
                $tagData['clients'][] = $v['id'];
            }
        }
        $tagData['langs'] = [];
        if(count($tag_langs) > 0){
            foreach($tag_langs as $k=>$v) {
                $tagData['langs'][$v['id']] = $v['pivot'];
            }
        }
        //echo "<pre>"; print_r($tagData); die;
        $clients = Client::all()->toArray();
        $langs = Lang::getEnabledLanguages();
        return view('tags.edit')->with("tag",$tagData)->with('clients',$clients)->with('langs',$langs);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response 
     */
    public function update(Request $request, $id) {

        //- - - - - - - - NEED TO DELETE LATER - - - - - - - 
        $tag_lang_data = $request->input('langdata');
        $tag_name           = $tag_lang_data['1']['tag_name'];
        $tag_description    = $tag_lang_data['1']['tag_description'];
        //- - - - - - - - - - - - - - - - - - - - - - - - - 

        $role = auth()->user()->getRole();
        $role_segment = ($role == 'admin')?'/admin':'';
        // Validate Fields
        $request->validate([
            'client' => ['required']
        ]);
        $tag = Tag::find($id);
        $tag->name = $tag_name; //$request->get('name');
        $tag->description = $tag_description; // $request->get('description');
        $tag->save();

        // Update Clients of Tag
        $tag_client = $request->input('client');
        $tag->clients()->sync($tag_client);

        // Attach client with this languages
        $tag_lang = $request->input('langdata');
        $tag->langs()->sync($tag_lang); // Sync with name & description in different languages. 

        return redirect($role_segment.'/tags')->with('success', 'Category has been updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)  {
        $role = auth()->user()->getRole();
        $role_segment = ($role == 'admin')?'/admin':'';

        $count   = [];
        $tag     = Tag::find($id);
        $clients = $tag->clients()->get()->toArray();   
        $count['clients'] = count($clients);

        $tagData = Tag::where('id',$id)->first()->toArray();
        if( $count['clients'] == 0 ) {
            $tag->delete();
            return redirect($role_segment.'/tags')->with('success', 'Category "'.$tagData['name'].'" has been deleted successfully.');
        } else {
            return redirect($role_segment.'/tags')->with('error', 'Sorry, You can\'t delete category "'.$tagData['name'].'". This tag is associated with '.$count['clients'].' clients.' );
        }
    }
}
