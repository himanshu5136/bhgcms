<?php

namespace App\Http\Controllers;

use App\User;
use App\Tag;
use App\Content;
use App\Block;
use App\Client;
use App\Variable;
use App\Blockdata;  
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\FilesystemManager;

class BlockController extends Controller
{

    /* ~ ~ ~ INDEX - Blocks LISTING ~ ~ ~ ~ ~ */
    public function index()  {
        $bnm = request()->query('bnm'); // Block Title
        $btp = request()->query('btp'); // Block Type

        $condition = [];
        if(!empty($bnm)){ $condition[] = ['title', 'like', '%'.$bnm.'%']; }
        if(!empty($btp)){ $condition[] = ['type', '=', $btp]; }

        $blocks = Block::where($condition)->get()->toArray();
        //echo "<pre>"; print_r($blocks); die;
        //$tags = Tag::all()->toArray();
        $type  = ['advertisement'=>'Advertisement','video'=>'Video','text'=>'Custom Text','qrcode'=>'QRCode'];
        //echo "<pre>"; print_r($contentData); die;
        return view('blocks.index')->with("blocks",$blocks)->with("type",$type)->with('qry',['bnm'=>$bnm,'btp'=>$btp]);
        //->;;with("tags",$tags)
    }

    ///////////////////////////////////////////////////
    ////////////////// ADVERTISEMENT ///////////////////
    ///////////////////////////////////////////////////
    /* ~ ~ ~ CREATE - Advertisement Block ~ ~ ~ ~ ~ */
    public function create_advertisement() {
        $contents = Content::all()->toArray();
        $regions  = ['content_top'=>'Content Top','content_bottom'=>'Content Bottom','left_sidebar'=>'Left Sidebar','right_sidebar'=>'Right Sidebar','footer'=>'Footer'];
        return view('blocks.create_advertisement')->with("contents",$contents)->with("regions",$regions);
    }

    /* ~ ~ ~ STORE - Advertisement Block ~ ~ ~ ~ ~ */
    public function store_advertisement(Request $request)  {
        $dt = $request->all();
        echo "<pre>"; print_r($dt); die; 
        // Validate Fields
        $validateAry = [
            'block_title' => ['required','string','max:512'],
            'block_description' => ['required'],
            'advertisement_image' => ['required']
        ];  
        $request->validate($validateAry);
        // Upload Advertisement Image to S3 server
        $file = $request->file('advertisement_image');
        $attachment_url = '';
        try {
            if(!empty($file)) {
                $file_mime_type = $file->getMimeType();
                $file_name = $file->getClientOriginalName();
                $file_name = $request->file('advertisement_image')->getClientOriginalName();
                $file_path = "blocks/advertisement/".date('Y_m_d_h_i_s').'_'.$file_name;
                $s3 = Storage::disk('s3');
                $s3->put($file_path, file_get_contents($file), array('ContentDisposition' => 'inline; filename=' . $file_name . '', 'ACL' => 'public-read'));
                $attachment_url = Storage::disk('s3')->url($file_path);
            }
            $dsbld = $request->input('block_disabled');
            $block_disabled = (!empty($dsbld))?$request->input('block_disabled'):'0';
            $block_display = [];
            $bdisplay = $request->input('display');
            foreach($bdisplay as $k=>$v) {
                if(!empty($v['content'])) {
                    $tempAry = [];
                    $tempAry['content_pages']       = (!empty($v['content_page']))?implode(',',$v['content_page']):'';
                    $tempAry['display_location']    = (!empty($v['location']))?$v['location']:'';
                    $block_display[$v['content']] = $tempAry;
                }
            }
            // Create Block
            $block =  Block::create([
                'title'       => $request->input('block_title'),
                'description' => $request->input('block_description'),
                'type'        => 'advertisement',
                'disabled'    => $block_disabled,
            ]);

            // Save Blockdata Infor of created Block
            $blockData = new Blockdata;
            $blockData->ad_image       = $attachment_url;
            $block->blockdata()->save($blockData);
            
            // Attach content Display with Block
            $block->contents()->sync($block_display);

            if($block) {
                return redirect()->to('/blocks')->with('success','Congrats! New Advertisement block has been saved successfully!');
            } else {
                return redirect()->to('/blocks')->with('error','Sorry! Invalid Request.');
            }
        } catch (Exception $e) {
            return redirect()->to('/blocks')->with('error','Invalid Request.');
        }
    }

    /* ~ ~ ~ UPDATE - Advertisement Block ~ ~ ~ ~ ~ */
    public function update_advertisement(Request $request, $id) {
        //$dt = $request->all();
        //echo "<pre>"; print_r($dt); die;

        $blockData = BlockController::getBlockData($id);

        // Validate Fields
        $validateAry = [
            'block_title' => ['required','string','max:512'],
            'block_description' => ['required'],
        ];  
        $request->validate($validateAry);
        // Upload Advertisement Image to S3 server
        $file = $request->file('advertisement_image');
        $attachment_url = $blockData['data']['ad_image'];
        try {
            if(!empty($file)) {
                $file_mime_type = $file->getMimeType();
                $file_name = $file->getClientOriginalName();
                $file_name = $request->file('advertisement_image')->getClientOriginalName();
                $file_path = "blocks/advertisement/".date('Y_m_d_h_i_s').'_'.$file_name;
                $s3 = Storage::disk('s3');
                $s3->put($file_path, file_get_contents($file), array('ContentDisposition' => 'inline; filename=' . $file_name . '', 'ACL' => 'public-read'));
                $attachment_url = Storage::disk('s3')->url($file_path);
            }
            $dsbld = $request->input('block_disabled');
            $block_disabled = (!empty($dsbld))?$request->input('block_disabled'):'0';
            $block_display = [];
            $bdisplay = $request->input('display');
            foreach($bdisplay as $k=>$v) {
                if(!empty($v['content'])) {
                    $tempAry = [];
                    $tempAry['content_pages']       = (!empty($v['content_page']))?implode(',',$v['content_page']):'';
                    $tempAry['display_location']    = (!empty($v['location']))?$v['location']:'';
                    $block_display[$v['content']] = $tempAry;
                }
            }
            $block = Block::find($id);
            $block->title         = $request->get('block_title');
            $block->description   = $request->get('block_description');
            $block->type          = 'advertisement';
            $block->disabled      = $block_disabled;
            $block->save();

            // Update Blockdata Infor of created Block
            $block = Block::find($id);
            $blockData = [];
            $blockData['ad_image']       = $attachment_url;
            $block->blockdata()->update($blockData);
            
            // Attach content Display with Block
            $block->contents()->sync($block_display);

            if($block) {
                return redirect()->to('/blocks')->with('success','Congrats! Advertisement block has been updated successfully!');
            } else {
                return redirect()->to('/blocks')->with('error','Sorry! Invalid Request.');
            }
        } catch (Exception $e) {
            return redirect()->to('/blocks')->with('error','Invalid Request.');
        }

    }

    ///////////////////////////////////////////////////
    ////////////////// VIDEO //////////////////////////
    ///////////////////////////////////////////////////
    /* ~ ~ ~ CREATE - Video Block ~ ~ ~ ~ ~ */
    public function create_video() {
        $contents = Content::all()->toArray();
        $regions  = ['content_top'=>'Content Top','content_bottom'=>'Content Bottom','left_sidebar'=>'Left Sidebar','right_sidebar'=>'Right Sidebar','footer'=>'Footer'];
        return view('blocks.create_video')->with("contents",$contents)->with("regions",$regions);
    }

    /* ~ ~ ~ STORE - Video Block ~ ~ ~ ~ ~ */
    public function store_video(Request $request)  {
        // Validate Fields
        $validateAry = [
            'block_title' => ['required','string','max:512'],
            'block_description' => ['required'],
        ];  
        $upload_video = $request->input('upload_video');
        $embed_video = $request->input('embed_video');
        if((empty($upload_video)) && (empty($embed_video))) {   // If Upload Video & Embed Video both are empty.
            $validateAry['upload_video'] = ['required'];
        } 
        $request->validate($validateAry);

        // Upload Advertisement Image to S3 server
        $file = $request->file('upload_video');
        $attachment_url = '';
        try {
            if(!empty($file)) {
                $file_mime_type = $file->getMimeType();
                $file_name = $file->getClientOriginalName();
                $file_name = $request->file('upload_video')->getClientOriginalName();
                $file_path = "blocks/videos/".date('Y_m_d_h_i_s').'_'.$file_name;
                $s3 = Storage::disk('s3');
                $s3->put($file_path, file_get_contents($file), array('ContentDisposition' => 'inline; filename=' . $file_name . '', 'ACL' => 'public-read'));
                $attachment_url = Storage::disk('s3')->url($file_path);
            }
            $dsbld = $request->input('block_disabled');
            $block_disabled = (!empty($dsbld))?$request->input('block_disabled'):'0';
            $block_display = [];
            $bdisplay = $request->input('display');
            foreach($bdisplay as $k=>$v) {
                if(!empty($v['content'])) {
                    $tempAry = [];
                    //$tempAry['content_pages']       = (!empty($v['content_page']))?$v['content_page']:'';
                    $tempAry['content_pages']       = (!empty($v['content_page']))?implode(',',$v['content_page']):'';
                    $tempAry['display_location']    = (!empty($v['location']))?$v['location']:'';
                    $block_display[$v['content']] = $tempAry;
                }
            }
            // Create Block
            $block =  Block::create([
                'title'       => $request->input('block_title'),
                'description' => $request->input('block_description'),
                'type'        => 'video',
                'disabled'    => $block_disabled,
            ]);

            // Save Blockdata Infor of created Block
            $blockData = new Blockdata;
            $embed_video = $request->input('embed_video');
            $blockData->uploaded_video      = $attachment_url;
            $blockData->embed_video         = $embed_video;
            $block->blockdata()->save($blockData);
            
            // Attach content Display with Block
            $block->contents()->sync($block_display);

            if($block) {
                return redirect()->to('/blocks')->with('success','Congrats! New Video block has been saved successfully!');
            } else {
                return redirect()->to('/blocks')->with('error','Sorry! Invalid Request.');
            }
        } catch (Exception $e) {
            return redirect()->to('/blocks')->with('error','Invalid Request.');
        }
    }

    /* ~ ~ ~ UPDATE - Video Block ~ ~ ~ ~ ~ */
    public function update_video(Request $request, $id) {
        //$dt = $request->all();
       // echo "<pre>"; print_r($dt); die;

        $blockData = BlockController::getBlockData($id);

        // Validate Fields
        $validateAry = [
            'block_title' => ['required','string','max:512'],
            'block_description' => ['required'],
        ];  
        $upload_video = $request->input('upload_video_content'); //Hidden field which is having uploaded video URL
        $embed_video = $request->input('embed_video');
        if((empty($upload_video)) && (empty($embed_video))) {   // If Upload Video & Embed Video both are empty.
            $validateAry['upload_video'] = ['required'];
        } 
        $request->validate($validateAry);
        // Upload Advertisement Image to S3 server
        $file = $request->file('upload_video');
        $attachment_url = $blockData['data']['uploaded_video'];
        try {
            if(!empty($file)) {
                $file_mime_type = $file->getMimeType();
                $file_name = $file->getClientOriginalName();
                $file_name = $request->file('upload_video')->getClientOriginalName();
                $file_path = "blocks/videos/".date('Y_m_d_h_i_s').'_'.$file_name;
                $s3 = Storage::disk('s3');
                $s3->put($file_path, file_get_contents($file), array('ContentDisposition' => 'inline; filename=' . $file_name . '', 'ACL' => 'public-read'));
                $attachment_url = Storage::disk('s3')->url($file_path);
            }
            $dsbld = $request->input('block_disabled');
            $block_disabled = (!empty($dsbld))?$request->input('block_disabled'):'0';
            $block_display = [];
            $bdisplay = $request->input('display');
            foreach($bdisplay as $k=>$v) {
                if(!empty($v['content'])) {
                    $tempAry = [];
                    //$tempAry['content_pages']       = (!empty($v['content_page']))?$v['content_page']:'';
                    $tempAry['content_pages']       = (!empty($v['content_page']))?implode(',',$v['content_page']):'';
                    $tempAry['display_location']    = (!empty($v['location']))?$v['location']:'';
                    $block_display[$v['content']] = $tempAry;
                }
            }

            $block = Block::find($id);
            $block->title         = $request->get('block_title');
            $block->description   = $request->get('block_description');
            $block->type          = 'video';
            $block->disabled      = $block_disabled;
            $block->save();

            // Update Blockdata Infor of created Block
            $block = Block::find($id);
            $blockData = [];
            $blockData['uploaded_video']    = $attachment_url;
            $blockData['embed_video']       = $request->get('embed_video');
            $block->blockdata()->update($blockData);
            
            // Attach content Display with Block
            $block->contents()->sync($block_display);

            if($block) {
                return redirect()->to('/blocks')->with('success','Congrats! Video block has been updated successfully!');
            } else {
                return redirect()->to('/blocks')->with('error','Sorry! Invalid Request.');
            }
        } catch (Exception $e) {
            return redirect()->to('/blocks')->with('error','Invalid Request.');
        }
    }

    ///////////////////////////////////////////////////
    ///////////////////// TEXT ////////////////////////
    //////////////////////////////////////////////////
    /* ~ ~ ~ CREATE - Text Block ~ ~ ~ ~ ~ */
    public function create_text() {
        $contents = Content::all()->toArray();
        $regions  = ['content_top'=>'Content Top','content_bottom'=>'Content Bottom','left_sidebar'=>'Left Sidebar','right_sidebar'=>'Right Sidebar','footer'=>'Footer'];
        return view('blocks.create_text')->with("contents",$contents)->with("regions",$regions);
    }

    /* ~ ~ ~ STORE - Text Block ~ ~ ~ ~ ~ */
    public function store_text(Request $request)  {
        // $req = $request->all();
        // $dt = html_entity_decode($req['body']);
        // echo $dt;
        // echo "<pre>"; print_r($req); die;   
       // Validate Fields
       $validateAry = [
            'block_title' => ['required','string','max:512'],
            'block_description' => ['required'],
            'body' => ['required']
        ];  
        $request->validate($validateAry);
        // Upload Advertisement Image to S3 server
        $dsbld = $request->input('block_disabled');
        $block_disabled = (!empty($dsbld))?$request->input('block_disabled'):'0';
        $block_display = [];
        $bdisplay = $request->input('display');
        foreach($bdisplay as $k=>$v) {
            if(!empty($v['content'])) {
                $tempAry = [];
                //$tempAry['content_pages']       = (!empty($v['content_page']))?$v['content_page']:'';
                $tempAry['content_pages']       = (!empty($v['content_page']))?implode(',',$v['content_page']):'';
                $tempAry['display_location']    = (!empty($v['location']))?$v['location']:'';
                $block_display[$v['content']] = $tempAry;
            }
        }
        // Create Block
        $block =  Block::create([
            'title'       => $request->input('block_title'),
            'description' => $request->input('block_description'),
            'type'        => 'text',
            'disabled'    => $block_disabled,
        ]);

        // Save Blockdata Information of created Block
        $text_body = $request->input('body');
        $blockData              = new Blockdata;
        $blockData->text_body   = html_entity_decode($text_body);
        $block->blockdata()->save($blockData);
        
        // Attach content Display with Block
        $block->contents()->sync($block_display);

        if($block) {
            return redirect()->to('/blocks')->with('success','Congrats! New Text block has been saved successfully!');
        } else {
            return redirect()->to('/blocks')->with('error','Sorry! Invalid Request.');
        }
    }

    /* ~ ~ ~ UPDATE - Text Block ~ ~ ~ ~ ~ */
    public function update_text(Request $request, $id) {
        //$dt = $request->all();
        //echo "<pre>"; print_r($dt); die;

        $blockData = BlockController::getBlockData($id);

        // Validate Fields
        $validateAry = [
            'block_title' => ['required','string','max:512'],
            'block_description' => ['required'],
            'body' => ['required']
        ];  
        $request->validate($validateAry);

        $dsbld = $request->input('block_disabled');
        $block_disabled = (!empty($dsbld))?$request->input('block_disabled'):'0';
        $block_display = [];
        $bdisplay = $request->input('display');
        foreach($bdisplay as $k=>$v) {
            if(!empty($v['content'])) {
                $tempAry = [];
                //$tempAry['content_pages']       = (!empty($v['content_page']))?$v['content_page']:'';
                $tempAry['content_pages']       = (!empty($v['content_page']))?implode(',',$v['content_page']):'';
                $tempAry['display_location']    = (!empty($v['location']))?$v['location']:'';
                $block_display[$v['content']] = $tempAry;
            }
        }
        $block = Block::find($id);
        $block->title         = $request->get('block_title');
        $block->description   = $request->get('block_description');
        $block->type          = 'text';
        $block->disabled      = $block_disabled;
        $block->save();

        // Update Blockdata Infor of created Block
        $text_body = $request->input('body');
        $block = Block::find($id);
        $blockData = [];
        $blockData['text_body']   = html_entity_decode($text_body);
        $block->blockdata()->update($blockData);
        
        // Attach content Display with Block
        $block->contents()->sync($block_display);

        if($block) {
            return redirect()->to('/blocks')->with('success','Congrats! Custom Text block has been updated successfully!');
        } else {
            return redirect()->to('/blocks')->with('error','Sorry! Invalid Request.');
        }
        

    }

    ///////////////////////////////////////////////////
    ///////////////////// QRCode //////////////////////
    //////////////////////////////////////////////////
    /* ~ ~ ~ CREATE - QRCode Block ~ ~ ~ ~ ~ */
    public function create_qrcode() {
        $contents = Content::all()->toArray();
        $regions  = ['content_top'=>'Content Top','content_bottom'=>'Content Bottom','left_sidebar'=>'Left Sidebar','right_sidebar'=>'Right Sidebar','footer'=>'Footer'];
        $qrcode_types = ['text'=>'Text', 'link'=>'Link', 'content'=>'Content', 'upload'=>'Upload'];
        return view('blocks.create_qrcode')->with("contents",$contents)->with("regions",$regions)->with("qrcode_types",$qrcode_types);
    }

    /* ~ ~ ~ STORE - QRCode Block ~ ~ ~ ~ ~ */
    public function store_qrcode(Request $request)  {
        // $req = $request->all();
        // echo "<pre>"; print_r($req); die;  
        // Validate Fields
        $validateAry = [
            'block_title' => ['required','string','max:512'],
            'block_description' => ['required'],
            'qrcode_type' => ['required']
        ];  
        
        $qrcode_type = $request->input('qrcode_type');
        if($qrcode_type == 'text') {
            $validateAry['qr_code_text'] = ['required'];
        } else if($qrcode_type == 'link') {
            $validateAry['qr_code_link'] = ['required'];    
        } else if($qrcode_type == 'content') {
            $validateAry['qr_code_content'] = ['required'];
        } else if($qrcode_type == 'upload') {
            $validateAry['qr_code_upload'] = ['required'];
        }
        $request->validate($validateAry);

        $qrCodeData = '';
        $qrcode_type = $request->input('qrcode_type');
        if($qrcode_type == 'text') {
            $qrCodeData = $request->input('qr_code_text');
        } else if($qrcode_type == 'link') {
            $qrCodeData = $request->input('qr_code_link');
        } else if($qrcode_type == 'content') {
            $content_id = $request->input('qr_code_content');
            $contents = Content::whereId($content_id)->first()->toArray();
            $qrCodeData = $contents['guid'];
        } else if($qrcode_type == 'upload') {
            $qrCodeData = 'uploaded_an_image';//$request->input('qr_code_upload');
        }
        // GENERATE & SAVE QRCODE to S3 BUCKET
        $attachment_url = '';
        if(!empty($qrCodeData)) {
            if($qrcode_type == 'upload') {
                $file = $request->file('qr_code_upload');
                if(!empty($file)) {
                    $file_name = $file->getClientOriginalName();
                    $file_name = $request->file('qr_code_upload')->getClientOriginalName();
                    $file_path = "blocks/qrcode/".date('Y_m_d_h_i_s').'_'.$file_name;
                    $s3 = Storage::disk('s3');
                    $s3->put($file_path, file_get_contents($file), array('ContentDisposition' => 'inline; filename=' . $file_name . '', 'ACL' => 'public-read'));
                    $attachment_url = Storage::disk('s3')->url($file_path);
                }
            } else {
                $barcode = new \Com\Tecnick\Barcode\Barcode();
                $QRCodeImg = $barcode->getBarcodeObj('QRCODE,H', $qrCodeData, - 16, - 16, 'black', array(- 2,- 2,- 2,- 2))->setBackgroundColor('#fff')->getPngData();
                $rndnum = rand(999,9999);
                $file_name = 'QR_'.date('Y_m_d_h_i_s').'_'.$rndnum.'.png';
                $file_path = "blocks/qrcode/".$file_name;
                $s3 = Storage::disk('s3');
                $s3->put($file_path, $QRCodeImg, array('ContentDisposition' => 'inline; filename=' . $file_name . '', 'ACL' => 'public-read'));
                $attachment_url = Storage::disk('s3')->url($file_path);
            }
        }
        $dsbld = $request->input('block_disabled');
        $block_disabled = (!empty($dsbld))?$request->input('block_disabled'):'0';
        $block_display = [];
        $bdisplay = $request->input('display');
        foreach($bdisplay as $k=>$v) {
            if(!empty($v['content'])) {
                $tempAry = [];
                //$tempAry['content_pages']       = (!empty($v['content_page']))?$v['content_page']:'';
                $tempAry['content_pages']       = (!empty($v['content_page']))?implode(',',$v['content_page']):'';
                $tempAry['display_location']    = (!empty($v['location']))?$v['location']:'';
                $block_display[$v['content']] = $tempAry;
            }
        }
        // Create Block
        $block =  Block::create([
            'title'       => $request->input('block_title'),
            'description' => $request->input('block_description'),
            'type'        => 'qrcode',
            'disabled'    => $block_disabled,
        ]);

        // Save Blockdata Infor of created Block
        $blockData = new Blockdata;
        $blockData->qrcode_type     = $request->input('qrcode_type');
        $blockData->qrcode_text     = $request->input('qr_code_text');
        $blockData->qrcode_link     = $request->input('qr_code_link');
        $blockData->qrcode_content  = $request->input('qr_code_content');
        $blockData->qrcode_upload   = 'Uploaded Image of QR Code';
        $blockData->qrcode_image    = $attachment_url;
        $block->blockdata()->save($blockData);
        
        // Attach content Display with Block
        $block->contents()->sync($block_display);

        if($block) {
            return redirect()->to('/blocks')->with('success','Congrats! New QRCode block has been saved successfully!');
        } else {
            return redirect()->to('/blocks')->with('error','Sorry! Invalid Request.');
        }

       
    }

    /* ~ ~ ~ UPDATE - QRCode Block ~ ~ ~ ~ ~ */
    public function update_qrcode(Request $request, $id) {
        //$dt = $request->all();
        //echo "<pre>"; print_r($dt); die;
        $validateAry = [
            'block_title' => ['required','string','max:512'],
            'block_description' => ['required'],
            'qrcode_type' => ['required']
        ];  
        
        $qrcode_type = $request->input('qrcode_type');
        if($qrcode_type == 'text') {
            $validateAry['qr_code_text'] = ['required'];
        } else if($qrcode_type == 'link') {
            $validateAry['qr_code_link'] = ['required'];    
        } else if($qrcode_type == 'content') {
            $validateAry['qr_code_content'] = ['required'];
        }
        $request->validate($validateAry);

        $qrCodeData = '';
        $qrcode_type = $request->input('qrcode_type');
        if($qrcode_type == 'text') {
            $qrCodeData = $request->input('qr_code_text');
        } else if($qrcode_type == 'link') {
            $qrCodeData = $request->input('qr_code_link');
        } else if($qrcode_type == 'content') {
            $content_id = $request->input('qr_code_content');
            $contents = Content::whereId($content_id)->first()->toArray();
            $qrCodeData = $contents['guid'];
        } else if($qrcode_type == 'upload') {
            $qrCodeData = 'uploaded_an_image';//$request->input('qr_code_upload');
        }

        // GENERATE & SAVE QRCODE to S3 BUCKET
        $attachment_url = '';
        if(!empty($qrCodeData)) {
            if($qrcode_type == 'upload') {
                $file = $request->file('qr_code_upload');
                if(!empty($file)) {
                    $file_name = $file->getClientOriginalName();
                    $file_name = $request->file('qr_code_upload')->getClientOriginalName();
                    $file_path = "blocks/qrcode/".date('Y_m_d_h_i_s').'_'.$file_name;
                    $s3 = Storage::disk('s3');
                    $s3->put($file_path, file_get_contents($file), array('ContentDisposition' => 'inline; filename=' . $file_name . '', 'ACL' => 'public-read'));
                    $attachment_url = Storage::disk('s3')->url($file_path);
                }
            } else {
                $barcode = new \Com\Tecnick\Barcode\Barcode();
                $QRCodeImg = $barcode->getBarcodeObj('QRCODE,H', $qrCodeData, - 16, - 16, 'black', array(- 2,- 2,- 2,- 2))->setBackgroundColor('#fff')->getPngData();
                $rndnum = rand(999,9999);
                $file_name = 'QR_'.date('Y_m_d_h_i_s').'_'.$rndnum.'.png';
                $file_path = "blocks/qrcode/".$file_name;
                $s3 = Storage::disk('s3');
                $s3->put($file_path, $QRCodeImg, array('ContentDisposition' => 'inline; filename=' . $file_name . '', 'ACL' => 'public-read'));
                $attachment_url = Storage::disk('s3')->url($file_path);
            }
        }

        
        $dsbld = $request->input('block_disabled');
        $block_disabled = (!empty($dsbld))?$request->input('block_disabled'):'0';
        $block_display = [];
        $bdisplay = $request->input('display');
        foreach($bdisplay as $k=>$v) {
            if(!empty($v['content'])) {
                $tempAry = [];
                //$tempAry['content_pages']       = (!empty($v['content_page']))?$v['content_page']:'';
                $tempAry['content_pages']       = (!empty($v['content_page']))?implode(',',$v['content_page']):'';
                $tempAry['display_location']    = (!empty($v['location']))?$v['location']:'';
                $block_display[$v['content']] = $tempAry;
            }
        }
        $block = Block::find($id);
        $block->title         = $request->get('block_title');
        $block->description   = $request->get('block_description');
        $block->type          = 'qrcode';
        $block->disabled      = $block_disabled;
        $block->save();

        // Update Blockdata Infor of created Block
        $block = Block::find($id);
        $blockData = [];
        $qrcode_typ = $request->input('qrcode_type');
        $blockData['qrcode_type']     = $qrcode_typ;
        $blockData['qrcode_text']     = ($qrcode_typ == 'text')?$request->input('qr_code_text'):'';
        $blockData['qrcode_link']     = ($qrcode_typ == 'link')?$request->input('qr_code_link'):'';
        $blockData['qrcode_content']  = ($qrcode_typ == 'content')?$request->input('qr_code_content'):'';
        $blockData['qrcode_upload']   = ($qrcode_typ == 'upload')?'Uploaded Image of QR Code':'';
        $blockData['qrcode_image']    = $attachment_url;
        $block->blockdata()->update($blockData);
        
        // Attach content Display with Block
        $block->contents()->sync($block_display);

        if($block) {
            return redirect()->to('/blocks')->with('success','Congrats! QRCode block has been updated successfully!');
        } else {
            return redirect()->to('/blocks')->with('error','Sorry! Invalid Request.');
        }
       

    }


    public function edit($id) {
        $block          = Block::where('id',$id)->first();
        $block_data     = $block->blockdata()->first()->toArray();
        $block_display  = $block->contents()->get()->toArray();
        $blockData      = $block->toArray();
        $blockData['data'] = $block_data;
        $blockData['display'] = $block_display;
        $contents = Content::all()->toArray();
        $regions  = ['content_top'=>'Content Top','content_bottom'=>'Content Bottom','left_sidebar'=>'Left Sidebar','right_sidebar'=>'Right Sidebar','footer'=>'Footer'];
        $qrcode_types = ['text'=>'Text', 'link'=>'Link', 'content'=>'Content','upload'=>'Upload'];
        return view('blocks.edit')->with('block',$blockData)->with("contents",$contents)->with("regions",$regions)->with("qrcode_types",$qrcode_types);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $block          = Block::where('id',$id)->first();
        $block_data     = $block->blockdata()->first()->toArray();
        $block_display  = $block->contents()->get()->toArray();
        $blockData      = $block->toArray();
        $blockData['data'] = $block_data;
        $blockData['display'] = $block_display;

        //echo "<pre>"; print_r($blockData); die;
        $regions  = ['content_top'=>'Content Top','content_bottom'=>'Content Bottom','left_sidebar'=>'Left Sidebar','right_sidebar'=>'Right Sidebar','footer'=>'Footer'];
        return view('blocks.show')->with("block",$blockData)->with("regions",$regions);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $blockData = Block::where('id',$id)->first()->toArray();
        $block     = Block::find($id);
        $block->delete();
        return redirect('/blocks')->with('success', 'Block "'.$blockData['title'].'" has been deleted successfully.');
    }


    public function getBlockData($id) {
        $block          = Block::where('id',$id)->first();
        $block_data     = $block->blockdata()->first()->toArray();
        $block_display  = $block->contents()->get()->toArray();
        $blockData      = $block->toArray();
        $blockData['data'] = $block_data;
        $blockData['display'] = $block_display;
        return $blockData;
    }
}
