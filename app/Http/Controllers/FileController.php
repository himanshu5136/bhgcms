<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\FilesystemManager;
use ZipArchive;

class FileController extends Controller
{
    public function index() {
        $filesAry = [];
        $s3Dir = Storage::disk('s3')->directories();
        if(!empty($s3Dir)) {
            foreach($s3Dir as $key=>$val) {
                $s3innerDir = Storage::disk('s3')->directories($val);
                $filesAry[$val]['dirs'] = $s3innerDir;
                $files = Storage::disk('s3')->files($val);
                $filesAry[$val]['files'] = $files;
                
            }
        }
        //echo "<pre>"; print_r($filesAry); die;
        return view('files.index')->with('files',$filesAry);
    }

    public function create() {
        return view('files.create');
    }

    public function store(Request $request) {
       // echo storage_path('app/public/content/jhg'); die;
        $validateAry = [
            'upload_file' => ['required']
        ];
        $request->validate($validateAry);
        
        $uploadedFile = $request->file('upload_file');
        $file_mime_type = $uploadedFile->getMimeType();
        $file_name = $uploadedFile->getClientOriginalName();
        if($file_mime_type == 'application/zip') {  // Check for ZIp Content
            $base_url  = env('APP_URL').'/storage/app/public';
            $storagePublic = Storage::disk('public');
            $filePath = $storagePublic->putFile('file', $uploadedFile);
            $fpath = storage_path('app/public/').$filePath;
            $zip = new ZipArchive();
            if ($zip->open($fpath)) {
                $prefix_text = date('Y_m_d_h_i_s_');
                $dirName = storage_path('app/public/content/'.$prefix_text.str_replace('.zip','',$file_name));
                if (!file_exists($dirName)) {
                    mkdir($dirName, 0775);
                } else {
                    FileController::deleteAll($dirName);
                }
                $zip->extractTo($dirName);
                $zip->close();
                echo 'woot!';
                $all_files = FileController::scanFullDir($dirName); 
                $res = [];
                $paths = [];
                foreach($all_files as $k=>$fl) {
                    $s3_file_path_ary = [];
                    //$res[$k]['old_path']   = $fl;
                    $local_file_path    = storage_path('app/public/content'.'/'.$prefix_text.str_replace('.zip','',$file_name)).'/';
                    //$res[$k]['lcl_dir']   = $local_file_path;
                    $s3_file_path_ary   = str_replace($local_file_path,'',$fl);
                    //$res[$k]['new_path']   = 'content/'.$prefix_text.str_replace('\\','/',$s3_file_path_ary);
                    $local_path         = $fl;
                    $s3_path            = 'content/'.$prefix_text.$s3_file_path_ary;
                    $explode_s3_path    = explode('/',$s3_path);
                    $s3_file_name       = $explode_s3_path[count($explode_s3_path)-1];
                    //$res[$k]['s3_name'] = $s3_file_name;
                    //$res[$k]['s3_path'] = 'content/'.$prefix_text.str_replace('.zip','',$file_name);
                     $s3                = Storage::disk('s3');
                    $s3->put($s3_path, file_get_contents($local_path), array('ContentDisposition' => 'inline; filename=' . $s3_file_name . '', 'ACL' => 'public-read'));
                    //$res[$k]['path'] = Storage::disk('s3')->url($s3_path);
                    $paths[$k][] = Storage::disk('s3')->url($s3_path);
                }
                if(!empty($paths)) {
                FileController::deleteAll($dirName); // Remove extracted files
                 $uploaded_path  =  'https://borderlesshealthbkt.s3.ap-southeast-1.amazonaws.com/content/'.$prefix_text.str_replace('.zip','',$file_name);
                 return redirect()->to('/files')->with('success','Uploaded & extracted successfully at '.$uploaded_path);
                } else {
                    return redirect()->to('/files')->with('error','Invalid S3 permission.');
                }
            } else {
                return redirect()->to('/files')->with('error','Corrupted Zip File Attached.');
            }            
        } else {
            $file_name = $request->file('upload_file')->getClientOriginalName();
            //$file_path_info = path_info($file_name);
            $file_path = "images/".date('Ymdhis').'_'.$file_name;
            $s3 = Storage::disk('s3');
            $s3->put($file_path, file_get_contents($uploadedFile), array('ContentDisposition' => 'inline; filename=' . $file_name . '', 'ACL' => 'public-read'));
            $picture_attachment_url = Storage::disk('s3')->url($file_path);
            return redirect()->to('/files')->with('success','File Path : '.$picture_attachment_url);
        }
    }

    // Delete all files & folders of a Directory
    function deleteAll($str) { 
         if (is_file($str)) {               
            return unlink($str); 
        } 
        elseif (is_dir($str)) {     
            $scan = glob(rtrim($str, '/').'/*'); 
             foreach($scan as $index=>$path) { 
                 FileController::deleteAll($path); 
            } 
             return @rmdir($str); 
        }
    }

    // Fetch list of all files
    function scanFullDir($dir, &$results = array()) {
        $files = scandir($dir);
        foreach ($files as $key => $value) {
            $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
            if (!is_dir($path)) {
                $results[] = $path;
            } else if ($value != "." && $value != "..") {
                FileController::scanFullDir($path, $results);
                //$results[] = $path;
            }
        }
    
        return $results;
    }


}
