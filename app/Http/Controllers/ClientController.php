<?php

namespace App\Http\Controllers;

use App\User;
use App\Tag ;
use App\Client;
use App\Lang;
use App\Cpage;
use App\Doctor;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $cnm = request()->query('cnm');
        $doc_id = request()->query('did');
        $tag_id = request()->query('tid');
        $lang_id = request()->query('lng');
        $condition = [];
        if(!empty($cnm)){ $condition[] = ['name', 'like', '%'.$cnm.'%']; }
        $query = Client::where($condition);
        if(!empty($doc_id)) {
            $query = $query->whereHas('users', function ($q) use ($doc_id) {
                return $q->where('id', $doc_id);
            });
        }
        if(!empty($tag_id)) {
            $query = $query->whereHas('tags', function ($q) use ($tag_id) {
                return $q->where('id', $tag_id);
            });
        }
        if(!empty($lang_id)) {
            $query = $query->whereHas('langs', function ($q) use ($lang_id) {
                return $q->where('id', $lang_id);
            });
        }
        //$clients = $query->orderBy('created_at', 'desc')->get();
        $clients = $query->orderBy('created_at', 'desc')->paginate(10);
        $clientData = [];
        foreach($clients as $client) {
            $tempData = [];
            $client_doctors = $client->doctorUsers()->get()->toArray();
            $client_users = $client->users()->get()->toArray();
            $client_tags = $client->tags()->get()->toArray();
            $client_langs = $client->langs()->get()->toArray();
            $client_pages = $client->cpages()->get()->toArray();
            $tempData = $client->toArray();
            $tempData['doctors'] = $client_doctors;
            $tempData['users'] = $client_users;
            $tempData['tags'] = $client_tags;
            $tempData['langs'] = $client_langs;
            $tempData['cpages'] = $client_pages;
            $clientData[] = $tempData;
        }
        //echo "<pre>"; print_r($clientData); die;
        $doctors = User::allDoctors()->toArray(); 
        $tags    = Tag::all()->toArray();
        $langs   = Lang::where([])->orderByRaw('name ASC')->get()->toArray();
        $qry_data = ['cnm'=>$cnm,'lng'=>$lang_id,'tid'=>$tag_id, 'did'=>$doc_id];
        return view('clients.index')
        ->with("clients",$clientData)
        ->with("paginate",$clients->appends($qry_data))
        ->with('doctors',$doctors)
        ->with('tags',$tags)
        ->with('langs',$langs)
        ->with('page_title','List of Clients')
        ->with('qry',);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $doctors = User::allDoctors()->toArray();
        $tags = Tag::all()->toArray();
        $langs = Lang::where([['is_enable', '=', '1']])->orderByRaw('name ASC')->get()->toArray();
        return view('clients.create')->with("doctors",$doctors)->with('tags',$tags)->with('langs',$langs);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        // Validate Fields
        $validateAry = [
            'name' => ['required','string','max:255','unique:clients'],
            'description' => ['required']
        ];
        $request->validate($validateAry);
        
        // Create Client
        $client =  Client::create([
            'name' => $request->input('name'),
            'description' => $request->input('description')
        ]);
        // Sync with Languages
        $clangs = [];
        $clangs = $request->input('clangs');
        $clangs[] = '1'; // For English Default 
        $client->langs()->sync($clangs); // Sync with Languages

        if($client) {
            return redirect()->to('/clients')->with('success','Congrats! New client has been added successfully!');
        } else {
            return redirect()->to('/clients')->with('error','Sorry! Invalid Request.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $userProfiles = User::allProfilesByUid();
        $client  = Client::where('id',$id)->first();
        $client_doctors = $client->doctorUsers()->get()->toArray();
        $client_tags = $client->tags()->get()->toArray();
        $clientData = $client->toArray();
        $clientData['tags'] = $client_tags;
        $clientData['doctors'] = [];
        if(count($client_doctors) > 0) {
            foreach($client_doctors as $k=>$v) {
                $tempDoc = $v;
                $tempDoc['full_name'] = $userProfiles[$v['id']];
                $clientData['doctors'][] = $tempDoc;
            }
        }
        return view('clients.show')->with("client",$clientData);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $client  = Client::where('id',$id)->first();
        $client_doctors = $client->doctorUsers()->get()->toArray();
        $client_tags = $client->tags()->get()->toArray();
        $client_langs = $client->langs()->get()->toArray();
        $clientData['client'] = $client->toArray();

        $clientData['tags'] = [];
        if(count($client_tags) > 0){
            foreach($client_tags as $k=>$v) {
                $clientData['tags'][] = $v['id'];
            }
        }
        $clientData['langs'] = [];
        if(count($client_langs) > 0){
            foreach($client_langs as $k=>$v) {
                $clientData['langs'][] = $v['id'];
            }
        }
        $clientData['doctors'] = [];
        if(count($client_doctors) > 0){
            foreach($client_doctors as $k=>$v) {
                $clientData['doctors'][] = $v['id'];
            }
        }
        //echo "<pre>"; print_r($clientData); die;
        $doctors = User::allDoctors()->toArray();
        $tags = Tag::all()->toArray();
        $langs = Lang::where([['is_enable', '=', '1']])->orderByRaw('name ASC')->get()->toArray();
        return view('clients.edit')->with("client",$clientData)->with('tags',$tags)->with('langs',$langs)->with("doctors",$doctors);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        // Validate Fields
        $request->validate([
            'name'=>['required','string','max:255'],
            'description' => ['required'],
            
        ]);
        $client = Client::find($id);
        $client->name = $request->get('name');
        $client->description = $request->get('description');
        $client->save();
        
        // Sync with Languages
        $clangs = [];
        $clangs = $request->input('clangs');
        $clangs[] = '1'; // For English Default 
        $client->langs()->sync($clangs); // Sync with Languages


        return redirect('/clients')->with('success', 'Client has been updated successfully!');
    }
 
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $count   = [];
        $client  = Client::find($id);
        $doctors = $client->doctorUsers()->get()->toArray();
        $tags    = $client->tags()->get()->toArray();   
        $count['doctors'] = count($doctors);
        $count['tags'] = count($tags);

        $clientData = Client::where('id',$id)->first()->toArray();
        if(( $count['doctors'] == 0 ) && ( $count['tags'] == 0 )) {
            $client->delete();
            return redirect('/clients')->with('success', 'Client "'.$clientData['name'].'" has been deleted successfully.');
        } else {
            return redirect('/clients')->with('error', 'Sorry, You can\'t delete client "'.$clientData['name'].'". This client is associated with '.$count['doctors'].' doctors & '.$count['tags'].' categories.' );
        }
    }

    // Clients Listing page for 
    public function admin_client_detail($id) {
        $userProfiles = User::allProfilesByUid();
        $client  = Client::where('id',$id)->first();
        $client_doctors = $client->doctorUsers()->get()->toArray();
        $client_tags = $client->tags()->get()->toArray();
        $clientData = $client->toArray();
        $clientData['tags'] = $client_tags;
        $clientData['doctors'] = [];
        if(count($client_doctors) > 0) {
            foreach($client_doctors as $k=>$v) {
                $tempDoc = $v;
                $tempDoc['full_name'] = $userProfiles[$v['id']];
                $clientData['doctors'][] = $tempDoc;
            }
        }
        return view('clients.show_admin')->with("client",$clientData);
    }
}
