<?php

namespace App\Http\Controllers;

use App\Doctor;
use App\Client;
use App\User;
use App\Role;
use App\Permission;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    public function Permission() {   
		return redirect()->to('/users');
	}
	
	/**
     * Show the form for editing the specified resource.
     *
     */
    public function edit() {
		$allPerms = Permission::all()->toArray();
		$roles = Role::where('slug','admin')->first();
		$rolePermsData = $roles->permissions()->get()->toArray();
		$rolePerms = [];
		foreach($rolePermsData as $k=>$v){ $rolePerms[] = $v['id']; }
		//echo "<pre>"; print_r($rolePerms); die;
        return view('permissions.edit')->with('allPerms',$allPerms)->with('rolePerms',$rolePerms);
	}
	
	// Post - Save Admin User Permissions
	public function update(Request $request) {
		$postData = $request->input('perm');
		$roles = Role::where('slug','admin')->first();
		foreach($postData as $k=>$v) {
			if((isset($v['pid'])) && (!empty($v['pid']))) {
				$syncAry[] = $v['pid'];
			}
		}
		$roles->permissions()->sync($syncAry); // Sync data in DB
		return redirect()->to('/users/permissions')->with('success','Congrats! Permission has been updated successfully!');
	}
}
