<?php
namespace App\Http\Controllers;

use App\Stype;
use Illuminate\Http\Request;

class StypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $condition = [];
        $query = Stype::where($condition);
        $stypes = $query->orderBy('created_at', 'desc')->paginate(10);
        $stypeData = [];
        foreach($stypes as $stype) {
            $tempData = [];
            $tempData = $stype->toArray();
            $stypeData[] = $tempData;
        }
        return view('stypes.index')->with("stypes",$stypeData)->with("paginate",$stypes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('stypes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        // Validate Fields
        $validateAry = [
            'name' => ['required','string','max:64','unique:stypes'],
        ];
        $request->validate($validateAry);
        
        // Create Service Type
        $stype =  Stype::create([
            'name' => $request->input('name'),
        ]);

        if($stype) {
            return redirect()->to('/users/stypes')->with('success','Congrats! New Service Type has been added successfully!');
        } else {
            return redirect()->to('/users/stypes')->with('error','Sorry! Invalid Request.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $stype  = Stype::where('id',$id)->first();
        $stypeData = $stype->toArray();
        return view('stypes.show')->with("stype",$stypeData);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $stype  = Stype::where('id',$id)->first();
        $stypeData = $stype->toArray();
        return view('stypes.edit')->with("stype",$stypeData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        // Validate Fields
        $request->validate([
            'name'=>['required','string','max:64'],
        ]);
        $stype = Stype::find($id);
        $stype->name = $request->get('name');
        $stype->save();

        return redirect('/users/stypes')->with('success', 'Service Type has been updated successfully!');
    }
 
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $stype  = Stype::find($id);
        $stype->delete();
        return redirect('/users/stypes')->with('success', 'Service Type has been deleted successfully.');
        
    }
}
