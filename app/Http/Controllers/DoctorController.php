<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\Client;
use App\Lang;
use App\Stype;
use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DoctorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $client_id = request()->query('cid');
        $condition[] = ['is_deleted','=','0'];
        $query = User::where($condition);
        $query = $query->whereHas('roles', function ($q) {
            return $q->where('id', '3'); // 3 for Doctor Role
        });
        if(!empty($client_id)) {
            $query = $query->whereHas('clients', function ($q) use($client_id) {
                return $q->where('id', $client_id); // 3 for Doctor Role
            });
        }
        $userRole = auth()->user()->getRole();
        if ($userRole == 'admin') {  // Check for ADMIN User Role
            $userClients = auth()->user()->getClients();
            $userClientsId = [];
            foreach($userClients as $key=>$val) {
                $userClientsId[] = $val['id'];
            }
            $query = $query->whereHas('clients', function ($q) use ($userClientsId) {
                return $q->whereIn('id', $userClientsId ); // Check in assigned Clients of current user
            });
        }

        $docData = $query->orderBy('created_at', 'desc')->paginate(10);
        $doctors = [];
        foreach($docData as $user) {
            $tempUser = [];
            $tempUser = $user->toArray();
            $user_profile = $user->profile()->get()->toArray();
            $user_clients = $user->clients()->get()->toArray();
            $user_stypes = $user->stypes()->get()->toArray();
            $tempUser['profile']  = (!empty($user_profile))?$user_profile['0']:[];
            $tempUser['clients']  = $user_clients;
            $tempUser['stypes']  = $user_stypes;
            $doctors [] = $tempUser;
        }
        $clients = Client::getAllClients();
        $qry_data = ['cid'=>$client_id];
        return view('doctors.index')
        ->with("doctors",$doctors)
        ->with("paginate",$docData->appends($qry_data))
        ->with("clients",$clients)
        ->with('qry',$qry_data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)  {
        $query = User::where(['id' => $id]);
        $query = $query->whereHas('roles', function ($q) { return $q->where('id', '3');  });
        $doctor = $query->first();
        $doctorProfile = $doctor->profile()->get()->toArray();
        $user_stype     = $doctor->stypes()->get()->toArray();
        $user_langs     = $doctor->langs()->get()->toArray();
        $doctorData = $doctor->toArray();
        $doctorData['profile'] = (!empty($doctorProfile))?$doctorProfile['0']:[];
        $doctorData['stype'] = $user_stype;
        $doctorData['langs'] = $user_langs;
        return view('doctors.show')->with("doctor",$doctorData);
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        $user                   = User::where(['id' => $id])->first();
        $user_role              = $user->roles()->first()->toArray();
        $user_profile           = $user->profile()->get()->toArray();
        $user_client            = $user->clients()->get()->toArray();
        $user_lang              = $user->langs()->get()->toArray();
        $user_stypes       = $user->stypes()->get()->toArray();
        $userData               = $user->toArray();
        $userData['role']       = $user_role;
        $userData['profile']    = (!empty($user_profile))?$user_profile['0']:['full_name'=>'','title'=>'','contact_number'=>'','bio'=>'','picture'=>''];
        $userData['stypes']       = [];
        if(count($user_stypes) > 0) {
            foreach($user_stypes as $k=>$v) {
                $userData['stypes'][]    = $v['id'];
            }
        }
        $userData['clients']    = [];
        if(count($user_client) > 0) {
            foreach($user_client as $k=>$v) {
                $userData['clients'][]    = $v['id'];
            }
        }
        $userData['langs']    = [];
        if(count($user_lang) > 0) {
            foreach($user_lang as $k=>$v) {
                //$userData['langs'][]    = $v['id'];
                $userData['langs'][$v['id']] = $v['pivot'];
            }
        }
        //echo "<pre>"; print_r($userData); die;
        $roles                  = Role::all()->toArray();
        $clients                = Client::all()->toArray();
        $stypes                 = Stype::all()->toArray();   // Doctor Service Type
        $status                 = ['1'=>'Active','2'=>'Blocked'];
        $langs = Lang::getEnabledLanguages();
        return view('doctors.edit')
        ->with("user",$userData)
        ->with("status",$status)
        ->with("roles",$roles)
        ->with("langs",$langs)
        ->with("stypes",$stypes)
        ->with("clients",$clients);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)  {
        // $dt = $request->all();
        // echo "<pre>"; print_r($dt); die;

        $validateAry = [
            'status'     => ['required'],
            'client'     => ['required'],
            'doc_rate_id'=> ['required'],
            'stype'      => ['required']
        ];
        $request->validate($validateAry);

        $exstUser               = User::where('id',$id)->first();
        $exstUser_langs         = $exstUser->langs()->get()->toArray();
        $exstUserData           = $exstUser->toArray();
        $exstUserData['langs']  = [];
        if(count($exstUser_langs) > 0) {
            $tempUserLang = [];
            foreach($exstUser_langs as $k=>$v) {
                $tempUserLang[$v['id']] = $v['pivot'];
            }
            $exstUserData['langs'] = $tempUserLang;
        }
        //echo"<pre>"; print_r($exstUserData); die;
        //- - - - - - - - NEED TO DELETE LATER - - - - - - - 
        $doclangdata = []; 
        $uld = $request->input('langdata');
        //echo "<pre>"; print_r($uld); die;
        foreach($uld as $key=>$val) {
            $doclangdata[$key]['doc_full_name']  = (isset($val['full_name']))?$val['full_name']:'';
            $doclangdata[$key]['doc_title']      = (isset($val['title']))?$val['title']:'';
            $doclangdata[$key]['doc_bio']        = (isset($val['user_brief']))?$val['user_brief']:'';
            // Background Image
            $bgimg_url      = '';
            $removeBgImg    = $request->input('remove_'.$key.'_doc_bg_image');
            if($removeBgImg == '1') {
                $bgimg_url  = '';
            } else {
                $bgimg_url  = (!empty($exstUserData['langs'][$key]))?$exstUserData['langs'][$key]['doc_bg_image']:'bg_img_here';
            }
            $bgimgFile     = $request->file('langdata_'.$key.'_bg_image');
            if(!empty($bgimgFile)) {
                $bgimgFileName = $bgimgFile->getClientOriginalName();
                $bgimg_file_path = "background/".date('Ymdhis').'_'.$bgimgFileName;
                $s3 = Storage::disk('s3');
                $s3->put($bgimg_file_path, file_get_contents($bgimgFile), array('ContentDisposition' => 'inline; filename=' . $bgimgFileName . '', 'ACL' => 'public-read'));
                $bgimg_url = Storage::disk('s3')->url($bgimg_file_path);
            }
            $doclangdata[$key]['doc_bg_image']     = $bgimg_url;

            // Profile Image            
            $prfimg_url = '11';
            $removePrfImg  = $request->input('remove_'.$key.'_doc_picture');
            if($removePrfImg == '1') {
                $prfimg_url = '';
            } else {
                $prfimg_url = (!empty($exstUserData['langs'][$key]))?$exstUserData['langs'][$key]['doc_picture']:'hereeeeee';
            }    
            $prfimgFile     = $request->file('langdata_'.$key.'_doc_picture');
            if(!empty($prfimgFile)) {
                $prfimgFileName = $prfimgFile->getClientOriginalName();
                $prfimg_file_path = "profile/".date('Ymdhis').'_'.$prfimgFileName;
                $s3 = Storage::disk('s3');
                $s3->put($prfimg_file_path, file_get_contents($prfimgFile), array('ContentDisposition' => 'inline; filename=' . $prfimgFileName . '', 'ACL' => 'public-read'));
                $prfimg_url = Storage::disk('s3')->url($prfimg_file_path);
            }
            $doclangdata[$key]['doc_picture']     = $prfimg_url;

            // Profile Video
            $prfvdo_url = '';
            $removePrfVdo  = $request->input('remove_'.$key.'_doc_profile_video');
            if($removePrfVdo == '1') {
                $prfvdo_url = '';
            } else {
                $prfvdo_url = (!empty($exstUserData['langs'][$key]))?$exstUserData['langs'][$key]['doc_profile_video']:'vdo_here';
            }
            $prfvdoFile     = $request->file('langdata_'.$key.'_profile_video');
            if(!empty($prfvdoFile)) {
                $prfvdoFileName = $prfvdoFile->getClientOriginalName();
                $prfvdo_file_path = "video/".date('Ymdhis').'_'.$prfvdoFileName;
                $s3 = Storage::disk('s3');
                $s3->put($prfvdo_file_path, file_get_contents($prfvdoFile), array('ContentDisposition' => 'inline; filename=' . $prfvdoFileName . '', 'ACL' => 'public-read'));
                $prfvdo_url = Storage::disk('s3')->url($prfvdo_file_path);
            }
            $doclangdata[$key]['doc_profile_video']     = $prfvdo_url;
        }
        //- - - - - - - - ENGLISH FIELDS - - - - - - - 
        $doc_full_name      = $doclangdata['1']['doc_full_name'];
        $doc_title          = $doclangdata['1']['doc_title'];
        $doc_bio            = $doclangdata['1']['doc_bio'];
        $doc_profile_video  = $doclangdata['1']['doc_profile_video'];
        $doc_picture        = $doclangdata['1']['doc_picture'];
        $doc_bg_image       = $doclangdata['1']['doc_bg_image'];
        //- - - - - - - - - - - - - - - - - - - - - - - - - 

        $userPutData = [];
        $userPutData['status'] = $request->input('status');
        $usr = User::whereId($id)->update($userPutData);

        $user = User::where('id',$id)->first();
        $usrProfileData = [];
        $usrProfileData['user_id']          = $id;
        $usrProfileData['full_name']        = $doc_full_name;
        $usrProfileData['title']            = $doc_title;
        $usrProfileData['bio']              = $doc_bio;
        $usrProfileData['contact_number']   = $request->input('contact_number');
        $usrProfileData['picture']          = $doc_picture;
        $usrProfileData['doc_rate_id']      = $request->input('doc_rate_id');
        $usrProfileData['bg_image']         = $doc_bg_image;
        $usrProfileData['profile_video']    = $doc_profile_video;
        $user->profile()->update($usrProfileData);

        // Attach Selected Role with this User
        $user_role = ['3'];
        $user->roles()->sync($user_role);

        // Attach Selected Clients with this User
        $user_clients = $request->input('client');
        $user->clients()->sync($user_clients);

        // Attach Service Type with this User
        $user_stype = $request->input('stype');
        $user->stypes()->sync($user_stype);

        // Attach Language Profile with this Doctor
        $user->langs()->sync($doclangdata);

        if($user) {
            return redirect()->to('/doctors')->with('success','Congrats! Doctor has been updated successfully!');
        } else {
            return redirect()->to('/doctors')->with('error','Sorry! Invalid Request.');
        }
    }

    // Soft Delete
    public function trash($id)   {
        $role = auth()->user()->getRole();
        $role_segment = ($role == 'admin')?'/admin':'';
        $user = User::whereId($id)->update(['status'=>'2','is_deleted'=>'1']);
        return redirect()->to($role_segment.'/doctors')->with('success','Doctor has been successfully moved to trash!');
    }


    //  OLD - METHODS
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editold($id) {
        $user                   = User::where(['id' => $id])->first();
        $user_role              = $user->roles()->first()->toArray();
        $user_profile           = $user->profile()->get()->toArray();
        $user_client            = $user->clients()->get()->toArray();
        $user_stypes       = $user->stypes()->get()->toArray();
        $userData               = $user->toArray();
        $userData['role']       = $user_role;
        $userData['profile']    = (!empty($user_profile))?$user_profile['0']:['full_name'=>'','title'=>'','contact_number'=>'','bio'=>'','picture'=>''];
        $userData['stypes']       = [];
        if(count($user_stypes) > 0) {
            foreach($user_stypes as $k=>$v) {
                $userData['stypes'][]    = $v['id'];
            }
        }
        $userData['clients']    = [];
        if(count($user_client) > 0) {
            foreach($user_client as $k=>$v) {
                $userData['clients'][]    = $v['id'];
            }
        }
        //echo "<pre>"; print_r($userData); die;
        $roles                  = Role::all()->toArray();
        $clients                = auth()->user()->getClients();
        $stypes                 = Stype::all()->toArray();   // Doctor Service Type
        $status                 = ['1'=>'Active','2'=>'Blocked'];
        return view('doctors.edit')
        ->with("user",$userData)
        ->with("status",$status)
        ->with("roles",$roles)
        ->with("stypes",$stypes)
        ->with("clients",$clients);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateold(Request $request, $id) {
         // Validate Fields
         $validateAry = [
            'full_name'     => ['required','string','max:255'],
            'status'        => ['required'],
            'client'        => ['required'],
            'doc_rate_id'   => ['required'],
            'stype'         => ['required']
        ];
        $validatedData = $request->validate($validateAry);
        // Saved Data of User
        $usrData            = User::where(['id' => $id])->first();
        $usrProfile         = $usrData->profile()->get()->toArray();
        try {
            // ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
            // Profile Picture
            // ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
            $remove_picture = $request->get('remove_picture');
            if($remove_picture == '1') {
                $picture_attachment_url = '';
            } else {
                $picture_attachment_url = $usrProfile['0']['picture'];
            }
            $file = $request->file('picture');
            if(!empty($file)) {
                $file_name = $request->file('picture')->getClientOriginalName();
                $file_path = "profile/".date('Ymdhis').'_'.$file_name; 
                $s3 = Storage::disk('s3');
                $s3->put($file_path, file_get_contents($file), array('ContentDisposition' => 'inline; filename=' . $file_name . '', 'ACL' => 'public-read'));
                $picture_attachment_url = Storage::disk('s3')->url($file_path);
            }
            // ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
            // Background Image
            // ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
            $remove_bg_image = $request->get('remove_bg_image');
            if($remove_bg_image == '1') {
                $bg_image_attachment_url = '';
            } else {
                $bg_image_attachment_url = $usrProfile['0']['bg_image'];
            }
            $file = $request->file('bg_image');
            if(!empty($file)) {
                $file_name = $request->file('bg_image')->getClientOriginalName();
                $file_path = "bg_image/".date('Ymdhis').'_'.$file_name; 
                $s3 = Storage::disk('s3');
                $s3->put($file_path, file_get_contents($file), array('ContentDisposition' => 'inline; filename=' . $file_name . '', 'ACL' => 'public-read'));
                $bg_image_attachment_url = Storage::disk('s3')->url($file_path);
            }

            // ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
            // Profile Video
            // ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
            $remove_profile_video = $request->get('remove_profile_video');
            if($remove_profile_video == '1') {
                $profile_video_attachment_url = '';
            } else {
                $profile_video_attachment_url = $usrProfile['0']['profile_video'];
            }
            $file = $request->file('profile_video');
            if(!empty($file)) {
                $file_name = $request->file('profile_video')->getClientOriginalName();
                $file_path = "profile_video/".date('Ymdhis').'_'.$file_name; 
                $s3 = Storage::disk('s3');
                $s3->put($file_path, file_get_contents($file), array('ContentDisposition' => 'inline; filename=' . $file_name . '', 'ACL' => 'public-read'));
                $profile_video_attachment_url = Storage::disk('s3')->url($file_path);
            }

            // Update User Data
            $userData = [];
            $userData['status'] = $validatedData['status'];
            $usr = User::whereId($id)->update($userData);
            // Save Profile Info of updated User
            $user = User::where('id',$id)->first();
            $usrProfileData = [];
            $usrProfileData['user_id']          = $id;
            $usrProfileData['full_name']        = $request->input('full_name');
            $usrProfileData['title']            = $request->input('title');
            $usrProfileData['bio']              = $request->input('user_brief');
            $usrProfileData['contact_number']   = $request->input('contact_number');
            $usrProfileData['picture']          = $picture_attachment_url;
            $usrProfileData['doc_rate_id']      = $request->input('doc_rate_id');
            $usrProfileData['bg_image']         = $bg_image_attachment_url;
            $usrProfileData['profile_video']    = $profile_video_attachment_url;
            $user->profile()->update($usrProfileData);

            // Attach Selected Clients with this User
            $user_clients = $request->input('client');
            $user->clients()->sync($user_clients);

            // Sync Service Type with this doctor
            $user_stype = $request->input('stype');
            $user->stypes()->sync($user_stype);
            
            $role = auth()->user()->getRole();
            $role_segment = ($role == 'admin')?'/admin':'';
            if($usr) {
                return redirect()->to($role_segment.'/doctors')->with('success','Congrats! User has been updated successfully!');
            } else {
                return redirect()->to($role_segment.'/doctors')->with('error','Sorry! Invalid Request.');
            }
        } catch (Exception $e) {
            return redirect()->to($role_segment.'/doctors')->with('error','Invalid Request.');
        }
    }

}
