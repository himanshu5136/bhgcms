<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Client;
use App\Doctor;
use App\Tag;
use App\Content;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        $user       = auth()->user();
        $userRole   = auth()->user()->getRole();
        if($userRole == 'superadmin') {  // Role : Superadmin User
            $allUsers       = User::all()->toArray();
            $allClients     = Client::all()->toArray();
            $allDoctors     = User::allDoctors()->toArray();
            $allCategories  = Tag::all()->toArray();
            $allContents    = Content::all()->toArray();
            $counts         = ['users'=>count($allUsers),'clients'=>count($allClients),'doctors'=>count($allDoctors),'categories'=>count($allCategories),'contents'=>count($allContents)];
            return view('dashboard_superadmin')->with('counts',$counts);
        } else if($userRole == 'admin') {    // Role : Admin User
            $user_clients = auth()->user()->getClients();
            return view('dashboard_admin')->with('clients',$user_clients);
        } else if($userRole == 'doctor') {    // Role : Doctor User
            return view('dashboard_doctor');
        } else {    
            return view('dashboard');
        }
    }
}