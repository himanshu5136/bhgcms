<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\Client;
use App\Stype;
use App\Lang;
use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()  {
        $status = request()->query('sts');
        $fname = request()->query('fnm');
        $role_id = request()->query('rid');
        $include_trash_user = request()->query('tusr');
        $condition = [];
        if($include_trash_user != '1') { // Include in listing
            $condition[] = ['is_deleted','=','0'];
        } 
        if(!empty($fname)){ $condition[] = ['full_name', 'like', '%'.$fname.'%']; }
        if(!empty($status)){ $condition[] = ['status', '=', $status]; }
        $query = User::where($condition);
        if(!empty($role_id)) {
            $query = $query->whereHas('roles', function ($q) use ($role_id) {
                return $q->where('id', $role_id);
            });
        }
        $userData = $query->orderBy('created_at', 'desc')->paginate(10);
        $users = [];
        foreach($userData as $user) {
            $tempUser = [];
            $tempUser = $user->toArray();
            $user_stype = $user->roles()->get()->toArray();
            $tempUser['stype']  = $user_stype;
            $user_role = $user->roles()->get()->toArray();
            $tempUser['role']  = (!empty($user_role))?$user_role['0']:[];
            $user_profile = $user->profile()->get()->toArray();
            $tempUser['profile']  = (!empty($user_profile))?$user_profile['0']:[];
            $users [] = $tempUser;
        }
        $qry_data = ['sts'=>$status,'fnm'=>$fname,'rid'=>$role_id,'tusr'=>$include_trash_user];
        $roles = Role::getAllRoles();
        return view('users.index')
        ->with("users",$users)
        ->with("paginate",$userData->appends($qry_data))
        ->with("roles",$roles)
        ->with('qry', $qry_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $roles   = Role::getAllAdminRoles(); // only roles i.e. Superadmin & Admin
        $clients = Client::getAllClients();
        $status  = ['1'=>'Active','2'=>'Blocked'];
        $stypes   =    [];
        return view('users.create')
        ->with("status",$status)
        ->with("stypes",$stypes)
        ->with("roles",$roles)
        ->with("clients",$clients);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        // Validate Fields
        $validateAry = [
            'name' => ['required','string','max:255','unique:users'],
            'email' => ['required','string','email','max:255','unique:users'],
            'password' => ['required','string','min:8','confirmed'],
            'full_name' => ['required','string','max:255'],
            'status' => ['required'],
            'role' => ['required']
        ];
        $user_role = $request->input('role');
        if(in_array($user_role,['2'])) {   // Admin
            $validateAry['client'] = ['required'];
        }
        $request->validate($validateAry);
        try {
            $pictureFile = $request->file('picture');
            $picture_attachment = '';
            if(!empty($pictureFile)) {
                $file_name = $request->file('picture')->getClientOriginalName();
                $file_path = "profile/".date('Ymdhis').'_'.$file_name;
                $s3 = Storage::disk('s3');
                $s3->put($file_path, file_get_contents($pictureFile), array('ContentDisposition' => 'inline; filename=' . $file_name . '', 'ACL' => 'public-read'));
                $picture_attachment = Storage::disk('s3')->url($file_path);
            }

            // Create User
            $user =  User::create([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'password' => Hash::make($request->input('password')),
                'status' => $request->input('status'),
            ]);
            // Save Profile Infor of created User
            $usrProfile = new Profile;
            $usrProfile->full_name      = $request->input('full_name');
            $usrProfile->title          = $request->input('title');
            $usrProfile->bio            = $request->input('user_brief');
            $usrProfile->picture        = $picture_attachment;
            $usrProfile->contact_number = $request->input('contact_number');
            $user->profile()->save($usrProfile);

            // Attach Selected Role with this User
            $user_role = $request->input('role');
            $user->roles()->sync($user_role);

            if(in_array($user_role,['2'])) { // Admin
                // Attach Selected Clients with this User
                $user_clients = $request->input('client');
                $user->clients()->sync($user_clients);
            }

            if($user) {
                return redirect()->to('/users')->with('success','Congrats! New user added successfully!');
            } else {
                return redirect()->to('/users')->with('error','Sorry! Invalid Request.');
            }
            // Response : {"name":"teamlead","email":"himanshu5136@gmail.com","full_name":"Team Lead","updated_at":"2020-07-26T06:07:03.000000Z","created_at":"2020-07-26T06:07:03.000000Z","id":2}
        } catch (Exception $e) {
            return redirect()->to('/users')->with('error','Invalid Request.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $user = User::where(['id' => $id])->first();
        $user_stype     = $user->stypes()->get()->toArray();
        $user_clients   = $user->clients()->get()->toArray();
        $user_role      = $user->roles()->first()->toArray();
        $user_profile   = $user->profile()->get()->toArray();
        $user_langs     = $user->langs()->get()->toArray();
        $userData       = $user->toArray();
        $userData['client'] = $user_clients;
        $userData['stype'] = $user_stype;
        $userData['role'] = $user_role;
        $userData['profile'] = (!empty($user_profile))?$user_profile['0']:[];
        $userData['langs'] = $user_langs;
        //echo "<pre>"; print_r($userData); die;
        return view('users.show')->with("user",$userData);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
    */
    public function edit($id) {
        $user                   = User::where(['id' => $id])->first();
        $user_role              = $user->roles()->first()->toArray();
        if($user_role['id'] == '3'){ return redirect()->to('/usersdoc'.'/'.$user['id'].'/edit'); }
        $user_profile           = $user->profile()->get()->toArray();
        $user_client            = $user->clients()->get()->toArray();
        $user_stypes            = $user->stypes()->get()->toArray();
        $userData               = $user->toArray();
        $userData['role']       = $user_role;
        $userData['profile']    = (!empty($user_profile))?$user_profile['0']:['full_name'=>'','title'=>'','contact_number'=>'','bio'=>'','picture'=>''];
        $userData['stypes']       = [];
        if(count($user_stypes) > 0) {
            foreach($user_stypes as $k=>$v) {
                $userData['stypes'][]    = $v['id'];
            }
        }
        $userData['clients']    = [];
        if(count($user_client) > 0) {
            foreach($user_client as $k=>$v) {
                $userData['clients'][]    = $v['id'];
            }
        }
        //echo "<pre>"; print_r($userData); die; 
        //$roles                  = Role::all()->toArray();
        $roles                  = Role::getAllAdminRoles(); // only roles i.e. Superadmin & Admin
        $clients                = Client::all()->toArray();
        //$stypes                 = Stype::all()->toArray();   // Doctor Service Type
        $status                 = ['1'=>'Active','2'=>'Blocked'];
        //echo "<pre>"; print_r($clients); die;
        return view('users.edit')
        ->with("user",$userData)
        ->with("status",$status)
        ->with("roles",$roles)
        ->with("clients",$clients);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)  {
        // Validate Fields
        $validateAry = [
            'full_name' => ['required','string','max:255'],
            'status' => ['required'],
            'role' => ['required']
        ];
        $user_role = $request->input('role');
        if(in_array($user_role,['2','3'])) {   // Admin / Doctor user role
            $validateAry['client'] = ['required'];
            if($user_role == '3') {     // DOCTOR ROLE
                $validateAry['doc_rate_id'] = ['required'];
                $validateAry['stype']       = ['required'];
            }
        }
        $validatedData = $request->validate($validateAry);

        // Saved Data of User
        $usrData            = User::where(['id' => $id])->first();
        $usrProfile         = $usrData->profile()->get()->toArray();
  
        try {
            // ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
            // Profile Picture
            // ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
            $remove_picture = $request->get('remove_picture');
            if($remove_picture == '1') {
                $picture_attachment_url = '';
            } else {
                $picture_attachment_url = $usrProfile['0']['picture'];
            }
            $file = $request->file('picture');
            if(!empty($file)) {
                $file_name = $request->file('picture')->getClientOriginalName();
                $file_path = "profile/".date('Ymdhis').'_'.$file_name; 
                $s3 = Storage::disk('s3');
                $s3->put($file_path, file_get_contents($file), array('ContentDisposition' => 'inline; filename=' . $file_name . '', 'ACL' => 'public-read'));
                $picture_attachment_url = Storage::disk('s3')->url($file_path);
            }
            // ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
            // Background Image
            // ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
            $remove_bg_image = $request->get('remove_bg_image');
            if($remove_bg_image == '1') {
                $bg_image_attachment_url = '';
            } else {
                $bg_image_attachment_url = $usrProfile['0']['bg_image'];
            }
            $file = $request->file('bg_image');
            if(!empty($file)) {
                $file_name = $request->file('bg_image')->getClientOriginalName();
                $file_path = "bg_image/".date('Ymdhis').'_'.$file_name; 
                $s3 = Storage::disk('s3');
                $s3->put($file_path, file_get_contents($file), array('ContentDisposition' => 'inline; filename=' . $file_name . '', 'ACL' => 'public-read'));
                $bg_image_attachment_url = Storage::disk('s3')->url($file_path);
            }

            // ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
            // Profile Video
            // ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
            $remove_profile_video = $request->get('remove_profile_video');
            if($remove_profile_video == '1') {
                $profile_video_attachment_url = '';
            } else {
                $profile_video_attachment_url = $usrProfile['0']['profile_video'];
            }
            $file = $request->file('profile_video');
            if(!empty($file)) {
                $file_name = $request->file('profile_video')->getClientOriginalName();
                $file_path = "profile_video/".date('Ymdhis').'_'.$file_name; 
                $s3 = Storage::disk('s3');
                $s3->put($file_path, file_get_contents($file), array('ContentDisposition' => 'inline; filename=' . $file_name . '', 'ACL' => 'public-read'));
                $profile_video_attachment_url = Storage::disk('s3')->url($file_path);
            }


            // Update User Data
            $userData = [];
            $userData['status'] = $validatedData['status'];
            $usr = User::whereId($id)->update($userData);
            // Save Profile Info of updated User
            $user = User::where('id',$id)->first();
            $usrProfileData = [];
            $usrProfileData['user_id']          = $id;
            $usrProfileData['full_name']        = $request->input('full_name');
            $usrProfileData['title']            = $request->input('title');
            $usrProfileData['bio']              = $request->input('user_brief');
            $usrProfileData['contact_number']   = $request->input('contact_number');
            $usrProfileData['picture']          = $picture_attachment_url;
            $usrProfileData['doc_rate_id']      = $request->input('doc_rate_id');
            $usrProfileData['bg_image']         = $bg_image_attachment_url;
            $usrProfileData['profile_video']    = $profile_video_attachment_url;
            $user->profile()->update($usrProfileData); 
            // Attach Selected Role with this User
            $user_role = $request->input('role');
            $user->roles()->sync($user_role);
            // Attach Selected Clients with this User
            if(in_array($user_role,['2','3'])) { // Admin / Doctor user role
                $user_clients = $request->input('client');
                $user->clients()->sync($user_clients);
                if($user_role == '3'){ // Sync Service Type
                    $user_stype = $request->input('stype');
                    $user->stypes()->sync($user_stype);
                }
            }
            if($usr) {
                return redirect()->to('/users')->with('success','Congrats! User has been updated successfully!');
            } else {
                return redirect()->to('/users')->with('error','Sorry! Invalid Request.');
            }
        } catch (Exception $e) {
            return redirect()->to('/contents')->with('error','Invalid Request.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)  {
        //
    }

    // Soft Delete
    public function trash($id)   {
        $user = User::whereId($id)->update(['status'=>'2','is_deleted'=>'1']);
        return redirect()->to('/users')->with('success','User has been successfully moved to trash!');
    }

    // Restore - Soft Deleted User
    public function restore($id)   {
        $user = User::whereId($id)->update(['status'=>'1','is_deleted'=>'0']);
        return redirect()->to('/users')->with('success','User has been successfully restored!');
    }

    // User Roles Page
    public function userRoles()  {
        $roles = Role::all()->toArray();
        return view('users.roles')->with("roles",$roles);
    }

    /**  Method for "Update Profile" for Doctor User.
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
    **/
    public function edit_profile() {
        $id = auth()->user()->id;
        $user                   = User::where(['id' => $id])->first();
        $user_role              = $user->roles()->first()->toArray();
        $user_profile           = $user->profile()->get()->toArray();
        $user_client            = $user->clients()->get()->toArray();
        $userData               = $user->toArray();
        $userData['role']       = $user_role;
        $userData['profile']    = (!empty($user_profile))?$user_profile['0']:['full_name'=>'','title'=>'','contact_number'=>'','bio'=>'','picture'=>''];
        $userData['clients']    = [];
        if(count($user_client) > 0) {
            foreach($user_client as $k=>$v) {
                $userData['clients'][]    = $v['id'];
            }
        }
        //echo "<pre>"; print_r($userData); die;
        $roles                  = Role::all()->toArray();
        $clients                = Client::all()->toArray();
        $status                 = ['1'=>'Active','2'=>'Blocked'];
        return view('users.edit_profile')
        ->with("user",$userData)
        ->with("status",$status)
        ->with("roles",$roles)
        ->with("clients",$clients);
    }

    public function update_profile(Request $request) {
        $id = auth()->user()->id;
        // Validate Fields
        $validateAry = [
           'full_name' => ['required','string','max:255'],
           'title' => ['required']
       ];
       $validatedData = $request->validate($validateAry);

       $remove_picture = $request->get('remove_picture');
       if($remove_picture == '1') {
           $picture_attachment_url = '';
       } else {
           $usrData            = User::where(['id' => $id])->first();
           $usrProfile         = $usrData->profile()->get()->toArray();
           $picture_attachment_url = $usrProfile['0']['picture'];
       }
       $file = $request->file('picture');
       try {
           if(!empty($file)) {
               $file_name = $request->file('picture')->getClientOriginalName();
               //$file_path_info = path_info($file_name);
               $file_path = "profile/".date('Ymdhis').'_'.$file_name;
               $s3 = Storage::disk('s3');
               $s3->put($file_path, file_get_contents($file), array('ContentDisposition' => 'attachment; filename=' . $file_name . '', 'ACL' => 'public-read'));
               $picture_attachment_url = Storage::disk('s3')->url($file_path);
           }
           // Update User Data
           //$userData = [];
           //$userData['status'] = $validatedData['status'];
           //$usr = User::whereId($id)->update($userData);
           // Save Profile Info of updated User
           $user = User::where('id',$id)->first();
           $usrProfileData = [];
           $usrProfileData['user_id']          = $id;
           $usrProfileData['full_name']        = $request->input('full_name');
           $usrProfileData['title']            = $request->input('title');
           $usrProfileData['bio']              = $request->input('user_brief');
           $usrProfileData['contact_number']   = $request->input('contact_number');
           $usrProfileData['picture']          = $picture_attachment_url;
           $user->profile()->update($usrProfileData);

           $role = auth()->user()->getRole();
           $role_segment = ($role == 'doctor')?'/doctor':'';
           if($user) {
               return redirect()->to($role_segment.'/home')->with('success','Congrats! Profile has been updated successfully!');
           } else {
               return redirect()->to($role_segment.'/update-profile')->with('error','Sorry! Invalid Request.');
           }
       } catch (Exception $e) {
           return redirect()->to($role_segment.'/update-profile')->with('error','Invalid Request.');
       }
    }

    //~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //
    //             DOCTOR USERS METHODS               //
    //~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ //

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createdoc() {
        $roles   = Role::getAllRoles();
        $clients = Client::getAllClients();
        $stypes  = Stype::all()->toArray();   // Doctor Service Type
        $status  = ['1'=>'Active','2'=>'Blocked'];
        $langs = Lang::getEnabledLanguages();
        return view('users.createdoc')
        ->with("status",$status)
        ->with("roles",$roles)
        ->with("langs",$langs)
        ->with("clients",$clients)
        ->with("stypes",$stypes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storedoc(Request $request) {
        // $dt = $request->all();
        // echo "<pre>"; print_r($dt); die;
        // Validate Fields
        $validateAry = [
            'name' => ['required','string','max:255','unique:users'],
            'email' => ['required','string','email','max:255','unique:users'],
            'password'   => ['required','string','min:8','confirmed'],
            'status'     => ['required'],
            'client'     => ['required'],
            'doc_rate_id'=> ['required'],
            'stype'      => ['required']
        ];
        $request->validate($validateAry);

        //- - - - - - - - NEED TO DELETE LATER - - - - - - - 
        $doclangdata = []; 
        $ald = $request->input('langdata');
        foreach($ald as $key=>$val) {
            $doclangdata[$key]['doc_full_name']  = (isset($val['full_name']))?$val['full_name']:'';
            $doclangdata[$key]['doc_title']      = (isset($val['title']))?$val['title']:'';
            $doclangdata[$key]['doc_bio']        = (isset($val['user_brief']))?$val['user_brief']:'';
            // Background Image
            $bgimgFile                            = $request->file('langdata_'.$key.'_bg_image');
            $bgimg_url = '';
            if(!empty($bgimgFile)) {
                $bgimgFileName = $bgimgFile->getClientOriginalName();
                $bgimg_file_path = "background/".date('Ymdhis').'_'.$bgimgFileName;
                $s3 = Storage::disk('s3');
                $s3->put($bgimg_file_path, file_get_contents($bgimgFile), array('ContentDisposition' => 'inline; filename=' . $bgimgFileName . '', 'ACL' => 'public-read'));
                $bgimg_url = Storage::disk('s3')->url($bgimg_file_path);
            }
            $doclangdata[$key]['doc_bg_image']     = $bgimg_url;

            // Profile Image
            $prfimgFile                            = $request->file('langdata_'.$key.'doc_picture');
            $prfimg_url = '';
            if(!empty($prfimgFile)) {
                $prfimgFileName = $prfimgFile->getClientOriginalName();
                $prfimg_file_path = "profile/".date('Ymdhis').'_'.$prfimgFileName;
                $s3 = Storage::disk('s3');
                $s3->put($prfimg_file_path, file_get_contents($prfimgFile), array('ContentDisposition' => 'inline; filename=' . $prfimgFileName . '', 'ACL' => 'public-read'));
                $prfimg_url = Storage::disk('s3')->url($prfimg_file_path);
            }
            $doclangdata[$key]['doc_picture']     = $prfimg_url;

            // Profile Video
            $prfvdoFile                            = $request->file('langdata_'.$key.'doc_picture');
            $prfvdo_url = '';
            if(!empty($prfvdoFile)) {
                $prfvdoFileName = $prfvdoFile->getClientOriginalName();
                $prfvdo_file_path = "profile/".date('Ymdhis').'_'.$prfvdoFileName;
                $s3 = Storage::disk('s3');
                $s3->put($prfvdo_file_path, file_get_contents($prfvdoFile), array('ContentDisposition' => 'inline; filename=' . $prfvdoFileName . '', 'ACL' => 'public-read'));
                $prfvdo_url = Storage::disk('s3')->url($prfvdo_file_path);
            }
            $doclangdata[$key]['doc_profile_video']     = $prfvdo_url;
        }

        //- - - - - - - - ENGLISH FIELDS - - - - - - - 
        $doc_full_name      = $doclangdata['1']['doc_full_name'];
        $doc_title          = $doclangdata['1']['doc_title'];
        $doc_bio            = $doclangdata['1']['doc_bio'];
        $doc_profile_video  = $doclangdata['1']['doc_profile_video'];
        $doc_picture        = $doclangdata['1']['doc_picture'];
        $doc_bg_image       = $doclangdata['1']['doc_bg_image'];
        //- - - - - - - - - - - - - - - - - - - - - - - - - 

        // Create User
        $user =  User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
            'status' => $request->input('status'),
        ]);
        // Save Profile Infor of created User
        $usrProfile = new Profile;
        $usrProfile->full_name      = $doc_full_name; //$request->input('full_name');
        $usrProfile->title          = $doc_title; //$request->input('title');
        $usrProfile->bio            = $doc_bio; //$request->input('user_brief');
        $usrProfile->picture        = $doc_picture;
        $usrProfile->contact_number = $request->input('contact_number');
        $usrProfile->doc_rate_id    = $request->input('doc_rate_id');
        $usrProfile->bg_image       = $doc_bg_image;
        $usrProfile->profile_video  = $doc_profile_video;
        $user->profile()->save($usrProfile);


        // Attach Selected Role with this User
        $user_role = ['3'];
        $user->roles()->sync($user_role);

        // Attach Selected Clients with this User
        $user_clients = $request->input('client');
        $user->clients()->sync($user_clients);

        // Attach Service Type with this User
        $user_stype = $request->input('stype');
        $user->stypes()->sync($user_stype);

        // Attach Language Profile with this Doctor
        $user_stype = $request->input('stype');
        $user->langs()->sync($doclangdata);

        if($user) {
            return redirect()->to('/users')->with('success','Congrats! New Doctor added successfully!');
        } else {
            return redirect()->to('/users')->with('error','Sorry! Invalid Request.');
        }
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function editdoc($id) {
        $user                   = User::where(['id' => $id])->first();
        $user_role              = $user->roles()->first()->toArray();
        $user_profile           = $user->profile()->get()->toArray();
        $user_client            = $user->clients()->get()->toArray();
        $user_lang              = $user->langs()->get()->toArray();
        $user_stypes       = $user->stypes()->get()->toArray();
        $userData               = $user->toArray();
        $userData['role']       = $user_role;
        $userData['profile']    = (!empty($user_profile))?$user_profile['0']:['full_name'=>'','title'=>'','contact_number'=>'','bio'=>'','picture'=>''];
        $userData['stypes']       = [];
        if(count($user_stypes) > 0) {
            foreach($user_stypes as $k=>$v) {
                $userData['stypes'][]    = $v['id'];
            }
        }
        $userData['clients']    = [];
        if(count($user_client) > 0) {
            foreach($user_client as $k=>$v) {
                $userData['clients'][]    = $v['id'];
            }
        }
        $userData['langs']    = [];
        if(count($user_lang) > 0) {
            foreach($user_lang as $k=>$v) {
                //$userData['langs'][]    = $v['id'];
                $userData['langs'][$v['id']] = $v['pivot'];
            }
        }
        //echo "<pre>"; print_r($userData); die;
        $roles                  = Role::all()->toArray();
        $clients                = Client::all()->toArray();
        $stypes                 = Stype::all()->toArray();   // Doctor Service Type
        $status                 = ['1'=>'Active','2'=>'Blocked'];
        $langs = Lang::getEnabledLanguages();
        return view('users.editdoc')
        ->with("user",$userData)
        ->with("status",$status)
        ->with("roles",$roles)
        ->with("langs",$langs)
        ->with("stypes",$stypes)
        ->with("clients",$clients);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatedoc(Request $request, $id)  {
        // $dt = $request->all();
        // echo "<pre>"; print_r($dt); die;

        $validateAry = [
            'status'     => ['required'],
            'client'     => ['required'],
            'doc_rate_id'=> ['required'],
            'stype'      => ['required']
        ];
        $request->validate($validateAry);

        $exstUser               = User::where('id',$id)->first();
        $exstUser_langs         = $exstUser->langs()->get()->toArray();
        $exstUserData           = $exstUser->toArray();
        $exstUserData['langs']  = [];
        if(count($exstUser_langs) > 0) {
            $tempUserLang = [];
            foreach($exstUser_langs as $k=>$v) {
                $tempUserLang[$v['id']] = $v['pivot'];
            }
            $exstUserData['langs'] = $tempUserLang;
        }
        //echo"<pre>"; print_r($exstUserData); die;
        //- - - - - - - - NEED TO DELETE LATER - - - - - - - 
        $doclangdata = []; 
        $uld = $request->input('langdata');
        //echo "<pre>"; print_r($uld); die;
        foreach($uld as $key=>$val) {
            $doclangdata[$key]['doc_full_name']  = (isset($val['full_name']))?$val['full_name']:'';
            $doclangdata[$key]['doc_title']      = (isset($val['title']))?$val['title']:'';
            $doclangdata[$key]['doc_bio']        = (isset($val['user_brief']))?$val['user_brief']:'';
            // Background Image
            $bgimg_url      = '';
            $removeBgImg    = $request->input('remove_'.$key.'_doc_bg_image');
            if($removeBgImg == '1') {
                $bgimg_url  = '';
            } else {
                $bgimg_url  = (!empty($exstUserData['langs'][$key]))?$exstUserData['langs'][$key]['doc_bg_image']:'';
            }
            $bgimgFile     = $request->file('langdata_'.$key.'_bg_image');
            if(!empty($bgimgFile)) {
                $bgimgFileName = $bgimgFile->getClientOriginalName();
                $bgimg_file_path = "background/".date('Ymdhis').'_'.$bgimgFileName;
                $s3 = Storage::disk('s3');
                $s3->put($bgimg_file_path, file_get_contents($bgimgFile), array('ContentDisposition' => 'inline; filename=' . $bgimgFileName . '', 'ACL' => 'public-read'));
                $bgimg_url = Storage::disk('s3')->url($bgimg_file_path);
            }
            $doclangdata[$key]['doc_bg_image']     = $bgimg_url;

            // Profile Image            
            $prfimg_url = '11';
            $removePrfImg  = $request->input('remove_'.$key.'_doc_picture');
            if($removePrfImg == '1') {
                $prfimg_url = '';
            } else {
                $prfimg_url = (!empty($exstUserData['langs'][$key]))?$exstUserData['langs'][$key]['doc_picture']:'';
            }    
            $prfimgFile     = $request->file('langdata_'.$key.'_doc_picture');
            if(!empty($prfimgFile)) {
                $prfimgFileName = $prfimgFile->getClientOriginalName();
                $prfimg_file_path = "profile/".date('Ymdhis').'_'.$prfimgFileName;
                $s3 = Storage::disk('s3');
                $s3->put($prfimg_file_path, file_get_contents($prfimgFile), array('ContentDisposition' => 'inline; filename=' . $prfimgFileName . '', 'ACL' => 'public-read'));
                $prfimg_url = Storage::disk('s3')->url($prfimg_file_path);
            }
            $doclangdata[$key]['doc_picture']     = $prfimg_url;

            // Profile Video
            $prfvdo_url = '';
            $removePrfVdo  = $request->input('remove_'.$key.'_doc_profile_video');
            if($removePrfVdo == '1') {
                $prfvdo_url = '';
            } else {
                $prfvdo_url = (!empty($exstUserData['langs'][$key]))?$exstUserData['langs'][$key]['doc_profile_video']:'';
            }
            $prfvdoFile     = $request->file('langdata_'.$key.'_profile_video');
            if(!empty($prfvdoFile)) {
                $prfvdoFileName = $prfvdoFile->getClientOriginalName();
                $prfvdo_file_path = "video/".date('Ymdhis').'_'.$prfvdoFileName;
                $s3 = Storage::disk('s3');
                $s3->put($prfvdo_file_path, file_get_contents($prfvdoFile), array('ContentDisposition' => 'inline; filename=' . $prfvdoFileName . '', 'ACL' => 'public-read'));
                $prfvdo_url = Storage::disk('s3')->url($prfvdo_file_path);
            }
            $doclangdata[$key]['doc_profile_video']     = $prfvdo_url;
        }
        //- - - - - - - - ENGLISH FIELDS - - - - - - - 
        $doc_full_name      = $doclangdata['1']['doc_full_name'];
        $doc_title          = $doclangdata['1']['doc_title'];
        $doc_bio            = $doclangdata['1']['doc_bio'];
        $doc_profile_video  = $doclangdata['1']['doc_profile_video'];
        $doc_picture        = $doclangdata['1']['doc_picture'];
        $doc_bg_image       = $doclangdata['1']['doc_bg_image'];
        //- - - - - - - - - - - - - - - - - - - - - - - - - 

        $userPutData = [];
        $userPutData['status'] = $request->input('status');
        $usr = User::whereId($id)->update($userPutData);

        $user = User::where('id',$id)->first();
        $usrProfileData = [];
        $usrProfileData['user_id']          = $id;
        $usrProfileData['full_name']        = $doc_full_name;
        $usrProfileData['title']            = $doc_title;
        $usrProfileData['bio']              = $doc_bio;
        $usrProfileData['contact_number']   = $request->input('contact_number');
        $usrProfileData['picture']          = $doc_picture;
        $usrProfileData['doc_rate_id']      = $request->input('doc_rate_id');
        $usrProfileData['bg_image']         = $doc_bg_image;
        $usrProfileData['profile_video']    = $doc_profile_video;
        $user->profile()->update($usrProfileData);

        // Attach Selected Role with this User
        $user_role = ['3'];
        $user->roles()->sync($user_role);

        // Attach Selected Clients with this User
        $user_clients = $request->input('client');
        $user->clients()->sync($user_clients);

        // Attach Service Type with this User
        $user_stype = $request->input('stype');
        $user->stypes()->sync($user_stype);

        // Attach Language Profile with this Doctor
        $user->langs()->sync($doclangdata);

        if($user) {
            return redirect()->to('/users')->with('success','Congrats! Doctor has been updated successfully!');
        } else {
            return redirect()->to('/users')->with('error','Sorry! Invalid Request.');
        }
    }

}