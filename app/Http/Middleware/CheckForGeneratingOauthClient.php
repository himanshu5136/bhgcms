<?php

namespace App\Http\Middleware;

use Closure;

class CheckForGeneratingOauthClient
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $response = $next($request);
        $customAuthKey = $request->header('Authorization');
        $envOauthKey   = env('CSTM_OAUTH_KEY', '');
        if($envOauthKey != $customAuthKey) {
            return response()->json(['message'=>'Unauthorized Access'],401);
        }
        return $response;
    }
}
