<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckUserStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $response = $next($request);
        //If the status is not approved redirect to login 
        if(Auth::check() && Auth::user()->status != '1' && Auth::user()->is_deleted != '0'){
            Auth::logout();
            return redirect('/')->with('error', 'Sorry! Invalid Login request from blocked user. Please contact admin department.');
        }
        return $response;
    }
}
