@extends('layouts.dashboard')

@section('content')
<div class="page-heading"><h3>Add User</h3></div>
@include('flash-message')
<style type="text/css">
    h4.subheading{border-bottom: 2px solid #F4D03F;color: #333;font-size: 20px;margin-bottom: 20px;background: #ECDDA199;padding: 6px;}
</style>

<div class="col-md-8 col-md-offset-2 users-create-page superusers form large-9 medium-8 columns content">
	<section class="panel">
		<div class="panel-body">
            <form method="POST" action="{{ route('users.store') }}">
            @csrf
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="subheading">Account Information</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="required">Username</label><br />
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                            @error('name')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="required">Email</label><br />
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                            @error('email')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="required">Password</label><br />
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                            @error('password')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>               
                    <div class="col-lg-6">
                        <div class="form-group required">
                            <label class="required">Confirm Password</label><br />
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                        </div>
                    </div>
                </div>
                 <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="required">User Role</label><br />
                            <select class="form-control @error('role') is-invalid @enderror" name="role" id="user-roles" required autocomplete="role">
                                <?php foreach($roles as $k=>$v) { ?>
                                <option {{ old('role')==$k ? 'selected' : ''  }} value="<?= $k ?>"><?= $v ?></option>
                                <?php } ?>
                            </select>
                            @error('role')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="required">Status</label><br />
                            <select class="form-control @error('status') is-invalid @enderror" name="status" id="status" required>
                                <?php foreach($status as $k=>$v) { ?>
                                <option value="<?= $k ?>"><?= $v ?></option>
                                <?php } ?>
                            </select>
                            @error('status')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        @php
                            $old_role_client_display = ((old('role') == '2') || (old('role') == '3')) ? 'display_block' : 'display_none';
                        @endphp
                        <div id="form-field-clients" class="form-checkbox-wrapper  {{ $old_role_client_display }}">
                            <label>Clients</label><br />
                            <div id="create_user_client_listing_form_group" class="form-control">
                                <?php foreach($clients as $k=>$v) { ?>
                                    <div class="form-item">
                                    <input id="client_<?= $k ?>" type="checkbox" class="form-control @error('client') is-invalid @enderror" name="client[]" value="<?= $k ?>" autocomplete="client">
                                    <label for="client_<?= $k ?>"><?= $v ?></label>
                                    </div>
                                <?php } ?>
                            </div>
                            <span class="help-text">( Choose minimum one client for "Admin" User Role )</span>
                            @error('client')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="subheading">Profile Information</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="required">Full Name</label><br />
                            <input id="full_name" type="text" class="form-control @error('full_name') is-invalid @enderror" name="full_name" value="{{ old('full_name') }}" required autocomplete="full_name" autofocus>
                            @error('full_name')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label >Profile Picture</label><br />
                            <input id="picture" type="file" class="form-control @error('picture') is-invalid @enderror" name="picture" value="{{ old('picture') }}" autocomplete="picture" autofocus>
                            @error('picture')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label >Contact Number</label><br />
                            <input id="contact_number" type="text" class="form-control @error('contact_number') is-invalid @enderror" name="contact_number" value="{{ old('contact_number') }}" autocomplete="contact_number">
                            @error('contact_number')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="required">Title</label><br />
                            <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}" required autocomplete="title" autofocus>
                            @error('title')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>Bio</label><br />
                            <textarea  id="user_brief" name="user_brief" class="form-control @error('user_brief') is-invalid @enderror" cols="5" rows="4">{{ old('user_brief') }}</textarea>
                            @error('user_brief')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                
                <br />
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">{{ __('Add User') }}</button>
                        </div>
                    </div>
                </div>
            </form>
		</div>
	</section>
</div>
@endsection