@extends('layouts.dashboard')

@section('content')
<div class="page-heading"><h3>Content Detail</h3></div>
@include('flash-message')
<?php 
$role = auth()->user()->getRole();
$role_segment = ($role == 'admin')?'/admin':'';
$base_url  = URL::to('/').$role_segment;
?>
<style type="text/css">
    .panel-header{ padding: 10px; }
</style>
<div class="col-md-8 col-md-offset-2 tag-view-page tags form large-9 medium-8 columns content">
	<section class="panel">
        <div class="panel-header">
            <div class="row action-items">
                <div class="col-md-6 text-left">
                    <a title="Back to Content Listing" href="<?= $base_url ?>/contents" class="btn btn-primary"><i class="fa fa-backward"></i></a>
                </div>
                <div class="col-md-6 text-right">
                    <a title="Edit Content" href="<?= $base_url ?>/contents/<?=  $content['id'] ?>/edit" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                </div>
            </div>
        </div>
		<div class="panel-body">
        <?php if(count($content) > 0) { ?>
        <table class="table table-bordered tags-view table-striped">
            <thead>
                <tr>
                    <th width="50%">KEY</th>
                    <th width="50%">VALUE</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Id</td>
                    <td><?=  $content['id'] ?></td>
                </tr>
                <tr>
                    <td>Title</td>
                    <td><?= $content['title'] ?></td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td><?=  $content['status'] ?></td>
                </tr>
                <tr>
                    <td>Author</td>
                    <td><?=  $content['user_full_name'] ?>
                    <?php if($role != 'admin') { ?>
                    &nbsp;<a href="<?= $base_url ?>/users/<?= $content['user_id'] ?>" target="_blank"><i class="fa fa-external-link"></i></a>
                    <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td>Attachment</td>
                    <td><a href="<?= $content['attachment'] ?>" target="_blank"><?= $content['attachment'] ?>&nbsp;<i class="fa fa-external-link"></i></a></td>
                </tr>
                
                <tr>
                    <td>Categories<br /><span class="help-text">( Associated with this content )</span></td>
                    <td>
                    <?php 
                    if(count($content['tags']) > 0 ) {
                        echo "<ul>";
                        foreach($content['tags'] as $k=>$v) {
                        ?>
                        <li><?= $v['name'] ?>
                        <?php if($role != 'admin') { ?>
                        &nbsp;&nbsp;<a target="_blank" href="<?= $base_url ?>/tags/<?= $v['id'] ?>" title="View Category"><i class="fa fa-tag"></i></a>
                        <?php } ?>
                        </li>
                        <?php 
                        } 
                        echo "</ul>";
                    }
                    ?>
                    
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="text-center"><strong>Content Body</strong></td>
                </tr>
                <tr>
                    <td colspan="2"><?= $content['body'] ?></td>
                </tr>
                
            </tbody>
        </table>
        <?php } else {
            echo '<div class="text-center">No Record Found.</div>';
        } ?>

		</div>
	</section>
</div>
@endsection