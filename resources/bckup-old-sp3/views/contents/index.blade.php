@extends('layouts.dashboard')

@section('content')
<div class="page-heading"><h3>List of Content</h3></div>
@include('flash-message')
<?php 
$role = auth()->user()->getRole();
$role_segment = ($role == 'admin')?'/admin':'';
$base_url  = URL::to('/').$role_segment;
?>
<style type="text/css">
    table tr td.content-tag-count-col span{display: block; text-align: center;}
</style>
<div class="contents form large-9 medium-8 columns content">
	<section class="panel">
        <div class="panel-heading ">
            <div class="row contents_filters_wrapper bg-secondary">
                <form method="get" id="form_contents_filter" >
                    <div class="col-md-3 form-group">
                        <label>Title</label><br />
                        <input class="form-control" type="text" name="cnm" id="contents-filter-cnm" value="<?= (!empty($qry['cnm']))?$qry['cnm']:'' ?>" /> 
                    </div>
                    <div class="col-md-3 form-group">
                        <label>Category</label><br />
                        <select class="form-control @error('tid') is-invalid @enderror" name="tid" id="tid">
                            <option value="">- Select One -</option>
                            @foreach( $tags as $k=>$v)
                                <option <?php if((!empty($qry['tid'])) && ($qry['tid'] == $v['id'])){ echo "selected"; } ?> value="{{ $v['id'] }}">{{ $v['name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-3 form-group">
                        <label>Status</label><br />
                        <select class="form-control @error('sts') is-invalid @enderror" name="sts" id="sts">
                            <option value="">- Select One -</option>
                            @foreach( $status as $k=>$v)
                                <option <?php if((!empty($qry['sts'])) && ($qry['sts'] == $k)){ echo "selected"; } ?> value="{{ $k }}">{{ $v }}</option>
                            @endforeach
                        </select>
                    </div>	
                    <div class="col-md-3 form-group">  
                        <label>&nbsp;</label><br />
                        <input type="submit" name="submit" value="Search" class="btn btn-primary" />
                        <a href="<?= $base_url ?>/contents" class="btn btn-secondary">Reset</a>
                    </div>	
                </form>
            </div>	
        </div>
		<div class="panel-body">
        <?php if(count($contents) > 0) { ?>
        <table class="table  table-hover general-table">
            <thead>
                <tr>
                    <th width="10%">Content Id</th>
                    <th width="20%">Title</th>
                    <th width="10%">Status</th>
                    <?php /* ?><th width="10%">Categories</th><?php */ ?>
                    <th width="10%">Attachment</th>
                    <th width="20%">Created Date</th>
                    <th width="10%">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($contents as $content)
                <tr>
                    <td>{{ $content['id'] }}</td>
                    <td>{{ $content['title'] }}</td>
                    <td>{{ $status[$content['status']] }}</td>
                    <?php /* ?><td class="content-tag-count-col">
                        <span><strong>{{ count($content['tags']) }}</strong></span>
                    </td><?php */ ?>
                    <td>{{ (!empty($content['attachment']))?'Yes':'No' }}</td>
                    <td>{{ date('M d, Y H:i:s',strtotime($content['created_at'])) }}</td>
                    <td class="actions">
                        <a href="<?= $base_url ?>/contents/<?= $content['id'] ?>" title="View Content"><i class="fa fa-search-plus"></i></a>&nbsp;&nbsp;
                        <a href="<?= $base_url ?>/contents/<?= $content['id'] ?>/edit" title="Edit Content"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;
                    
                        <form name="post_content_<?= $content['id'] ?>" style="display:none;" method="post" action="{{ route('contents.destroy', $content['id']) }}" > @method('DELETE')  @csrf</form>
                        <a href="#" onclick="if (confirm(&quot;Are you sure you want to delete content <?= $content['title'] ?>?&quot;)) { document.post_content_<?= $content['id'] ?>.submit(); } event.returnValue = false; return false;" title="Delete Category"><i class="fa fa-trash-o"></i></a>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        <?php } else {
            echo '<div class="text-center">No Record Found.</div>';
        } ?>
		</div>
	</section>
</div>
@endsection