@extends('layouts.dashboard')

@section('content')
<style type="text/css">
    #create_content_tag_listing_form_group{ height: 166px; overflow-y: scroll; }
    #create_content_tag_listing_form_group .form-item{display: flex;}
    #create_content_tag_listing_form_group .form-item input {width: auto; height: auto; margin-right: 5px;}
    #create_content_tag_listing_form_group .form-item label{font-weight: normal;}
</style>
<div class="page-heading"><h3>Add Content</h3></div>
@include('flash-message')
<div class="col-md-8 col-md-offset-2 contents-create-page contents form large-9 medium-8 columns content">
	<section class="panel">
		<div class="panel-body">
            <form method="POST" enctype="multipart/form-data" action="{{ route('contents.store') }}">
            @csrf
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="required">Title</label><br />
                            <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}" required autocomplete="title" autofocus>
                            @error('title')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class="required">Status</label><br />
                            <select class="form-control @error('status') is-invalid @enderror" name="status" id="status" required>
                                <?php foreach($status as $k=>$v) { ?>
                                <option value="<?= $k ?>"><?= $v ?></option>
                                <?php } ?>
                            </select>
                            @error('status')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label >Attachment</label><br />
                            <input id="attachment" type="file" class="form-control @error('attachment') is-invalid @enderror" name="attachment" value="{{ old('attachment') }}" autocomplete="attachment" autofocus>
                            @error('attachment')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div id="form-field-tags" class="form-group form-checkbox-wrapper">
                            <label class="required">Categories</label><br />
                            <div id="create_content_tag_listing_form_group" class="form-control">
                                <?php foreach($tags as $k=>$v) { ?>
                                    <div class="form-item">
                                    <input id="tag_<?= $v['id'] ?>" type="checkbox" class="form-control @error('tag') is-invalid @enderror" name="tag[]" value="<?= $v['id'] ?>" autocomplete="tag">
                                    <label for="tag_<?= $v['id'] ?>"><?= $v['name'] ?></label>
                                    </div>
                                <?php } ?>
                            </div>
                            <span class="help-text">( Choose minimum one category to create Content. )</span>
                            @error('tag')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Body</label><br />
                            <textarea  id="body" name="body" class="form-control @error('body') is-invalid @enderror" cols="5" rows="8">{{ old('body') }}</textarea>
                            @error('body')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                <br />
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">{{ __('Add Content') }}</button>
                        </div>
                    </div>
                </div>
            </form>
		</div>
	</section>
</div>
@endsection