@extends('layouts.dashboard')

@section('content')

<?php 
$role = auth()->user()->getRole();
$role_segment = ($role == 'admin')?'/admin':'';
$base_url  = URL::to('/').$role_segment;
?>
<style type="text/css">
    #create_content_tag_listing_form_group{ height: 166px; overflow-y: scroll; }
    #create_content_tag_listing_form_group .form-item{display: flex;}
    #create_content_tag_listing_form_group .form-item input {width: auto; height: auto; margin-right: 5px;}
    #create_content_tag_listing_form_group .form-item label{font-weight: normal;}
    .existing-attachment{text-align:center;background: #ECDDA199; height: 76px; display: block; width: 50px; margin-top: 5px; border: 1px solid #F4D03F;}
    .existing-attachment a{font-size: 10px;line-height: 0px;color: #333;text-decoration: underline;}
    .panel-header{ padding: 10px; }
</style>
<div class="page-heading"><h3>Update Content</h3></div>
@include('flash-message')
<div class="col-md-8 col-md-offset-2 contents-edit-page contents form large-9 medium-8 columns content">
	<section class="panel">
        <div class="panel-header">
            <div class="row action-items">
                <div class="col-md-6 text-left">
                    <a title="Back to Content Listing" href="<?= $base_url ?>/contents" class="btn btn-primary"><i class="fa fa-backward"></i></a>
                </div>
                <div class="col-md-6 text-right">
                    <a title="View Content" href="<?= $base_url ?>/contents/<?=  $content['id'] ?>" class="btn btn-primary"><i class="fa fa-search-plus"></i></a>
                </div>
            </div>
        </div>
		<div class="panel-body">
            <form method="post" enctype="multipart/form-data" action="{{ route('contents.update', $content['id']) }}">
            @method('PATCH') 
            @csrf
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="required">Title</label><br />
                            <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" required autocomplete="title" autofocus value="{{ $content['title'] }}">
                            @error('title')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class="required">Status</label><br />
                            <select class="form-control @error('status') is-invalid @enderror" name="status" id="status" required>
                                <?php foreach($status as $k=>$v) { ?>
                                <option 
                                <?= ($k == $content['status'])?'selected':''?> 
                                value="<?= $k ?>"><?= $v ?></option>
                                <?php } ?>
                            </select>
                            @error('status')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <?php if(!empty($content['attachment'])) { ?>
                                <div class="col-md-2 existing-attachment">
                                <a href="{{ $content['attachment'] }}" target="blank">Link to file</a>
                                </div>
                                <div class="col-md-10">
                            <?php } ?>
                                <label >Attachment</label><br />
                                <input id="attachment" type="file" class="form-control @error('attachment') is-invalid @enderror" name="attachment" value="{{ $content['attachment'] }}" autocomplete="attachment" autofocus>
                                @error('attachment')
                                    <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                @enderror
                                <div class=""><input value="1" type="checkbox" name="remove_attachment" id="remove_attachment"/><label for="remove_attachment">&nbsp;Remove Attachment</label></div>
                            <?php if(!empty($content['attachment'])) { ?>
                                </div>
                            <?php } ?>
                        </div>
                        
                    </div>
                    <div class="col-lg-6">
                        <div id="form-field-tags" class="form-group form-checkbox-wrapper">
                            <label class="required">Tags</label><br />
                            <div id="create_content_tag_listing_form_group" class="form-control">
                                <?php foreach($tags as $k=>$v) { ?>
                                    <div class="form-item">
                                    <input  
                                    <?= (in_array($v['id'],$content['tags']))?'checked':''?> 
                                    id="tag_<?= $v['id'] ?>" type="checkbox" class="form-control @error('tag') is-invalid @enderror" name="tag[]" value="<?= $v['id'] ?>" autocomplete="tag">
                                    <label for="tag_<?= $v['id'] ?>"><?= $v['name'] ?></label>
                                    </div>
                                <?php } ?>
                            </div>
                            <span class="help-text">( Choose minimum one category for updating content. )</span>
                            @error('tag')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Body</label><br />
                            <textarea  id="body" name="body" class="form-control @error('body') is-invalid @enderror" cols="5" rows="8">{{ $content['body'] }}</textarea>
                            @error('body')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">{{ __('Update Client') }}</button>
                        </div>
                    </div>
                </div>
            </form>
		</div>
	</section>
</div>
@endsection