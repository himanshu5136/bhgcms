@extends('layouts.dashboard')

@section('content')
<?php 
// Base URL
$role           = auth()->user()->getRole();
$role_segment   = ($role == 'doctor')?'/doctor':'';
$base_url       = URL::to('/').$role_segment;
$statusAry      = ['1'=>'Active','2'=>'Blocked', ];
$currentUser 	    = auth()->user(); // Current User Array
$user               = $currentUser->toArray();
$user['role']       = $currentUser->roles()->first()->toArray();
$user['profile']    = $currentUser->profile()->first()->toArray();
?>
<style type="text/css">
td.subheading{border-bottom: 2px solid #F4D03F !important;color: #333;font-size: 20px;margin-bottom: 20px;background: #ECDDA199 !important;padding: 6px !important;}
</style>
<!-- page heading start-->
<div class="page-heading">
    <h3>Welcome to Doctor Dashboard</h3>
</div>
@include('flash-message')
<!-- page heading end--> 
<div class="col-md-8 col-md-offset-2 superusers-view-page form large-9 medium-8 columns content">
	<section class="panel">
        <div class="panel-header">
            <div class="row action-items">
                <div class="col-md-12 text-center">
                    <a class="btn btn-primary" href="<?= $base_url ?>/update-profile">Update your profile</a>
                </div>
            </div>
        </div>
		<div class="panel-body">
        <?php if(count($user) > 0) { ?>
        <table class="table table-bordered users-view table-striped">
            <tbody>
                <tr>
                    <td class="subheading" colspan="2">Account Information</td>
                </tr>
                <tr>
                    <td width="50%">Id</td>
                    <td width="50%"><?=  $user['id'] ?></td>
                </tr>
                
                <tr>
                    <td>User Role</td>
                    <td><?=  $user['role']['name'] ?></td>
                </tr>
                <tr>
                    <td>Username</td>
                    <td><?= $user['name'] ?></td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td><?= $user['email'] ?></td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td><?= $statusAry[$user['status']] ?></td>
                </tr>
                <tr>
                    <td>Created At</td>
                    <td><?= date('M d,Y H:i:s',strtotime($user['created_at'])) ?></td>
                </tr>
                <tr>
                    <td>Updated At</td>
                    <td><?= date('M d,Y H:i:s',strtotime($user['updated_at'])) ?></td>
                </tr>
                <tr>
                    <td class="subheading" colspan="2">Profile Information</td>
                </tr>
                <tr>
                    <td>Full Name</td>
                    <td><?=  $user['profile']['full_name'] ?></td>
                </tr>
                <tr>
                    <td>Title</td>
                    <td><?=  $user['profile']['title'] ?></td>
                </tr>
                <tr>
                    <td>Picture</td>
                    <td>
                    <?php if(!empty($user['profile']['picture'])){ ?>
                        <img src="<?= $user['profile']['picture'] ?>" width="100px"/>
                    <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td>Contact Number</td>
                    <td><?=  $user['profile']['contact_number'] ?></td>
                </tr>
                <tr>
                    <td>Bio</td>
                    <td><?=  $user['profile']['bio'] ?></td>
                </tr>
            </tbody>
        </table>
        <?php } else {
            echo '<div class="text-center">No Record Found.</div>';
        } ?>

		</div>
	</section>
</div>
@endsection
