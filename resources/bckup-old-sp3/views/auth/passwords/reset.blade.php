@extends('layouts.login')

@section('content')
<?php /* ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div><?php */ ?>


<div class="container">
    <div id="user-reset-password-wrapper">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        <form class="form-signin" method="POST" action="{{ route('password.update') }}">
            @csrf
            <input type="hidden" name="token" value="{{ $token }}">
            <div class="form-signin-heading text-center">
                <img class="img-responsive" src="{{ asset('backend/images/bhg_logo.png') }}" alt=""/>
            </div>
            <div class="form-title text-center">
                <h4>Reset Password</h4>
            </div>
            <div class="login-wrap">
                <!-- EMAIL -->
                <div class="form-group row">
                    <div class="col-md-12">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" placeholder="E-Mail Address" autofocus>
                        @error('email')
                            <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i>{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <!-- PSW -->
                <div class="form-group row">
                    <div class="col-md-12">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required placeholder="Password" autocomplete="new-password">
                        @error('password')
                            <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i>{{ $message }}</span>
                        @enderror

                    </div>
                </div>
                <!-- CNFRM PSW -->
                <div class="form-group row">
                    <div class="col-md-12">                      
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Confirm Password" >
                    </div>
                </div>
                <div class="form-group row mb-0">
                    <div class="col-md-12 offset-md-4">
                        <button title="Reset Password" type="submit" class="btn btn-login btn-block">
                            <i class="fa fa-refresh"></i>
                        </button>
                    </div>
                </div>
                <div class="registration">
                        Remember Credentials?
                        <a class="" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </div>
            </div>
        </form>
    </div>
</div>
@endsection
