@extends('layouts.dashboard')

@section('content')
<?php 
// Base URL
$base_url 		= URL::to('/');
/* ?><div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div><?php */ ?>
<!-- page heading start-->
<div class="page-heading">
            <h3>Dashboard</h3>
        </div>
        <!-- page heading end-->

        <!--body wrapper start-->
        <div class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <!--statistics start-->
                    <div class="row state-overview">
                        <div class="col-md-4 col-xs-12 col-sm-6">
                            <div class="panel purple">
                                <div class="symbol">
                                    <i class="fa fa-users"></i>
                                </div>
                                <div class="state-value">
                                    <div class="value"><?= $usersCount ?></div>
                                    <div class="title">Users</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-12 col-sm-6">
                            <div class="panel green">
                                <div class="symbol">
                                    <i class="fa fa-laptop"></i>
                                </div>
                                <div class="state-value">
                                    <div class="value">4</div>
                                    <div class="title">Clients</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-12 col-sm-6">
                            <div class="panel blue">
                                <div class="symbol">
                                    <i class="fa fa-user-md"></i>
                                </div>
                                <div class="state-value">
                                    <div class="value">214</div>
                                    <div class="title"> Doctors</div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="row state-overview">
                        <div class="col-md-2 col-xs-12 col-sm-6"></div>
                        <div class="col-md-4 col-xs-12 col-sm-6">
                            <div class="panel green">
                                <div class="symbol">
                                    <i class="fa fa-tags"></i>
                                </div>
                                <div class="state-value">
                                    <div class="value">20</div>
                                    <div class="title">Categories</div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-4 col-xs-12 col-sm-6">
                            <div class="panel blue">
                                <div class="symbol">
                                    <i class="fa fa-file-text"></i>
                                </div>
                                <div class="state-value">
                                    <div class="value">220</div>
                                    <div class="title">Content</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-xs-12 col-sm-6"></div>
                    </div>
                    <!--statistics end-->
                </div>
               
            </div>
            <div class="dashboard-action-items">
                <div class="row">
                    <div class="col-md-6 text-center col-md-offset-3">
                        <div class="dashboard-action-items-heading"><h3>Add New</h3></div>
                        <div class="dashboard-action-items-body">
                            <ul>
                                <li><a href="<?= $base_url ?>/users/add" title="Add New User"><i class="fa fa-users"></i><span>User</span></a></li>
                                <li><a href="<?= $base_url ?>/clients/add" title="Add New Client"><i class="fa fa-laptop"></i><span>Client</span></a></li>
                                <li><a href="<?= $base_url ?>/doctors/add" title="Add New Doctor"><i class="fa fa-user-md"></i><span>Doctor</span></a></li>
                                <li><a href="<?= $base_url ?>/tags/add" title="Add New Category"><i class="fa fa-tag"></i><span>Category</span></a></li>
                                <li><a href="<?= $base_url ?>/contents/add" title="Add New Content"><i class="fa fa-file-text"></i><span>Content</span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--body wrapper end-->
@endsection
