@extends('layouts.dashboard')

@section('content')
<?php 
// Base URL
$base_url 		= URL::to('/');
?>
<!-- page heading start-->
<div class="page-heading">
            <h3>Dashboard</h3>
        </div>
        <!-- page heading end-->

        <!--body wrapper start-->
        <div class="wrapper">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <!--statistics start-->
                    <div class="row state-overview">
                        <div class="col-md-4 col-xs-12 col-sm-6">
                            <div class="panel purple">
                                <div class="symbol">
                                    <i class="fa fa-users"></i>
                                </div>
                                <div class="state-value" style="width: 42%;">
                                    <div class="value"><?= $counts['users'] ?></div>
                                    <div class="title">Users</div>
                                </div>
                                <div class="state-link" style="display: inline-block;width: 17%;">
                                    <a title="View Users" href="<?= $base_url ?>/users" style="font-size: 32px;color: #fff;">
                                        <i class="fa fa-arrow-circle-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-12 col-sm-6">
                            <div class="panel green">
                                <div class="symbol">
                                    <i class="fa fa-laptop"></i>
                                </div>
                                <div class="state-value" style="width: 42%;">
                                    <div class="value"><?= $counts['clients'] ?></div>
                                    <div class="title">Clients</div>
                                </div>
                                <div class="state-link" style="display: inline-block;width: 17%;">
                                    <a title="View Clients" href="<?= $base_url ?>/clients" style="font-size: 32px;color: #fff;">
                                        <i class="fa fa-arrow-circle-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-12 col-sm-6">
                            <div class="panel red">
                                <div class="symbol">
                                    <i class="fa fa-user-md"></i>
                                </div>
                                <div class="state-value" style="width: 42%;">
                                    <div class="value"><?= $counts['doctors'] ?></div>
                                    <div class="title"> Doctors</div>
                                </div>
                                <div class="state-link" style="display: inline-block;width: 17%;">
                                    <a title="View Doctors" href="<?= $base_url ?>/doctors" style="font-size: 32px;color: #fff;">
                                        <i class="fa fa-arrow-circle-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row state-overview">
                        <div class="col-md-2 col-xs-12 col-sm-6"></div>
                        <div class="col-md-4 col-xs-12 col-sm-6">
                            <div class="panel blue">
                                <div class="symbol">
                                    <i class="fa fa-tags"></i>
                                </div>
                                <div class="state-value" style="width: 42%;">
                                    <div class="value"><?= $counts['categories'] ?></div>
                                    <div class="title">Categories</div>
                                </div>
                                <div class="state-link" style="display: inline-block;width: 17%;">
                                    <a title="View Categories" href="<?= $base_url ?>/tags" style="font-size: 32px;color: #fff;">
                                        <i class="fa fa-arrow-circle-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-4 col-xs-12 col-sm-6">
                            <div class="panel purple">
                                <div class="symbol">
                                    <i class="fa fa-file-text"></i>
                                </div>
                                <div class="state-value" style="width: 42%;">
                                    <div class="value"><?= $counts['contents'] ?></div>
                                    <div class="title">Content</div>
                                </div>
                                <div class="state-link" style="display: inline-block;width: 17%;">
                                    <a title="View Contents" href="<?= $base_url ?>/contents" style="font-size: 32px;color: #fff;">
                                        <i class="fa fa-arrow-circle-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-xs-12 col-sm-6"></div>
                    </div>
                    <!--statistics end-->
                </div>
               
            </div>
            <div class="dashboard-action-items">
                <div class="row">
                    <div class="col-md-6 text-center col-md-offset-3">
                        <div class="dashboard-action-items-heading"><h3>Add New</h3></div>
                        <div class="dashboard-action-items-body">
                            <ul>
                                <li><a href="<?= $base_url ?>/users/create" title="Add New User"><i class="fa fa-users"></i><span>User</span></a></li>
                                <li><a href="<?= $base_url ?>/clients/create" title="Add New Client"><i class="fa fa-laptop"></i><span>Client</span></a></li>
                                <li><a href="<?= $base_url ?>/tags/create" title="Add New Category"><i class="fa fa-tag"></i><span>Category</span></a></li>
                                <li><a href="<?= $base_url ?>/contents/create" title="Add New Content"><i class="fa fa-file-text"></i><span>Content</span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--body wrapper end-->
@endsection
