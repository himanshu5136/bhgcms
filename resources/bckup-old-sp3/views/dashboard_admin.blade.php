@extends('layouts.dashboard')

@section('content')
<?php 
// Base URL
$base_url 		= URL::to('/');
/* ?><div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div><?php */ ?>
<!-- page heading start-->
<div class="page-heading">
            <h3>Welcome to Admin Dashboard</h3>
        </div>
        <!-- page heading end-->
<div class="wrapper">
    <div class="row">
        <div class="col-md-offset-2 col-md-8 text-center">
            <h4 class="text-center">Assigned Clients to you</h4>
            <table class="table table-hover general-table">
                <thead>
                    <tr>
                        <th class="text-center" width="10%">Client ID</th>
                        <th class="text-center" width="40%">Name</th>
                        <th class="text-center" width="50%">Description</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($clients as $k=>$v) { ?>
                        <tr>
                            <td><?= $v['id'] ?></td>
                            <td><?= $v['name'] ?></td>
                            <td><?= $v['description'] ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
