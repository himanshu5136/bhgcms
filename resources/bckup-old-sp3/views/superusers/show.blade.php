@extends('layouts.dashboard')

@section('content')
<div class="page-heading"><h3>User Detail</h3></div>
@include('flash-message')
<?php 
$statusAry = ['1'=>'Active','2'=>'Blocked', ];
$base_url  = URL::to('/');
$user      = (array)$user;
?>

<div class="col-md-8 col-md-offset-2 superusers-view-page form large-9 medium-8 columns content">
	<section class="panel">
        <div class="panel-header">
            <div class="row action-items">
                <div class="col-md-6 text-left">
                    <a href="<?= $base_url ?>/users" class="btn btn-primary">Back</a>
                </div>
                <div class="col-md-6 text-right">
                    <a href="<?= $base_url ?>/user/<?=  $user['id'] ?>/edit" class="btn btn-primary">Edit User</a>
                </div>
            </div>
        </div>
		<div class="panel-body">
        <?php if(count($user) > 0) { ?>
        <table class="table table-bordered superusers-view table-striped">
            <thead>
                <tr>
                    <th width="50%">KEY</th>
                    <th width="50%">VALUE</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Id</td>
                    <td><?=  $user['id'] ?></td>
                </tr>
                <tr>
                    <td>Username</td>
                    <td><?= $user['name'] ?></td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td><?= $user['email'] ?></td>
                </tr>
                <tr>
                    <td>Full Name</td>
                    <td><?= $user['full_name'] ?></td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td><?= $statusAry[$user['status']] ?></td>
                </tr>
                <tr>
                    <td>Created At</td>
                    <td><?= $user['created_at'] ?></td>
                </tr>
                <tr>
                    <td>Contact Number</td>
                    <td><?= $user['contact_number'] ?></td>
                </tr>
                <tr>
                    <td>Biography</td>
                    <td><?= $user['user_brief'] ?></td>
                </tr>
            </tbody>
        </table>
        <?php } else {
            echo '<div class="text-center">No Record Found.</div>';
        } ?>

		</div>
	</section>
</div>
@endsection