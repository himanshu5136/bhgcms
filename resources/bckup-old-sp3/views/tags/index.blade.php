@extends('layouts.dashboard')

@section('content')
<div class="page-heading"><h3>List of Categories</h3></div>
@include('flash-message')
<?php 
$role = auth()->user()->getRole();
$role_segment = ($role == 'admin')?'/admin':'';
$base_url  = URL::to('/').$role_segment;
?>
<style type="text/css">
    table tr td.client-doctor-count-col span{display: block; text-align: center;}
</style>
<div class="clients form large-9 medium-8 columns content">
	<section class="panel">
        <div class="panel-heading ">
            <div class="row clients_filters_wrapper bg-secondary">
                <form method="get" id="form_clients_filter" >
                    <div class="col-md-3 form-group">
                        <label>Category Name</label><br />
                        <input class="form-control" type="text" name="tnm" id="clients-filter-tnm" value="<?= (!empty($qry['tnm']))?$qry['tnm']:'' ?>" /> 
                    </div>
                    <div class="col-md-3 form-group">
                        <label>Client</label><br />
                        <select class="form-control @error('cid') is-invalid @enderror" name="cid" id="cid">
                            <option value="">- Select One -</option>
                            @foreach($clients as $k=>$v)
                                <option <?php if((!empty($qry['cid'])) && ($qry['cid'] == $v['id'])){ echo "selected"; } ?> value="{{ $v['id'] }}">{{ $v['name'] }}</option>
                            @endforeach
                        </select>
                    </div>	
                    <div class="col-md-3 form-group">  
                        <label>&nbsp;</label><br />
                        <input type="submit" name="submit" value="Search" class="btn btn-primary" />
                        <a href="<?= $base_url ?>/clients" class="btn btn-secondary">Reset</a>
                    </div>	
                </form>
            </div>	
        </div>
		<div class="panel-body">
        <?php if(count($tags) > 0) { ?>
        <table class="table  table-hover general-table">
            <thead>
                <tr>
                    <th width="8%">Cat_Id</th>
                    <th width="12%">Name</th>
                    <th width="40%">Description</th>
                    <th width="10%">Clients</th>
                    <th width="20%">Created Date</th>
                    <th width="10%">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($tags as $tag)
                <tr>
                    <td>{{ $tag['id'] }}</td>
                    <td>{{ $tag['name'] }}</td>
                    <td>{{ $tag['description'] }}</td>
                    <td class="tag-doctor-count-col">
                        <span><strong>{{ count($tag['clients']) }}</strong></span>
                        <?php if($role != 'admin') { ?>
                            <span>
                                <a target="_blank" href="<?= $base_url ?>/clients?tid=<?= $tag['id'] ?>" title="View Clients under this Category">[ view ]</a>
                            </span>
                        <?php } ?>
                    </td>
                    <td>{{ date('M d, Y H:i:s',strtotime($tag['created_at'])) }}</td>
                    <td class="actions">
                        <a href="<?= $base_url ?>/tags/<?= $tag['id'] ?>" title="View Category"><i class="fa fa-search-plus"></i></a>&nbsp;&nbsp;
                        <a href="<?= $base_url ?>/tags/<?= $tag['id'] ?>/edit" title="Edit Category"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;
                    
                        <form name="post_tag_<?= $tag['id'] ?>" style="display:none;" method="post" action="<?= $base_url ?>/tags/<?= $tag['id'] ?>" > @method('DELETE')  @csrf</form>
                        <a href="#" onclick="if (confirm(&quot;Are you sure you want to delete tag <?= $tag['name'] ?>?&quot;)) { document.post_tag_<?= $tag['id'] ?>.submit(); } event.returnValue = false; return false;" title="Delete Category"><i class="fa fa-trash-o"></i></a>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        <?php } else {
            echo '<div class="text-center">No Record Found.</div>';
        } ?>
		</div>
	</section>
</div>
@endsection