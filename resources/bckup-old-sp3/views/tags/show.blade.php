@extends('layouts.dashboard')

@section('content')
<div class="page-heading"><h3>Category Detail</h3></div>
@include('flash-message')
<?php 
$role = auth()->user()->getRole();
$role_segment = ($role == 'admin')?'/admin':'';
$base_url  = URL::to('/').$role_segment;
?>
<style type="text/css">
    .panel-header{ padding: 10px; }
</style>
<div class="col-md-8 col-md-offset-2 tag-view-page tags form large-9 medium-8 columns content">
	<section class="panel">
        <div class="panel-header">
            <div class="row action-items">
                <div class="col-md-6 text-left">
                    <a title="Back to Category Listing" href="<?= $base_url ?>/tags" class="btn btn-primary"><i class="fa fa-backward"></i></a>
                </div>
                <div class="col-md-6 text-right">
                    <a title="Edit Category" href="<?= $base_url ?>/tags/<?=  $tag['id'] ?>/edit" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                </div>
            </div>
        </div>
		<div class="panel-body">
        <?php if(count($tag) > 0) { ?>
        <table class="table table-bordered tags-view table-striped">
            <thead>
                <tr>
                    <th width="50%">KEY</th>
                    <th width="50%">VALUE</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Id</td>
                    <td><?=  $tag['id'] ?></td>
                </tr>
                <tr>
                    <td>Name</td>
                    <td><?= $tag['name'] ?></td>
                </tr>
                <tr>
                    <td>Description</td>
                    <td><?= $tag['description'] ?></td>
                </tr>
                <tr>
                    <td>Clients<br /><span class="help-text">( Associated with this Category )</span></td>
                    <td>
                    <?php 
                    if(count($tag['clients']) > 0 ){
                        echo "<ul>";
                        foreach($tag['clients'] as $k=>$v) { 
                        ?>
                        <li><?= $v['name'] ?>
                        <?php if($role != 'admin') { ?>
                            &nbsp;&nbsp;<a target="_blank" href="<?= $base_url ?>/clients/<?= $v['id'] ?>" title="View Client"><i class="fa fa-laptop"></i></a>
                        <?php } ?>
                        </li>
                        <?php 
                        } 
                        echo "</ul>";
                    }
                    ?>
                    
                    </td>
                </tr>
                
            </tbody>
        </table>
        <?php } else {
            echo '<div class="text-center">No Record Found.</div>';
        } ?>

		</div>
	</section>
</div>
@endsection