@extends('layouts.dashboard')

@section('content')
<?php
$role = auth()->user()->getRole();
$role_segment = ($role == 'admin')?'/admin':'';
$base_url  = URL::to('/').$role_segment;
?>
<style type="text/css">
    #create_tag_client_listing_form_group{ height: 100px; overflow-y: scroll; }
    #create_tag_client_listing_form_group .form-item{display: flex;}
    #create_tag_client_listing_form_group .form-item input {width: auto; height: auto; margin-right: 5px;}
    #create_tag_client_listing_form_group .form-item label{font-weight: normal;}
    .panel-header{ padding: 10px; }
</style>
<div class="page-heading"><h3>Update Category</h3></div>
@include('flash-message')
<div class="col-md-8 col-md-offset-2 tags-edit-page tags form large-9 medium-8 columns content">
	<section class="panel">
        <div class="panel-header">
            <div class="row action-items">
                <div class="col-md-6 text-left">
                    <a title="Back to Category Listing" href="<?= $base_url ?>/tags" class="btn btn-primary"><i class="fa fa-backward"></i></a>
                </div>
                <div class="col-md-6 text-right">
                    <a title="View Category" href="<?= $base_url ?>/tags/<?=  $tag['id'] ?>" class="btn btn-primary"><i class="fa fa-search-plus"></i></a>
                </div>
            </div>
        </div>
		<div class="panel-body">
            <form method="post" action="{{ route('tags.update', $tag['id']) }}">
            @method('PATCH') 
            @csrf
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="required">Tag Name</label><br />
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" required autocomplete="name" autofocus value="{{ $tag['name'] }}">
                            @error('name')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                        <div id="form-field-clients" class="form-group form-checkbox-wrapper">
                            <label class="required">Clients</label><br />
                            <div id="create_tag_client_listing_form_group" class="form-control">
                                <?php foreach($clients as $k=>$v) { ?>
                                    <div class="form-item">
                                    <input  
                                    <?= (in_array($v['id'],$tag['clients']))?'checked':''?> 
                                    
                                    id="client_<?= $v['id'] ?>" type="checkbox" class="form-control @error('client') is-invalid @enderror" name="client[]" value="<?= $v['id'] ?>" autocomplete="client">
                                    <label for="client_<?= $v['id'] ?>"><?= $v['name'] ?></label>
                                    </div>
                                <?php } ?>
                            </div>
                            <span class="help-text">( Choose minimum one client for updating category. )</span>
                            @error('client')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Description</label><br />
                            <textarea  id="description" name="description" class="form-control @error('description') is-invalid @enderror" cols="5" rows="8">{{ $tag['description'] }}</textarea>
                            @error('description')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">{{ __('Update Client') }}</button>
                        </div>
                    </div>
                </div>
            </form>
		</div>
	</section>
</div>
@endsection