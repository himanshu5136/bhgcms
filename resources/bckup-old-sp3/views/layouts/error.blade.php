<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}"> {{--  CSRF Token  --}}
    <title>CMS | Borderless Healthcare Group</title>  
    {{-- config('app.name', 'Laravel') --}}
    @include('elements.assets_css')  {{-- Include JS files --}}
    
</head>
<body class="sticky-header">
    <section>
        {{--  main content start --}}
        <div class="main-content" >
            {{--  content section start --}}
            <main class="py-4">
                @yield('content')
            </main>
            {{--  content section end --}}
            {{-- footer section start --}}
            <footer>
                @include('elements.footer')
            </footer>
            {{-- footer section end --}}
        </div>
        {{--  main content end --}}
    </section>
    {{-- Include JS files - Start --}}
        @include('elements.assets_js')
    {{-- Include JS files - End --}}
</body>
</html>