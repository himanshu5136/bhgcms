<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}"> {{--  CSRF Token  --}}
    <title><?= ((isset($page_title)) && (!empty($page_title)))? $page_title : 'CMS' ?> | Borderless Healthcare Group</title>  
    {{-- config('app.name', 'Laravel') --}}
    @include('elements.assets_css')  {{-- Include JS files --}}
    
</head>
<body class="sticky-header">
    <div id="app">
        <?php /* ?>
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    {{--  Left Side Of Navbar  --}}
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    {{--  Right Side Of Navbar  --}}
                    <ul class="navbar-nav ml-auto">
                        {{--  Authentication Links  --}}
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
        <main class="py-4">
            @yield('content')
        </main>
        <?php */ ?>
    </div>

    <section>
        {{--  left side start --}}
        <div class="left-side sticky-left-side">
            @include('elements.sidebar')
        </div>
        {{--  left side end --}}
        {{--  main content start --}}
        <div class="main-content" >
            {{--  header section start --}}
            <div class="header-section">
                @include('elements.header')
            </div>
            {{--  header section end --}}
            {{--  content section start --}}
            <main class="py-4">
                @yield('content')
            </main>
            {{--  content section end --}}
            {{-- footer section start --}}
            <footer>
                @include('elements.footer')
            </footer>
            {{-- footer section end --}}
        </div>
        {{--  main content end --}}
    </section>
    {{-- Include JS files - Start --}}
        @include('elements.assets_js')
    {{-- Include JS files - End --}}
</body>
</html>