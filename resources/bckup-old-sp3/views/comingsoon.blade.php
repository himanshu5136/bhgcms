@extends('layouts.dashboard')

@section('content')
<?php 
$nm  = \Request::route()->getName(); 
$base_url 		= URL::to('/');
?>
<div class="page-heading"><h3><?= $nm ?></h3></div>
@include('flash-message')
<div class="wrapper">
    <div class="row">
        <div class="col-md-12 text-center">
            <img style="width: 50%;margin: 60px auto;" src="<?= $base_url ?>/backend/images/coming-soon.png" class="img-responsive" />
            <div class="custom-alert"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> This page is under construction.</div>
        </div>
    </div>
</div>
<style type="text/css">
.custom-alert{ padding: 10px; border-radius: 20px;color: #ff9900db;font-size: 16px;background: #ff99003b;width: 50%;border: 1px solid #ff9900a6;margin: 0 auto;}
</style>
@endsection
