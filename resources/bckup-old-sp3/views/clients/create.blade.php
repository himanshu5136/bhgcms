@extends('layouts.dashboard')

@section('content')
<style type="text/css">
    #create_client_doctor_listing_form_group{ height: 100px; overflow-y: scroll; }
    #create_client_doctor_listing_form_group .form-item{display: flex;}
    #create_client_doctor_listing_form_group .form-item input {width: auto; height: auto; margin-right: 5px;}
    #create_client_doctor_listing_form_group .form-item label{font-weight: normal;}

    #create_client_tag_listing_form_group{ height: 100px; overflow-y: scroll; }
    #create_client_tag_listing_form_group .form-item{display: flex;}
    #create_client_tag_listing_form_group .form-item input {width: auto; height: auto; margin-right: 5px;}
    #create_client_tag_listing_form_group .form-item label{font-weight: normal;}
  
</style>
<div class="page-heading"><h3>Add Client</h3></div>
@include('flash-message')
<div class="col-md-8 col-md-offset-2 clients-create-page clients form large-9 medium-8 columns content">
	<section class="panel">
		<div class="panel-body">
            <form method="POST" action="{{ route('clients.store') }}">
            @csrf
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="required">Client Name</label><br />
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                            @error('name')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>Description</label><br />
                            <textarea  id="description" name="description" class="form-control @error('description') is-invalid @enderror" cols="5" rows="8">{{ old('description') }}</textarea>
                            @error('description')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                        
                    </div>
                    <div class="col-lg-6">
                        <div id="form-field-doctors" class="form-group form-checkbox-wrapper">
                            <label class="required">Doctors</label><br />
                            <div id="create_client_doctor_listing_form_group" class="form-control">
                                <?php foreach($doctors as $k=>$v) { ?>
                                    <div class="form-item">
                                    <input id="doctor_<?= $v['id'] ?>" type="checkbox" class="form-control @error('doctor') is-invalid @enderror" name="doctor[]" value="<?= $v['id'] ?>" autocomplete="doctor">
                                    <label for="doctor_<?= $v['id'] ?>"><?= $v['full_name'] ?></label>
                                    </div>
                                <?php } ?>
                            </div>
                            <span class="help-text">( Choose minimum one doctor for creating client. )</span>
                            @error('doctor')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                        <div id="form-field-tags" class="form-group form-checkbox-wrapper">
                            <label class="required">Categories</label><br />
                            <div id="create_client_tag_listing_form_group" class="form-control">
                                <?php foreach($tags as $k=>$v) { ?>
                                    <div class="form-item">
                                    <input id="tag_<?= $v['id'] ?>" type="checkbox" class="form-control @error('tag') is-invalid @enderror" name="tag[]" value="<?= $v['id'] ?>" autocomplete="tag">
                                    <label for="tag_<?= $v['id'] ?>"><?= $v['name'] ?></label>
                                    </div>
                                <?php } ?>
                            </div>
                            <span class="help-text">( Choose minimum one category for creating client. )</span>
                            @error('tag')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">{{ __('Add Client') }}</button>
                        </div>
                    </div>
                </div>
            </form>
		</div>
	</section>
</div>
@endsection