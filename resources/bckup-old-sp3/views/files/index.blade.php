@extends('layouts.dashboard')

@section('content')
<div class="page-heading"><h3>S3 Bucket Files</h3></div>
@include('flash-message')
<style type="text/css">
    table tr td.client-doctor-count-col span{display: block; text-align: center;}
</style>
<div class="clients form large-9 medium-8 columns content">
	<section class="panel">
		<div class="panel-body">
        <?php if(count($files) > 0) { ?>
        <table class="table  table-hover general-table">
            <thead>
                <tr>
                    <th>Sr. No.</th>
                    <th>Path</th>
                    <th>Link</th>
                </tr>
            <thead>
            <tbody>
            <?php foreach($files as $key => $val) { ?>
                <tr>
                    <td class="subheading" colspan="3">Folder : <?= $key ?></td>
                </tr>
                <?php foreach($val as $k => $v) { ?>
                    <tr>
                        <td>{{ $k+1 }}</td>
                        <td>{{ $v }}</td>
                        <td><a target="_blank" href="https://borderlesshealthbkt.s3.ap-southeast-1.amazonaws.com/{{ $v }}" title="View File"><i class="fa fa-search-plus"></i></td>
                    </tr>
                <?php } ?>
            <?php } ?>
            <tbody>
        </table>
        <?php } else {
            echo '<div class="text-center">No File Found.</div>';
        } ?>
		</div>
	</section>
</div>
@endsection