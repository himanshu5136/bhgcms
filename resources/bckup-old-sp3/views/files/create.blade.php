@extends('layouts.dashboard')

@section('content')
<div class="page-heading"><h3>Upload File</h3></div>
@include('flash-message')
<style type="text/css">
    h4.subheading{border-bottom: 2px solid #F4D03F;color: #333;font-size: 20px;margin-bottom: 20px;background: #ECDDA199;padding: 6px;}
</style>

<div class="col-md-8 col-md-offset-2 users-create-page superusers form large-9 medium-8 columns content">
	<section class="panel">
		<div class="panel-body">
        <form action="{{ route('files.store') }}" method="post" enctype="multipart/form-data">
        @csrf
            <input type="file" name="image" id="image">
            <button type="submit">Upload</button>
        </form>
		</div>
	</section>
</div>
@endsection