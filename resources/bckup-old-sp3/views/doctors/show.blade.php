@extends('layouts.dashboard')

@section('content')
<div class="page-heading"><h3>Doctor Detail</h3></div>
@include('flash-message')
<?php 
$statusAry = ['1'=>'Active','2'=>'Blocked', ];
$role = auth()->user()->getRole();
$role_segment = ($role == 'admin')?'/admin':'';
$base_url  = URL::to('/').$role_segment;
?>
<style type="text/css">
table.users-view tr td.subheading{border-bottom: 2px solid #F4D03F;color: #333;font-size: 20px;margin-bottom: 20px;background: #ECDDA199;padding: 6px;}
.panel-header{ padding: 10px; }
</style>
<div class="col-md-8 col-md-offset-2 doctors-view-page form large-9 medium-8 columns content">
	<section class="panel">
        <div class="panel-header">
            <div class="row action-items">
                <div class="col-md-6 text-left">
                    <a title="Back to Doctors Listing" href="<?= $base_url ?>/doctors" class="btn btn-primary"><i class="fa fa-backward"></i></a>
                </div>
                <div class="col-md-6 text-right">
                    <a title="Edit Doctor" href="<?= $base_url ?>/doctors/<?=  $doctor['id'] ?>/edit" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                </div>
            </div>
        </div>
		<div class="panel-body">
        <?php if(count($doctor) > 0) { ?>
        <table class="table table-bordered doctor-view table-striped">
            <tbody>
                <tr>
                    <td width="25%"><strong>Key</strong></td>
                    <td width="75%"><strong>Value</strong></td>
                </tr>
                <tr>
                    <td>Username</td>
                    <td><?= $doctor['name'] ?></td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td><?= $doctor['email'] ?></td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td><?= $statusAry[$doctor['status']] ?></td>
                </tr>
                <tr>
                    <td>Created At</td>
                    <td><?= date('M d,Y H:i:s',strtotime($doctor['created_at'])) ?></td>
                </tr>
                <tr>
                    <td>Full Name</td>
                    <td><?=  $doctor['profile']['full_name'] ?></td>
                </tr>
                <tr>
                    <td>Title</td>
                    <td><?=  $doctor['profile']['title'] ?></td>
                </tr>
                <tr>
                    <td>Picture</td>
                    <td><?=  $doctor['profile']['picture'] ?>
                    <?= (!empty($doctor['profile']['picture']))?'<a href='.$doctor['profile']['picture'].' target="_blank"><i class="fa fa-external-link"></i></a>':'';
                    ?>
                    
                    </td>
                </tr>
                <tr>
                    <td>Contact Number</td>
                    <td><?=  $doctor['profile']['contact_number'] ?></td>
                </tr>
                <tr>
                    <td>Bio</td>
                    <td><?=  $doctor['profile']['bio'] ?></td>
                </tr>
            </tbody>
        </table>
        <?php } else {
            echo '<div class="text-center">No Record Found.</div>';
        } ?>

		</div>
	</section>
</div>
@endsection