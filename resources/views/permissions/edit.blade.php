@extends('layouts.dashboard')

@section('content')
<div class="page-heading"><h3>Setup Permission</h3></div>
@include('flash-message')
<?php $base_url  = URL::to('/'); ?>

<div class="col-md-8 col-md-offset-2 users-edit-page form large-9 medium-8 columns content">
	<section class="panel">
        <div class="panel-header">
            <div class="row action-items">
                <div class="col-lg-12 text-center">
                   <h4 style="    border-bottom: 1px solid #ccc; padding: 10px; margin-top: 2px; background: #eee;">Set permission for "Admin" user role under condition of assigned clients.</h4>
                </div>
            </div>
        </div>
		<div class="panel-body">
            <form method="POST" accept-charset="UTF-8">
            @csrf
                <div class="row">
                    <div class="col-lg-6 col-lg-offset-3">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Permission</th>
                                    <th class="text-center">Admin User</th>
                                </tr>
                            </thead>
                        <?php foreach($allPerms as $k=>$v) { ?>
                            <tr>
                                <td width="60%">
                                    <label style="font-weight: normal"for="perm_<?= $v['id'] ?>"><?= $v['name'] ?></label>
                                </td>
                                <td class="text-center" width="60%">
                                    <input <?= (in_array($v['id'],$rolePerms))?'checked':'' ?>  type="checkbox" value="<?= $v['id'] ?>" name="perm[<?= ''.$v['id'].'' ?>][pid]" id="perm_<?= $v['id'] ?>" />
                                    <input name="perm[<?= ''.$v['id'].'' ?>][rid]" type="hidden" value="2" />
                                    <input name="perm[<?= ''.$v['id'].'' ?>][hid]" type="hidden" value="<?= $v['id'] ?>" />                                
                                </td>
                            </tr>
                        <?php } ?>
                        </table>
                    </div>                  
                </div>
                <br />
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">{{ __('Update Permissions') }}</button>
                        </div>
                    </div>
                </div>
            </form>
		</div>
	</section>
</div>
@endsection