@extends('layouts.dashboard')

@section('content')
<div class="page-heading"><h3>Section Detail</h3></div>
@include('flash-message')
<?php 
$base_url  = URL::to('/');
?>
<style type="text/css">
    .panel-header{ padding: 10px; }
</style>
<div class="col-md-8 col-md-offset-2 csection-view-page csections form large-9 medium-8 columns content">
	<section class="panel">
        <div class="panel-header">
            <div class="row action-items">
                <div class="col-md-6 text-left">
                    <a title="Back to Sections Listing" href="<?= $base_url ?>/cpages/csections" class="btn btn-primary"><i class="fa fa-backward"></i></a>
                </div>
                <div class="col-md-6 text-right">
                    <a title="Edit Section" href="<?= $base_url ?>/cpages/csections/<?=  $csection['id'] ?>/edit" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                </div>
            </div>
        </div>
		<div class="panel-body">
        <?php if(count($csection) > 0) { ?>
        <table class="table table-bordered superusers-view table-striped">
            <thead>
                <tr>
                    <th class="bg-primary" width="50%">KEY</th>
                    <th class="bg-primary" width="50%">VALUE</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Id</td>
                    <td><?=  $csection['id'] ?></td>
                </tr>
                <tr>
                    <td>Name</td>
                    <td><?= $csection['name'] ?></td>
                </tr>
                <tr>
                    <td>Title</td>
                    <td><?= $csection['title'] ?></td>
                </tr>
                <tr>
                    <td>Contents</td>
                    <td><ul>
                            <?php 
                            $cids = json_decode($csection['cids']);
                            foreach($cids as $k=>$v){
                                echo '<li><a href="'.$base_url.'/contents'.'/'.$v.'" target="_blank">'.$contents[$v].'</a></li>'; 
                            }
                            ?>
                        </ul></td>
                </tr>             
                <tr>
                    <td colspan="2" class="text-center"><b>Language Based Data</b></td>
                </tr>
                <?php 
                if(!empty($csection['langs'])) {
                foreach($csection['langs'] as $k=>$v) { ?>
                    <tr>
                        <td><?= $v['name']?> ( <?= $v['code']?> )</td>
                        <td><ul>
                            <li><b>Title</b><br /> <?=  $v['pivot']['csec_title'] ?></li>
                        </td>
                    </tr>
                <?php } 
                } ?>
            </tbody>
        </table>
        <?php } else {
            echo '<div class="text-center">No Record Found.</div>';
        } ?>
		</div>
	</section>
</div>
@endsection