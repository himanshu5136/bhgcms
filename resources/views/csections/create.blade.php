@extends('layouts.dashboard')

@section('content')
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style type="text/css">
 span.select2-selection--multiple{min-height:120px !important;}
.ui-tabs-vertical { width: 55em; }
.ui-tabs-vertical .ui-tabs-nav { overflow-y: scroll; max-height: 200px; padding: .2em .1em .2em .2em; float: left; width: 12em; min-height: 200px; overflow-y: scroll;}
.ui-tabs-vertical .ui-tabs-nav li { clear: left; width: 100%; border-bottom-width: 1px !important; border-right-width: 0 !important; margin: 0 -1px .2em 0; }
.ui-tabs-vertical .ui-tabs-nav li a { display:block; }
.ui-tabs-vertical .ui-tabs-nav li.ui-tabs-active { padding-bottom: 0; padding-right: .1em; border-right-width: 1px; }
.ui-tabs-vertical .ui-tabs-panel { padding: 1em; float: right; width: 40em;}
body {font-family: Arial, Helvetica, sans-serif;}
h4.lang-subheading{border-bottom: 2px solid #333;font-weight: bold;background: #F4EAC3;padding: 5px;margin-bottom: 16px;}
table {font-size: 1em;} 
.ui-draggable, .ui-droppable {background-position: top;}
</style>
<div class="page-heading"><h3>Add Section</h3></div>
@include('flash-message')
<div class="col-md-8 col-md-offset-2 csections-create-page csections form large-9 medium-8 columns content">
	<section class="panel"> 
		<div class="panel-body">
            <form method="POST" action="{{ route('csections.store') }}">
            @csrf
                <div class="row">
                    <div class="col-lg-6 col-lg-offset-3">
                        <div class="form-group">
                            <label class="required">Section Name</label><br />
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                            <span class="help-text">This Name is only for understanding of sections in CMS.</span>
                            @error('name')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class="required">Contents</label><br />
                            <select multiple="multiple" class="select-multiple-list form-control @error('content') is-invalid @enderror" name="content[]" id="csection-content-list" required>
                                <?php foreach($contents as $k=>$v) { ?>
                                <option value="<?= $v['id'] ?>"><?= $v['title'] ?></option>
                                <?php } ?>
                            </select>
                            <span class="help-text">Choose multiple content from this list. You can also search content by typing initial words.</span>
                            @error('content')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 ">
                    <div id="tabs">
                        <!-- TABS Labels-->
                        <ul>
                            <?php foreach($langs as $key=>$val) {  ?>
                                <li><a href="#lang-<?= $val['code'] ?>-<?= $val['id'] ?>"><?= $val['name'].' ( '.$val['code'].' )'?></a></li>
                            <?php }  ?>
                        </ul>
                        <!-- TABS Wrappers-->
                        <?php foreach($langs as $key=>$val) {  
                            $reqField = ($val['id'] == '1')?'required':''; // Fields can be required only for english language.
                            ?>
                            <div id="lang-<?= $val['code'] ?>-<?= $val['id'] ?>">
                                <h4 class="lang-subheading" ><?= $val['name'] ?> Language </h4>
                                <div class="row">
                                    <div class="col-lg-8 ">
                                        <div class="form-group">
                                            <label class="<?= $reqField ?>">(<?= $val['code'] ?> ) Title</label><br />
                                            <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="langdata[<?= $val['id'] ?>][csec_title]" value="{{ old('title') }}" <?= $reqField ?> >
                                            <span class="help-text">This title would be display in Application.</span>
                                            @error('title')
                                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        </div>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">{{ __('Add Section') }}</button>
                        </div>
                    </div>
                </div>
            </form>
		</div>
	</section>
</div>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
@endsection