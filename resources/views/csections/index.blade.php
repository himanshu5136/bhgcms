@extends('layouts.dashboard')

@section('content')
<div class="page-heading"><h3>List of Sections</h3></div>
@include('flash-message')
<?php 
$base_url  = URL::to('/');
?>
<style type="text/css">
</style>
<div class="csections form large-9 medium-8 columns csection">
	<section class="panel">
        <div class="panel-heading ">
            <div class="row csections_filters_wrapper bg-secondary">
                <form method="get" id="form_csections_filter" >
                    <div class="col-md-3 form-group">
                        <label>Name</label><br />
                        <input class="form-control" type="text" name="snm" id="csections-filter-snm" value="<?= (!empty($qry['snm']))?$qry['snm']:'' ?>" /> 
                    </div>
                    <div class="col-md-3 form-group">
                        <label>Title</label><br />
                        <input class="form-control" type="text" name="ttl" id="csections-filter-ttl" value="<?= (!empty($qry['ttl']))?$qry['ttl']:'' ?>" /> 
                    </div>
                    <div class="col-md-3 form-group">
                        <label>&nbsp;</label><br />
                        <input type="submit" name="submit" value="Search" class="btn btn-primary" />
                        <a href="<?= $base_url ?>/cpages/csections" class="btn btn-secondary">Reset</a>
                    </div>	
                </form>
            </div>	
        </div>
		<div class="panel-body">
        <?php if(count($csections) > 0) { ?>
            <table class="table  table-hover general-table">
                <thead>
                    <tr>
                        <th width="10%">Section Id</th>
                        <th width="15%">Name</th>
                        <th width="20%">Title ( English )</th>
                        <th width="30%">Contents</th>
                        <th width="15%">Created Date</th>
                        <th width="10%">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($csections as $csection)
                    <tr>
                        <td>{{ $csection['id'] }}</td>
                        <td>{{ $csection['name'] }}</td>
                        <td>{{ $csection['langs']['1']['csec_title'] }}</td>
                        <td><ul>
                            <?php 
                            if((isset($csection['cids'])) && (!empty($csection['cids']))){
                                $cids = json_decode($csection['cids']);
                                foreach($cids as $k=>$v){
                                    echo '<li><a href="'.$base_url.'/contents'.'/'.$v.'" target="_blank">';
                                    echo (!empty($contents[$v]))?$contents[$v]:$v;
                                    echo '</a></li>'; 
                                }
                            }
                            ?>
                        </ul></td>
                        <td>{{ date('M d, Y H:i:s',strtotime($csection['created_at'])) }}</td>
                        <td class="actions">
                            <a href="<?= $base_url ?>/cpages/csections/<?= $csection['id'] ?>" title="View Section"><i class="fa fa-search-plus"></i></a>&nbsp;&nbsp;
                            <a href="<?= $base_url ?>/cpages/csections/<?= $csection['id'] ?>/edit" title="Edit Section"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;
                        
                            <form name="post_csection_<?= $csection['id'] ?>" style="display:none;" method="post" action="<?= $base_url ?>/cpages/csections/<?= $csection['id'] ?>" > @method('DELETE')  @csrf</form>
                            <a href="#" onclick="if (confirm(&quot;Are you sure you want to delete section <?= $csection['name'] ?>?&quot;)) { document.post_csection_<?= $csection['id'] ?>.submit(); } event.returnValue = false; return false;" title="Delete Section"><i class="fa fa-trash-o"></i></a>
                        </td>
                    </tr>
                    @endforeach
                <tbody>
            </table>
            <div class="text-center">
                {{ $paginate->links() }}
            </div>
        <?php } else {
            echo '<div class="text-center">No Record Found.</div>';
        } ?>
		</div>
	</section>
</div>
@endsection