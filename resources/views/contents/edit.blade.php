@extends('layouts.dashboard')

@section('content')

<?php 
$role = auth()->user()->getRole();
$role_segment = ($role == 'admin')?'/admin':'';
$base_url  = URL::to('/').$role_segment;
$form_action = $base_url.'/contents'.'/'.$content['id'];
//{{ route('contents.update', $content['id']) }}
?>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style type="text/css">
    #create_content_tag_listing_form_group{ height: 130px; overflow-y: scroll; }
    #create_content_tag_listing_form_group .form-item{display: flex;}
    #create_content_tag_listing_form_group .form-item input {width: auto; height: auto; margin-right: 5px;}
    #create_content_tag_listing_form_group .form-item label{font-weight: normal;}
    .existing-attachment{text-align:center;background: #ECDDA199; height: 76px; display: block; width: 50px; margin-top: 5px; border: 1px solid #F4D03F;}
    .existing-attachment a{font-size: 10px;line-height: 0px;color: #333;text-decoration: underline;}
    .panel-header{ padding: 10px; }
    .display_heading{border-bottom: 2px solid #000;background: #FFF3C5;font-size: 14px;padding: 1px 10px;}
    .display_heading h4.subheading{font-size: 14px;}
    .display_fields .form-group{width: 90%;display: inline-block;padding: 6px 10px;background: #f7f7f7;border: 1px solid #ccc;border-radius: 10px;margin: 0px 4px 10px 14px;}
    .display_fields .form-group .form-item-left-text{width: 20%;float: left;min-height: 60px; background: #FFF3C5; padding: 5px; max-width: 48px; margin-right: 10px; text-align: center; font-size: 12px;}    .display_fields .form-group .form-item-right-text{width: 76%;float: left;}
    span.select2-selection--multiple{min-height:120px !important;}
    .display_none{display:none;}
    .form-item-left-text{width: 20%;float: left;min-height: 60px; background: #FFF3C5; padding: 5px; max-width: 48px; margin-right: 10px; text-align: center; font-size: 12px;}    
    .form-item-right-text{width: 76%;float: left;}
    .ui-tabs-vertical { width: 55em; }
    .ui-tabs-vertical .ui-tabs-nav { overflow-y: scroll; max-height: 720px; padding: .2em .1em .2em .2em; float: left; width: 12em; min-height: 720px; overflow-y: scroll;}
    .ui-tabs-vertical .ui-tabs-nav li { clear: left; width: 100%; border-bottom-width: 1px !important; border-right-width: 0 !important; margin: 0 -1px .2em 0; }
    .ui-tabs-vertical .ui-tabs-nav li a { display:block; }
    .ui-tabs-vertical .ui-tabs-nav li.ui-tabs-active { padding-bottom: 0; padding-right: .1em; border-right-width: 1px; }
    .ui-tabs-vertical .ui-tabs-panel { padding: 1em; float: right; width: 40em;}
    body {font-family: Arial, Helvetica, sans-serif;}
    h4.lang-subheading{border-bottom: 2px solid #333;font-weight: bold;background: #F4EAC3;padding: 5px;margin-bottom: 16px;}
    table {font-size: 1em;}
    .ui-draggable, .ui-droppable {background-position: top;}
</style>
<div class="page-heading"><h3>Update Content</h3></div>
@include('flash-message')
<div class="col-md-8 col-md-offset-2 contents-edit-page contents form large-9 medium-8 columns content">
	<section class="panel">
        <div class="panel-header">
            <div class="row action-items">
                <div class="col-md-6 text-left">
                    <a title="Back to Content Listing" href="<?= $base_url ?>/contents" class="btn btn-primary"><i class="fa fa-backward"></i></a>
                </div>
                <div class="col-md-6 text-right">
                    <a title="View Content" href="<?= $base_url ?>/contents/<?=  $content['id'] ?>" class="btn btn-primary"><i class="fa fa-search-plus"></i></a>
                </div>
            </div>
        </div>
		<div class="panel-body">
            <form method="post" enctype="multipart/form-data" action="{{ $form_action }}">
            @method('PATCH') 
            @csrf
                <div class="row">
                    <div class="col-lg-6">
                        <div id="form-field-tags" class="form-group form-checkbox-wrapper">
                            <label class="required">Categories</label><br />
                            <div id="create_content_tag_listing_form_group" class="form-control">
                                <?php foreach($tags as $k=>$v) { ?>
                                    <div class="form-item">
                                    <input  
                                    <?= (in_array($v['id'],$content['tags']))?'checked':''?> 
                                    id="tag_<?= $v['id'] ?>" type="checkbox" class="form-control @error('tag') is-invalid @enderror" name="tag[]" value="<?= $v['id'] ?>" autocomplete="tag">
                                    <label for="tag_<?= $v['id'] ?>"><?= $v['name'] ?></label>
                                    </div>
                                <?php } ?>
                            </div>
                            <span class="help-text">( Choose minimum one category for updating content. )</span>
                            @error('tag')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="required">Status</label><br />
                            <select class="form-control @error('status') is-invalid @enderror" name="status" id="status" required>
                                <?php foreach($status as $k=>$v) { ?>
                                <option 
                                <?= ($k == $content['status'])?'selected':''?> 
                                value="<?= $k ?>"><?= $v ?></option>
                                <?php } ?>
                            </select>
                            @error('status')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class="required">Type</label><br />
                            <select class="form-control @error('ctype') is-invalid @enderror" name="ctype" id="content-ctype-val" required>
                                <option value="">- Choose Type -</option>
                                <?php foreach($ctype as $k=>$v) { ?>
                                <option  <?= ($k == $content['ctype'])?'selected':''?> value="<?= $k ?>"><?= $v ?></option>
                                <?php } ?>
                            </select>
                            @error('ctype')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-lg-12 ">
                    <div id="tabs">
                        <!-- TABS Labels-->
                        <ul>
                            <?php foreach($langs as $key=>$val) {  ?>
                                <li><a href="#lang-<?= $val['code'] ?>-<?= $val['id'] ?>"><?= $val['name'].' ( '.$val['code'].' )'?></a></li>
                            <?php }  ?>
                        </ul>
                        <!-- TABS Wrappers-->
                        <?php foreach($langs as $key=>$val) {  
                            $reqField = ($val['id'] == '1')?'required':''; // Fields can be required only for english language.

                            $cntnt_title_value = ((!empty($content['langs'][$val['id']])) && (isset($content['langs'][$val['id']])))?$content['langs'][$val['id']]['cntnt_title']:''; //$content['title'];

                            $cntnt_bg_images_value = ((!empty($content['langs'][$val['id']])) && (isset($content['langs'][$val['id']])))?$content['langs'][$val['id']]['cntnt_bg_images']:'a:0:{}'; //$content['bg_images'];
                            $cntnt_bg_images_value = unserialize($cntnt_bg_images_value);

                            $cntnt_tn_images_value = ((!empty($content['langs'][$val['id']])) && (isset($content['langs'][$val['id']])))?$content['langs'][$val['id']]['cntnt_tn_images']:'a:0:{}';//$content['tn_images'];
                            $cntnt_tn_images_value = unserialize($cntnt_tn_images_value);

                            $cntnt_body_value = ((!empty($content['langs'][$val['id']])) && (isset($content['langs'][$val['id']])))?$content['langs'][$val['id']]['cntnt_body']:'';//$content['body'];

                            $cntnt_author_id_value = ((!empty($content['langs'][$val['id']])) && (isset($content['langs'][$val['id']])))?$content['langs'][$val['id']]['cntnt_author_id']:'';//$content['author_id'];
                            
                            $cntnt_attachment_value = ((!empty($content['langs'][$val['id']])) && (isset($content['langs'][$val['id']])))?$content['langs'][$val['id']]['cntnt_attachment']:'';//$content['attachment'];

                            $cntnt_product_id_value = ((!empty($content['langs'][$val['id']])) && (isset($content['langs'][$val['id']])))?$content['langs'][$val['id']]['cntnt_product_id']:'';//$content['product_id'];


                            ?>
                            <div id="lang-<?= $val['code'] ?>-<?= $val['id'] ?>">
                                <h4 class="lang-subheading" ><?= $val['name'] ?> Language </h4>
                                <div class="row">
                                    <div class="col-lg-8 "> 
                                        <div class="form-group" id="content-attachment-field">
                                            <?php if((isset($cntnt_attachment_value)) && (!empty($cntnt_attachment_value))) { ?>
                                                <div class="form-item-left-text existing-attachment">
                                                    <span><a href="<?= $cntnt_attachment_value ?>">Link to File</a></span>
                                                </div>
                                                <div class="form-item-right-text">
                                                <input type="hidden" name="langdata_<?= $val['id'] ?>_attachment_hdn_url" value="<?= $cntnt_attachment_value ?>" />
                                            <?php } else { ?>
                                                <input type="hidden" name="langdata_<?= $val['id'] ?>_attachment_hdn_url" value="" />
                                            <?php } ?>
                                                <label >( <?= $val['code'] ?> ) Attachment</label><br />
                                                <input id="attachment" type="file" class="form-control @error('attachment') is-invalid @enderror" name="langdata_<?= $val['id'] ?>_attachment" value="{{ old('attachment') }}" autocomplete="attachment" autofocus>
                                                <span class="help-text">For type i.e. Zip / Video / Image. If you have choosen "Product" in above mentioned field, value of this field will not save. </span>
                                                <div class=""><input value="1" type="checkbox" name="remove_<?= $val['id'] ?>_attachment" id="remove_<?= $val['id'] ?>_attachment" /><label for="remove_<?= $val['id'] ?>_attachment">&nbsp;Remove Attachment</label></div>
                                                @error('attachment')
                                                    <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                                @enderror
                                            <?php if((isset($cntnt_attachment_value)) && (!empty($cntnt_attachment_value))) {   ?>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <div class="form-group" id="content-product-field" >
                                            <label>( <?= $val['code'] ?> ) Product </label><br />
                                            <select class="select-multiple-list form-control @error('product') is-invalid @enderror" name="langdata[<?= $val['id'] ?>][cntnt_product_id]" id="product_<?= $val['id'] ?>">
                                                <option value="">- Choose Product -</option>
                                                <?php foreach($products as $k=>$v) { ?>
                                                <option <?= ($v['id'] == $cntnt_product_id_value)?'selected':''?> value="<?= $v['id'] ?>"><?= $v['name'].' ( '.$v['sku'].' )' ?></option>
                                                <?php } ?>
                                            </select>
                                            <span class="help-text">For type i.e. Product. If you have choosen "Zip / Video / Image" in above mentioned field, value of this field will not save</span>
                                            @error('product')
                                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label class="<?= $reqField ?>">( <?= $val['code'] ?> ) Title</label><br />
                                            <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="langdata[<?= $val['id'] ?>][cntnt_title]" value="{{ $cntnt_title_value }}" <?= $reqField ?> autocomplete="title" autofocus>
                                            @error('title')
                                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label class="<?= $reqField ?>">( <?= $val['code'] ?> ) Author</label><br />
                                            <select class="select-multiple-list form-control @error('author') is-invalid @enderror" name="langdata[<?= $val['id'] ?>][cntnt_author_id]" id="author" <?= $reqField ?>>
                                                <option value="">- Select One -</option>
                                                <?php foreach($authors as $k=>$v) { ?>
                                                <option <?= ($v['id'] == $cntnt_author_id_value)?'selected':''?> value="<?= $v['id'] ?>"><?= $v['full_name'] ?></option>
                                                <?php } ?>
                                            </select>
                                            @error('author')
                                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <div class="images_block_wrapper">
                                                <div class="display_heading">
                                                    <h4 class="subheading">( <?= $val['code'] ?> ) Background Images </h4>
                                                </div><br />
                                                <div class="display_fields">
                                                    <?php for($i=0; $i<1; $i++) { ?>
                                                        <div class="form-group">
                                                            <?php 
                                                            if((isset($cntnt_bg_images_value[$i])) && (!empty($cntnt_bg_images_value[$i]))) {
                                                            ?>
                                                                <div class="form-item-left-text">
                                                                    <span><a href="<?= $cntnt_bg_images_value[$i] ?>">View Image</a></span>
                                                                </div>
                                                                <div class="form-item-right-text">
                                                                <input type="hidden" name="langdata_<?= $val['id'] ?>_bg_images_hdn_url_<?= $i ?>" value="<?= $cntnt_bg_images_value[$i] ?>" />
                                                            <?php } else { ?>
                                                                <input type="hidden" name="langdata_<?= $val['id'] ?>_bg_images_hdn_url_<?= $i ?>" value="" />
                                                            <?php } ?>
                                                                <label >Background  - <?= $i+1 ?></label><br />
                                                                <input id="bg_image" type="file" class="form-control @error('bg_image') is-invalid @enderror" name="langdata_<?= $val['id'] ?>_bg_image_<?= $i ?>" value="{{ old('bg_image') }}" />
                                                                <div class="">
                                                                <input value="1" type="checkbox" name="remove_<?= $val['id'] ?>_bg_image_<?= $i ?>" id="remove_<?= $val['id'] ?>_bg_image_<?= $i ?>"/><label for="remove_<?= $val['id'] ?>_bg_image_<?= $i ?>">&nbsp;Remove</label></div>
                                                            <?php if((isset($cntnt_bg_images_value[$i])) && (!empty($cntnt_bg_images_value[$i]))) {   ?>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    <?php }  ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="images_block_wrapper">
                                                <div class="display_heading">
                                                    <h4 class="subheading">( <?= $val['code'] ?> ) Thumbnail Images </h4>
                                                </div><br />
                                                <div class="display_fields">
                                                    <?php for($i=0; $i<1; $i++) { ?>
                                                        <div class="form-group">
                                                            <?php 
                                                            if((isset($cntnt_tn_images_value[$i])) && (!empty($cntnt_tn_images_value[$i]))) {
                                                            ?>
                                                                <div class="form-item-left-text">
                                                                    <span><a href="<?= $cntnt_tn_images_value[$i] ?>">View Image</a></span>
                                                                </div>
                                                                <div class="form-item-right-text">
                                                                <input type="hidden" name="langdata_<?= $val['id'] ?>_tn_images_hdn_url_<?= $i ?>" value="<?= $cntnt_tn_images_value[$i] ?>" />
                                                            <?php } else { ?>
                                                                <input type="hidden" name="langdata_<?= $val['id'] ?>_tn_images_hdn_url_<?= $i ?>" value="" />
                                                            <?php } ?>
                                                                <label >Thumbnail  - <?= $i+1 ?></label><br />
                                                                <input id="tn_image" type="file" class="form-control @error('tn_image') is-invalid @enderror" name="langdata_<?= $val['id'] ?>_tn_image_<?= $i ?>" value="{{ old('tn_image') }}" />
                                                                <div class="">
                                                                <input value="1" type="checkbox" name="remove_<?= $val['id'] ?>_tn_image_<?= $i ?>" id="remove_<?= $val['id'] ?>_tn_image_<?= $i ?>"/><label for="remove_<?= $val['id'] ?>_tn_image_<?= $i ?>">&nbsp;Remove</label></div>
                                                            <?php if((isset($cntnt_tn_images_value[$i])) && (!empty($cntnt_tn_images_value[$i]))) {   ?>
                                                                </div>
                                                            <?php } ?>

                                                        </div>
                                                    <?php }  ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="<?= $reqField ?>">( <?= $val['code'] ?> ) Body</label><br />
                                            <textarea <?= $reqField ?> id="body" name="langdata[<?= $val['id'] ?>][cntnt_body]" class="form-control @error('body') is-invalid @enderror" cols="5" rows="4">{{ $cntnt_body_value }}</textarea>
                                            @error('body')
                                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        </div>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">{{ __('Update Content') }}</button>
                        </div>
                    </div>
                </div>
            </form>
		</div>
	</section>
</div>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
@endsection