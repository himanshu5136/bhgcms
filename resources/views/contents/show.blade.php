@extends('layouts.dashboard')

@section('content')
<div class="page-heading"><h3>Content Detail</h3></div>
@include('flash-message')
<?php 
$role = auth()->user()->getRole();
$role_segment = ($role == 'admin')?'/admin':'';
$base_url  = URL::to('/').$role_segment;
$ntbu = '<span class="bg-danger">[ Need to update ]</span>';
?>
<style type="text/css">
    .panel-header{ padding: 10px; }
</style>
<div class="col-md-8 col-md-offset-2 tag-view-page tags form large-9 medium-8 columns content">
	<section class="panel">
        <div class="panel-header">
            <div class="row action-items">
                <div class="col-md-6 text-left">
                    <a title="Back to Content Listing" href="<?= $base_url ?>/contents" class="btn btn-primary"><i class="fa fa-backward"></i></a>
                </div>
                <div class="col-md-6 text-right">
                    <a title="Edit Content" href="<?= $base_url ?>/contents/<?=  $content['id'] ?>/edit" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                </div>
            </div>
        </div>
		<div class="panel-body">
        <?php if(count($content) > 0) { ?>
        <table class="table table-bordered tags-view table-striped">
            <thead>
                <tr>
                    <th class="bg-primary" width="50%">KEY</th>
                    <th class="bg-primary" width="50%">VALUE</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Id</td>
                    <td><?=  $content['id'] ?></td>
                </tr>
                <tr>
                    <td>Type</td>
                    <td><?=  (!empty($ctype[$content['ctype']]))?$ctype[$content['ctype']]:$ntbu ?></td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td><?=  $content['status'] ?></td>
                </tr>
                <tr>
                    <td>Created By</td>
                    <td><?=  $content['user_full_name'] ?>
                    <?php if($role != 'admin') { ?>
                    &nbsp;<a href="<?= $base_url ?>/users/<?= $content['user_id'] ?>" target="_blank"><i class="fa fa-external-link"></i></a>
                    <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td>Attachment</td>
                    <td>
                    <?php if(!empty($content['attachment'])) { ?>
                        <a href="<?= $content['attachment'] ?>" target="_blank"><?= $content['attachment'] ?>&nbsp;<i class="fa fa-external-link"></i></a>
                    <?php } ?>
                    </td>
                </tr>
                
                <tr>
                    <td>Categories<br /><span class="help-text">( Associated with this content )</span></td>
                    <td>
                    <?php 
                    if(count($content['tags']) > 0 ) {
                        echo "<ul>";
                        foreach($content['tags'] as $k=>$v) {
                        ?>
                        <li><?= $v['name'] ?>
                        <?php if($role != 'admin') { ?>
                        &nbsp;&nbsp;<a target="_blank" href="<?= $base_url ?>/tags/<?= $v['id'] ?>" title="View Category"><i class="fa fa-tag"></i></a>
                        <?php } ?>
                        </li>
                        <?php 
                        } 
                        echo "</ul>";
                    }
                    ?>
                    </td>
                </tr>
                <tr>
                    <td>Product</td>
                    <td><?= (!empty($content['product_id']))?$products[$content['product_id']]['name']:$ntbu ?></td>
                </tr>
                <tr>
                    <td>GUID</td>
                    <td><?= $content['guid'] ?><br /><br />
                    {!! QrCode::format('svg')->size(100)->generate($content['guid']); !!}</td>
                </tr>
                <tr>
                    <td>Is this ZIP</td>
                    <td><?= ($content['is_this_zip'] == '1')?'Yes':'No' ?></td>
                </tr>
                <tr>
                    <td>ZIP Files</td>
                    <td><?php
                    $zpfls = [];
                     if (!empty($content['zip_files'])) {
                         $zpfls = json_decode(base64_decode($content['zip_files']));
                     }
                     $zpfls = (array)$zpfls;
                     echo '<ul>';
                     foreach($zpfls as $k=>$v){
                        echo '<li>'.$v.'</li>';
                     }
                     echo '</ul>';
                    ?></td>
                </tr>
                <tr>
                    <td colspan="2" class="text-center"><b>Language Based Data</b></td>
                </tr>
                <?php 
                if(!empty($content['langs'])) {
                foreach($content['langs'] as $k=>$v) { ?>
                    <tr>
                        <td><?= $v['name']?> ( <?= $v['code']?> )</td>
                        <td><ul>
                            <li><b>Title</b><br /> <?=  $v['pivot']['cntnt_title'] ?></li>
                            <li><b>Background Images</b><br /> 
                            <?php 
                            if(!empty($v['pivot']['cntnt_bg_images'])) {
                                $imgs = unserialize($v['pivot']['cntnt_bg_images']);
                                echo '<ul>';
                                foreach($imgs as $k1=>$v1){
                                    echo '<li>'.$v1.'</li>';    
                                }
                                echo '</ul>';
                            }
                            ?>
                            </li>
                            <li><b>Thubnail Images</b><br />
                            <?php 
                            if(!empty($v['pivot']['cntnt_tn_images'])) {
                                $imgs = unserialize($v['pivot']['cntnt_tn_images']);
                                echo '<ul>';
                                foreach($imgs as $k1=>$v1){
                                    echo '<li>'.$v1.'</li>';    
                                }
                                echo '</ul>';
                            }
                            ?>
                            </li>
                            <li><b>Body</b><br /> <?=  $v['pivot']['cntnt_body'] ?></li>
                            <li><b>Author</b><br /> <?=  ((!empty($v['pivot']['cntnt_author_id'])) && ($v['pivot']['cntnt_author_id'] != '0'))?$authors[$v['pivot']['cntnt_author_id']]['full_name']:'Not Selected Any Author' ?></li>
                            <li><b>Product</b><br /> <?=  ((!empty($v['pivot']['cntnt_product_id'])) && ($v['pivot']['cntnt_product_id'] != '0'))?$products[$v['pivot']['cntnt_product_id']]['name']:'Not Selected Any Product' ?></li>
                            <li><b>Attachment</b><br /> <?=  $v['pivot']['cntnt_attachment'] ?></li>
                        </td>
                    </tr>
                <?php } 
                } ?>
                
            </tbody>
        </table>
        <?php } else {
            echo '<div class="text-center">No Record Found.</div>';
        } ?>

		</div>
	</section>
</div>
@endsection