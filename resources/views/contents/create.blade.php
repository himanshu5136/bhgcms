@extends('layouts.dashboard')

@section('content')
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style type="text/css">
    #create_content_tag_listing_form_group{ height: 130px; overflow-y: scroll; }
    #create_content_tag_listing_form_group .form-item{display: flex;}
    #create_content_tag_listing_form_group .form-item input {width: auto; height: auto; margin-right: 5px;}
    #create_content_tag_listing_form_group .form-item label{font-weight: normal;}
    .display_heading{border-bottom: 2px solid #000;background: #FFF3C5;font-size: 14px;padding: 1px 10px;}
    .display_heading h4.subheading{font-size: 14px;}
    .display_fields .form-group{padding: 6px 10px;background: #f7f7f7;border: 1px solid #ccc;border-radius: 10px;margin: 0px 4px 10px 14px;}
    .images_block_wrapper{background: #eee; padding-bottom: 10px;}
    span.select2-selection--multiple{min-height:120px !important;}
    .display_none{display:none;}
    span.select2-container{width:100% !important;}

    .ui-tabs-vertical { width: 55em; }
    .ui-tabs-vertical .ui-tabs-nav { overflow-y: scroll; max-height: 720px; padding: .2em .1em .2em .2em; float: left; width: 12em; min-height: 720px; overflow-y: scroll;}
    .ui-tabs-vertical .ui-tabs-nav li { clear: left; width: 100%; border-bottom-width: 1px !important; border-right-width: 0 !important; margin: 0 -1px .2em 0; }
    .ui-tabs-vertical .ui-tabs-nav li a { display:block; }
    .ui-tabs-vertical .ui-tabs-nav li.ui-tabs-active { padding-bottom: 0; padding-right: .1em; border-right-width: 1px; }
    .ui-tabs-vertical .ui-tabs-panel { padding: 1em; float: right; width: 40em;}
    body {font-family: Arial, Helvetica, sans-serif;}
    h4.lang-subheading{border-bottom: 2px solid #333;font-weight: bold;background: #F4EAC3;padding: 5px;margin-bottom: 16px;}
    table {font-size: 1em;}
    .ui-draggable, .ui-droppable {background-position: top;}
</style>
<?php 
$role = auth()->user()->getRole();
$role_segment = ($role == 'admin')?'/admin':'';
$base_url  = URL::to('/').$role_segment;
$form_action = $base_url.'/contents';
//{{ route('contents.store') }}
?>
<div class="page-heading"><h3>Add Content</h3></div>
@include('flash-message')
<div class="col-md-8 col-md-offset-2 contents-create-page contents form large-9 medium-8 columns content">
	<section class="panel">
		<div class="panel-body">
            <form method="POST" enctype="multipart/form-data" action="{{ $form_action }}">
            @csrf
                <div class="row">
                    <div class="col-lg-6">
                        <div id="form-field-tags" class="form-group form-checkbox-wrapper">
                            <label class="required">Categories</label><br />
                            <div id="create_content_tag_listing_form_group" class="form-control">
                                <?php foreach($tags as $k=>$v) { ?>
                                    <div class="form-item">
                                    <input id="tag_<?= $v['id'] ?>" type="checkbox" class="form-control @error('tag') is-invalid @enderror" name="tag[]" value="<?= $v['id'] ?>" autocomplete="tag">
                                    <label for="tag_<?= $v['id'] ?>"><?= $v['name'] ?></label>
                                    </div>
                                <?php } ?>
                            </div>
                            <span class="help-text">( Choose minimum one category to create Content. )</span>
                            @error('tag')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="required">Status</label><br />
                            <select class="form-control @error('status') is-invalid @enderror" name="status" id="status" required>
                                <?php foreach($status as $k=>$v) { ?>
                                <option value="<?= $k ?>"><?= $v ?></option>
                                <?php } ?>
                            </select>
                            @error('status')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class="required">Type</label><br />
                            <select class="form-control @error('ctype') is-invalid @enderror" name="ctype" id="content-ctype-val" required>
                                <option value="">- Choose Type -</option>
                                <?php foreach($ctype as $k=>$v) { ?>
                                <option  {{ (old('ctype') == $k) ? 'selected' : ''  }} value="<?= $k ?>"><?= $v ?></option>
                                <?php } ?>
                            </select>
                            @error('ctype')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>           
                <br />
                <div class="row">
                    <div class="col-lg-12 ">
                    <div id="tabs">
                        <!-- TABS Labels-->
                        <ul>
                            <?php foreach($langs as $key=>$val) {  ?>
                                <li><a href="#lang-<?= $val['code'] ?>-<?= $val['id'] ?>"><?= $val['name'].' ( '.$val['code'].' )'?></a></li>
                            <?php }  ?>
                        </ul>
                        <!-- TABS Wrappers-->
                        <?php foreach($langs as $key=>$val) {  
                            $reqField = ($val['id'] == '1')?'required':''; // Fields can be required only for english language.
                            ?>
                            <div id="lang-<?= $val['code'] ?>-<?= $val['id'] ?>">
                                <h4 class="lang-subheading" ><?= $val['name'] ?> Language </h4>
                                <div class="row">
                                    <div class="col-lg-8 "> 
                                        <div class="form-group" id="content-attachment-field"  >
                                            <label >( <?= $val['code'] ?> ) Attachment</label><br />
                                            <input id="attachment" type="file" class="form-control @error('attachment') is-invalid @enderror" name="langdata_<?= $val['id'] ?>_attachment" value="{{ old('attachment') }}" autocomplete="attachment" autofocus>
                                            <span class="help-text">For type i.e. Zip / Video / Image. If you have choosen "Product" in above mentioned field, value of this field will not save. </span>
                                            @error('attachment')
                                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group" id="content-product-field" >
                                            <label>( <?= $val['code'] ?> ) Product </label><br />
                                            <select class="select-multiple-list form-control @error('product') is-invalid @enderror" name="langdata[<?= $val['id'] ?>][cntnt_product_id]" id="product_<?= $val['id'] ?>">
                                                <option value="">- Choose Product -</option>
                                                <?php foreach($products as $k=>$v) { ?>
                                                <option value="<?= $v['id'] ?>"><?= $v['name'].' ( '.$v['sku'].' )' ?></option>
                                                <?php } ?>
                                            </select>
                                            <span class="help-text">For type i.e. Product. If you have choosen "Zip / Video / Image" in above mentioned field, value of this field will not save</span>
                                            @error('product')
                                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label class="<?= $reqField ?>">( <?= $val['code'] ?> ) Title</label><br />
                                            <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="langdata[<?= $val['id'] ?>][cntnt_title]" value="{{ old('title') }}" <?= $reqField ?> autocomplete="title" autofocus>
                                            @error('title')
                                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label class="<?= $reqField ?>">( <?= $val['code'] ?> ) Author</label><br />
                                            <select class="select-multiple-list form-control @error('author') is-invalid @enderror" name="langdata[<?= $val['id'] ?>][cntnt_author_id]" id="author_<?= $val['id'] ?>" <?= $reqField ?>>
                                                <option value="">- Select One -</option>
                                                <?php foreach($authors as $k=>$v) { ?>
                                                <option value="<?= $v['id'] ?>"><?= $v['full_name'] ?></option>
                                                <?php } ?>
                                            </select>
                                            @error('author')
                                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <div class="images_block_wrapper">
                                                <div class="display_heading">
                                                    <h4 class="subheading">( <?= $val['code'] ?> ) Background Images </h4>
                                                </div><br />
                                                <div class="display_fields">
                                                    <?php for($i=1; $i<=1; $i++) { ?>
                                                        <div class="form-group">
                                                            <label >Background - <?= $i ?></label><br />
                                                            <input id="bg_image" type="file" class="form-control @error('bg_image') is-invalid @enderror" name="langdata_<?= $val['id'] ?>_bg_images[]" value="{{ old('bg_image') }}" />
                                                        </div>
                                                    <?php }  ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="images_block_wrapper">
                                                <div class="display_heading">
                                                    <h4 class="subheading">( <?= $val['code'] ?> ) Thumbnail Images </h4>
                                                </div><br />
                                                <div class="display_fields">
                                                    <?php for($i=1; $i<=1; $i++) { ?>
                                                        <div class="form-group">
                                                            <label >Thumbnail  - <?= $i ?></label><br />
                                                            <input id="tn_image" type="file" class="form-control @error('tn_image') is-invalid @enderror" name="langdata_<?= $val['id'] ?>_tn_images[]" value="{{ old('tn_image') }}" />
                                                        </div>
                                                    <?php }  ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="<?= $reqField ?>">( <?= $val['code'] ?> ) Body</label><br />
                                            <textarea <?= $reqField ?> id="body" name="langdata[<?= $val['id'] ?>][cntnt_body]" class="form-control @error('body') is-invalid @enderror" cols="5" rows="4">{{ old('body') }}</textarea>
                                            @error('body')
                                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">{{ __('Save Content') }}</button>
                        </div>
                    </div>
                </div>
            </form>
		</div>
	</section>
</div>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
@endsection