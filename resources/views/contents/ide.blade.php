@extends('layouts.dashboard')

@section('content')
<style type="text/css">
    #create_content_tag_listing_form_group{ height: 166px; overflow-y: scroll; }
    #create_content_tag_listing_form_group .form-item{display: flex;}
    #create_content_tag_listing_form_group .form-item input {width: auto; height: auto; margin-right: 5px;}
    #create_content_tag_listing_form_group .form-item label{font-weight: normal;}
</style>
<?php 
$role = auth()->user()->getRole();
$role_segment = ($role == 'admin')?'/admin':'';
$base_url  = URL::to('/').$role_segment;
$form_action = $base_url.'/contents/ide';
//{{ route('contents.store') }}
?>
<div class="page-heading"><h3>IDE Content</h3></div>
@include('flash-message')
<div class="col-md-8 col-md-offset-2 contents-ide-page contents form large-9 medium-8 columns content">
	<section class="panel">
		<div class="panel-body">
            <form method="POST" enctype="multipart/form-data" action="{{ $form_action }}">
            @csrf
                <div class="row">
                    <div class="col-lg-6 col-lg-offset-3">
                        <div class="form-group">
                            <label for="ide_client" class="required" >Clients</label><br />
                            <select required name="ide_client" id="ide_client" class="form-control">
                                <option value="">- Select Client -</option>
                                <?php foreach($clients as $k=>$v){ ?>
                                    <option 
                                    <?php 
                                    if(!empty($ide['client'])) {
                                        if($v['id'] == $ide['client']['value']){
                                            echo "selected";
                                        }
                                    }
                                    ?>
                                    
                                    value="<?= $v['id'] ?>"><?= $v['name'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <?php if(!empty($ide['images'])) {
                    $uploadedImages = json_decode($ide['images']['value']);
                } else {
                    $uploadedImages = [];
                }
                ?>
               
                <?php  for($i=0;$i<=3; $i++) { 
                    $j = $i+1;
                    ?>
                    <div class="row">
                        <?php if((isset($uploadedImages[$i])) && (!empty($uploadedImages[$i]))) {  ?>
                            <div class="col-lg-2 col-lg-offset-2">
                                <img style="border: 1px solid #bbb; padding: 5px; margin-top: 10px;" src="<?= $uploadedImages[$i] ?>" height="80px" width="80px" />
                                <input type="hidden" name="hidden_ide_image_{{ $j }}" value="<?= $uploadedImages[$i] ?>" />
                            </div>
                            <div class="col-lg-6">
                        <?php } else { ?>
                        <div class="col-lg-8 col-lg-offset-2">
                        <?php } ?>
                            <div class="form-group">
                                <label for="ide_image_{{ $j }}">IDE Image 0{{ $j }}</label><br />
                                <input accept=".jpg, .png, .jpeg" id="ide_image_{{ $j }}" type="file" class="form-control" name="ide_image_{{ $j }}" value="{{ old('ide_image_$j') }}" />
                                @error('ide_image_{{ $j }}')
                                    <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                @enderror
                                <?php if((isset($uploadedImages[$i])) && (!empty($uploadedImages[$i]))) {  ?>
                                    <input type="checkbox" value="1" name="ide_rmv_image_{{ $j }}" id="ide_rmv_image_{{ $j }}" /> <label for="ide_rmv_image_{{ $j }}">Remove Image</label>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <br />
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">{{ __('Save IDE Content') }}</button>
                        </div>
                    </div>
                </div>
            </form>
		</div>
	</section>
</div>
@endsection