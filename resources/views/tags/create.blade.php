@extends('layouts.dashboard')

@section('content')
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style type="text/css">
    #create_tag_client_listing_form_group{ height: 100px; overflow-y: scroll; }
    #create_tag_client_listing_form_group .form-item{display: flex;}
    #create_tag_client_listing_form_group .form-item input {width: auto; height: auto; margin-right: 5px;}
    #create_tag_client_listing_form_group .form-item label{font-weight: normal;}


    .ui-tabs-vertical { width: 55em; }
    .ui-tabs-vertical .ui-tabs-nav { overflow-y: scroll; max-height: 400px; padding: .2em .1em .2em .2em; float: left; width: 12em; min-height: 400px; overflow-y: scroll;}
    .ui-tabs-vertical .ui-tabs-nav li { clear: left; width: 100%; border-bottom-width: 1px !important; border-right-width: 0 !important; margin: 0 -1px .2em 0; }
    .ui-tabs-vertical .ui-tabs-nav li a { display:block; }
    .ui-tabs-vertical .ui-tabs-nav li.ui-tabs-active { padding-bottom: 0; padding-right: .1em; border-right-width: 1px; }
    .ui-tabs-vertical .ui-tabs-panel { padding: 1em; float: right; width: 40em;}
    body {font-family: Arial, Helvetica, sans-serif;}
    h4.lang-subheading{border-bottom: 2px solid #333;font-weight: bold;background: #F4EAC3;padding: 5px;margin-bottom: 16px;}
    table {font-size: 1em;}
    .ui-draggable, .ui-droppable {background-position: top;}
</style>
<?php 
$role = auth()->user()->getRole();
$role_segment = ($role == 'admin')?'/admin':'';
$base_url  = URL::to('/').$role_segment;
$form_action = $base_url.'/tags';
//{{ route('tags.store') }}
?>
<div class="page-heading"><h3>Add Category</h3></div>
@include('flash-message')
<div class="col-md-8 col-md-offset-2 tags-create-page tags form large-9 medium-8 columns content">
	<section class="panel">
		<div class="panel-body">
            <form method="POST" action="{{ $form_action }}">
            @csrf
                <div class="row">
                    <div class="col-lg-6 col-lg-offset-3">
                        <div id="form-field-clients" class="form-group form-checkbox-wrapper">
                            <label class="required">Clients</label><br />
                            <div id="create_tag_client_listing_form_group" class="form-control">
                                <?php foreach($clients as $k=>$v) { ?>
                                    <div class="form-item">
                                    <input id="client_<?= $v['id'] ?>" type="checkbox" class="form-control @error('client') is-invalid @enderror" name="client[]" value="<?= $v['id'] ?>" autocomplete="client">
                                    <label for="client_<?= $v['id'] ?>"><?= $v['name'] ?></label>
                                    </div>
                                <?php } ?>
                            </div>
                            <span class="help-text">( Choose minimum one client for creating Category. )</span>
                            @error('client')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-lg-12 ">
                    <div id="tabs">
                        <!-- TABS Labels-->
                        <ul>
                            <?php foreach($langs as $key=>$val) {  ?>
                                <li><a href="#lang-<?= $val['code'] ?>-<?= $val['id'] ?>"><?= $val['name'].' ( '.$val['code'].' )'?></a></li>
                            <?php }  ?>
                        </ul>
                        <!-- TABS Wrappers-->
                        <?php foreach($langs as $key=>$val) {  
                            $reqField = ($val['id'] == '1')?'required':''; // Fields can be required only for english language.
                            ?>
                            <div id="lang-<?= $val['code'] ?>-<?= $val['id'] ?>">
                                <h4 class="lang-subheading" ><?= $val['name'] ?> Language </h4>
                                <div class="row">
                                    <div class="col-lg-8 ">
                                        <div class="form-group">
                                            <label class="<?= $reqField ?>">( <?= $val['code'] ?> ) Category Name</label><br />
                                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="langdata[<?= $val['id'] ?>][tag_name]" value="{{ old('name') }}" <?= $reqField ?> >
                                            @error('name')
                                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label>( <?= $val['code'] ?> ) Description</label><br />
                                            <textarea  id="description" name="langdata[<?= $val['id'] ?>][tag_description]" class="form-control @error('description') is-invalid @enderror" cols="5" rows="3">{{ old('description') }}</textarea>
                                            @error('description')
                                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        </div>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">{{ __('Add Category') }}</button>
                        </div>
                    </div>
                </div>
            </form>
		</div>
	</section>
</div>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
@endsection