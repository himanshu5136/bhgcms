@extends('layouts.dashboard')

@section('content')
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<div class="page-heading"><h3>Edit Doctor</h3></div>
@include('flash-message')
<?php 
$role = auth()->user()->getRole();
$role_segment = ($role == 'admin')?'/admin':'';
$base_url  = URL::to('/').$role_segment;
$form_action = $base_url.'/doctors'.'/'.$user['id'];
//{{ route('doctors.update', $user['id']) }}
?> 
<style type="text/css">
    h4.subheading{border-bottom: 2px solid #F4D03F;color: #333;font-size: 20px;margin-bottom: 20px;background: #ECDDA199;padding: 6px;}
    .users-create-page form .form-checkbox-wrapper #create_user_client_listing_form_group{height: 180px;}
    .form-item-display p{background: #eee;margin:0px;border: 1px solid #ccc;padding: 6px;border-radius: 5px;}
    .existing-attachment{text-align:center;background: #ECDDA199; height: 76px; display: block; width: 50px; margin-top: 5px; border: 1px solid #F4D03F;}
    .existing-attachment a{font-size: 10px;line-height: 0px;color: #333;text-decoration: underline;}
    .panel-header{ padding: 10px; }
    .ui-tabs-vertical { width: 55em; }
    .ui-tabs-vertical .ui-tabs-nav { overflow-y: scroll; max-height: 700px; padding: .2em .1em .2em .2em; float: left; width: 12em; min-height: 700px; overflow-y: scroll;}
    .ui-tabs-vertical .ui-tabs-nav li { clear: left; width: 100%; border-bottom-width: 1px !important; border-right-width: 0 !important; margin: 0 -1px .2em 0; }
    .ui-tabs-vertical .ui-tabs-nav li a { display:block; }
    .ui-tabs-vertical .ui-tabs-nav li.ui-tabs-active { padding-bottom: 0; padding-right: .1em; border-right-width: 1px; }
    .ui-tabs-vertical .ui-tabs-panel { padding: 1em; float: right; width: 40em;}
    body {font-family: Arial, Helvetica, sans-serif;}
    h4.lang-subheading{border-bottom: 2px solid #333;font-weight: bold;background: #F4EAC3;padding: 5px;margin-bottom: 16px;}
    table {font-size: 1em;}
    .ui-draggable, .ui-droppable {background-position: top;}
</style>
<div class="col-md-8 col-md-offset-2 users-edit-page form large-9 medium-8 columns content">
	<section class="panel">
        <div class="panel-header">
            <div class="row action-items"> 
                <div class="col-md-6 text-left">
                    <a title="Back to Users Listing" href="<?= $base_url ?>/users" class="btn btn-primary"><i class="fa fa-backward"></i></a>
                </div>
                <div class="col-md-6 text-right">
                    <a title="View User" href="<?= $base_url ?>/users/<?=  $user['id'] ?>" class="btn btn-primary"><i class="fa fa-search-plus"></i></a>
                </div>
            </div>
        </div>
		<div class="panel-body">
            <form method="post" enctype="multipart/form-data" action="{{ $form_action }}">
            @method('PATCH') 
            @csrf
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="subheading">Account Information</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group form-item-display">
                            <label>Username</label>
                            <p>{{ $user['name'] }}</p>
                            <span class="help-text">( Cannot update username once created. )</span>
                        </div>

                    </div>
                    <div class="col-lg-6">
                        <div class="form-group form-item-display">
                            <label>Email</label><br />
                            <p>{{ $user['email'] }}</p>
                            <span class="help-text">( Cannot update email once created. )</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Status</label><br />
                            <select class="form-control @error('status') is-invalid @enderror" name="status" id="status" required>
                                <?php foreach($status as $k=>$v) { ?>
                                    <option <?= ($user['status'] == $k)?'selected':'' ?> value="<?= $k ?>"><?= $v ?></option>
                                <?php }  ?>
                            </select>
                            @error('status')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                        
                    </div>
                    <div class="col-lg-6">
                        <div id="form-field-clients" class="form-checkbox-wrapper">
                            <label class="required">Clients</label><br />
                            <div id="create_user_client_listing_form_group" class="form-control">
                                <?php 
                                foreach($clients as $k=>$v) { ?>
                                    <div class="form-item">
                                    <input id="client_<?= $v['id'] ?>" type="checkbox" class="form-control @error('client') is-invalid @enderror" name="client[]" <?= ((!empty($user['clients'])) && (in_array($v['id'],$user['clients']))) ? 'checked':''; ?> value="<?= $v['id'] ?>" autocomplete="client">
                                    <label for="client_<?= $v['id'] ?>"><?= $v['name'] ?></label>
                                    </div>
                                <?php } ?>
                            </div>
                            <span class="help-text">( Choose minimum one client for "Admin" User Role )</span>
                            @error('client')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="subheading">Profile Information</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="required">Doctor ID ( For Rates )</label><br />
                            <input id="doc_rate_id" type="text" class="form-control @error('doc_rate_id') is-invalid @enderror" name="doc_rate_id" value="{{ $user['profile']['doc_rate_id'] }}" >
                            @error('doc_rate_id')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label >Contact Number</label><br />
                            <input id="contact_number" type="text" class="form-control @error('contact_number') is-invalid @enderror" name="contact_number" autocomplete="contact_number" value="{{ $user['profile']['contact_number'] }}" >
                            @error('full_name')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div> 
                    <div class="col-lg-6">
                        <div id="form-field-doctors" class="form-checkbox-wrapper">
                            <label class="required">Service Type</label><br />
                            <div id="create_user_doc_service_type_form_group" class="form-control">
                                <?php foreach($stypes as $k=>$v) { ?>
                                    <div class="form-item" <?= ($v['is_required'] == '1')?'style="pointer-events: none; opacity: 0.6;"':'' ?>>
                                    <input <?= (((!empty($user['stypes'])) && (in_array($v['id'],$user['stypes']))) || ($v['is_required'] == '1')) ? 'checked':''; ?> id="doctor_stype_<?= $v['id'] ?>" type="checkbox" class="form-control @error('client') is-invalid @enderror" name="stype[]" value="<?= $v['id'] ?>" >
                                    <label for="doctor_stype_<?= $v['id'] ?>"><?= $v['name'] ?></label>
                                    </div>
                                <?php } ?>
                            </div>
                            <span class="help-text">All doctors provide Interactive discussion but not all doctors provide second opinion.</span>
                            @error('stype')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div> 
                <div class="row">
                    <div class="col-lg-12 ">
                    <div id="tabs">
                        <!-- TABS Labels-->
                        <ul>
                            <?php foreach($langs as $key=>$val) {  ?>
                                <li><a href="#lang-<?= $val['code'] ?>-<?= $val['id'] ?>"><?= $val['name'].' ( '.$val['code'].' )'?></a></li>
                            <?php }  ?>
                        </ul>
                        <!-- TABS Wrappers-->
                        <?php foreach($langs as $key=>$val) {
                            $reqField = ($val['id'] == '1')?'required':''; // Fields can be required only for english language.

                            $doc_full_name_value = ((!empty($user['langs'][$val['id']])) && (isset($user['langs'][$val['id']])))?$user['langs'][$val['id']]['doc_full_name']:$user['profile']['full_name'];

                            $doc_title_value = ((!empty($user['langs'][$val['id']])) && (isset($user['langs'][$val['id']])))?$user['langs'][$val['id']]['doc_title']:$user['profile']['title'];

                            $doc_bg_image_value = ((!empty($user['langs'][$val['id']])) && (isset($user['langs'][$val['id']])))?$user['langs'][$val['id']]['doc_bg_image']:$user['profile']['bg_image'];

                            $doc_profile_video_value = ((!empty($user['langs'][$val['id']])) && (isset($user['langs'][$val['id']])))?$user['langs'][$val['id']]['doc_profile_video']:$user['profile']['profile_video'];

                            $doc_picture_value = ((!empty($user['langs'][$val['id']])) && (isset($user['langs'][$val['id']])))?$user['langs'][$val['id']]['doc_picture']:$user['profile']['picture'];

                            $doc_bio_value = ((!empty($user['langs'][$val['id']])) && (isset($user['langs'][$val['id']])))?$user['langs'][$val['id']]['doc_bio']:$user['profile']['bio'];

                            ?>
                            <div id="lang-<?= $val['code'] ?>-<?= $val['id'] ?>">
                                <h4 class="lang-subheading" ><?= $val['name'] ?> Language </h4>
                                <div class="row">
                                    <div class="col-lg-8 ">
                                        <div class="form-group">
                                            <label class="<?= $reqField ?>">( <?= $val['code'] ?> ) Full Name</label><br />
                                            <input id="full_name" type="text" class="form-control @error('full_name') is-invalid @enderror" name="langdata[<?= $val['id'] ?>][full_name]" value="{{ $doc_full_name_value }}" <?= $reqField ?> autocomplete="full_name" autofocus>
                                            @error('full_name')
                                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label class="<?= $reqField ?>">( <?= $val['code'] ?> ) Title</label><br />
                                            <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="langdata[<?= $val['id'] ?>][title]" value="{{ $doc_title_value }}" <?= $reqField ?> >
                                            @error('title')
                                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <?php if(!empty($doc_bg_image_value)) { ?>
                                                <div class="col-md-2 existing-attachment">
                                                <a href="{{ $doc_bg_image_value }}" target="blank">Link to BG Image</a>
                                                </div>
                                                <div class="col-md-10">
                                            <?php } ?>
                                                <label >( <?= $val['code'] ?> ) Background Image</label><br />
                                                <input accept="image/*" id="bg_image" type="file" class="form-control @error('bg_image') is-invalid @enderror" name="langdata_<?= $val['id'] ?>_bg_image" value="{{ old('bg_image') }}" autocomplete="bg_image">
                                                
                                                @error('bg_image')
                                                    <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                                @enderror
                                                <div class=""><input value="1" type="checkbox" name="remove_<?= $val['id'] ?>_doc_bg_image" id="remove_<?= $val['id'] ?>_doc_bg_image"/><label for="remove_<?= $val['id'] ?>_doc_bg_image">&nbsp;Remove BG Image</label></div>
                                                <span class="help-text">Each doctor needs background imageespecially for lean back theme/Android TV layout.</span>
                                            <?php if(!empty($doc_bg_image_value)) { ?>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <div class="form-group">
                                            <?php if(!empty($doc_profile_video_value)) { ?>
                                                <div class="col-md-2 existing-attachment">
                                                <a href="{{ $doc_profile_video_value }}" target="blank">Link to Video</a>
                                                </div>
                                                <div class="col-md-10">
                                            <?php } ?>
                                                <label >( <?= $val['code'] ?> ) Profile Video</label><br />
                                                <input accept="video/*" id="profile_video" type="file" class="form-control @error('profile_video') is-invalid @enderror" name="langdata_<?= $val['id'] ?>_profile_video" value="{{ old('profile_video') }}" >
                                                <span class="help-text">Doctor can upload Profile Video.</span>
                                                @error('profile_video')
                                                    <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                                @enderror
                                                <div class=""><input value="1" type="checkbox" name="remove_<?= $val['id'] ?>_doc_profile_video" id="remove_<?= $val['id'] ?>_doc_profile_video"/><label for="remove_<?= $val['id'] ?>_doc_profile_video">&nbsp;Remove Video</label></div>
                                            <?php if(!empty($doc_profile_video_value)) { ?>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        
                                        <div class="form-group">
                                            <?php if(!empty($doc_picture_value)) { ?>
                                                <div class="col-md-2 existing-attachment">
                                                <a href="{{ $doc_picture_value }}" target="blank">Link to Image</a>
                                                </div>
                                                <div class="col-md-10">
                                            <?php } ?>
                                                <label >( <?= $val['code'] ?> ) Profile Picture</label><br />
                                                <input accept=".jpg, .png, .jpeg" id="picture" type="file" class="form-control @error('picture') is-invalid @enderror" name="langdata_<?= $val['id'] ?>_doc_picture" value="{{ old('picture') }}" >
                                                @error('picture')
                                                    <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                                @enderror
                                                <div class=""><input value="1" type="checkbox" name="remove_<?= $val['id'] ?>_doc_picture" id="remove_<?= $val['id'] ?>_doc_picture"/><label for="remove_<?= $val['id'] ?>_doc_picture">&nbsp;Remove Picture</label></div>
                                            <?php if(!empty($doc_picture_value)) { ?>
                                                </div>
                                            <?php } ?>
                                        </div>

                                        <div class="form-group">
                                            <label>( <?= $val['code'] ?> ) Bio</label><br />
                                            <textarea  id="user_brief" name="langdata[<?= $val['id'] ?>][user_brief]" class="form-control @error('user_brief') is-invalid @enderror" cols="5" rows="4">{{ $doc_bio_value }}</textarea>
                                            @error('user_brief')
                                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        </div>
                    </div>
                </div>   
                <br />
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">{{ __('Update User') }}</button>
                        </div>
                    </div>
                </div>
            </form>
		</div>
	</section>
</div>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
@endsection