@extends('layouts.dashboard')

@section('content')
<div class="page-heading"><h3>List of Doctors</h3></div>
@include('flash-message')
<?php 
$statusAry = ['1'=>'Active','2'=>'Blocked'];
$role = auth()->user()->getRole();
$role_segment = ($role == 'admin')?'/admin':'';
$base_url  = URL::to('/').$role_segment;
?>
<style type="text/css">
tr.deleted-user{background:#ff000030;}
</style>
<div class="doctors form large-9 medium-8 columns content">
	<section class="panel">
    <div class="panel-heading ">
            <div class="row users_filters_wrapper bg-secondary">
                <form method="get" id="form_doctors_filter" >
                    <div class="col-md-2 form-group">
                        <label>Clients</label><br />
                        <select class="form-control @error('rid') is-invalid @enderror" name="cid" id="cid">
                            <option value="">- Select One -</option>
                            @foreach($clients as $k=>$v)
                                <option <?php if((!empty($qry['cid'])) && ($qry['cid'] == $k)){ echo "selected"; } ?> value="{{ $k }}">{{ $v }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-2 form-group">  
                        <label>&nbsp;</label><br />
                        <input type="submit" name="submit" value="Search" class="btn btn-primary" />
                        <a href="<?= $base_url ?>/doctors" class="btn btn-secondary">Reset</a>
                    </div>	
                </form>
            </div>	
        </div>
		<div class="panel-body">
        <?php //echo count($users); die;// echo "<pre>"; print_r($users); die; ?>
        <?php if(count($doctors) > 0) { ?>
        <table class="table table-hover general-table">
            <thead>
                <tr>
                    <th width="15%">Username</th>
                    <th width="20%">Full Name</th>
                    <th width="20%">Email</th>
                    <th width="10%">Services</th>
                    <th width="15%">Clients</th>
                    <th width="10%">Status</th>
                    <th width="15%">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($doctors as $doc)
                <tr>
                    <td>{{ $doc['name'] }}</td>
                    <td>{{ (isset($doc['profile']['full_name'])) ? $doc['profile']['full_name']:'' }}</td>
                    <td>{{ $doc['email'] }}</td>
                    <td><ul><?php 
                        foreach($doc['stypes'] as $k=>$v){
                            echo '<li>'.$v['name'].'</li>';
                        }
                    ?></ul></td>
                    <td><ul><?php 
                        foreach($doc['clients'] as $k=>$v){
                            echo '<li>'.$v['name'].'</li>';
                        }
                    ?></ul></td>
                    <td>{{ $statusAry[$doc['status']] }}</td>
                    <td class="actions">
                        <div class="text-center">
                            <a href="<?= $base_url ?>/doctors/<?= $doc['id'] ?>" title="View Doctor"><i class="fa fa-search-plus"></i></a>&nbsp;&nbsp;
                            <a href="<?= $base_url ?>/doctors/<?= $doc['id'] ?>/edit" title="Edit User"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;
                            <form name="trash_doc_<?= $doc['id'] ?>" style="display:none;" method="post" action="<?= $base_url ?>/doctors/<?= $doc['id'] ?>/trash">  @csrf <input type="hidden" name="_method" value="POST"></form>
                            <a href="#" onclick="if (confirm(&quot;Are you sure you want to delete doctor <?= $doc['name'] ?>?&quot;)) { document.trash_doc_<?= $doc['id'] ?>.submit(); } event.returnValue = false; return false;" title="Delete Doctor"><i class="fa fa-trash-o"></i></a>
                        </div>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        <div class="text-center">
            {{ $paginate->links() }}
        </div>
        <?php } else {
            echo '<div class="text-center">No Record Found.</div>';
        } ?>
		</div>
	</section>
</div>
@endsection