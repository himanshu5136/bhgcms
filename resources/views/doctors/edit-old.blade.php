@extends('layouts.dashboard')

@section('content')
<div class="page-heading"><h3>Edit Doctor</h3></div>
@include('flash-message')
<?php 
$role = auth()->user()->getRole();
$role_segment = ($role == 'admin')?'/admin':'';
$base_url  = URL::to('/').$role_segment;
$form_action = $base_url.'/doctors'.'/'.$user['id'];
//{{ route('doctors.update', $user['id']) }}
?>
<style type="text/css">
    .users-edit-page form .form-checkbox-wrapper #create_user_client_listing_form_group{height: 156px;}
    h4.subheading{border-bottom: 2px solid #F4D03F;color: #333;font-size: 20px;margin-bottom: 20px;background: #ECDDA199;padding: 6px;}
    .form-item-display p{background: #eee;margin:0px;border: 1px solid #ccc;padding: 6px;border-radius: 5px;}
    .existing-attachment{text-align:center;background: #ECDDA199; height: 76px; display: block; width: 50px; margin-top: 5px; border: 1px solid #F4D03F;}
    .existing-attachment a{font-size: 10px;line-height: 0px;color: #333;text-decoration: underline;}
    .panel-header{ padding: 10px; }
</style>
<div class="col-md-8 col-md-offset-2 users-edit-page form large-9 medium-8 columns content">
	<section class="panel">
        <div class="panel-header">
            <div class="row action-items"> 
                <div class="col-md-6 text-left">
                    <a title="Back to Users Listing" href="<?= $base_url ?>/doctors" class="btn btn-primary"><i class="fa fa-backward"></i></a>
                </div>
                <div class="col-md-6 text-right">
                    <a title="View User" href="<?= $base_url ?>/doctors/<?=  $user['id'] ?>" class="btn btn-primary"><i class="fa fa-search-plus"></i></a>
                </div>
            </div>
        </div>
		<div class="panel-body">
            <form method="post" enctype="multipart/form-data" action="{{ $form_action }}">
            @method('PATCH') 
            @csrf
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group form-item-display">
                            <label>Username</label>
                            <p>{{ $user['name'] }}</p>
                            <span class="help-text">( Cannot update username once created. )</span>
                        </div>
                        <div class="form-group">
                            <label class="required">Full Name</label><br />
                            <input id="full_name" type="text" class="form-control @error('full_name') is-invalid @enderror" name="full_name" required autocomplete="full_name" value="{{ $user['profile']['full_name'] }}">
                            @error('full_name')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div> 
                        <div class="form-group">
                            <label class="required">Title</label><br />
                            <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ $user['profile']['title'] }}" required autocomplete="title">
                            @error('title')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label >Contact Number</label><br />
                            <input id="contact_number" type="text" class="form-control @error('contact_number') is-invalid @enderror" name="contact_number" autocomplete="contact_number" value="{{ $user['profile']['contact_number'] }}" >
                            @error('full_name')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>Status</label><br />
                            <select class="form-control @error('status') is-invalid @enderror" name="status" id="status" required>
                                <?php foreach($status as $k=>$v) { ?>
                                    <option <?= ($user['status'] == $k)?'selected':'' ?> value="<?= $k ?>"><?= $v ?></option>
                                <?php }  ?>
                            </select>
                            @error('status')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>

                    </div>
                    <div class="col-lg-6">
                        <div class="form-group form-item-display">
                            <label>Email</label><br />
                            <p>{{ $user['email'] }}</p>
                            <span class="help-text">( Cannot update email once created. )</span>
                        </div>
                        <?php 
                         $old_role_client_display = (in_array($user['role']['id'],['2','3'])) ? 'display_block' : 'display_none';
                         ?>
                        <div id="form-field-clients" class="form-checkbox-wrapper  {{ $old_role_client_display }}">
                            <label>Clients</label><br />
                            <div id="create_user_client_listing_form_group" class="form-control">
                                <?php 
                                foreach($clients as $k=>$v) { ?>
                                    <div class="form-item">
                                    <input id="client_<?= $v['id'] ?>" type="checkbox" class="form-control @error('client') is-invalid @enderror" name="client[]" <?= ((!empty($user['clients'])) && (in_array($v['id'],$user['clients']))) ? 'checked':''; ?> value="<?= $v['id'] ?>" autocomplete="client">
                                    <label for="client_<?= $v['id'] ?>"><?= $v['name'] ?></label>
                                    </div>
                                <?php } ?>
                            </div>
                            <span class="help-text">( Choose minimum one client for "Admin" User Role )</span>
                            @error('client')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>

                        <br/>
                        <div class="form-group">
                            <?php if(!empty($user['profile']['picture'])) { ?>
                                <div class="col-md-2 existing-attachment">
                                <a href="{{ $user['profile']['picture'] }}" target="blank">Link to Image</a>
                                </div>
                                <div class="col-md-10">
                            <?php } ?>
                                <label >Profile Picture</label><br />
                                <input accept=".jpg, .png, .jpeg" id="picture" type="file" class="form-control @error('picture') is-invalid @enderror" name="picture" value="{{ old('picture') }}" autocomplete="picture" >
                                @error('picture')
                                    <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                @enderror
                            <?php if(!empty($user['profile']['picture'])) { ?>
                                <div class=""><input value="1" type="checkbox" name="remove_picture" id="remove_picture"/><label for="remove_picture">&nbsp;Remove Picture</label></div>
                            
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                @php
                    $old_role_doc_block_display = ($user['role']['id'] == '3') ? 'display_block' : 'display_none';
                @endphp
                <div id="doctor-information-block" class="row form-checkbox-wrapper {{ $old_role_doc_block_display }}">
                    <div class="col-lg-6">
                        <?php if(!empty($user['profile']['bg_image'])) { ?>
                            <div class="form-group row">
                                <div class="col-md-2 existing-attachment">
                                <a href="{{ $user['profile']['bg_image'] }}" target="blank">View Image</a>
                                </div>
                                <div class="col-md-10">
                        <?php } else { ?>
                            <div class="form-group">
                        <?php } ?>
                                <label >Background Image</label><br />
                                <input accept="image/*" id="bg_image" type="file" class="form-control @error('bg_image') is-invalid @enderror" name="bg_image" value="{{ old('bg_image') }}" autocomplete="bg_image">
                                @error('bg_image')
                                    <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                @enderror
                                <div class=""><input value="1" type="checkbox" name="remove_bg_image" id="remove_bg_image"/><label for="remove_bg_image">&nbsp;Remove Background Image</label></div>
                            <?php if(!empty($user['profile']['bg_image'])) { ?>
                                </div>
                            <?php } ?>
                        </div>
                        
                        <?php if(!empty($user['profile']['profile_video'])) { ?>
                        <div class="form-group row">
                            <div class="col-md-2 existing-attachment">
                            <a href="{{ $user['profile']['profile_video'] }}" target="blank">View Video</a>
                            </div>
                            <div class="col-md-10">
                        <?php } else { ?>
                            <div class="form-group">
                        <?php } ?>
                                <label >Profile Video</label><br />
                                <input accept="video/*" id="profile_video" type="file" class="form-control @error('profile_video') is-invalid @enderror" name="profile_video" value="{{ old('profile_video') }}" autocomplete="profile_video" >
                                <span class="help-text">Doctor can upload Profile Video.</span>
                                @error('profile_video')
                                    <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                @enderror
                                <div class=""><input value="1" type="checkbox" name="remove_profile_video" id="remove_profile_video"/><label for="remove_profile_video">&nbsp;Remove Profile Video</label></div>
                            <?php if(!empty($user['profile']['profile_video'])) { ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="required">Doctor ID ( For Rates )</label><br />
                            <input id="doc_rate_id" type="text" class="form-control @error('doc_rate_id') is-invalid @enderror" name="doc_rate_id" value="{{ $user['profile']['doc_rate_id'] }}" >
                            @error('doc_rate_id')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                        <div id="form-field-doctors" class="form-checkbox-wrapper">
                            <label class="required">Service Type</label><br />
                            <div id="create_user_doc_service_type_form_group" class="form-control">
                                <?php foreach($stypes as $k=>$v) { ?>
                                    <div class="form-item" <?= ($v['is_required'] == '1')?'style="pointer-events: none; opacity: 0.6;"':'' ?>>
                                    <input <?= (((!empty($user['stypes'])) && (in_array($v['id'],$user['stypes']))) || ($v['is_required'] == '1')) ? 'checked':''; ?> id="doctor_stype_<?= $v['id'] ?>" type="checkbox" class="form-control @error('client') is-invalid @enderror" name="stype[]" value="<?= $v['id'] ?>" >
                                    <label for="doctor_stype_<?= $v['id'] ?>"><?= $v['name'] ?></label>
                                    </div>
                                <?php } ?>
                            </div>
                            <span class="help-text">All doctors provide Interactive discussion but not all doctors provide second opinion.</span>
                            @error('stype')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">                      
                        <div class="form-group">
                            <label>Doctor Bio</label><br />
                            <textarea  id="user_brief" name="user_brief" class="form-control @error('user_brief') is-invalid @enderror" cols="5" rows="8">{{ $user['profile']['bio'] }}</textarea>
                            @error('user_brief')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">{{ __('Update User') }}</button>
                        </div>
                    </div>
                </div>
            </form>
		</div>
	</section>
</div>
@endsection