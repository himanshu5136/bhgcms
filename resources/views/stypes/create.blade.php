@extends('layouts.dashboard')

@section('content')
<style type="text/css">
    #create_client_doctor_listing_form_group{ height: 100px; overflow-y: scroll; }
    #create_client_doctor_listing_form_group .form-item{display: flex;}
    #create_client_doctor_listing_form_group .form-item input {width: auto; height: auto; margin-right: 5px;}
    #create_client_doctor_listing_form_group .form-item label{font-weight: normal;}

    #create_client_tag_listing_form_group{ height: 100px; overflow-y: scroll; }
    #create_client_tag_listing_form_group .form-item{display: flex;}
    #create_client_tag_listing_form_group .form-item input {width: auto; height: auto; margin-right: 5px;}
    #create_client_tag_listing_form_group .form-item label{font-weight: normal;}
    #configure_langs_listing_form_group{ height: 100px; overflow-y: scroll; }
    #configure_langs_listing_form_group .form-item{display: flex;}
    #configure_langs_listing_form_group .form-item input {width: auto; height: auto; margin-right: 5px;}
    #configure_langs_listing_form_group .form-item label{font-weight: normal;}
</style>
<div class="page-heading"><h3>Add Service Type</h3></div>
@include('flash-message')
<div class="col-md-8 col-md-offset-2 stypes-create-page stypes form large-9 medium-8 columns content">
	<section class="panel">
		<div class="panel-body">
            <form method="POST" action="{{ route('stypes.store') }}">
            @csrf
                <div class="row">
                    <div class="col-lg-6 col-lg-offset-3">
                        <div class="form-group">
                            <label class="required">Service Name</label><br />
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                            @error('name')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">{{ __('Save Service Type') }}</button>
                        </div>
                    </div>
                </div>
            </form>
		</div>
	</section>
</div>
@endsection