@extends('layouts.dashboard')

@section('content')
<div class="page-heading"><h3>List of Service Types</h3></div>
@include('flash-message')
<?php 
$base_url  = URL::to('/');
?>
<style type="text/css">
</style>
<div class="stypes form large-9 medium-8 columns content">
	<section class="panel">
		<div class="panel-body">
        <?php if(count($stypes) > 0) { ?>
        <table class="table  table-hover general-table">
            <thead>
                <tr>
                    <th width="8%">Service type Id</th>
                    <th width="15%">Service Name</th>
                    <th width="15%">Created Date</th>
                    <th width="10%">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($stypes as $stype)
                <tr>
                    <td>{{ $stype['id'] }}</td>
                    <td>{{ $stype['name'] }}</td>
                    <td>{{ date('M d, Y H:i:s',strtotime($stype['created_at'])) }}</td>
                    <td class="actions">
                        <a href="<?= $base_url ?>/users/stypes/<?= $stype['id'] ?>" title="View Service"><i class="fa fa-search-plus"></i></a>&nbsp;&nbsp;
                        <a href="<?= $base_url ?>/users/stypes/<?= $stype['id'] ?>/edit" title="Edit Service"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;
                        <form name="post_stype_<?= $stype['id'] ?>" style="display:none;" method="post" action="{{ route('stypes.destroy', $stype['id']) }}" > @method('DELETE')  @csrf</form>
                        <a href="#" onclick="if (confirm(&quot;Are you sure you want to delete service <?= $stype['name'] ?>?&quot;)) { document.post_stype_<?= $stype['id'] ?>.submit(); } event.returnValue = false; return false;" title="Delete Service"><i class="fa fa-trash-o"></i></a>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        <div class="text-center">
            {{ $paginate->links() }}
        </div>
        <?php } else {
            echo '<div class="text-center">No Record Found.</div>';
        } ?>
		</div>
	</section>
</div>
@endsection