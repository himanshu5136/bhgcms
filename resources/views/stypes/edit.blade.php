@extends('layouts.dashboard')

@section('content')
<style type="text/css">
</style>
<div class="page-heading"><h3>Update Service Type</h3></div>
@include('flash-message')
<div class="col-md-8 col-md-offset-2 stypes-create-page stypes form large-9 medium-8 columns content">
	<section class="panel">
		<div class="panel-body">
            <form method="post" action="{{ route('stypes.update', $stype['id']) }}">
            @method('PATCH') 
            @csrf
                <div class="row">
                    <div class="col-lg-6 col-lg-offset-3">
                        <div class="form-group">
                            <label class="required">Service Name</label><br />
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" required autocomplete="name" autofocus value="{{ $stype['name'] }}">
                            @error('name')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">{{ __('Update Service') }}</button>
                        </div>
                    </div>
                </div>
            </form>
		</div>
	</section>
</div>
@endsection