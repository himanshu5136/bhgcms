@extends('layouts.dashboard')

@section('content')
<div class="page-heading"><h3>Service Type Detail</h3></div>
@include('flash-message')
<?php 
$base_url  = URL::to('/');
?>
<style type="text/css">
    .panel-header{ padding: 10px; }
</style>
<div class="col-md-8 col-md-offset-2 stype-view-page stypes form large-9 medium-8 columns content">
	<section class="panel">
        <div class="panel-header">
            <div class="row action-items">
                <div class="col-md-6 text-left">
                    <a title="Back to Service Listing" href="<?= $base_url ?>/users/stypes" class="btn btn-primary"><i class="fa fa-backward"></i></a>
                </div>
                <div class="col-md-6 text-right">
                    <a title="Edit Service Type" href="<?= $base_url ?>/users/stypes/<?=  $stype['id'] ?>/edit" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                </div>
            </div>
        </div>
		<div class="panel-body">
        <?php if(count($stype) > 0) { ?>
        <table class="table table-bordered superusers-view table-striped">
            <thead>
                <tr>
                    <th class="bg-primary" width="50%">KEY</th>
                    <th class="bg-primary" width="50%">VALUE</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Id</td>
                    <td><?=  $stype['id'] ?></td>
                </tr>
                <tr>
                    <td>Name</td>
                    <td><?= $stype['name'] ?></td>
                </tr>
                <tr>
                    <td>Created At</td>
                    <td><?= date('M d, Y H:i:s',strtotime($stype['created_at'])) ?></td>
                </tr>
            </tbody>
        </table>
        <?php } else {
            echo '<div class="text-center">No Record Found.</div>';
        } ?>
		</div>
	</section>
</div>
@endsection