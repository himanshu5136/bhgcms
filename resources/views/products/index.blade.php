@extends('layouts.dashboard')

@section('content')
<div class="page-heading"><h3>List of Products</h3></div>
@include('flash-message')
<?php 
$base_url  = URL::to('/');
?>
<style type="text/css">
</style>
<div class="products form large-9 medium-8 columns product">
	<section class="panel">
        <div class="panel-heading ">
            <div class="row products_filters_wrapper bg-secondary">
                <form method="get" id="form_products_filter" >
                    <div class="col-md-3 form-group">
                        <label>Name</label><br />
                        <input class="form-control" type="text" name="pnm" id="products-filter-pnm" value="<?= (!empty($qry['pnm']))?$qry['pnm']:'' ?>" /> 
                    </div>
                    <div class="col-md-3 form-group">
                        <label>SKU</label><br />
                        <input class="form-control" type="text" name="sku" id="products-filter-sku" value="<?= (!empty($qry['sku']))?$qry['sku']:'' ?>" /> 
                    </div>
                    <div class="col-md-3 form-group">
                        <label>&nbsp;</label><br />
                        <input type="submit" name="submit" value="Search" class="btn btn-primary" />
                        <a href="<?= $base_url ?>/contents/products" class="btn btn-secondary">Reset</a>
                    </div>	
                </form>
            </div>	
        </div>
		<div class="panel-body">
        <?php if(count($products) > 0) { ?>
            <table class="table  table-hover general-table">
                <thead>
                    <tr>
                        <th width="10%">Product Id</th>
                        <th width="20%">Name (English)</th>
                        <th width="10%">SKU</th>
                        <th width="20%">Created Date</th>
                        <th width="10%">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($products as $product)
                    <tr>
                        <td>{{ $product['id'] }}</td>
                        <td>{{ $product['langs']['1']['prdct_name'] }}</td>
                        <td>{{ $product['sku'] }}</td>
                        <td>{{ date('M d, Y H:i:s',strtotime($product['created_at'])) }}</td>
                        <td class="actions">
                            <a href="<?= $base_url ?>/contents/products/<?= $product['id'] ?>" title="View Product"><i class="fa fa-search-plus"></i></a>&nbsp;&nbsp;
                            <a href="<?= $base_url ?>/contents/products/<?= $product['id'] ?>/edit" title="Edit Product"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;
                        
                            <form name="post_product_<?= $product['id'] ?>" style="display:none;" method="post" action="<?= $base_url ?>/contents/products/<?= $product['id'] ?>" > @method('DELETE')  @csrf</form>
                            <a href="#" onclick="if (confirm(&quot;Are you sure you want to delete product <?= $product['name'] ?>?&quot;)) { document.post_product_<?= $product['id'] ?>.submit(); } event.returnValue = false; return false;" title="Delete Product"><i class="fa fa-trash-o"></i></a>
                        </td>
                    </tr>
                    @endforeach
                <tbody>
            </table>
            <div class="text-center">
                {{ $paginate->links() }}
            </div>
        <?php } else {
            echo '<div class="text-center">No Record Found.</div>';
        } ?>
		</div>
	</section>
</div>
@endsection