@extends('layouts.dashboard')

@section('content')
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style type="text/css"> 
.ui-tabs-vertical { width: 55em; }
.ui-tabs-vertical .ui-tabs-nav { overflow-y: scroll; max-height: 600px; padding: .2em .1em .2em .2em; float: left; width: 12em; min-height: 600px; overflow-y: scroll;}
.ui-tabs-vertical .ui-tabs-nav li { clear: left; width: 100%; border-bottom-width: 1px !important; border-right-width: 0 !important; margin: 0 -1px .2em 0; }
.ui-tabs-vertical .ui-tabs-nav li a { display:block; }
.ui-tabs-vertical .ui-tabs-nav li.ui-tabs-active { padding-bottom: 0; padding-right: .1em; border-right-width: 1px; }
.ui-tabs-vertical .ui-tabs-panel { padding: 1em; float: right; width: 40em;}
body {font-family: Arial, Helvetica, sans-serif;}
h4.lang-subheading{border-bottom: 2px solid #333;font-weight: bold;background: #F4EAC3;padding: 5px;margin-bottom: 16px;}
table {font-size: 1em;}
.ui-draggable, .ui-droppable {background-position: top;}
</style>
<div class="page-heading"><h3>Add Product</h3></div>
@include('flash-message')
<div class="col-md-8 col-md-offset-2 products-create-page products form large-9 medium-8 columns content">
	<section class="panel">
		<div class="panel-body">
            <form method="POST" enctype="multipart/form-data" action="{{ route('products.store') }}">
            @csrf
                <div class="row">
                    <div class="col-lg-6 col-lg-offset-3">
                        <div class="form-group">
                            <label class="required">SKU</label><br />
                            <input id="sku" type="text" class="form-control @error('sku') is-invalid @enderror" name="sku" value="{{ old('sku') }}" required>
                            @error('sku')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>              
                <div class="row">
                    <div class="col-lg-12 ">
                    <div id="tabs">
                        <!-- TABS Labels-->
                        <ul>
                            <?php foreach($langs as $key=>$val) {  ?>
                                <li><a href="#lang-<?= $val['code'] ?>-<?= $val['id'] ?>"><?= $val['name'].' ( '.$val['code'].' )'?></a></li>
                            <?php }  ?>
                        </ul>
                        <!-- TABS Wrappers-->
                        <?php foreach($langs as $key=>$val) {  
                            $reqField = ($val['id'] == '1')?'required':''; // Fields can be required only for english language.
                            ?>
                            <div id="lang-<?= $val['code'] ?>-<?= $val['id'] ?>">
                                <h4 class="lang-subheading" ><?= $val['name'] ?> Language </h4>
                                <div class="row">
                                    <div class="col-lg-8 ">
                                        <!-- PRODUCT Name -->
                                        <div class="form-group">
                                            <label class="<?= $reqField ?>">( <?= $val['code'] ?> ) Product Name</label><br />
                                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="langdata[<?= $val['id'] ?>][prdct_name]" value="{{ old('name') }}" <?= $reqField ?> >
                                            @error('name')
                                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <!-- PRODUCT Picture -->
                                        <div class="form-group">
                                            <label >(<?= $val['code'] ?> ) Product Picture</label><br />
                                            <input accept="image/*"  id="picture" type="file" class="form-control @error('picture') is-invalid @enderror" name="langdata_<?= $val['id'] ?>_prdct_picture" value="{{ old('picture') }}">
                                            @error('picture')
                                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <!-- PRODUCT Video -->
                                        <div class="form-group">
                                            <label >(<?= $val['code'] ?> ) Product Video</label><br />
                                            <input accept="video/*"  id="video" type="file" class="form-control @error('video') is-invalid @enderror" name="langdata_<?= $val['id'] ?>_prdct_video" value="{{ old('video') }}">
                                            @error('picture')
                                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <!-- QR Code Text/ Link -->
                                        <div class="form-group">
                                            <label class="">( <?= $val['code'] ?> ) QR Code Text/ Link</label><br />
                                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="langdata[<?= $val['id'] ?>][prdct_qrcode]" value="{{ old('name') }}" >
                                            @error('name')
                                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <span style="display: block; color: #ef0d0d; margin-bottom: 20px;" class=" text-center help-text">You need to fill only one field either QR Code or QR Image.</span>
                                        <!-- QR Image -->
                                        <div class="form-group">
                                            <label >(<?= $val['code'] ?> ) QR Image</label><br />
                                            <input accept="image/*"  id="qrimage" type="file" class="form-control @error('qrimage') is-invalid @enderror" name="langdata_<?= $val['id'] ?>_prdct_qrimage" value="{{ old('qrimage') }}">
                                            @error('qrimage')
                                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <!-- Product Description -->                                       
                                        <div class="form-group">
                                            <label>( <?= $val['code'] ?> ) Description</label><br />
                                            <textarea  id="description" name="langdata[<?= $val['id'] ?>][prdct_description]" class="form-control @error('description') is-invalid @enderror" cols="5" rows="3">{{ old('description') }}</textarea>
                                            @error('description')
                                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        </div>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">{{ __('Save Product') }}</button>
                        </div>
                    </div>
                </div>
            </form>
		</div>
	</section>
</div>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
@endsection
