@extends('layouts.dashboard')

@section('content')
<div class="page-heading"><h3>Product Detail</h3></div>
@include('flash-message')
<?php 
$base_url  = URL::to('/');
?>
<style type="text/css">
    .panel-header{ padding: 10px; }
</style>
<div class="col-md-8 col-md-offset-2 product-view-page products form large-9 medium-8 columns content">
	<section class="panel">
        <div class="panel-header">
            <div class="row action-items">
                <div class="col-md-6 text-left">
                    <a title="Back to Auhtors Listing" href="<?= $base_url ?>/contents/products" class="btn btn-primary"><i class="fa fa-backward"></i></a>
                </div>
                <div class="col-md-6 text-right">
                    <a title="Edit Product" href="<?= $base_url ?>/contents/products/<?=  $product['id'] ?>/edit" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                </div>
            </div>
        </div>
		<div class="panel-body">
        <?php if(count($product) > 0) { ?>
        <table class="table table-bordered superusers-view table-striped">
            <thead>
                <tr>
                    <th class="bg-primary" width="50%">KEY</th>
                    <th class="bg-primary" width="50%">VALUE</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Id</td>
                    <td><?=  $product['id'] ?></td>
                </tr>
                <tr>
                    <td>SKU</td>
                    <td><?= $product['sku'] ?></td>
                </tr>
                 <tr>
                    <td colspan="2" class="text-center"><b>Language Based Data</b></td>
                </tr>
                <?php 
                if(!empty($product['langs'])) {
                foreach($product['langs'] as $k=>$v) { ?>
                    <tr>
                        <td><?= $v['name']?> ( <?= $v['code']?> )</td>
                        <td><ul>
                            <li><b>Name</b><br /> <?=  $v['pivot']['prdct_name'] ?></li>
                            <li><b>QR Code</b><br /> <?=  $v['pivot']['prdct_qrcode'] ?></li>
                            <li><b>Picture</b><br /> <?=  $v['pivot']['prdct_picture'] ?></li>
                            <li><b>Video</b><br /> <?=  $v['pivot']['prdct_video'] ?></li>
                            <li><b>QR Image</b><br /> <?=  $v['pivot']['prdct_qrimg'] ?></li>
                            <li><b>Description</b><br /> <?=  $v['pivot']['prdct_description'] ?></li>
                        </td>
                    </tr>
                <?php } 
                } ?>           
            </tbody>
        </table>
        <?php } else {
            echo '<div class="text-center">No Record Found.</div>';
        } ?>
		</div>
	</section>
</div>
@endsection