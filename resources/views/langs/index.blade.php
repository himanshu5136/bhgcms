@extends('layouts.dashboard')

@section('content')
<style type="text/css">
    #configure_langs_listing_form_group{ height: 400px; overflow-y: scroll; }
    #configure_langs_listing_form_group .form-item{display: flex;}
    #configure_langs_listing_form_group .form-item input {width: auto; height: auto; margin-right: 5px;}
    #configure_langs_listing_form_group .form-item label{font-weight: normal;}
    span#enabled_langs{background: #CBF6CC;display: block;padding: 5px;}
    span#disabled_langs{background: #F6D5CB;display: block;padding: 5px;}
</style>
<?php 
$base_url  = URL::to('/');
?>
<div class="page-heading"><h3>Language Configuration</h3></div>
@include('flash-message')
<div class="col-md-8 col-md-offset-2 langs-create-lang langs form large-9 medium-8 columns content">
	<section class="panel">
		<div class="panel-body">
            <form method="POST" action="{{ route('langs.store') }}">
            @csrf
                <div class="row">
                    <div class="col-lg-6 col-lg-offset-3">
                        <div id="form-field-langs" class="form-group form-checkbox-wrapper">
                            <label class="required">World's All Languages</label><br />
                            <div id="configure_langs_listing_form_group" class="form-control">
                                <span id="enabled_langs">Enabled Languages</span>
                                <?php foreach($langs['on'] as $k=>$v) { ?>
                                    <div class="form-item">
                                    <input 
                                    <?= ($v['is_enable'] == '1')?'checked':''?> 
                                    <?= ($v['is_freeze'] == '1')?'disabled':''?> 
                                    id="lang_<?= $v['id'] ?>" type="checkbox" class="form-control @error('lang') is-invalid @enderror" name="lang[]" value="<?= $v['id'] ?>" autocomplete="lang">
                                    <label for="lang_<?= $v['id'] ?>"><?= $v['name'] ?> ( <?= $v['code'] ?> )</label>
                                    </div>
                                <?php } ?>
                                <span id="disabled_langs">Disabled Languages</span>
                                <?php foreach($langs['off'] as $k=>$v) { ?>
                                    <div class="form-item">
                                    <input 
                                    <?= ($v['is_enable'] == '1')?'checked':''?> 
                                    <?= ($v['is_freeze'] == '1')?'disabled':''?> 
                                    id="lang_<?= $v['id'] ?>" type="checkbox" class="form-control @error('lang') is-invalid @enderror" name="lang[]" value="<?= $v['id'] ?>" autocomplete="lang">
                                    <label for="lang_<?= $v['id'] ?>"><?= $v['name'] ?> ( <?= $v['code'] ?> )</label>
                                    </div>
                                <?php } ?>
                            </div>
                            <span class="help-text">( Only enabled languages will display in client. )</span>
                            @error('lang')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">{{ __('Save Languages') }}</button>
                        </div>
                    </div>
                </div>
            </form>
		</div>
	</section>
</div>
@endsection