@extends('layouts.dashboard')

@section('content')
<div class="page-heading"><h3>Client Detail</h3></div>
@include('flash-message')
<?php 
$base_url  = URL::to('/');
?>
<style type="text/css">
    .panel-header{ padding: 10px; }
</style>
<div class="col-md-8 col-md-offset-2 client-view-page clients form large-9 medium-8 columns content">
	<section class="panel">
        <div class="panel-header">
            <div class="row action-items">
                <div class="col-md-6 text-left">
                    <a title="Back to Client Listing" href="<?= $base_url ?>/clients" class="btn btn-primary"><i class="fa fa-backward"></i></a>
                </div>
                <div class="col-md-6 text-right">
                    <a title="Edit Client" href="<?= $base_url ?>/clients/<?=  $client['id'] ?>/edit" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                </div>
            </div>
        </div>
		<div class="panel-body">
        <?php if(count($client) > 0) { ?>
        <table class="table table-bordered superusers-view table-striped">
            <thead>
                <tr>
                    <th class="bg-primary" width="50%">KEY</th>
                    <th class="bg-primary" width="50%">VALUE</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Id</td>
                    <td><?=  $client['id'] ?></td>
                </tr>
                <tr>
                    <td>Name</td>
                    <td><?= $client['name'] ?></td>
                </tr>
                <tr>
                    <td>Description</td>
                    <td><?= $client['description'] ?></td>
                </tr>
                <tr>
                    <td>Doctors<br /><span class="help-text">( Associated with this Client )</span></td>
                    <td>
                    <?php 
                    if(count($client['doctors']) > 0 ){
                        echo "<ul>";
                        foreach($client['doctors'] as $k=>$v) { 
                        ?>
                        <li><?= $v['full_name'] ?>&nbsp;&nbsp;<a href="<?= $base_url ?>/doctors/<?= $v['id'] ?>" title="View Doctor"><i class="fa fa-user-md"></i></a></li>
                        <?php 
                        } 
                        echo "</ul>";
                    }
                    ?>
                    
                    </td>
                </tr>
                <tr>
                    <td>Categories<br /><span class="help-text">( Associated with this Client )</span></td>
                    <td>
                    <?php 
                    if(count($client['tags']) > 0 ){
                        echo "<ul>";
                        foreach($client['tags'] as $k=>$v) { 
                        ?>
                        <li><?= $v['name'] ?>&nbsp;&nbsp;<a href="<?= $base_url ?>/tags/<?= $v['id'] ?>" title="View Category"><i class="fa fa-tag"></i></a></li>
                        <?php 
                        } 
                        echo "</ul>";
                    }
                    ?>
                    
                    </td>
                </tr>
                
            </tbody>
        </table>
        <?php } else {
            echo '<div class="text-center">No Record Found.</div>';
        } ?>

		</div>
	</section>
</div>
@endsection