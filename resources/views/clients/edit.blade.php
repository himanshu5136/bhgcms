@extends('layouts.dashboard')

@section('content')
<style type="text/css">
    #create_client_doctor_listing_form_group{ height: 100px; overflow-y: scroll; }
    #create_client_doctor_listing_form_group .form-item{display: flex;}
    #create_client_doctor_listing_form_group .form-item input {width: auto; height: auto; margin-right: 5px;}
    #create_client_doctor_listing_form_group .form-item label{font-weight: normal;}
    #create_client_tag_listing_form_group{ height: 100px; overflow-y: scroll; }
    #create_client_tag_listing_form_group .form-item{display: flex;}
    #create_client_tag_listing_form_group .form-item input {width: auto; height: auto; margin-right: 5px;}
    #create_client_tag_listing_form_group .form-item label{font-weight: normal;}
    /* Language Field */
    #configure_langs_listing_form_group{ height: 100px; overflow-y: scroll; }
    #configure_langs_listing_form_group .form-item{display: flex;}
    #configure_langs_listing_form_group .form-item input {width: auto; height: auto; margin-right: 5px;}
    #configure_langs_listing_form_group .form-item label{font-weight: normal;}
</style>
<div class="page-heading"><h3>Update Client</h3></div>
@include('flash-message')
<div class="col-md-8 col-md-offset-2 clients-create-page clients form large-9 medium-8 columns content">
	<section class="panel">
		<div class="panel-body">
            <form method="post" action="{{ route('clients.update', $client['client']['id']) }}">
            @method('PATCH') 
            @csrf
                <div class="row">
                    <div class="col-lg-6 col-lg-offset-3">
                        <div class="form-group">
                            <label class="required">Client Name</label><br />
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" required autocomplete="name" autofocus value="{{ $client['client']['name'] }}">
                            @error('name')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                        <div id="form-field-langs" class="form-group form-checkbox-wrapper">
                            <label class="required">Client's Languages</label><br />
                            <div id="configure_langs_listing_form_group" class="form-control">
                                <?php foreach($langs as $k=>$v) { ?>
                                    <div class="form-item">
                                    <input 
                                    <?= (in_array($v['id'], $client['langs']))?'checked ':''?>
                                    <?= ($v['is_freeze'] == '1')?'disabled checked ':''?> 
                                    id="lang_<?= $v['id'] ?>" type="checkbox" class="form-control @error('lang') is-invalid @enderror" name="clangs[]" value="<?= $v['id'] ?>">
                                    <label for="lang_<?= $v['id'] ?>"><?= $v['name'] ?> ( <?= $v['code'] ?> )</label>
                                    </div>
                                <?php } ?>
                            </div>
                            <span class="help-text">( Only enabled languages will display in client. )</span>
                            @error('clangs')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class="required" >Description</label><br />
                            <textarea required id="description" name="description" class="form-control @error('description') is-invalid @enderror" cols="5" rows="8">{{ $client['client']['description'] }}</textarea>
                            @error('description')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">{{ __('Update Client') }}</button>
                        </div>
                    </div>
                </div>
            </form>
		</div>
	</section>
</div>
@endsection