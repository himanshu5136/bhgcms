@extends('layouts.dashboard')

@section('content')
<div class="page-heading"><h3>List of Clients</h3></div>
@include('flash-message')
<?php 
$base_url  = URL::to('/');
?>
<style type="text/css">
    table tr td.client-doctor-count-col span{display: block; text-align: center;}
    table tr td.client-tags-count-col span{display: block; text-align: center;}
</style>
<div class="clients form large-9 medium-8 columns content">
	<section class="panel">
        <div class="panel-heading ">
            <div class="row clients_filters_wrapper bg-secondary">
                <form method="get" id="form_clients_filter" >
                    <div class="col-md-3 form-group">
                        <label>Client Name</label><br />
                        <input class="form-control" type="text" name="cnm" id="clients-filter-cnm" value="<?= (!empty($qry['cnm']))?$qry['cnm']:'' ?>" /> 
                    </div>
                    <div class="col-md-2 form-group">
                        <label>Doctor</label><br />
                        <select class="form-control @error('did') is-invalid @enderror" name="did" id="did">
                            <option value="">- Select One -</option>
                            @foreach($doctors as $k=>$v)
                                <option <?php if((!empty($qry['did'])) && ($qry['did'] == $v['id'])){ echo "selected"; } ?> value="{{ $v['id'] }}">{{ $v['full_name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-2 form-group">
                        <label>Category</label><br />
                        <select class="form-control @error('tid') is-invalid @enderror" name="tid" id="tid">
                            <option value="">- Select One -</option>
                            @foreach($tags as $k=>$v)
                                <option <?php if((!empty($qry['tid'])) && ($qry['tid'] == $v['id'])){ echo "selected"; } ?> value="{{ $v['id'] }}">{{ $v['name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-2 form-group">
                        <label>Languages</label><br />
                        <select class="form-control @error('tid') is-invalid @enderror" name="lng" id="lng">
                            <option value="">- Select One -</option>
                            @foreach($langs as $k=>$v)
                                <option <?php if((!empty($qry['lng'])) && ($qry['lng'] == $v['id'])){ echo "selected"; } ?> value="{{ $v['id'] }}">{{ $v['name'] }} ( {{ $v['code'] }} )</option>
                            @endforeach
                        </select>
                    </div>	
                    <div class="col-md-2 form-group">  
                        <label>&nbsp;</label><br />
                        <input type="submit" name="submit" value="Search" class="btn btn-primary" />
                        <a href="<?= $base_url ?>/clients" class="btn btn-secondary">Reset</a>
                    </div>	
                </form>
            </div>	
        </div>
		<div class="panel-body">
        <?php if(count($clients) > 0) { ?>
        <table class="table  table-hover general-table">
            <thead>
                <tr>
                    <th width="8%">Client Id</th>
                    <th width="15%">Client Name</th>
                    <th width="15%">Description</th>
                    <th width="15%">Languages</th>
                    <th width="10%">Doctors</th>
                    <th width="10%">Categories</th>
                    <th width="15%">Created Date</th>
                    <th width="10%">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($clients as $client)
                <tr>
                    <td>{{ $client['id'] }}</td>
                    <td>{{ $client['name'] }}</td>
                    <td> <?php echo substr($client['description'], 0,30); ?>...   </td>
                    <td><ul>
                    <?php foreach($client['langs'] as $k=>$v ) {
                        echo '<li>'.$v['name'].' ( '.$v['code'].' )'.'</li>';
                    }  ?>
                    </ul></td>
                    <td class="client-doctor-count-col">
                        <span><strong>{{ count($client['doctors']) }}</strong></span>
                        <span>
                            <a target="_blank" href="<?= $base_url ?>/doctors?cid=<?= $client['id'] ?>" title="View Doctors under this client">[ view ]</a>
                        </span>
                    </td>
                    <td class="client-tags-count-col">
                        <span><strong>{{ count($client['tags']) }}</strong></span>
                        <span>
                            <a target="_blank" href="<?= $base_url ?>/tags?cid=<?= $client['id'] ?>" title="View Categories under this client">[ view ]</a>
                        </span>
                    </td>
                    <td>{{ date('M d, Y H:i:s',strtotime($client['created_at'])) }}</td>
                    <td class="actions">
                        <a href="<?= $base_url ?>/clients/<?= $client['id'] ?>" title="View Client"><i class="fa fa-search-plus"></i></a>&nbsp;&nbsp;
                        <a href="<?= $base_url ?>/clients/<?= $client['id'] ?>/edit" title="Edit Client"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;
                        <form name="post_client_<?= $client['id'] ?>" style="display:none;" method="post" action="{{ route('clients.destroy', $client['id']) }}" > @method('DELETE')  @csrf</form>

                        <a href="#" onclick="if (confirm(&quot;Are you sure you want to delete client <?= $client['name'] ?>?&quot;)) { document.post_client_<?= $client['id'] ?>.submit(); } event.returnValue = false; return false;" title="Delete Client"><i class="fa fa-trash-o"></i></a>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        <div class="text-center">
            {{ $paginate->links() }}
        </div>
        <?php } else {
            echo '<div class="text-center">No Record Found.</div>';
        } ?>
		</div>
	</section>
</div>
@endsection