@extends('layouts.dashboard')

@section('content')
<div class="page-heading"><h3>List of Blocks</h3></div>
@include('flash-message')
<?php 
$role = auth()->user()->getRole();
$role_segment = ($role == 'admin')?'/admin':'';
$base_url  = URL::to('/').$role_segment;
?>
<style type="text/css">
    table tr td.content-tag-count-col span{display: block; text-align: center;}
</style>
<div class="blocks form large-9 medium-8 columns content">
	<section class="panel">
    <div class="panel-heading ">
            <div class="row blocks_filters_wrapper bg-secondary">
                <form method="get" id="form_blocks_filter" >
                    <div class="col-md-4 form-group">
                        <label>Title</label><br />
                        <input class="form-control" type="text" name="bnm" id="blocks-filter-bnm" value="<?= (!empty($qry['bnm']))?$qry['bnm']:'' ?>" /> 
                    </div>
                    <div class="col-md-4 form-group">
                        <label>Type</label><br />
                        <select class="form-control @error('tid') is-invalid @enderror" name="btp" id="btp">
                            <option value="">- Select One -</option>
                            @foreach( $type as $k=>$v)
                                <option <?php if((!empty($qry['btp'])) && ($qry['btp'] == $k)){ echo "selected"; } ?> value="{{ $k }}">{{ $v }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4 form-group">  
                        <label>&nbsp;</label><br />
                        <input type="submit" name="submit" value="Search" class="btn btn-primary" />
                        <a href="<?= $base_url ?>/blocks" class="btn btn-secondary">Reset</a>
                    </div>	
                </form>
            </div>	
        </div>
		<div class="panel-body">
        <?php if(count($blocks) > 0) { ?>
        <table class="table  table-hover general-table">
            <thead>
                <tr>
                    <th width="10%">Block Id</th>
                    <th width="10%">Title</th>
                    <th width="20%">Description</th>
                    <th width="10%">Type</th>
                    <th width="10%">Disabled</th>
                    <th width="20%">Created Date</th>
                    <th width="10%">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($blocks as $block)
                <tr>
                    <td>{{ $block['id'] }}</td>
                    <td>{{ $block['title'] }}</td>
                    <td>{{ $block['description'] }}</td>
                    <td>{{ $type[$block['type']] }}</td>
                    <td><?= ($block['disabled'] == '1')?'Yes':'No' ?></td>
                    <td>{{ date('M d, Y H:i:s',strtotime($block['created_at'])) }}</td>
                    <td class="actions">
                        <a href="<?= $base_url ?>/blocks/<?= $block['id'] ?>" title="View Block"><i class="fa fa-search-plus"></i></a>&nbsp;&nbsp;
                        <a href="<?= $base_url ?>/blocks/<?= $block['id'] ?>/edit" title="Edit Block"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;
                        <form name="post_block_<?= $block['id'] ?>" style="display:none;" method="post" action="<?= $base_url ?>/blocks/<?= $block['id'] ?>" > @method('DELETE')  @csrf</form>
                        <a href="#" onclick="if (confirm(&quot;Are you sure you want to delete block <?= $block['title'] ?>?&quot;)) { document.post_block_<?= $block['id'] ?>.submit(); } event.returnValue = false; return false;" title="Delete Block"><i class="fa fa-trash-o"></i></a>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        <?php } else {
            echo '<div class="text-center">No Record Found.</div>';
        } ?>
		</div>
	</section>
</div>
@endsection