@extends('layouts.dashboard')

@section('content')
<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>

 <script type="text/javascript">
    $(document).ready(function() {
        // $('.form-display-content, .form-display-location').select2({
        //     placeholder: "Select a option",
        //     initSelection: function(element, callback) {                   
        //     }
        // });
        
        // $(".display_content_field").change(function() {
        //     var display_id = $(this).attr('data-did'); 
        //     var is_this_zip = $(this).find(":selected").attr('data-zip');
        //     if(is_this_zip == 1) {
        //         $('#zipped_content_'+display_id+'_wrapper').show('slow');
        //     } else {
        //         $('#zipped_content_'+display_id+'_wrapper').hide('slow');
        //     }

        // });
        $(".display_content_field").change(function() {
            $('#zipped_content_'+display_id+'_wrapper').hide('slow');
            var display_id = $(this).attr('data-did'); 
            var is_this_zip = $(this).find(":selected").attr('data-zip');
            var selected_content_id = $(this).find(":selected").val();
            if(is_this_zip == 1) {
                var zip_pages = $('input#content-'+selected_content_id+'-zipped-pages').val();
                var jsnData = JSON.parse(zip_pages)
                var cpch = '';  //content_pages_checkboxes_html 
                    $.each(jsnData, function(key,val) {
                        cpch = cpch+'<div class="form-group-inner-checkbox-lineitem">';
                        cpch = cpch+'<input type="checkbox" id="display_'+display_id+'_content_'+selected_content_id+'_page_'+key+'" name="display['+display_id+'][content_page][]" value="'+val+'">';
                        cpch = cpch+'<label for="display_'+display_id+'_content_'+selected_content_id+'_page_'+key+'">'+val+'</label>';
                        cpch = cpch+'</div>';
                    });
                $('#content_'+display_id+'_pages_wrapper').html(cpch);
                $('#zipped_content_'+display_id+'_wrapper').show('slow');
            } else {
                $('#zipped_content_'+display_id+'_wrapper').hide('slow');
            }
        });

        $('#add_display').click(function() {
            $( "div.block_display_wrapper" ).each(function( key, val ) {
                var hide_class = $( this ).hasClass("hide");
                if(hide_class == true) {
                    $(this).removeClass('hide').show(500);
                    return false;
                }
            });
        });
        $('.remove_display').click(function() {
            var did = $(this).attr('data-id');
            $('select#content_'+did+' option[value=""]').prop('selected', true);
            $('select#location_'+did+' option[value=""]').prop('selected', true);
            $('#content_'+did+'_page').val('');
            $('#zipped_content_'+did+'_wrapper').hide('slow');
            $('#block_display_'+did).addClass('hide');
        });
    });
</script>
<style type="text/css">
    #create_content_tag_listing_form_group{ height: 166px; overflow-y: scroll; }
    #create_content_tag_listing_form_group .form-item{display: flex;}
    #create_content_tag_listing_form_group .form-item input {width: auto; height: auto; margin-right: 5px;}
    #create_content_tag_listing_form_group .form-item label{font-weight: normal;}
    .display_heading{border-bottom: 2px solid #000;background: #FFF3C5;padding: 0px;font-size: 14px;margin-bottom: 20px;}
    .display_heading h4.subheading{font-size: 14px;}
    span#add_display i{margin-top: 0px; padding: 10px; cursor: pointer; }
    .block_display_wrapper{margin-bottom:15px !important; margin: 0 15px; border: 1px solid #999; padding: 5px; background: #eee; border-radius: 12px;}
    span.select2{width:100% !important;}
    .remove_display{margin-top: 30px; display: block; font-size: 18px; color: #ea2a2a; cursor: pointer;}
    .form-group-inner{background: #fff;padding: 16px;width: 50%;border: 1px solid #ccc;border-radius: 12px;}
    .form-group-inner label{margin-left: 5px; text-transform: capitalize;}
</style>
<?php
$role = auth()->user()->getRole();
$role_segment = ($role == 'admin')?'/admin':'';
$base_url  = URL::to('/').$role_segment;
$form_action = $base_url.'/blocks/create_video';
//{{ route('contents.store') }}
?>
<div class="page-heading"><h3>Create Video Block</h3></div>
@include('flash-message')
<div class="col-md-8 col-md-offset-2 blocks-create blocks form large-9 medium-8 columns content">
	<section class="panel">
		<div class="panel-body">
            <form method="POST" enctype="multipart/form-data" action="<?= $form_action ?>">
            @csrf
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="required">Block Title</label><br />
                            <input id="block_title" type="text" class="form-control @error('block_title') is-invalid @enderror" name="block_title" value="{{ old('block_title') }}" required autocomplete="block_title" >
                            @error('block_title')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                            <span class="help-text">This Field is only for backend purpose only.</span>
                        </div>
                        <div class="form-group">
                            <label>Upload Video</label><br />
                            <input accept="video/*" id="upload_video" type="file" class="form-control @error('upload_video') is-invalid @enderror" name="upload_video" value="{{ old('upload_video') }}" >
                            @error('upload_video')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                            <span class="help-text">Need to fill one field i.e. upload video OR embed video. </span>
                        </div>
                        <div class="form-group form-checkbox-item">
                            <input id="block_disabled" type="checkbox" class=" @error('block_disabled') is-invalid @enderror" name="block_disabled" value="1" />
                            <label for="block_disabled">Disable this block</label><br />
                            @error('block_disabled')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="required">Block Description</label><br />
                            <input id="block_description" type="text" class="form-control @error('block_description') is-invalid @enderror" name="block_description" value="{{ old('block_description') }}" required autocomplete="block_description">
                            @error('block_description')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                            <span class="help-text">This Field is only for backend purpose only.</span>
                        </div>
                        <div class="form-group">
                            <label>Embed Video Url</label><br />
                            <input id="embed_video" type="text" class="form-control @error('embed_video') is-invalid @enderror" name="embed_video" value="{{ old('embed_video') }}" >
                            @error('embed_video')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                            <span class="help-text">You can add embed video url of youtube, vimeo etc. </span>
                        </div>

                    </div>
                </div>
                <div class="row display_heading">
                    <div class="col-md-11">
                        <h4 class="subheading">Where do you want to display this block : </h4>
                    </div>
                    <div class="col-md-1 text-center">
                    <span id="add_display" title="Add display" ><i class="fa fa-plus" aria-hidden="true"></i></span>
                    </div>
                </div>
                <?php for($i=0; $i<10; $i++){ ?>
                    <div id="block_display_<?= $i ?>" class="row block_display_wrapper <?= ($i == 0)?'show':'hide' ?> ">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Content</label><br />
                                <select class="form-control form-display-content display_content_field" data-did="<?= $i?>" name="display[<?= $i ?>][content]" id="content_<?= $i ?>" >
                                    <option value="">- Select One -</option>
                                    <?php foreach($contents as $k=>$v) { ?>
                                    <option data-zip="<?= $v['is_this_zip'] ?>" value="<?= $v['id'] ?>"><?= $v['title'] ?> <?= ($v['is_this_zip'] == '1')?' [ ZIP ]':'' ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="form-group">
                                <label >Display Location</label><br />
                                <select class="form-control form-display-location" name="display[<?= $i ?>][location]" id="location_<?= $i ?>" >
                                    <option value="">- Select One -</option>
                                    <?php foreach($regions as $k=>$v) { ?>
                                    <option value="<?= $k ?>"><?= $v ?></option>
                                    <?php } ?>
                                </select>
                                <span class="help-text">For application to define location of this block.</span>
                            </div>
                        </div>
                        <div class="col-lg-1">
                        <?php if($i > 0) { ?>
                            <span class="remove_display" data-id="<?= $i ?>" title="Remove display" ><i class="fa fa-minus-circle" aria-hidden="true"></i></span>     
                        <?php } ?>
                        </div>
                        <div class="col-lg-12" id="zipped_content_<?= $i ?>_wrapper" style="display:none;">
                            <div class="form-group">
                                <label>Choose Pages of Zipped Content</label><br />
                                <?php /* <input id="content_<?= $i ?>_page" type="text" name="display[<?= $i ?>][content_page]" class="form-control"> */ ?>
                                <div id="content_<?= $i ?>_pages_wrapper" class="form-group-inner">Here Pages will added</div>
                                <span class="help-text">You have selected content which is having zipped content. Add pages name like page01.html,page02.html,...</span>
                            </div>
                        </div>
                    </div>
                <?php } 
                
                // Set zip files in a hidden field
                foreach($contents as $k=>$v) { 
                    if($v['is_this_zip'] == '1') {  // Is this Zip
                        $zip_files_ary  = [];
                        $zip_files_jsn  = '';
                        $zip_files_ary  = json_decode(base64_decode($v['zip_files'])); 
                        $zip_files_jsn = json_encode($zip_files_ary, JSON_FORCE_OBJECT);
                        echo "<input id='content-".$v['id']."-zipped-pages' type='hidden' value='".$zip_files_jsn."' />";
                    }
                }

                ?>
                <br />
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">{{ __('Save Block') }}</button>
                        </div>
                    </div>
                </div>
            </form>
		</div>
	</section>
</div>
@endsection