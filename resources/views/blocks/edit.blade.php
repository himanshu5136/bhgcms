@extends('layouts.dashboard')

@section('content')
<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
<script src="https://cdn.ckeditor.com/4.15.0/standard/ckeditor.js"></script>

 <script type="text/javascript">
    $(document).ready(function() {  
        CKEDITOR.replace( 'body' );    
        // $(".display_content_field").change(function() {
        //     var display_id = $(this).attr('data-did'); 
        //     var is_this_zip = $(this).find(":selected").attr('data-zip');
        //     if(is_this_zip == 1) {
        //         $('#zipped_content_'+display_id+'_wrapper').show('slow');
        //     } else {
        //         $('#zipped_content_'+display_id+'_wrapper').hide('slow');
        //     }
        // });
        $(".display_content_field").change(function() {
            $('#zipped_content_'+display_id+'_wrapper').hide('slow');
            var display_id = $(this).attr('data-did'); 
            var is_this_zip = $(this).find(":selected").attr('data-zip');
            var selected_content_id = $(this).find(":selected").val();
            if(is_this_zip == 1) {
                var zip_pages = $('input#content-'+selected_content_id+'-zipped-pages').val();
                var jsnData = JSON.parse(zip_pages)
                var cpch = '';  //content_pages_checkboxes_html 
                    $.each(jsnData, function(key,val) {
                        cpch = cpch+'<div class="form-group-inner-checkbox-lineitem">';
                        cpch = cpch+'<input type="checkbox" id="display_'+display_id+'_content_'+selected_content_id+'_page_'+key+'" name="display['+display_id+'][content_page][]" value="'+val+'">';
                        cpch = cpch+'<label for="display_'+display_id+'_content_'+selected_content_id+'_page_'+key+'">'+val+'</label>';
                        cpch = cpch+'</div>';
                    });
                $('#content_'+display_id+'_pages_wrapper').html(cpch);
                $('#zipped_content_'+display_id+'_wrapper').show('slow');
            } else {
                $('#zipped_content_'+display_id+'_wrapper').hide('slow');
            }
        });

        $('#add_display').click(function() {
            $( "div.block_display_wrapper" ).each(function( key, val ) {
                var hide_class = $( this ).hasClass("hide");
                if(hide_class == true) {
                    $(this).removeClass('hide').show(500);
                    return false;
                }
            });
        });
        $('.remove_display').click(function() {
            var did = $(this).attr('data-id');
            $('select#content_'+did+' option[value=""]').prop('selected', true);
            $('select#location_'+did+' option[value=""]').prop('selected', true);
            $('#content_'+did+'_page').val('');
            $('#zipped_content_'+did+'_wrapper').hide('slow');
            $('#block_display_'+did).addClass('hide');
        });

        $("#qrcode_type").change(function() {
            var type = $(this).find(":selected").val();
            $.each([ 'text', 'link', 'content' ], function( index, value ) {
                if(type == value) {
                    $('#qr_code_'+value+'_wrapper').show('slow');
                } else {
                    $('#qr_code_'+value+'_wrapper').hide('slow');
                }
            });
        });
    });
</script>
<style type="text/css">
    #create_content_tag_listing_form_group{ height: 166px; overflow-y: scroll; }
    #create_content_tag_listing_form_group .form-item{display: flex;}
    #create_content_tag_listing_form_group .form-item input {width: auto; height: auto; margin-right: 5px;}
    #create_content_tag_listing_form_group .form-item label{font-weight: normal;}
    .display_heading{border-bottom: 2px solid #000;background: #FFF3C5;padding: 0px;font-size: 14px;margin-bottom: 20px;}
    .display_heading h4.subheading{font-size: 14px;}
    span#add_display i{margin-top: 0px; padding: 10px; cursor: pointer; }
    .block_display_wrapper{margin-bottom:15px !important; margin: 0 15px; border: 1px solid #999; padding: 5px; background: #eee; border-radius: 12px;}
    span.select2{width:100% !important;}
    .remove_display{margin-top: 30px; display: block; font-size: 18px; color: #ea2a2a; cursor: pointer;}
    .blocks-create .panel .panel-body .form-checkbox-item{margin-top:0px;}
    .qrcode_type_data_wrapper{background: #DFF5FF; margin: 10px 20px; border-radius: 20px; padding: 5px; border: 1px solid #ccc;}
    .uploaded_video_wrapper{background: #f3f3f3;width: auto;height: 99px;margin-left: 11px;border: 1px solid #333;}
    .uploaded_video_wrapper .view{text-align: center;height: 50px;}
    .uploaded_video_wrapper .view a{margin-top:10px;}
    .uploaded_video_wrapper .remove{text-align: center;}
    .form-group-inner{background: #fff;padding: 16px;width: 50%;border: 1px solid #ccc;border-radius: 12px;}
    .form-group-inner label{margin-left: 5px; text-transform: capitalize;}
</style>
<?php
$role = auth()->user()->getRole();
$role_segment = ($role == 'admin')?'/admin':'';
$base_url  = URL::to('/').$role_segment;

//{{ route('contents.store') }}
$blockTypeAry = ['advertisement'=>'Advertisement', 'video'=>'Video', 'text'=>'Custom Text', 'qrcode'=>'QRCode'];
$btype = $block['type'];
$form_action = $base_url.'/blocks'.'/'.$block['id'].'/edit_'.$btype;
?>
<div class="page-heading"><h3>Update <?= $blockTypeAry[$btype] ?> Block</h3></div>
@include('flash-message')
<div class="col-md-8 col-md-offset-2 blocks-create blocks form large-9 medium-8 columns content">
	<section class="panel">
		<div class="panel-body">
            <form method="POST" enctype="multipart/form-data" action="<?= $form_action ?>">
            @method('PATCH') 
            @csrf
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="required">Block Title</label><br />
                            <input id="block_title" type="text" class="form-control @error('block_title') is-invalid @enderror" name="block_title" value="{{ $block['title'] }}" required autocomplete="block_title" >
                            @error('block_title')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                            <span class="help-text">This Field is only for backend purpose only.</span>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="required">Block Description</label><br />
                            <input id="block_description" type="text" class="form-control @error('block_description') is-invalid @enderror" name="block_description" value="{{ $block['description'] }}" required autocomplete="block_description">
                            @error('block_description')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                            <span class="help-text">This Field is only for backend purpose only.</span>
                        </div>
                    </div>
                </div>

                <!-- START - Based on block Type -->
                <?php if($btype == 'advertisement' ) { ?>
                    <div class="row">
                        <div class="col-lg-2">
                            <a target="blank" title="View Image" href="<?= $block['data']['ad_image'] ?>">
                                <img src="<?= $block['data']['ad_image'] ?>" width="80px" height="80px" style="border:1px solid #ccc;" />
                            </a>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label >Advertisement Image</label><br />
                                <input accept="image/*" id="advertisement_image" type="file" class="form-control @error('advertisement_image') is-invalid @enderror" name="advertisement_image" value="{{ $block['data']['ad_image'] }}" >
                                @error('advertisement_image')
                                    <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                @enderror
                                <span class="help-text">Upload new image if you want to change</span>
                            </div>
                        </div>
                        <div class="col-lg-4">
                        </div>
                    </div>
                <?php } else if($btype == 'video' ) { ?>
                    <div class="row">
                    <?php if(!empty($block['data']['uploaded_video'])) { ?>
                        <div class="col-lg-2 uploaded_video_wrapper">
                            <input type="hidden" name="upload_video_content" value="<?= $block['data']['uploaded_video'] ?>" />
                            <div class="view"><a target="_blank" class="btn btn-primary" href="<?= $block['data']['uploaded_video'] ?>">View</a></div>
                            <?php /* ?><div class="remove">
                            <input type="checkbox" name="remove_uploaded_video" id="remove_uploaded_video" value="1" /><br /><label for="remove_uploaded_video">Remove it</label>
                            </div><?php */ ?>
                        </div>
                        <div class="col-lg-4">
                    <?php } else { ?>
                        <div class="col-lg-6">
                    <?php } ?>
                            <div class="form-group">
                                <label>Upload Video</label><br />
                                <input accept="video/*" id="upload_video" type="file" class="form-control @error('upload_video') is-invalid @enderror" name="upload_video" value="{{ $block['data']['uploaded_video'] }}" >
                                @error('upload_video')
                                    <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                @enderror
                                <span class="help-text">Need to fill one field i.e. upload video OR embed video. </span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Embed Video Url</label><br />
                                <input id="embed_video" type="text" class="form-control @error('embed_video') is-invalid @enderror" name="embed_video" value="{{ $block['data']['embed_video']  }}" >
                                @error('embed_video')
                                    <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                @enderror
                                <span class="help-text">You can add embed video url of youtube, vimeo etc. </span>
                            </div>
                        </div>
                    </div>

                <?php } else if($btype == 'text' ) { ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label class="required">Text Body</label><br />
                                <textarea required id="body" name="body" class="form-control @error('body') is-invalid @enderror" cols="5" rows="8"><?= $block['data']['text_body'] ?></textarea>
                                @error('body')
                                    <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                <?php } else if($btype == 'qrcode' ) { ?>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="required">QR Code Type</label><br />
                                <select required class="form-control" name="qrcode_type" id="qrcode_type" >
                                    <option value="">- Select One -</option>
                                    <?php foreach($qrcode_types as $k=>$v) { ?>
                                    <option {{ $block['data']['qrcode_type'] == $k ? 'selected' : ''  }} value="<?= $k ?>"><?= $v ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6">
                        </div>
                    </div>
                    <?php
                    $old_type_text_display = ($block['data']['qrcode_type'] == 'text') ? 'block' : 'none';
                    ?>
                    <div class="row qrcode_type_data_wrapper" id="qr_code_text_wrapper" style="display:{{ $old_type_text_display }};">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>QRCode Text</label><br />
                                <input id="qr_code_text" type="text" class="form-control @error('qr_code_text') is-invalid @enderror" name="qr_code_text" value="{{ $block['data']['qrcode_text'] }}" >
                                @error('qr_code_text')
                                    <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                @enderror
                                <br /><span class="help-text">Add text which you want to create as a QRCode in this block.</span>
                            </div>
                        </div>
                    </div>
                    <?php
                    $old_type_link_display = ($block['data']['qrcode_type'] == 'link') ? 'block' : 'none';
                    ?>
                    <div class="row qrcode_type_data_wrapper" id="qr_code_link_wrapper" style="display:{{ $old_type_link_display }};">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>QRCode Link / URL</label><br />
                                <input id="qr_code_link" type="url" class="form-control @error('qr_code_link') is-invalid @enderror" name="qr_code_link" value="{{ $block['data']['qrcode_link'] }}" >
                                @error('qr_code_link')
                                    <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                @enderror
                                <br /><span class="help-text">Add link / url which you want to create as a QRCode in this block.</span>
                            </div>
                        </div>
                    </div>
                    <?php
                    $old_type_content_display = ($block['data']['qrcode_type'] == 'content') ? 'block' : 'none';
                    ?>
                    <div class="row qrcode_type_data_wrapper" id="qr_code_content_wrapper" style="display:{{ $old_type_content_display }};">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>QRCode Content</label><br />
                                <select class="form-control" name="qr_code_content" id="qr_code_content" >
                                    <option value="">- Select One -</option>
                                    <?php foreach($contents as $k=>$v) { ?>
                                    <option <?= ($v['id'] == $block['data']['qrcode_content'])?'selected':'' ?> value="<?= $v['id'] ?>"><?= $v['title'] ?></option>
                                    <?php } ?>
                                </select>
                                @error('qr_code_content')
                                    <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                @enderror
                                <br /><span class="help-text">Choose content ( GUID ) which you want to create as a QRCode in this block.</span>
                            </div>
                        </div>
                    </div>
                    <?php
                    $old_type_upload_display = ($block['data']['qrcode_type'] == 'upload') ? 'block' : 'none';
                    ?>
                    <div class="row qrcode_type_data_wrapper" id="qr_code_upload_wrapper" style="display:{{ $old_type_upload_display }};">
                        <div class="col-lg-2">
                            <img src="<?= $block['data']['qrcode_image'] ?>" width="100px" />
                        </div>
                        <div class="col-lg-10">
                            <div class="form-group">
                                <label>QRCode Image Upload</label><br />
                                <input id="qr_code_upload" type="file" accept="image/*" class="form-control @error('qr_code_upload') is-invalid @enderror" name="qr_code_upload" value="{{ old('qr_code_upload') }}" >
                                @error('qr_code_upload')
                                    <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                @enderror
                                <br /><span class="help-text">Upload QR code image in this block.</span>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <!-- END - Based on block Type -->

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group form-checkbox-item">
                            <input <?= ($block['disabled'] == '1')?'checked':''?> id="block_disabled" type="checkbox" class=" @error('block_disabled') is-invalid @enderror" name="block_disabled" value="1" />
                            <label for="block_disabled">Disable this block</label><br />
                            @error('block_disabled')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="row display_heading">
                    <div class="col-md-11">
                        <h4 class="subheading">Where do you want to display this block : </h4>
                    </div>
                    <div class="col-md-1 text-center">
                    <span id="add_display" title="Add display" ><i class="fa fa-plus" aria-hidden="true"></i></span>
                    </div>
                </div>
                <?php 
                // Set zip files in a hidden field
                $contentJsonAry = [];
                foreach($contents as $k=>$v) { 
                    if($v['is_this_zip'] == '1') {  // Is this Zip
                        $zip_files_ary  = [];
                        $zip_files_jsn  = '';
                        $zip_files_ary  = json_decode(base64_decode($v['zip_files'])); 
                        $zip_files_jsn = json_encode($zip_files_ary, JSON_FORCE_OBJECT);
                        $contentJsonAry[$v['id']] = $zip_files_jsn;
                    }
                }
                
                // Displays
                for($i=0; $i<10; $i++) { 
                    
                    if((isset($block['display'][$i])) && (!empty($block['display'][$i]))) {
                        $content_id         = $block['display'][$i]['id'];
                        $content_pages      = explode(',',$block['display'][$i]['pivot']['content_pages']);
                        $display_location   = $block['display'][$i]['pivot']['display_location'];
                        $visible            = true;
                        $zip_content        = $block['display'][$i]['is_this_zip'];
                        $zip_files          = $block['display'][$i]['zip_files'];
                    } else {
                        $visible = false;
                        $content_id = '';
                        $content_pages = [];
                        $display_location = '';
                        $zip_content    =   '0';
                    }
                    
                    
                    ?>
                    <div id="block_display_<?= $i ?>" class="row block_display_wrapper <?= (($visible==true) || ($i == 0))?'show':'hide' ?> ">
                        <div class="col-lg-6">  
                            <div class="form-group">
                                <label>Content</label><br />
                                <select class="form-control form-display-content display_content_field" data-did="<?= $i?>" name="display[<?= $i ?>][content]" id="content_<?= $i ?>" >
                                    <option value="">- Select One -</option>
                                    <?php foreach($contents as $k=>$v) { ?>
                                    <option <?= ($content_id == $v['id'])?'selected':'' ?> data-zip="<?= $v['is_this_zip'] ?>" value="<?= $v['id'] ?>"><?= $v['title'] ?> <?= ($v['is_this_zip'] == '1')?' [ ZIP ]':'' ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="form-group">
                                <label >Display Location</label><br />
                                <select class="form-control form-display-location" name="display[<?= $i ?>][location]" id="location_<?= $i ?>" >
                                    <option value="">- Select One -</option>
                                    <?php foreach($regions as $k=>$v) { ?>
                                    <option <?= ($display_location == $k)?'selected':'' ?> value="<?= $k ?>"><?= $v ?></option>
                                    <?php } ?>
                                </select>
                                <span class="help-text">For application to define location of this block.</span>
                            </div>
                        </div>
                        <div class="col-lg-1">
                        <?php if($i > 0) { ?>
                            <span class="remove_display" data-id="<?= $i ?>" title="Remove display" ><i class="fa fa-minus-circle" aria-hidden="true"></i></span>     
                        <?php } ?>
                        </div>
                        <div class="col-lg-12" id="zipped_content_<?= $i ?>_wrapper" style="display:<?= ($zip_content == '1')?'block':'none'?>;">
                            <div class="form-group">
                                <label>Choose Pages of Zipped Content</label><br />
                                <?php /* <input value="<?= $content_pages ?>" id="content_<?= $i ?>_page" type="text" name="display[<?= $i ?>][content_page]" class="form-control"> */ ?>
                                <div id="content_<?= $i ?>_pages_wrapper" class="form-group-inner">
                                    <?php
                                    if((isset($contentJsonAry[$content_id])) && (!empty($contentJsonAry[$content_id]))) {
                                        $contentPagesAry = json_decode($contentJsonAry[$content_id]);
                                        foreach($contentPagesAry as $key=>$val){
                                           // echo 'KEY: '.$key.' VAL : '.$val;
                                            echo '<div class="form-group-inner-checkbox-lineitem">';
                                            echo '<input ';
                                            echo (in_array($val,$content_pages))?'checked ':'';
                                            echo 'type="checkbox" id="display_'.$i.'_content_'.$content_id.'_page_'.$key.'" name="display['.$i.'][content_page][]" value="'.$val.'">';
                                            echo '<label for="display_'.$i.'_content_'.$content_id.'_page_'.$key.'">'.$val.'</label>';
                                            echo '</div>';
                                        }
                                    }
                                    ?>
                                </div>
                                <span class="help-text">You have selected content which is having zipped content. Add pages name like page01.html,page02.html,...</span>
                            </div>
                        </div>
                    </div>
                <?php } 
                
                // Define Hidden zip files of pages of content
                foreach($contentJsonAry as $key=>$val){
                    echo "<input id='content-".$key."-zipped-pages' type='hidden' value='".$val."' />";
                }
                ?>
                <br />
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">{{ __('Update Block') }}</button>
                        </div>
                    </div>
                </div>
            </form>
		</div>
	</section>
</div>
@endsection