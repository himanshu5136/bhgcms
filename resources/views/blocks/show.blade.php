@extends('layouts.dashboard')

@section('content')
<?php 
$base_url  = URL::to('/');
$blockType = ['advertisement'=>'Advertisement', 'video'=>'Video', 'text'=>'Custom Text', 'qrcode'=>'QRCode'];

?>

<div class="page-heading"><h3><?= $blockType[$block['type']]?> Block Detail</h3></div>
@include('flash-message')

<style type="text/css">
    .panel-header{ padding: 10px; }
    h5.display_sub_heading{background: #FFF3C5; padding: 8px; margin-bottom: 0; font-weight: bold;}
</style>
<div class="col-md-8 col-md-offset-2 block-view-page blocks form large-9 medium-8 columns content">
	<section class="panel">
        <div class="panel-header">
            <div class="row action-items">
                <div class="col-md-6 text-left">
                    <a title="Back to Blocks Listing" href="<?= $base_url ?>/blocks" class="btn btn-primary"><i class="fa fa-backward"></i></a>
                </div>
                <div class="col-md-6 text-right">
                    <a title="Edit Block" href="<?= $base_url ?>/blocks/<?=  $block['id'] ?>/edit" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                </div>
            </div>
        </div>
		<div class="panel-body">
        <?php if(count($block) > 0) { ?>
        <table class="table table-bordered tags-view table-striped">
            <thead>
                <tr>
                    <th class="bg-primary" width="30%">KEY</th>
                    <th class="bg-primary" width="70%">VALUE</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Id</td>
                    <td><?=  $block['id'] ?></td>
                </tr>
                <tr>
                    <td>Title</td>
                    <td><?= $block['title'] ?></td>
                </tr>
                <tr>
                    <td>Description</td>
                    <td><?=  $block['description'] ?></td>
                </tr>
                <tr>
                    <td>Block Type</td>
                    <td><?=  $block['type'] ?></td>
                </tr>
                <tr>
                    <td>Disabled</td>
                    <td><?=  ($block['disabled'] == '1')?'yes':'No' ?></td>
                </tr>
                <tr>
                    <td>Created at</td>
                    <td><?=  date('Y-m-d H:i:s', strtotime($block['created_at'])) ?></td>
                </tr>
                <tr>
                    <td>Updated at</td>
                    <td><?=  date('Y-m-d H:i:s', strtotime($block['updated_at'])) ?></td>
                </tr>
                <?php if($block['type'] == 'advertisement') { ?>
                    <tr>
                        <td>Advertisement Image</td>
                        <td><?= $block['data']['ad_image'] ?>
                        <br /><br />
                        <img src="<?= $block['data']['ad_image'] ?>" width="200px" />
                        </td>
                    </tr>
                <?php } else if($block['type'] == 'video') { ?>
                    <tr>
                        <td>Uploaded Video Link</td>
                        <td><?= $block['data']['uploaded_video'] ?></td>
                    </tr>
                    <tr>
                        <td>Embed Video Link</td>
                        <td><?= $block['data']['embed_video'] ?></td>
                    </tr>

                <?php } else if($block['type'] == 'text') { ?>
                    <tr>
                        <td>Custom Text</td>
                        <td><?= $block['data']['text_body'] ?></td>
                    </tr>

                <?php } else if($block['type'] == 'qrcode') { ?>
                    <tr>
                        <td>QRCode Text</td>
                        <td><?= $block['data']['qrcode_text'] ?></td>
                    </tr>
                    <tr>
                        <td>QRCode Link</td>
                        <td><?= $block['data']['qrcode_link'] ?></td>
                    </tr>
                    <tr>
                        <td>QRCode Content ( GUID of content )</td>
                        <td><?= $block['data']['qrcode_content'] ?></td>
                    </tr>
                    <tr>
                        <td>QRCode Image</td>
                        <td>
                        <?= $block['data']['qrcode_image'] ?>
                        <br/>
                        <br/>
                        <img src="<?= $block['data']['qrcode_image'] ?>" width="100px" />
                        </td>
                    </tr>
                <?php } ?>
                <tr>
                    <td colspan="2" class="bg-primary text-center" style="background:#428BCA;" >Block Display Detail</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <?php  
                        if(!empty($block['display'])) {
                            foreach($block['display'] as $k=>$v) { 
                        ?>
                            <h5 class="display_sub_heading">Display <?= $k ?></h5>
                            <table class="table table-bordered tags-view ">
                                <tbody>
                                    <tr>
                                        <td width="30%">Content</td>
                                        <td width="70%"><strong>Title :</strong> <?= $v['title'] ?><br/><strong>GUID :</strong> <?= $v['guid'] ?></td>
                                    </tr>
                                    <?php  if((isset($v['pivot']['content_pages'])) && (!empty($v['pivot']['content_pages']))) { 
                                        $pagesAry = explode(',',$v['pivot']['content_pages']);
                                    ?>
                                        <tr>
                                            <td>Content Pages</td>
                                            <td><ul>
                                            <?php foreach($pagesAry as $key=>$val){
                                                echo '<li>'.$val.'</li>';
                                            } ?>
                                            </ul></td>
                                        </tr>
                                    <?php } ?>
                                    <tr>
                                        <td>Location</td>
                                        <td><?= (!empty($v['pivot']['display_location']))?$regions[$v['pivot']['display_location']]:'' ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        <?php } 
                        } else { ?>
                            <tr>
                                <td colspan="2" class="">No display available for this block.</td>
                            </tr>
                        <?php } ?>    
                    </td>
                </tr>


                
                
                
                
            </tbody>
        </table>
        <?php } else {
            echo '<div class="text-center">No Record Found.</div>';
        } ?>

		</div>
	</section>
</div>
@endsection