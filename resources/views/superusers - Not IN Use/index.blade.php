@extends('layouts.dashboard')

@section('content')
<div class="page-heading"><h3>List of Users</h3></div>
@include('flash-message')
<?php 
$statusAry = ['1'=>'Active','2'=>'Blocked', ];
$base_url 		= URL::to('/');
?>

<div class="superusers form large-9 medium-8 columns content">
	<section class="panel">
        <div class="panel-heading ">
                <div class="row users_filters_wrapper bg-secondary">
                    <form method="get" id="form_users_filter" >
                        <div class="col-md-4 form-group">
                            <label>Full Name</label><br />
                            <input class="form-control" type="text" name="fname" id="users-filter-fname" value="<?= (!empty($qry['fname']))?$qry['fname']:'' ?>" /> 
                        </div>	
                        <div class="col-md-4 form-group">
                            <label>Status</label><br />
                            <select class="form-control @error('status') is-invalid @enderror" name="status" id="status">
                                <option value="">- Select One -</option>
                                @foreach($statusAry as $k=>$v)
                                    <option 
                                    <?php if((!empty($qry['status'])) && ($qry['status'] == $k)){ echo "selected"; }  ?>                                    
                                    value="{{ $k }}">{{ $v }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-4 form-group">  
                            <label>&nbsp;</label><br />
                            <input type="submit" name="submit" value="Search" class="btn btn-primary" />
                        </div>	
                    </form>
                </div>	
        </div>
		<div class="panel-body">
        <?php if(count($users) > 0) { ?>
        <table class="table  table-hover general-table">
            <thead>
                <tr>
                    <th>Username</th>
                    <th>Full Name</th>
                    <th>Email</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                <tr>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->full_name }}</td>
                    <td>{{ $user->email }}</td>
                    <td><?= $statusAry[$user->status] ?></td>
                    <td class="actions">
                        <a href="<?= $base_url ?>/user/<?= $user->id ?>" title="View User"><i class="fa fa-search-plus"></i></a>&nbsp;&nbsp;
                        <a href="<?= $base_url ?>/user/<?= $user->id ?>/edit" title="Edit User"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;
                        <form name="post_user_<?= $user->id ?>" style="display:none;" method="post" action="<?= $base_url ?>/user/<?= $user->id ?>/delete">  @csrf <input type="hidden" name="_method" value="POST"></form>

                        <a href="#" onclick="if (confirm(&quot;Are you sure you want to delete user <?= $user->name ?>?&quot;)) { document.post_user_<?= $user->id ?>.submit(); } event.returnValue = false; return false;" title="Delete User"><i class="fa fa-trash-o"></i></a>

                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        <?php } else {
            echo '<div class="text-center">No Record Found.</div>';
        } ?>
		</div>
	</section>
</div>
@endsection