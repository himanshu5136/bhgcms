@extends('layouts.dashboard')

@section('content')
<div class="page-heading"><h3>Edit User</h3></div>
@include('flash-message')
<?php $base_url  = URL::to('/'); ?>

<div class="col-md-8 col-md-offset-2 superusers-view-page form large-9 medium-8 columns content">
	<section class="panel">
        <div class="panel-header">
            <div class="row action-items">
                <div class="col-md-6 text-left">
                    <a href="<?= $base_url ?>/users" class="btn btn-primary">Back</a>
                </div>
                <div class="col-md-6 text-right">
                    <a href="<?= $base_url ?>/user/<?=  $user->id ?>" class="btn btn-primary">View User</a>
                </div>
            </div>
        </div>
		<div class="panel-body">
            <form method="POST">
            @csrf
  
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="required">Full Name</label><br />
                            <input id="full_name" type="text" class="form-control @error('full_name') is-invalid @enderror" name="full_name" required autocomplete="full_name" autofocus value="{{ $user->full_name }}">
                            @error('full_name')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>Status</label><br />
                            <select class="form-control @error('status') is-invalid @enderror" name="status" id="status" required>
                                <option <?= ($user->status == '1')?'selected':'' ?> value="1">Active</option>
                                <option <?= ($user->status == '2')?'selected':'' ?> value="2">Blocked</option>
                            </select>
                            @error('status')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label >Contact Number</label><br />
                            <input id="contact_number" type="text" class="form-control @error('contact_number') is-invalid @enderror" name="contact_number" autocomplete="contact_number" value="{{ $user->contact_number }}" >
                            @error('full_name')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>User Brief</label><br />
                            <textarea  id="user_brief" name="user_brief" class="form-control @error('user_brief') is-invalid @enderror" cols="5" rows="9">{{ $user->user_brief }}</textarea>
                            @error('user_brief')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                            
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="required">User Role</label><br />
                            <select class="form-control @error('role') is-invalid @enderror" name="role" id="user-roles" required autocomplete="role">
                                <?php foreach($roles as $k=>$v) { ?>
                                <option {{ old('role')==$k ? 'selected' : ''  }} value="<?= $k ?>"><?= $v ?></option>
                                <?php } ?>
                            </select>
                            @error('role')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    
                    </div>
                    <div class="col-lg-6">
                        @php
                            $old_role_display = (old('role') == '3') ? 'display_block' : 'display_none';
                        @endphp
                        <div id="form-field-doctors" class="form-group {{ $old_role_display }}">
                            <label>Doctor</label><br />
                            <select class="form-control @error('doctor') is-invalid @enderror" name="doctor" id="doctor">
                                <?php foreach($doctors as $k=>$v) { ?>
                                <option value="<?= $k ?>"><?= $v ?></option>
                                <?php } ?>
                            </select>
                            <span class="help-text">( Choose a doctor profile for "Doctor" User Role )</span>
                            @error('doctor')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                        @php
                            $old_role_client_display = (old('role') == '2') ? 'display_block' : 'display_none';
                        @endphp
                        <div id="form-field-clients" class="form-checkbox-wrapper  {{ $old_role_client_display }}">
                            <label>Clients</label><br />
                            <div id="create_user_client_listing_form_group" class="form-control">
                                <?php foreach($clients as $k=>$v) { ?>
                                    <div class="form-item">
                                    <input id="client_<?= $k ?>" type="checkbox" class="form-control @error('client') is-invalid @enderror" name="client[]" value="<?= $k ?>" autocomplete="client">
                                    <label for="client_<?= $k ?>"><?= $v ?></label>
                                    </div>
                                <?php } ?>
                            </div>
                            <span class="help-text">( Choose minimum one client for "Admin" User Role )</span>
                            @error('client')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">{{ __('Update User') }}</button>
                        </div>
                    </div>
                </div>
            </form>
		</div>
	</section>
</div>
@endsection