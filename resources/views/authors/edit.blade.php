@extends('layouts.dashboard')

@section('content')
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style type="text/css">
 .file-left-side{width: 76%;float: left;margin-right: 10px;}
 .file-right-side{width: 20%;float: left;text-align: center;background: #eee;height: 70px;}
 .file-right-side img{max-width: 60px; min-height: 60px; margin-top: 4px;}
 .ui-tabs-vertical { width: 55em; }
.ui-tabs-vertical .ui-tabs-nav { overflow-y: scroll; max-height: 460px; padding: .2em .1em .2em .2em; float: left; width: 12em; min-height: 460px; overflow-y: scroll;}
.ui-tabs-vertical .ui-tabs-nav li { clear: left; width: 100%; border-bottom-width: 1px !important; border-right-width: 0 !important; margin: 0 -1px .2em 0; }
.ui-tabs-vertical .ui-tabs-nav li a { display:block; }
.ui-tabs-vertical .ui-tabs-nav li.ui-tabs-active { padding-bottom: 0; padding-right: .1em; border-right-width: 1px; }
.ui-tabs-vertical .ui-tabs-panel { padding: 1em; float: right; width: 40em;}
body {font-family: Arial, Helvetica, sans-serif;}
h4.lang-subheading{margin-top: 0px;border-bottom: 2px solid #333;font-weight: bold;background: #F4EAC3;padding: 5px;margin-bottom: 16px;}
table {font-size: 1em;} 
.ui-draggable, .ui-droppable {background-position: top;}
</style> 
<div class="page-heading"><h3>Update Author</h3></div> 
@include('flash-message')
<div class="col-md-8 col-md-offset-2 authors-create-page authors form large-9 medium-8 columns content">
	<section class="panel">
		<div class="panel-body">
            <form method="post" enctype="multipart/form-data" action="{{ route('authors.update', $author['id']) }}">
            @method('PATCH')
            @csrf
                <div class="row">
                    <div class="col-lg-12 ">
                    <div id="tabs">
                        <!-- TABS Labels-->
                        <ul>
                            <?php foreach($langs as $key=>$val) {  ?>
                                <li><a href="#lang-<?= $val['code'] ?>-<?= $val['id'] ?>"><?= $val['name'].' ( '.$val['code'].' )'?></a></li>
                            <?php }  ?>
                        </ul>
                        <!-- TABS Wrappers-->
                        <?php foreach($langs as $key=>$val) {  
                            $reqField = ($val['id'] == '1')?'required':''; // Fields can be required only for english language.
                            $author_name_value = ((!empty($author['langs'][$val['id']])) && (isset($author['langs'][$val['id']])))?$author['langs'][$val['id']]['athr_full_name']:'';//$author['full_name'];

                            $author_title_value = ((!empty($author['langs'][$val['id']])) && (isset($author['langs'][$val['id']])))?$author['langs'][$val['id']]['athr_title']:'';//$author['title'];

                            $author_picture_value = ((!empty($author['langs'][$val['id']])) && (isset($author['langs'][$val['id']])))?$author['langs'][$val['id']]['athr_picture']:'';//$author['picture'];

                            $author_bio_value = ((!empty($author['langs'][$val['id']])) && (isset($author['langs'][$val['id']])))?$author['langs'][$val['id']]['athr_bio']:'';//$author['bio'];
                            ?>
                            <div id="lang-<?= $val['code'] ?>-<?= $val['id'] ?>">
                                <h4 class="lang-subheading" ><?= $val['name'] ?> Language </h4>
                                <div class="row">
                                    <div class="col-lg-8 ">                                       
                                        <div class="form-group">
                                            <label class="<?= $reqField ?>">(<?= $val['code'] ?> ) Author Name</label><br />
                                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="langdata[<?= $val['id'] ?>][authr_name]" value="{{ $author_name_value }}" <?= $reqField ?> >
                                            @error('name')
                                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label class="<?= $reqField ?>">(<?= $val['code'] ?> ) Author Title</label><br />
                                            <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="langdata[<?= $val['id'] ?>][authr_title]" value="{{ $author_title_value }}" <?= $reqField ?>>
                                            @error('title')
                                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group" style="display: flex;">
                                            <span class="file-left-side">
                                                <label >(<?= $val['code'] ?> ) Author Picture</label><br />
                                                <input accept="image/*"  id="picture" type="file" class="form-control @error('picture') is-invalid @enderror" name="langdata_<?= $val['id'] ?>_authr_picture" value="{{ old('picture') }}">
                                                <div class=""><input value="1" type="checkbox" name="langdata_<?= $val['id'] ?>_remove_picture" id="langdata_<?= $val['id'] ?>_remove_picture"/><label for="langdata_<?= $val['id'] ?>_remove_picture">&nbsp;Remove Picture</label></div>
                                            </span>
                                            <span class="file-right-side">
                                                <?php if(!empty($author_picture_value)) { ?>
                                                <img src="{{ $author_picture_value }}" />
                                                <?php } else { echo 'No <br />Image'; } ?>
                                            </span>
                                            @error('picture')
                                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label class="<?= $reqField ?>">(<?= $val['code'] ?> ) Description</label><br />
                                            <textarea  id="bio" <?= $reqField ?> name="langdata[<?= $val['id'] ?>][authr_bio]" class="form-control @error('bio') is-invalid @enderror" cols="5" rows="4">{{ $author_bio_value }}</textarea>
                                            @error('bio')
                                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        </div>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">{{ __('Update Author') }}</button>
                        </div>
                    </div>
                </div>
            </form>
		</div>
	</section>
</div>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
@endsection
