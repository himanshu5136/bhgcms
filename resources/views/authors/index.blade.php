@extends('layouts.dashboard')

@section('content')
<div class="page-heading"><h3>List of Authors</h3></div>
@include('flash-message')
<?php 
$base_url  = URL::to('/');
?>
<style type="text/css">
</style>
<div class="authors form large-9 medium-8 columns author">
	<section class="panel">
        <div class="panel-heading ">
            <div class="row authors_filters_wrapper bg-secondary">
                <form method="get" id="form_authors_filter" >
                    <div class="col-md-3 form-group">
                        <label>Name</label><br />
                        <input class="form-control" type="text" name="anm" id="authors-filter-anm" value="<?= (!empty($qry['anm']))?$qry['anm']:'' ?>" /> 
                    </div>
                    <div class="col-md-3 form-group">
                        <label>Title</label><br />
                        <input class="form-control" type="text" name="atl" id="authors-filter-atl" value="<?= (!empty($qry['atl']))?$qry['atl']:'' ?>" /> 
                    </div>
                    <div class="col-md-3 form-group">
                        <label>&nbsp;</label><br />
                        <input type="submit" name="submit" value="Search" class="btn btn-primary" />
                        <a href="<?= $base_url ?>/contents/authors" class="btn btn-secondary">Reset</a>
                    </div>	
                </form>
            </div>	
        </div>
		<div class="panel-body">
        <?php if(count($authors) > 0) { ?>
            <table class="table  table-hover general-table">
                <thead>
                    <tr>
                        <th width="10%">Author Id</th>
                        <th width="20%">Name ( English )</th>
                        <th width="10%">Title ( English )</th>
                        <th width="20%">Created Date</th>
                        <th width="10%">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($authors as $author)
                    <tr>
                        <td>{{ $author['id'] }}</td>
                        <td>{{ $author['langs']['1']['athr_full_name'] }}</td>
                        <td>{{ $author['langs']['1']['athr_title'] }}</td>
                        <td>{{ date('M d, Y H:i:s',strtotime($author['created_at'])) }}</td>
                        <td class="actions">
                            <a href="<?= $base_url ?>/contents/authors/<?= $author['id'] ?>" title="View Author"><i class="fa fa-search-plus"></i></a>&nbsp;&nbsp;
                            <a href="<?= $base_url ?>/contents/authors/<?= $author['id'] ?>/edit" title="Edit Author"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;
                        
                            <form name="post_author_<?= $author['id'] ?>" style="display:none;" method="post" action="<?= $base_url ?>/contents/authors/<?= $author['id'] ?>" > @method('DELETE')  @csrf</form>
                            <a href="#" onclick="if (confirm(&quot;Are you sure you want to delete author <?= $author['full_name'] ?>?&quot;)) { document.post_author_<?= $author['id'] ?>.submit(); } event.returnValue = false; return false;" title="Delete Author"><i class="fa fa-trash-o"></i></a>
                        </td>
                    </tr>
                    @endforeach
                <tbody>
            </table>
            <div class="text-center">
                {{ $paginate->links() }}
            </div>
        <?php } else {
            echo '<div class="text-center">No Record Found.</div>';
        } ?>
		</div>
	</section>
</div>
@endsection