@extends('layouts.dashboard')

@section('content')
<div class="page-heading"><h3>Author Detail</h3></div>
@include('flash-message')
<?php 
$base_url  = URL::to('/');
?>
<style type="text/css">
    .panel-header{ padding: 10px; }
</style>
<div class="col-md-8 col-md-offset-2 author-view-page authors form large-9 medium-8 columns content">
	<section class="panel">
        <div class="panel-header">
            <div class="row action-items">
                <div class="col-md-6 text-left">
                    <a title="Back to Auhtors Listing" href="<?= $base_url ?>/contents/authors" class="btn btn-primary"><i class="fa fa-backward"></i></a>
                </div>
                <div class="col-md-6 text-right">
                    <a title="Edit Author" href="<?= $base_url ?>/contents/authors/<?=  $author['id'] ?>/edit" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                </div>
            </div>
        </div>
		<div class="panel-body">
        <?php if(count($author) > 0) { ?>
        <table class="table table-bordered superusers-view table-striped">
            <thead>
                <tr>
                    <th class="bg-primary" width="50%">KEY</th>
                    <th class="bg-primary" width="50%">VALUE</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Id</td>
                    <td><?=  $author['id'] ?></td>
                </tr> 
                <tr>
                    <td colspan="2" class="text-center"><b>Language Based Data</b></td>
                </tr>
                <?php 
                if(!empty($author['langs'])) {
                foreach($author['langs'] as $k=>$v) { ?>
                    <tr>
                        <td><?= $v['name']?> ( <?= $v['code']?> )</td>
                        <td><ul>
                            <li><b>Full Name</b><br /> <?=  $v['pivot']['athr_full_name'] ?></li>
                            <li><b>Title</b><br /> <?=  $v['pivot']['athr_title'] ?></li>
                            <li><b>Picture</b><br /> <?=  $v['pivot']['athr_picture'] ?></li>
                            <li><b>Bio</b><br /> <?=  $v['pivot']['athr_bio'] ?></li>
                        </td>
                    </tr>
                <?php } 
                } ?>            
            </tbody>
        </table>
        <?php } else {
            echo '<div class="text-center">No Record Found.</div>';
        } ?>
		</div>
	</section>
</div>
@endsection