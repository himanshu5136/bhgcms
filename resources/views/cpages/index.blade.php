@extends('layouts.dashboard')

@section('content')
<div class="page-heading"><h3>List of Client Pages</h3></div>
@include('flash-message')
<?php 
$base_url  = URL::to('/');
?>
<style type="text/css">
</style>
<div class="cpages form large-9 medium-8 columns cpage">
	<section class="panel">
        <div class="panel-heading ">
            <div class="row cpages_filters_wrapper bg-secondary">
                <form method="get" id="form_cpages_filter" >
                    <div class="col-md-3 form-group">
                        <label>Name</label><br />
                        <input class="form-control" type="text" name="pnm" id="cpages-filter-pnm" value="<?= (!empty($qry['pnm']))?$qry['pnm']:'' ?>" /> 
                    </div>
                    <div class="col-md-3 form-group">
                        <label>Title</label><br />
                        <input class="form-control" type="text" name="ttl" id="cpages-filter-ttl" value="<?= (!empty($qry['ttl']))?$qry['ttl']:'' ?>" /> 
                    </div>
                    <div class="col-md-2 form-group">
                        <label>Page Type</label><br />
                        <select class="form-control @error('pty') is-invalid @enderror" name="pty" id="pty">
                            <option value="">- Select One -</option>
                            @foreach( $ptypes as $k=>$v)
                                <option <?php if((!empty($qry['pty'])) && ($qry['pty'] == $k)){ echo "selected"; } ?> value="{{ $k }}">{{ $v }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-2 form-group">
                        <label>Clients</label><br />
                        <select class="form-control @error('clt') is-invalid @enderror" name="clt" id="clt">
                            <option value="">- Select One -</option>
                            @foreach( $clients as $k=>$v)
                                <option <?php if((!empty($qry['clt'])) && ($qry['clt'] == $k)){ echo "selected"; } ?> value="{{ $k }}">{{ $v }}</option>
                            @endforeach
                        </select>
                    </div>	
                    <div class="col-md-2 form-group">
                        <label>&nbsp;</label><br />
                        <input type="submit" name="submit" value="Search" class="btn btn-primary" />
                        <a href="<?= $base_url ?>/cpages" class="btn btn-secondary">Reset</a>
                    </div>	
                </form>
            </div>	
        </div>
		<div class="panel-body">
        <?php if(count($cpages) > 0) { ?>
            <table class="table  table-hover general-table">
                <thead>
                    <tr>
                        <th width="10%">Page Id</th>
                        <th width="15%">Name</th>
                        <th width="15%">Title ( English )</th>
                        <th width="10%">Page Type</th>
                        <th width="25%">Clients</th>
                        <th width="15%">Created Date</th>
                        <th width="10%">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($cpages as $cpage)
                    <tr>
                        <td>{{ $cpage['id'] }}</td>
                        <td>{{ $cpage['name'] }}</td>
                        <td>{{ $cpage['langs']['1']['cpage_title'] }}</td>
                        <td>{{ $ptypes[$cpage['ptype']] }}</td>
                        <td><ul>
                            <?php 
                            foreach($cpage['clients'] as $k=>$v){
                                echo '<li><a href="'.$base_url.'/clients'.'/'.$v['id'].'" target="_blank">'.$v['name'].'</a></li>'; 
                            }
                            ?>
                        </ul></td>
                        <td>{{ date('M d, Y H:i:s',strtotime($cpage['created_at'])) }}</td>
                        <td class="actions">
                            <a href="<?= $base_url ?>/cpages/<?= $cpage['id'] ?>" title="View Page"><i class="fa fa-search-plus"></i></a>&nbsp;&nbsp;
                            <a href="<?= $base_url ?>/cpages/<?= $cpage['id'] ?>/edit" title="Edit Page"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;
                        
                            <form name="post_cpage_<?= $cpage['id'] ?>" style="display:none;" method="post" action="<?= $base_url ?>/cpages/<?= $cpage['id'] ?>" > @method('DELETE')  @csrf</form>
                            <a href="#" onclick="if (confirm(&quot;Are you sure you want to delete page <?= $cpage['name'] ?>?&quot;)) { document.post_cpage_<?= $cpage['id'] ?>.submit(); } event.returnValue = false; return false;" title="Delete Page"><i class="fa fa-trash-o"></i></a>
                        </td>
                    </tr>
                    @endforeach
                <tbody>
            </table>
            <div class="text-center">
                {{ $paginate->links() }}
            </div>
        <?php } else {
            echo '<div class="text-center">No Record Found.</div>';
        } ?>
		</div>
	</section>
</div>
@endsection