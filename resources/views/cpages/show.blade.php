@extends('layouts.dashboard')

@section('content')
<div class="page-heading"><h3>Client Page Detail</h3></div>
@include('flash-message')
<?php 
$base_url  = URL::to('/');
?>
<style type="text/css">
    .panel-header{ padding: 10px; }
</style>
<div class="col-md-8 col-md-offset-2 cpage-view-page cpages form large-9 medium-8 columns content">
	<section class="panel">
        <div class="panel-header">
            <div class="row action-items">
                <div class="col-md-6 text-left">
                    <a title="Back to Pages Listing" href="<?= $base_url ?>/cpages" class="btn btn-primary"><i class="fa fa-backward"></i></a>
                </div>
                <div class="col-md-6 text-right">
                    <a title="Edit Page" href="<?= $base_url ?>/cpages/<?=  $cpage['id'] ?>/edit" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                </div> 
            </div>
        </div>
		<div class="panel-body">
        <?php if(count($cpage) > 0) { ?>
        <table class="table table-bordered superusers-view table-striped">
            <thead>
                <tr>
                    <th class="bg-primary" width="50%">KEY</th>
                    <th class="bg-primary" width="50%">VALUE</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Id</td>
                    <td><?=  $cpage['id'] ?></td>
                </tr>
                <tr>
                    <td>Name</td>
                    <td><?= $cpage['name'] ?></td>
                </tr>
                <tr>
                    <td>Page Type</td>
                    <td><?= $ptypes[$cpage['ptype']] ?></td>
                </tr>
                <tr>
                    <td>Clients</td>
                    <td><ul>
                        <?php 
                        foreach($cpage['clients'] as $k=>$v) {
                            echo '<li><a href="'.$base_url.'/clients'.'/'.$v['id'].'" target="_blank">'.$v['name'].'</a></li>'; 
                        }
                        ?>
                    </ul></td>
                </tr> 
                <tr>
                    <td>Sections</td>
                    <td><ul>
                        <?php 
                        foreach($cpage['sections'] as $k=>$v){
                            echo '<li><a href="'.$base_url.'/clients'.'/'.$v['id'].'" target="_blank">'.$v['name'].'( '.$v['title'].' )</a></li>'; 
                        }
                        ?>
                    </ul></td>
                </tr>
                <tr>
                    <td colspan="2" class="text-center"><b>Language Based Data</b></td>
                </tr>
                <?php 
                if(!empty($cpage['langs'])) {
                foreach($cpage['langs'] as $k=>$v) { ?>
                    <tr>
                        <td><?= $v['name']?> ( <?= $v['code']?> )</td>
                        <td><ul>
                            <li><b>Title</b><br /> <?=  $v['pivot']['cpage_title'] ?></li>
                            <li><b>Description</b><br /> <?=  $v['pivot']['cpage_description'] ?></li>
                        </td>
                    </tr>
                <?php } 
                } ?>
            </tbody>
        </table>
        <?php } else {
            echo '<div class="text-center">No Record Found.</div>';
        } ?>
		</div>
	</section>
</div>
@endsection