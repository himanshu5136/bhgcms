    <!-- Include Backend Theme JS Files -->
    <script src="{{ asset('backend/js/jquery-1.10.2.min.js') }}" defer></script>
    <script src="{{ asset('backend/js/jquery-ui-1.9.2.custom.min.js') }}" defer></script>
    <script src="{{ asset('backend/js/jquery-migrate-1.2.1.min.js') }}" defer></script>
    <script src="{{ asset('backend/js/bootstrap.min.js') }}" defer></script>
    <script src="{{ asset('backend/js/modernizr.min.js') }}" defer></script>
    <script src="{{ asset('backend/js/jquery.nicescroll.js') }}" defer></script>
    <!-- Select2 -->
    <script src="{{ asset('backend/js/select2.min.js') }}" defer></script>
    <!--easy pie chart-->
    <script src="{{ asset('backend/js/easypiechart/jquery.easypiechart.js') }}" defer></script>
    <script src="{{ asset('backend/js/easypiechart/easypiechart-init.js') }}" defer></script>
    <!--Sparkline Chart-->
    <script src="{{ asset('backend/js/sparkline/jquery.sparkline.js') }}" defer></script>
    <script src="{{ asset('backend/js/sparkline/sparkline-init.js') }}" defer></script>
    <!--icheck -->
    <script src="{{ asset('backend/js/iCheck/jquery.icheck.js') }}" defer></script>
    <script src="{{ asset('backend/js/icheck-init.js') }}" defer></script>
    <!-- jQuery Flot Chart-->
    <script src="{{ asset('backend/js/flot-chart/jquery.flot.js') }}" defer></script>
    <script src="{{ asset('backend/js/flot-chart/jquery.flot.tooltip.js') }}" defer></script>
    <script src="{{ asset('backend/js/flot-chart/jquery.flot.resize.js') }}" defer></script>

    <!--common scripts for all pages-->
    <script src="{{ asset('backend/js/scripts.js') }}" defer></script>
    <!--Dashboard Charts-->
    <script src="{{ asset('backend/js/dashboard-chart-init.js') }}" defer></script>