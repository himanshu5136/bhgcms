<?php
$user 	 		 = auth()->user(); // Current User Array
$role 	 		 = auth()->user()->getRole(); // Current User Role Slug
$profile 		 = auth()->user()->getProfile(); // Current User Profile Array
$clients 		 = auth()->user()->getClients(); // Current User Clients Array
$user_full_name  = $profile['full_name'];
$user_last_login = $user->last_login_at;

// Fetch Current Path
$current_path = Request::path();
if(strpos($current_path, '/') !== false) {
	$cpAry 		= explode('/',$current_path);
	$controller = $cpAry[0];
	$action 	= $cpAry[1];
} else {
	$controller = $current_path;
	$action		= 'index';
}
// Base URL
$base_url 		= URL::to('/');
$webroot_path 	= $base_url.'/';
// Menu Array
$main_menu = array();
$main_menu['home']['url'] 								= "home";
$main_menu['home']['icon'] 								= "fa-home";
$main_menu['home']['text'] 								= "Dashboard";
$main_menu['home']['sub-menu'] 							= array();
if($role == 'superadmin') {  // Superadmin Role
	$main_menu['home']['url'] 								= "home";
	$main_menu['home']['icon'] 								= "fa-dashboard";
	$main_menu['home']['text'] 								= "Dashboard";
	$main_menu['home']['sub-menu'] 							= array();
	$main_menu['users']['url'] 								= "";
	$main_menu['users']['icon'] 							= "fa-users";
	$main_menu['users']['text'] 							= "System Users";
	$main_menu['users']['sub-menu']['index']['url'] 		= 'users';
	$main_menu['users']['sub-menu']['index']['text'] 		= 'List of users';
	$main_menu['users']['sub-menu']['create']['url'] 		= 'users/create';
	$main_menu['users']['sub-menu']['create']['text'] 		= 'Add User - Admin'; 
	$main_menu['users']['sub-menu']['createdoc']['url'] 	= 'users/createdoc';
	$main_menu['users']['sub-menu']['createdoc']['text'] 	= 'Add User - Doctor'; 
	$main_menu['users']['sub-menu']['roles']['url'] 		= 'users/roles';
	$main_menu['users']['sub-menu']['roles']['text'] 		= 'User Roles';
	$main_menu['users']['sub-menu']['stypes']['url'] 		= 'users/stypes';
	$main_menu['users']['sub-menu']['stypes']['text'] 		= '<b>List of Service Types</b>';
	$main_menu['users']['sub-menu']['stypes_create']['url'] 		= 'users/stypes/create';
	$main_menu['users']['sub-menu']['stypes_create']['text'] 		= '<i class="fa fa-plus"></i>Add Service Type'; 

	// $main_menu['users']['sub-menu']['permissions']['url'] 	= 'users/permissions';
	// $main_menu['users']['sub-menu']['permissions']['text'] 	= 'Set Permissions'; 
	$main_menu['clients']['url'] 							= "";
	$main_menu['clients']['icon'] 							= "fa-laptop";
	$main_menu['clients']['text'] 							= "Clients";
	$main_menu['clients']['sub-menu']['index']['url'] 		= 'clients';
	$main_menu['clients']['sub-menu']['index']['text'] 		= '<b>List of clients</b>';
	$main_menu['clients']['sub-menu']['create']['url'] 		= 'clients/create';
	$main_menu['clients']['sub-menu']['create']['text'] 	= 'Add New Client';
	$main_menu['cpages']['url'] 							= "";
	$main_menu['cpages']['icon'] 							= "fa-file-text";
	$main_menu['cpages']['text'] 							= "Client Pages";
	$main_menu['cpages']['sub-menu']['index']['url'] 		= 'cpages';
	$main_menu['cpages']['sub-menu']['index']['text'] 		= '<b>List of client pages</b>';
	$main_menu['cpages']['sub-menu']['create']['url'] 		= 'cpages/create';
	$main_menu['cpages']['sub-menu']['create']['text'] 		= '<i class="fa fa-plus"></i>Add client Page'; 
	// Content - Sections
	$main_menu['cpages']['sub-menu']['csections']['url'] 			= 'cpages/csections';
	$main_menu['cpages']['sub-menu']['csections']['text'] 	    	= '<b>List of Sections</b>'; 
	$main_menu['cpages']['sub-menu']['csections_create']['url'] 	= 'cpages/csections/create';
	$main_menu['cpages']['sub-menu']['csections_create']['text'] 	= '<i class="fa fa-plus"></i>Add Section';
}
if(in_array($role, ['superadmin','admin'])) {  // Sueradmin / Admin Role
	$main_menu['doctors']['url'] 						    = "";
	$main_menu['doctors']['icon'] 						    = "fa-user-md";
	$main_menu['doctors']['text'] 						    = "Doctors";
	$main_menu['doctors']['sub-menu']['index']['url'] 	    = 'doctors';
	$main_menu['doctors']['sub-menu']['index']['text'] 	    = 'List of doctor';
	$main_menu['tags']['url'] 						    	= "";
	$main_menu['tags']['icon'] 						    	= "fa-tags";
	$main_menu['tags']['text'] 						    	= "Categories";
	$main_menu['tags']['sub-menu']['index']['url'] 	    	= 'tags';
	$main_menu['tags']['sub-menu']['index']['text'] 		= 'List of Categories';
	$main_menu['tags']['sub-menu']['create']['url'] 		= 'tags/create';
	$main_menu['tags']['sub-menu']['create']['text'] 	    = 'Add Category'; 
	$main_menu['contents']['url'] 						    = "";
	$main_menu['contents']['icon'] 						    = "fa-file-text";
	$main_menu['contents']['text'] 						    = "Content";
	$main_menu['contents']['sub-menu']['index']['url'] 	    = 'contents';
	$main_menu['contents']['sub-menu']['index']['text'] 	= '<b>List of content</b>';
	$main_menu['contents']['sub-menu']['create']['url'] 	= 'contents/create';
	$main_menu['contents']['sub-menu']['create']['text'] 	= '<i class="fa fa-plus"></i>Add Content'; 
	$main_menu['blocks']['url'] 						    			= "";
	$main_menu['blocks']['icon'] 						    			= "fa-th-large";
	$main_menu['blocks']['text'] 						    			= "Block";
	$main_menu['blocks']['sub-menu']['index']['url'] 	    			= 'blocks';
	$main_menu['blocks']['sub-menu']['index']['text'] 					= 'List of blocks';
	$main_menu['blocks']['sub-menu']['create_advertisement']['url'] 	= 'blocks/create_advertisement';
	$main_menu['blocks']['sub-menu']['create_advertisement']['text'] 	= 'Create Advertisement Block'; 
	$main_menu['blocks']['sub-menu']['create_video']['url'] 			= 'blocks/create_video';
	$main_menu['blocks']['sub-menu']['create_video']['text'] 			= 'Create Video Block'; 
	$main_menu['blocks']['sub-menu']['create_text']['url'] 				= 'blocks/create_text';
	$main_menu['blocks']['sub-menu']['create_text']['text'] 			= 'Create Text Block'; 
	$main_menu['blocks']['sub-menu']['create_qrcode']['url'] 			= 'blocks/create_qrcode';
	$main_menu['blocks']['sub-menu']['create_qrcode']['text'] 			= 'Create QRCode Block'; 
}
if($role == 'superadmin') {  // Superadmin Role
	// Content - Author
	$main_menu['contents']['sub-menu']['authors']['url'] 		= 'contents/authors';
	$main_menu['contents']['sub-menu']['authors']['text'] 	    = '<b>List of Authors</b>'; 
	$main_menu['contents']['sub-menu']['authors_create']['url'] 		= 'contents/authors/create';
	$main_menu['contents']['sub-menu']['authors_create']['text'] 	    = '<i class="fa fa-plus"></i>Add Author'; 
	// Content - Products
	$main_menu['contents']['sub-menu']['products']['url'] 		= 'contents/products';
	$main_menu['contents']['sub-menu']['products']['text'] 	    = '<b>List of Products</b>'; 
	$main_menu['contents']['sub-menu']['products_create']['url'] 		= 'contents/products/create';
	$main_menu['contents']['sub-menu']['products_create']['text'] 	    = '<i class="fa fa-plus"></i>Add Product'; 
	// Content - IDE - GDE
	$main_menu['contents']['sub-menu']['ide']['url'] 		= 'contents/ide';
	$main_menu['contents']['sub-menu']['ide']['text'] 	    = 'IDE Content'; 
	$main_menu['contents']['sub-menu']['gde']['url'] 		= 'contents/gde';
	$main_menu['contents']['sub-menu']['gde']['text'] 	    = 'GDE Content';
	// S3 Files
	
	$main_menu['configs']['url'] 							= "";
	$main_menu['configs']['icon'] 							= "fa-folder";
	$main_menu['configs']['text'] 							= "Configurations";
	$main_menu['configs']['sub-menu']						= [];
	$main_menu['configs']['sub-menu']['files']['url'] 	    = 'configs/files';
	$main_menu['configs']['sub-menu']['files']['text'] 	    = 'S3 Bucket Files';
	$main_menu['configs']['sub-menu']['langs']['url'] 	    = 'configs/langs';
	$main_menu['configs']['sub-menu']['langs']['text'] 	    = 'Languages';

	// $main_menu['configs']['sub-menu']['s3']['url'] 			= 'configs/s3';
	// $main_menu['configs']['sub-menu']['s3']['text'] 	    = 'S3 Bucket'; 
}
if($role == 'doctor') {  // Doctor Role
	$main_menu['doc']['url'] 								= "update-profile";
	$main_menu['doc']['icon'] 								= "fa-home";
	$main_menu['doc']['text'] 								= "Update Profile";
	$main_menu['doc']['sub-menu'] 							= array();
}

?>
<div id="sidebar-wrapper">
	<div class="logo">
		<img src="<?= $webroot_path ?>backend/images/bhg_logo.png" class="img-responsive" />
	</div>
	<div class="logo-icon text-center">
		<img src="<?= $webroot_path ?>backend/images/bhg_logo.png" class="img-responsive" />
	</div>
	<div class="left-side-inner">
		<div class="user-account-wrapper">
			<span class="user-account-avatar"><i class="fa fa-user" aria-hidden="true"></i></span>
			<span class="user-account-brief">
				<?php /* $base_url ?>/user/<?= $user->id */ ?>
				<h4><a href="javascript:void(0);"><?= $user_full_name ?></a></h4>
				<span style="font-size:12px;">( <?= $role ?> )</span>
				<span>Last Login : <?= $user_last_login ?></span>
			</span>
		</div>
		<?php if(in_array($role,['admin','doctor'])) {  // ADMIN/DOCTOR ROLE ?>
			<style type="text/css">
				.user-clients-wrapper{padding-bottom: 0px 0px 16px 0px; border-bottom: 1px solid #ccc;color:#fff;}
				.user-clients-wrapper h3{font-size: 16px;padding-left: 10px;}
			</style>
			<div class="user-clients-wrapper">
				<h3>Assigned Clients :</h3>
				<ul>
				<?php foreach($clients as $k=>$v) { 
					echo'<li>'.$v['name'].'</li>';
				}
				?>
				</ul>
			</div>
		<?php } ?>

		<!--sidebar nav start-->
		<ul class="nav nav-pills nav-stacked custom-nav">
			<?php 
			if($role == 'admin') {
				$role_segment = 'admin/';
			} else if($role == 'doctor') {
				$role_segment = 'doctor/';
			} else {
				$role_segment = '';
			}
			foreach($main_menu as $key=>$val) {
				if(!empty($val['sub-menu']) && is_array($val['sub-menu'])) {
					$active_class = ($controller == $key)?'nav-active':'';
					echo '<li class="menu-list '.$active_class.'">';
				} else {
					$active_class = ($controller == $key)?'active':'';
					echo '<li class="'.$active_class .'">';
				}
				echo '<a href="'.$webroot_path.$role_segment.$val['url'].'">';
				echo '<i class="fa '.$val['icon'].'"></i>';
				echo '<span>'.$val['text'].'</span>';
				echo '</a>';
				if(!empty($val['sub-menu']) && is_array($val['sub-menu'])) {
					echo '<ul class="sub-menu-list">';
					foreach($val['sub-menu'] as $k=>$v) {
						$inner_active_class = (($controller == $key) && ($action == $k))?'active':'';
						echo '<li class="'.$inner_active_class.'">';
						echo '<a href="'.$webroot_path.$role_segment.$v['url'].'">';
						echo $v['text'];
						echo '</a>';
						echo '</li>';
					}
					echo '</ul>';
				}
				echo '</li>';
			}
			?>
			<li>
				<a class="dropdown-item" href="{{ route('logout') }}"
							onclick="event.preventDefault();
									document.getElementById('logout-form').submit();">
									<i class="fa fa-sign-out"></i> <span>{{ __('Logout') }}</span>
				</a>

				<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
					@csrf
				</form>
			</li>
		</ul>
		<!--sidebar nav end-->
	</div>
</div>