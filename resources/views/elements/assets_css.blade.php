<link href="{{ asset('backend/css/bootstrap.min.css') }}" rel="stylesheet">

<link rel="dns-prefetch" href="//fonts.gstatic.com"> {{--  Fonts  --}}
<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

<link href="{{ asset('backend/css/style.css') }}" rel="stylesheet">
<link href="{{ asset('backend/css/style-responsive.css') }}" rel="stylesheet">
<link href="{{ asset('backend/css/select2.min.css') }}" rel="stylesheet">