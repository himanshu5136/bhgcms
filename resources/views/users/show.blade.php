@extends('layouts.dashboard')

@section('content')
<div class="page-heading"><h3>User Detail</h3></div>
@include('flash-message')
<?php 
$statusAry = ['1'=>'Active','2'=>'Blocked', ];
$base_url  = URL::to('/');
$user      = (array)$user;
?>
<style type="text/css">
table.users-view tr td.subheading{border-bottom: 2px solid #F4D03F;color: #333;font-size: 20px;margin-bottom: 20px;background: #ECDDA199;padding: 6px;}
.panel-header{ padding: 10px; }
</style>
<div class="col-md-8 col-md-offset-2 superusers-view-page form large-9 medium-8 columns content">
	<section class="panel">
        <div class="panel-header">
            <div class="row action-items">
                <div class="col-md-6 text-left">
                    <a title="Back to Users Listing" href="<?= $base_url ?>/users" class="btn btn-primary"><i class="fa fa-backward"></i></a>
                </div>
                <div class="col-md-6 text-right">
                    <a title="Edit User" href="<?= $base_url ?>/users/<?=  $user['id'] ?>/edit" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                </div>
            </div>
        </div>
		<div class="panel-body">
        <?php if(count($user) > 0) { ?>
        <table class="table table-bordered users-view table-striped">
            <tbody>
                <tr>
                    <td class="subheading" colspan="2">Account Information</td>
                </tr>
                <tr>
                    <td class="bg-primary" width="25%">Id</td>
                    <td class="bg-primary" width="75%"><?=  $user['id'] ?></td>
                </tr>
                
                <tr>
                    <td>User Role</td>
                    <td><?=  $user['role']['name'] ?>&nbsp;&nbsp;<a target="_blank" href="<?= $base_url ?>/users?fnm=&amp;sts=&amp;rid=<?=  $user['role']['id'] ?>" title="View users under this role"><i class="fa fa-users"></i></a></td>
                </tr>
                <tr>
                    <td>Clients</td>
                    <td><ul><?php
                    foreach($user['client'] as $k=>$v){
                        echo '<li>'.$v['name'].'</li>';
                    }
                    ?>
                    </ul></td>
                </tr>
                <tr>
                    <td>Username</td>
                    <td><?= $user['name'] ?></td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td><?= $user['email'] ?></td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td><?= $statusAry[$user['status']] ?></td>
                </tr>
                <tr>
                    <td>Service Type</td>
                    <td><ul><?php
                    foreach($user['stype'] as $k=>$v){
                        echo '<li>'.$v['name'].'</li>';
                    }
                    ?>
                    </ul></td>
                </tr>
                <tr>
                    <td>Created At</td>
                    <td><?= date('M d,Y H:i:s',strtotime($user['created_at'])) ?></td>
                </tr>
                <tr>
                    <td>Updated At</td>
                    <td><?= date('M d,Y H:i:s',strtotime($user['updated_at'])) ?></td>
                </tr>
                <tr>
                    <td class="subheading" colspan="2">Profile Information</td>
                </tr>
                <tr>
                    <td>Contact Number</td>
                    <td><?=  $user['profile']['contact_number'] ?></td>
                </tr>
                <tr>
                    <td>Doctor Rate Id</td>
                    <td><?=  $user['profile']['doc_rate_id'] ?></td>
                </tr>
                <?php if($user['role']['id'] == '3') { // DOCTOR ?> 
                    <tr>
                        <td colspan="2" class="text-center"><b>Language Based Data</b></td>
                    </tr>
                    <?php 
                    if(!empty($user['langs'])) {
                    foreach($user['langs'] as $k=>$v) { ?>
                        <tr>
                            <td><?= $v['name']?> ( <?= $v['code']?> )</td>
                            <td><ul>
                                <li><b>Full Name</b><br /> <?=  $v['pivot']['doc_full_name'] ?></li>
                                <li><b>Title </b><br /> <?=  $v['pivot']['doc_title'] ?></li>
                                <li><b>Background Image </b><br /> <?=  (!empty($v['pivot']['doc_bg_image']))?$v['pivot']['doc_bg_image']:'No Value' ?></li>
                                <li><b>Profile Video </b><br /> <?=  (!empty($v['pivot']['doc_profile_video']))?$v['pivot']['doc_profile_video']:'No Value' ?></li>
                                <li><b>Profile Picture </b><br /> <?=  (!empty($v['pivot']['doc_picture']))?$v['pivot']['doc_picture']:'No Value' ?></li>
                                <li><b>Bio </b><br /> <?=  $v['pivot']['doc_bio'] ?></li>

                            </td>
                        </tr>
                    <?php } 
                    } ?>
                <?php } else { // SUPERADMIN / ADMIN ?>
                    <tr>
                        <td>Full Name</td>
                        <td><?=  $user['profile']['full_name'] ?></td>
                    </tr>
                    <tr>
                        <td>Title</td>
                        <td><?=  $user['profile']['title'] ?></td>
                    </tr>
                    <tr>
                        <td>Picture</td>
                        <td><?=  $user['profile']['picture'] ?></td>
                    </tr>
                    
                    <tr>
                        <td>Bio</td>
                        <td><?=  $user['profile']['bio'] ?></td>
                    </tr>
                    
                    <tr>
                        <td>Background Image</td>
                        <td><?=  $user['profile']['bg_image'] ?></td>
                    </tr>
                    <tr>
                        <td>Profile Video</td>
                        <td><?=  $user['profile']['profile_video'] ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <?php } else {
            echo '<div class="text-center">No Record Found.</div>';
        } ?>

		</div>
	</section>
</div>
@endsection