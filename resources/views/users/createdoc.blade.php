@extends('layouts.dashboard')
<?php
$service_types = ['ide' => 'Interactive Discussion', 'sd' => 'Second Opinion'];
?>
@section('content')
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<div class="page-heading"><h3>Add Doctor</h3></div>
@include('flash-message')
<style type="text/css">
    h4.subheading{border-bottom: 2px solid #F4D03F;color: #333;font-size: 20px;margin-bottom: 20px;background: #ECDDA199;padding: 6px;}
    .users-create-page form .form-checkbox-wrapper #create_user_client_listing_form_group{height: 180px;}

    .ui-tabs-vertical { width: 55em; }
    .ui-tabs-vertical .ui-tabs-nav { overflow-y: scroll; max-height: 700px; padding: .2em .1em .2em .2em; float: left; width: 12em; min-height: 700px; overflow-y: scroll;}
    .ui-tabs-vertical .ui-tabs-nav li { clear: left; width: 100%; border-bottom-width: 1px !important; border-right-width: 0 !important; margin: 0 -1px .2em 0; }
    .ui-tabs-vertical .ui-tabs-nav li a { display:block; }
    .ui-tabs-vertical .ui-tabs-nav li.ui-tabs-active { padding-bottom: 0; padding-right: .1em; border-right-width: 1px; }
    .ui-tabs-vertical .ui-tabs-panel { padding: 1em; float: right; width: 40em;}
    body {font-family: Arial, Helvetica, sans-serif;}
    h4.lang-subheading{border-bottom: 2px solid #333;font-weight: bold;background: #F4EAC3;padding: 5px;margin-bottom: 16px;}
    table {font-size: 1em;}
    .ui-draggable, .ui-droppable {background-position: top;}
</style>

<div class="col-md-8 col-md-offset-2 users-create-page superusers form large-9 medium-8 columns content">
	<section class="panel">
		<div class="panel-body">
            <form method="POST"  enctype="multipart/form-data" action="{{ route('users.storedoc') }}">
            @csrf
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="subheading">Account Information</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="required">Username</label><br />
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                            @error('name')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="required">Email</label><br />
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                            @error('email')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="required">Password</label><br />
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                            @error('password')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group required">
                            <label class="required">Confirm Password</label><br />
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                        </div>
                        <div class="form-group">
                            <label class="required">Status</label><br />
                            <select class="form-control @error('status') is-invalid @enderror" name="status" id="status" required>
                                <?php foreach($status as $k=>$v) { ?>
                                <option value="<?= $k ?>"><?= $v ?></option>
                                <?php } ?>
                            </select>
                            @error('status')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>               
                    <div class="col-lg-6">
                        <div id="form-field-clients" class="form-checkbox-wrapper">
                            <label class="required">Clients</label><br />
                            <div id="create_user_client_listing_form_group" class="form-control">
                                <?php foreach($clients as $k=>$v) { ?>
                                    <div class="form-item">
                                    <input id="client_<?= $k ?>" type="checkbox" class="form-control @error('client') is-invalid @enderror" name="client[]" value="<?= $k ?>" autocomplete="client">
                                    <label for="client_<?= $k ?>"><?= $v ?></label>
                                    </div>
                                <?php } ?>
                            </div>
                            <span class="help-text">( Choose minimum one client )</span>
                            @error('client')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>              
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="subheading">Profile Information</h4>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="required">Doctor ID ( For Rates )</label><br />
                            <input id="doc_rate_id" type="text" class="form-control @error('doc_rate_id') is-invalid @enderror" name="doc_rate_id" value="{{ old('doc_rate_id') }}" >
                            @error('doc_rate_id')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label >Contact Number</label><br />
                            <input id="contact_number" type="text" class="form-control @error('contact_number') is-invalid @enderror" name="contact_number" value="{{ old('contact_number') }}" autocomplete="contact_number">
                            @error('contact_number')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-checkbox-wrapper">
                            <label class="required">Service Type</label><br />
                            <div id="create_user_doc_service_type_form_group" class="form-control">
                                <?php foreach($stypes as $k=>$v) { ?>
                                    <div class="form-item" <?= ($v['is_required'] == '1')?'style="pointer-events: none; opacity: 0.6;"':'' ?>>
                                    <input <?= ($v['is_required'] == '1')?'checked ':'' ?> id="doctor_stype_<?= $v['id'] ?>" type="checkbox" class="form-control @error('client') is-invalid @enderror" name="stype[]" value="<?= $v['id'] ?>" >
                                    <label for="doctor_stype_<?= $v['id'] ?>"><?= $v['name'] ?></label>
                                    </div>
                                <?php } ?>
                            </div>
                            <span class="help-text">All doctors provide Interactive discussion but not all doctors provide second opinion.</span>
                            @error('stype')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 ">
                    <div id="tabs">
                        <!-- TABS Labels-->
                        <ul>
                            <?php foreach($langs as $key=>$val) {  ?>
                                <li><a href="#lang-<?= $val['code'] ?>-<?= $val['id'] ?>"><?= $val['name'].' ( '.$val['code'].' )'?></a></li>
                            <?php }  ?>
                        </ul>
                        <!-- TABS Wrappers-->
                        <?php foreach($langs as $key=>$val) {  
                            $reqField = ($val['id'] == '1')?'required':''; // Fields can be required only for english language.
                            ?>
                            <div id="lang-<?= $val['code'] ?>-<?= $val['id'] ?>">
                                <h4 class="lang-subheading" ><?= $val['name'] ?> Language </h4>
                                <div class="row">
                                    <div class="col-lg-8 ">
                                        <div class="form-group">
                                            <label class="<?= $reqField ?>">( <?= $val['code'] ?> ) Full Name</label><br />
                                            <input id="full_name" type="text" class="form-control @error('full_name') is-invalid @enderror" name="langdata[<?= $val['id'] ?>][full_name]" value="{{ old('full_name') }}" <?= $reqField ?> autocomplete="full_name" autofocus>
                                            @error('full_name')
                                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label class="<?= $reqField ?>">( <?= $val['code'] ?> ) Title</label><br />
                                            <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="langdata[<?= $val['id'] ?>][title]" value="{{ old('title') }}" <?= $reqField ?> >
                                            @error('title')
                                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label >( <?= $val['code'] ?> ) Background Image</label><br />
                                            <input accept="image/*" id="bg_image" type="file" class="form-control @error('bg_image') is-invalid @enderror" name="langdata_<?= $val['id'] ?>_bg_image" value="{{ old('bg_image') }}" autocomplete="bg_image">
                                            <span class="help-text">Each doctor needs background imageespecially for lean back theme/Android TV layout.</span>
                                            @error('bg_image')
                                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label >( <?= $val['code'] ?> ) Profile Video</label><br />
                                            <input accept="video/*" id="profile_video" type="file" class="form-control @error('profile_video') is-invalid @enderror" name="langdata_<?= $val['id'] ?>_profile_video" value="{{ old('profile_video') }}" autocomplete="profile_video" >
                                            <span class="help-text">Doctor can upload Profile Video.</span>
                                            @error('profile_video')
                                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label >( <?= $val['code'] ?> ) Profile Picture</label><br />
                                            <input id="picture" type="file" class="form-control @error('picture') is-invalid @enderror" name="langdata_<?= $val['id'] ?>_doc_picture" value="{{ old('picture') }}" autocomplete="picture" autofocus>
                                            @error('picture')
                                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label>( <?= $val['code'] ?> ) Bio</label><br />
                                            <textarea  id="user_brief" name="langdata[<?= $val['id'] ?>][user_brief]" class="form-control @error('user_brief') is-invalid @enderror" cols="5" rows="4">{{ old('user_brief') }}</textarea>
                                            @error('user_brief')
                                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        </div>
                    </div>
                </div>                
                <br />
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">{{ __('Save Doctor') }}</button>
                        </div>
                    </div>
                </div>
            </form>
		</div>
	</section>
</div>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
@endsection