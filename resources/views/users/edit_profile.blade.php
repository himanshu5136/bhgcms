@extends('layouts.dashboard')

@section('content')
<div class="page-heading"><h3>Update Profile</h3></div>
@include('flash-message')
<?php 
$role = auth()->user()->getRole();
$role_segment = ($role == 'admin')?'/admin':'';
$base_url  = URL::to('/').$role_segment;
?>
<style type="text/css">
    .users-edit-page form .form-checkbox-wrapper #create_user_client_listing_form_group{height: 156px;}
    h4.subheading{border-bottom: 2px solid #F4D03F;color: #333;font-size: 20px;margin-bottom: 20px;background: #ECDDA199;padding: 6px;}
    .form-item-display p{background: #eee;margin:0px;border: 1px solid #ccc;padding: 6px;border-radius: 5px;}
    .existing-attachment{text-align:center;background: #ECDDA199; height: 76px; display: block; width: 50px; margin-top: 5px; border: 1px solid #F4D03F;}
    .existing-attachment a{font-size: 10px;line-height: 0px;color: #333;text-decoration: underline;}
    .panel-header{ padding: 10px; }
</style>
<div class="col-md-8 col-md-offset-2 users-edit-page form large-9 medium-8 columns content">
	<section class="panel">
		<div class="panel-body">
            <form method="post" enctype="multipart/form-data" action="{{ route('update_profile') }}">
            @csrf
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group form-item-display">
                            <label>Username</label>
                            <p>{{ $user['name'] }}</p>
                            <span class="help-text">( Cannot update username once created. )</span>
                        </div>
                        <div class="form-group">
                            <label class="required">Full Name</label><br />
                            <input id="full_name" type="text" class="form-control @error('full_name') is-invalid @enderror" name="full_name" required autocomplete="full_name" value="{{ $user['profile']['full_name'] }}">
                            @error('full_name')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div> 
                        
                        <div class="form-group">
                            <label >Contact Number</label><br />
                            <input id="contact_number" type="text" class="form-control @error('contact_number') is-invalid @enderror" name="contact_number" autocomplete="contact_number" value="{{ $user['profile']['contact_number'] }}" >
                            @error('full_name')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group form-item-display">
                            <label>Email</label><br />
                            <p>{{ $user['email'] }}</p>
                            <span class="help-text">( Cannot update email once created. )</span>
                        </div>
                        <div class="form-group">
                            <label class="required">Title</label><br />
                            <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ $user['profile']['title'] }}" required autocomplete="title">
                            @error('title')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <?php if(!empty($user['profile']['picture'])) { ?>
                                <div class="col-md-2 existing-attachment">
                                <a href="{{ $user['profile']['picture'] }}" target="blank">Link to Image</a>
                                </div>
                                <div class="col-md-10">
                            <?php } ?>
                                <label >Profile Picture</label><br />
                                <input accept=".jpg, .png, .jpeg" id="picture" type="file" class="form-control @error('picture') is-invalid @enderror" name="picture" value="{{ old('picture') }}" autocomplete="picture" >
                                @error('picture')
                                    <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                                @enderror
                            <?php if(!empty($user['profile']['picture'])) { ?>
                                <div class=""><input value="1" type="checkbox" name="remove_picture" id="remove_picture"/><label for="remove_picture">&nbsp;Remove Picture</label></div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">                      
                        <div class="form-group">
                            <label class="required">Doctor Bio</label><br />
                            <textarea required id="user_brief" name="user_brief" class="form-control @error('user_brief') is-invalid @enderror" cols="5" rows="8">{{ $user['profile']['bio'] }}</textarea>
                            @error('user_brief')
                                <span class="invalid-feedback msg-error" role="alert"><i class="fa fa-exclamation-triangle"></i> {{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">{{ __('Update User') }}</button>
                        </div>
                    </div>
                </div>
            </form>
		</div>
	</section>
</div>
@endsection