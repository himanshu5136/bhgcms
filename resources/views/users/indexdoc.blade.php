@extends('layouts.dashboard')

@section('content')
<div class="page-heading"><h3>List of Doctor Users</h3></div>
@include('flash-message')
<?php 
$statusAry = ['1'=>'Active','2'=>'Blocked'];
$trashOptions = ['1'=>'Yes','2'=>'No'];
$base_url  = URL::to('/');
?>
<style type="text/css">
tr.deleted-user{background:#ff000030;}
</style>
<div class="superusers form large-9 medium-8 columns content">
	<section class="panel">
        <div class="panel-heading ">
            <div class="row users_filters_wrapper bg-secondary">
                <form method="get" id="form_users_filter" >
                    <div class="col-md-3 form-group">
                        <label>Full Name</label><br />
                        <input class="form-control" type="text" name="fnm" id="users-filter-fname" value="<?= (!empty($qry['fnm']))?$qry['fnm']:'' ?>" /> 
                    </div>	
                    <div class="col-md-2 form-group">
                        <label>Status</label><br />
                        <select class="form-control @error('sts') is-invalid @enderror" name="sts" id="sts">
                            <option value="">- Select One -</option>
                            @foreach($statusAry as $k=>$v)
                                <option 
                                <?php if((!empty($qry['sts'])) && ($qry['sts'] == $k)){ echo "selected"; }  ?>                                    
                                value="{{ $k }}">{{ $v }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-3 form-group">
                        <label>Include Deleted Users</label><br />
                        <select class="form-control @error('tusr') is-invalid @enderror" name="tusr" id="tusr">
                            <option value="">- Select One -</option>
                            @foreach($trashOptions as $k=>$v)
                                <option <?php if((!empty($qry['tusr'])) && ($qry['tusr'] == $k)){ echo "selected"; } ?> value="{{ $k }}">{{ $v }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-2 form-group">  
                        <label>&nbsp;</label><br />
                        <input type="submit" name="submit" value="Search" class="btn btn-primary" />
                        <a href="<?= $base_url ?>/users" class="btn btn-secondary">Reset</a>
                    </div>	
                </form>
            </div>	
        </div>
		<div class="panel-body">
        <?php //echo count($users); die;// echo "<pre>"; print_r($users); die; ?>
        <?php if(count($users) > 0) { ?>
        <table class="table  table-hover general-table">
            <thead>
                <tr>
                    <th width="15%">Username</th>
                    <th width="18%">Full Name</th>
                    <th width="20%">Email</th>
                    <th width="15%">Role</th>
                    <th width="15%">Status</th>
                    <th width="17%">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                <tr class="<?= ($user['is_deleted'] == '1' )?'deleted-user':'' ?>" >
                    <td>{{ $user['name'] }}</td>
                    <td>{{ (isset($user['profile']['full_name'])) ? $user['profile']['full_name']:'' }}</td>
                    <td>{{ $user['email'] }}</td>
                    <td>{{ $user['role']['name'] }}</td>
                    <td>{{ $statusAry[$user['status']] }}</td>
                    <td class="actions">
                        <div class="text-center">

                            <?php  if($user['is_deleted'] == '1' ) { ?>
                                <form name="restore_user_<?= $user['id'] ?>" style="display:none;" method="post" action="<?= $base_url ?>/users/<?= $user['id'] ?>/restore">  @csrf <input type="hidden" name="_method" value="POST"></form>
                                <a href="#" onclick="if (confirm(&quot;Are you sure you want to RESTORE user <?= $user['name'] ?>?&quot;)) { document.restore_user_<?= $user['id'] ?>.submit(); } event.returnValue = false; return false;" title="Restore Deleted User"><i class="fa fa-refresh"></i></a>
                            <?php } else { ?>
                                <a href="<?= $base_url ?>/users/<?= $user['id'] ?>" title="View User"><i class="fa fa-search-plus"></i></a>&nbsp;&nbsp;
                                <?php if($user['role']['id'] == '3' ) { ?>
                                    <a href="<?= $base_url ?>/usersdoc/<?= $user['id'] ?>/edit" title="Edit Doctor"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;
                                <?php } else {  ?>
                                    <a href="<?= $base_url ?>/users/<?= $user['id'] ?>/edit" title="Edit User"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;
                                <?php } ?>
                                <form name="trash_user_<?= $user['id'] ?>" style="display:none;" method="post" action="<?= $base_url ?>/users/<?= $user['id'] ?>/trash">  @csrf <input type="hidden" name="_method" value="POST"></form>
                                <a href="#" onclick="if (confirm(&quot;Are you sure you want to [soft] delete user <?= $user['name'] ?>?&quot;)) { document.trash_user_<?= $user['id'] ?>.submit(); } event.returnValue = false; return false;" title="Soft Delete User"><i class="fa fa-trash-o"></i></a>
                            <?php } ?>
                        </div>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        <div class="text-center">
            {{ $paginate->links() }}
        </div>
        <?php } else {
            echo '<div class="text-center">No Record Found.</div>';
        } ?>
        </div>
        
	</section>
</div>
@endsection