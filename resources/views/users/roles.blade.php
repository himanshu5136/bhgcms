@extends('layouts.dashboard')
<?php $base_url  = URL::to('/'); ?>
@section('content')
<div class="page-heading"><h3>User Roles</h3></div>
@include('flash-message')
<?php 
$base_url  = URL::to('/');
?>

<div class="superusers form large-9 medium-8 columns content">
	<section class="panel">
		<div class="panel-body">
        <?php if(count($roles) > 0) { ?>
        <table class="table  table-hover general-table">
            <thead>
                <tr>
                    <th width="10%">Role ID</th>
                    <th width="10%">Name</th>
                    <th width="10%">Slug</th>
                    <th width="60%">Description</th>
                    <th width="10%">View Users</th>
                </tr>
            </thead>
            <tbody>
                @foreach($roles as $role)
                <tr>
                    <td>{{ $role['id'] }}</td>
                    <td>{{ $role['name'] }}</td>
                    <td>{{ $role['slug'] }}</td>
                    <td>{{ $role['description'] }}</td>
                    <td class="actions text-center">
                        <a target="_blank" href="<?= $base_url ?>/users?fnm=&amp;sts=&amp;rid=<?= $role['id'] ?>" title="View users under this role"><i class="fa fa-users"></i></a>
                        <?php /*if($role['id'] == '2') { ?>
                            &nbsp;&nbsp;<a href="<?= $base_url ?>/users/permissions" title="Setup Permissions"><i class="fa fa-pencil-square-o"></i></a>
                        <?php }*/ ?>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        <?php } else {
            echo '<div class="text-center">No Role Found.</div>';
        } ?>
		</div>
	</section>
</div>
@endsection