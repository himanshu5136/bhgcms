<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('client_credentials')->post('/get/client/dashboard', 'ApiController@getDashboardDetail');
Route::middleware('client_credentials')->post('/get/type/call', 'ApiController@getTypeCall');
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'custom_oauth'], function () {
    // 00 - GenerateOauthClient
        Route::post('/register/oauth/client', 'ApiController@generateOauthClient');
});

Route::group(['middleware' => 'client_credentials'], function () {
    // 01 - GetAllClientList
        Route::get('/get/clients/all', 'ApiController@getAllClientList');
    // 02 - GetAllDoctorList
        Route::get('/get/doctors/all', 'ApiController@getAllDoctorList');
    // 03 - GetDoctorListByID
        Route::post('/get/doctors/by/client', 'ApiController@getDoctorListByClientId');
    // 04 - ChangeDoctorStatus
        Route::post('/update/doctor/status', 'ApiController@changeDoctorStatus');
    // 05 - GetDoctorDetailedProfiles
        Route::post('/get/doctor/profile', 'ApiController@getDoctorDetailedProfile');
    // 06 - GetAllCategoryListByClientId
        Route::post('/get/categories/by/client', 'ApiController@getAllCategoryListByClientId');
    // 07 - RetreiveContentByCategoryId
        Route::post('/get/content/by/category', 'ApiController@getContentByCategoryId');
    // 08 - RetreiveContentByTypeAndClientId
        Route::post('/get/content/by/type/client', 'ApiController@getContentByTypeAndClientId');
    // 09 - getContentListByCategoryId
        Route::post('/get/contentlist/by/category', 'ApiController@getContentListByCategoryId');
    // 10 - getContentById
        Route::post('/get/content/by/id', 'ApiController@getContentById');
    // 11 - getContentByGuid
        Route::post('/get/content/by/guid', 'ApiController@getContentByGuid');
    // 12 - getBlocksByGuid
        Route::post('/get/blocks/by/guid', 'ApiController@getblocksByGuid');
    // 13 - getDoctorListByServiceType
        Route::post('/get/doctors/by/stype', 'ApiController@getDoctorListByServiceType');
    // 14 - getDetailByPageTypeAndClientId
        Route::post('/get/detail/by/ptype/client', 'ApiController@getDetailByPageTypeAndClientId');
});