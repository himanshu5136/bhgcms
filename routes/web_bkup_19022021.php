<?php

use Illuminate\Support\Facades\Route;
//use Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
//Route::resource('clients', 'ClientController');
// GET/clients, mapped to the index() method,
// GET /clients/create, mapped to the create() method,
// POST /clients, mapped to the store() method,
// GET /clients/{id}, mapped to the show() method,
// GET /clients/{id}/edit, mapped to the edit() method,
// PUT/PATCH /clients/{id}, mapped to the update() method,
// DELETE /clients/{id}, mapped to the destroy() method.
*/
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
// Routes for "SUPERADMIN" user Role
// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    Route::group(['middleware' => 'auth'], function () {
        Route::get('dashboard', function () { 
            $userRole = Auth::user()->getRole();
            if($userRole == 'superadmin'){
                return redirect('home');
            } else if($userRole == 'admin') {
                return redirect('admin/home');
            } else if($userRole == 'doctor') {
                return redirect('doctor/home');
            } else {
                return redirect('/home');
            }
        });
        // ROUTES for SUPERADMIN
        Route::group(['middleware' => 'role:superadmin'], function () {
            // DASHBOARD
                Route::get('/home', 'HomeController@index')->name('home');
            // Languages
                Route::group(['prefix'=>'configs'], function () {
                    Route::resource('langs', 'LangController');
                });
            // Files
                Route::group(['prefix'=>'configs'], function () {
                    Route::resource('files', 'FileController')->only(['index','edit','update']);
                });
            // USERS
                Route::get('/users/roles', 'UserController@userRoles')->name('List of Roles');
                Route::post('/users/{id}/trash', 'UserController@trash');
                Route::post('/users/{id}/restore', 'UserController@restore');
                Route::get('/users/createdoc', 'UserController@createdoc')->name('users.createdoc');
                Route::post('/usersdoc', 'UserController@storedoc')->name('users.storedoc');
                Route::get('/usersdoc/{id}/edit', 'UserController@editdoc')->name('users.editdoc');
                Route::put('/usersdoc/{id}', 'UserController@updatedoc')->name('users.updatedoc');
                Route::patch('/usersdoc/{id}', 'UserController@updatedoc')->name('users.updatedoc');
                Route::get('/usersdoc', 'UserController@indexdoc')->name('users.indexdoc');
                Route::resource('users', 'UserController');
            // CLIENTS
                Route::resource('clients', 'ClientController');
            // CATEGORIES
                Route::resource('tags', 'TagController');
            // CONTENTS
                // IDE - GDE
                Route::get('/contents/ide', 'ContentController@ide');
                Route::post('/contents/ide', 'ContentController@ide_store');
                Route::get('/contents/gde', 'ContentController@gde');
                Route::post('/contents/gde', 'ContentController@gde_store');
                // Author
                Route::get('/contents/authors', 'AuthorController@index')->name('authors.index');
                Route::get('/contents/authors/create', 'AuthorController@create')->name('authors.create');
                Route::post('/contents/authors', 'AuthorController@store')->name('authors.store');
                Route::get('/contents/authors/{id}', 'AuthorController@show')->name('authors.show');
                Route::get('/contents/authors/{id}/edit', 'AuthorController@edit')->name('authors.edit');
                Route::put('/contents/authors/{id}', 'AuthorController@update')->name('authors.update');
                Route::patch('/contents/authors/{id}', 'AuthorController@update')->name('authors.update');
                Route::delete('/contents/authors/{id}', 'AuthorController@destroy')->name('authors.destroy');
                // Product
                Route::get('/contents/products', 'ProductController@index')->name('products.index');
                Route::get('/contents/products/create', 'ProductController@create')->name('products.create');
                Route::post('/contents/products', 'ProductController@store')->name('products.store');
                Route::get('/contents/products/{id}', 'ProductController@show')->name('products.show');
                Route::get('/contents/products/{id}/edit', 'ProductController@edit')->name('products.edit');
                Route::put('/contents/products/{id}', 'ProductController@update')->name('products.update');
                Route::patch('/contents/products/{id}', 'ProductController@update')->name('products.update');
                Route::delete('/contents/products/{id}', 'ProductController@destroy')->name('products.destroy');
                Route::resource('contents', 'ContentController');
            // CPAGES
                // Sections
                Route::get('/cpages/csections', 'CsectionController@index')->name('csections.index');
                Route::get('/cpages/csections/create', 'CsectionController@create')->name('csections.create');
                Route::post('/cpages/csections', 'CsectionController@store')->name('csections.store');
                Route::get('/cpages/csections/{id}', 'CsectionController@show')->name('csections.show');
                Route::get('/cpages/csections/{id}/edit', 'CsectionController@edit')->name('csections.edit');
                Route::put('/cpages/csections/{id}', 'CsectionController@update')->name('csections.update');
                Route::patch('/cpages/csections/{id}', 'CsectionController@update')->name('csections.update');
                Route::delete('/cpages/csections/{id}', 'CsectionController@destroy')->name('csections.destroy');
                Route::resource('cpages', 'CpageController');
            // BLOCKS              
                // blocks - advertisement
                Route::get('/blocks/create_advertisement', 'BlockController@create_advertisement');
                Route::post('/blocks/create_advertisement', 'BlockController@store_advertisement');
                Route::patch('/blocks/{id}/edit_advertisement', 'BlockController@update_advertisement');

                // blocks - videos
                Route::get('/blocks/create_video', 'BlockController@create_video');
                Route::post('/blocks/create_video', 'BlockController@store_video');
                Route::patch('/blocks/{id}/edit_video', 'BlockController@update_video');

                // blocks - text
                Route::get('/blocks/create_text', 'BlockController@create_text');
                Route::post('/blocks/create_text', 'BlockController@store_text');
                Route::patch('/blocks/{id}/edit_text', 'BlockController@update_text');

                // blocks - qrcode
                Route::get('/blocks/create_qrcode', 'BlockController@create_qrcode');
                Route::post('/blocks/create_qrcode', 'BlockController@store_qrcode');
                Route::patch('/blocks/{id}/edit_qrcode', 'BlockController@update_qrcode');

                // GLOBAL
                Route::get('/blocks', 'BlockController@index')->name('List of Blocks');
                Route::get('/blocks/{id}', 'BlockController@show');
                Route::get('/blocks/{id}/edit', 'BlockController@edit');
                Route::delete('/blocks/{id}', 'BlockController@destroy');

            // DOCTORS
                Route::post('/doctors/{id}/trash', 'DoctorController@trash');
                Route::resource('doctors', 'DoctorController')->except(['create','store']);               
            
        });
        // ROUTES for ADMIN
        Route::group(['prefix'=>'admin','middleware' => 'role:admin'], function () {
            // DASHBOARD
                Route::get('/home', 'HomeController@index')->name('home');
            // CLIENT
                Route::get('/clients/{id}', 'ClientController@admin_client_detail');
            // CATEGORIES
                Route::resource('tags', 'TagController');  
            // CONTENTS
                Route::resource('contents', 'ContentController');  
            // DOCTORS
                Route::post('/doctors/{id}/trash', 'DoctorController@trash');
                Route::resource('doctors', 'DoctorController')->except(['create','store']);
        });
        // ROUTES for DOCTOR
        Route::group(['prefix'=>'doctor', 'middleware' => 'role:doctor'], function () {
                // DASHBOARD
                Route::get('/home', 'HomeController@index')->name('home');
                // Update Profile
                Route::get('/update-profile', 'UserController@edit_profile')->name('edit_profile');
                Route::post('/update-profile', 'UserController@update_profile')->name('update_profile');
        });
        
        //Route::get('/users/permissions', 'PermissionController@edit')->name('Setup Permissions');
        //Route::post('/users/permissions', 'PermissionController@update');
        //Route::get('/comingsoon', 'comingsoonController@index')->name('ComingSoon Page');
        //Route::get('/permissions', 'PermissionController@Permission');
    });

Auth::routes(['register' => false]);
Route::get('/', 'Auth\LoginController@showLoginForm');
Route::post('/', 'Auth\LoginController@login');

