<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsCpagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients_cpages', function (Blueprint $table) {
            $table->unsignedInteger('client_id');
            $table->unsignedInteger('cpage_id');
            
            //FOREIGN KEY CONSTRAINTS
            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');           
            $table->foreign('cpage_id')->references('id')->on('cpages')->onDelete('cascade');
            
            //SETTING THE PRIMARY KEYS
            //$table->primary(['user_id','client_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients_cpages');
    }
}
