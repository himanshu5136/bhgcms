<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsLangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_langs', function (Blueprint $table) {
            $table->unsignedInteger('product_id');
            $table->unsignedInteger('lang_id');
            $table->string('prdct_name')->nullable();
            $table->string('prdct_qrcode')->nullable();
            $table->string('prdct_picture')->nullable();
            $table->string('prdct_video')->nullable();
            $table->string('prdct_qrimg')->nullable();
            $table->longText('prdct_description')->nullable();



            //FOREIGN KEY CONSTRAINTS
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');           
            $table->foreign('lang_id')->references('id')->on('langs')->onDelete('cascade');

            //SETTING THE PRIMARY KEYS
            //$table->primary(['user_id','client_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_langs');
    }
}
