<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCpagesCsectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cpages_csections', function (Blueprint $table) {
            $table->unsignedInteger('cpage_id');
            $table->unsignedInteger('csection_id');          

            //FOREIGN KEY CONSTRAINTS
            $table->foreign('cpage_id')->references('id')->on('cpages')->onDelete('cascade');
            $table->foreign('csection_id')->references('id')->on('csections')->onDelete('cascade');           

            //SETTING THE PRIMARY KEYS
            //$table->primary(['user_id','client_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cpages_csections');
    }
}
