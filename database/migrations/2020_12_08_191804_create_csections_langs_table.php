<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCsectionsLangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create('csections_langs', function (Blueprint $table) {
            $table->unsignedInteger('csection_id');
            $table->unsignedInteger('lang_id');
            $table->string('csec_title')->nullable(); 

            //FOREIGN KEY CONSTRAINTS
            $table->foreign('csection_id')->references('id')->on('csections')->onDelete('cascade');           
            $table->foreign('lang_id')->references('id')->on('langs')->onDelete('cascade');

            //SETTING THE PRIMARY KEYS
            //$table->primary(['user_id','client_id']);
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('csections_langs');
    }
}
