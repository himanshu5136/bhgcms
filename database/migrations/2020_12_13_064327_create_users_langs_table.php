<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersLangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_langs', function (Blueprint $table) {
            
            $table->unsignedBigInteger('user_id',20);
            $table->unsignedInteger('lang_id');
            $table->string('doc_full_name')->nullable();
            $table->string('doc_title')->nullable();
            $table->string('doc_bg_image')->nullable();
            $table->string('doc_profile_video')->nullable();
            $table->string('doc_picture')->nullable();
            $table->longText('doc_bio')->nullable();
            
            //FOREIGN KEY CONSTRAINTS
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('lang_id')->references('id')->on('langs')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_langs');
    }
}
