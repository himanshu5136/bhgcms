<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContentsLangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('contents_langs', function (Blueprint $table) {
            $table->unsignedInteger('content_id');
            $table->unsignedInteger('lang_id');
            $table->string('cntnt_title')->nullable();
            $table->string('cntnt_bg_images')->nullable();
            $table->string('cntnt_tn_images')->nullable();
            $table->longText('cntnt_body')->nullable();
            $table->integer('cntnt_author_id');

            //FOREIGN KEY CONSTRAINTS
            $table->foreign('content_id')->references('id')->on('contents')->onDelete('cascade');           
            $table->foreign('lang_id')->references('id')->on('langs')->onDelete('cascade');

            //SETTING THE PRIMARY KEYS
            //$table->primary(['user_id','client_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contents_langs');
    }
}
