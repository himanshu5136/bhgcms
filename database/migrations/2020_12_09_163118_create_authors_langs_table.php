<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuthorsLangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('authors_langs', function (Blueprint $table) {
            $table->unsignedInteger('author_id');
            $table->unsignedInteger('lang_id');
            $table->string('athr_full_name')->nullable();
            $table->string('athr_title')->nullable();
            $table->string('athr_picture')->nullable();
            $table->longText('athr_bio')->nullable();

            //FOREIGN KEY CONSTRAINTS
            $table->foreign('author_id')->references('id')->on('authors')->onDelete('cascade');           
            $table->foreign('lang_id')->references('id')->on('langs')->onDelete('cascade');

            //SETTING THE PRIMARY KEYS
            //$table->primary(['user_id','client_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('authors_langs');
    }
}
