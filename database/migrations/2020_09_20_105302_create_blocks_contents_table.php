<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlocksContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blocks_contents', function (Blueprint $table) {
            $table->unsignedInteger('block_id');
            $table->unsignedInteger('content_id');
            $table->string('content_pages')->nullable();
            $table->string('display_location')->nullable();
            

            //FOREIGN KEY CONSTRAINTS
            $table->foreign('block_id')->references('id')->on('blocks')->onDelete('cascade');           
            $table->foreign('content_id')->references('id')->on('contents')->onDelete('cascade');
            

            //SETTING THE PRIMARY KEYS
            //$table->primary(['user_id','client_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blocks_contents');
    }
}
