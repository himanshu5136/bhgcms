<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCpagesLangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cpages_langs', function (Blueprint $table) {
            $table->unsignedInteger('cpage_id');
            $table->unsignedInteger('lang_id');
            $table->string('cpage_title')->nullable(); 
            $table->longText('cpage_description')->nullable();

            //FOREIGN KEY CONSTRAINTS
            $table->foreign('cpage_id')->references('id')->on('cpages')->onDelete('cascade');           
            $table->foreign('lang_id')->references('id')->on('langs')->onDelete('cascade');

            //SETTING THE PRIMARY KEYS
            //$table->primary(['user_id','client_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cpages_langs');
    }
}
