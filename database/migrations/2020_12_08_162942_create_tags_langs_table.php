<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTagsLangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tags_langs', function (Blueprint $table) {
            $table->unsignedInteger('tag_id');
            $table->unsignedInteger('lang_id');
            $table->string('tag_name'); 
            $table->longText('tag_description')->nullable();
            
            //FOREIGN KEY CONSTRAINTS
            $table->foreign('tag_id')->references('id')->on('tags')->onDelete('cascade');           
            $table->foreign('lang_id')->references('id')->on('langs')->onDelete('cascade');

            //SETTING THE PRIMARY KEYS
            //$table->primary(['user_id','client_id']);
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tags_langs');
    }
}
