<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContentsCsectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contents_csections', function (Blueprint $table) {
            $table->unsignedInteger('content_id');
            $table->unsignedInteger('csection_id');          

            //FOREIGN KEY CONSTRAINTS
            $table->foreign('content_id')->references('id')->on('contents')->onDelete('cascade');
            $table->foreign('csection_id')->references('id')->on('csections')->onDelete('cascade');           

            //SETTING THE PRIMARY KEYS
            //$table->primary(['user_id','client_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contents_csections');
    }
}
