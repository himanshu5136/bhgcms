<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlockdatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blockdatas', function (Blueprint $table) {
            $table->unsignedInteger('block_id',20);
            $table->string('ad_image')->nullable();
            $table->string('uploaded_video')->nullable();
            $table->string('embed_video')->nullable();
            $table->longText('text_body')->nullable();
            $table->string('qrcode_text')->nullable();
            $table->string('qrcode_link')->nullable();
            $table->string('qrcode_content')->nullable();
            $table->string('qrcode_image')->nullable();
            $table->timestamps();
            //FOREIGN KEY CONSTRAINTS
            $table->foreign('block_id')->references('id')->on('blocks')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blockdatas');
    }
}
